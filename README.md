# VectunesGui

WxWidgets/OpenGL front end for Vectunes.
Compiled with Code::Blocks for Linux and Windows.

Code::Blocks project files for Vectunes with wx/opengl GUI front end:
VectunesGui_Linux.cbp
VectunesGui_Win.cbp

Code::Blocks project files for Vectunes with no UI, mainly for testing or creating services (not functional yet):
Vectunes_Linux.cbp
Vectunes_Win.cbp

NOTE: Vectunes is a personal project written hastily. It is a junk heap of experiments, many of which are disabled and should be deleted.

It was ported from Java and still looks like Java in many places. A code review would give it an F.

Vectunes does NOT represent how I would write code for an employer.
