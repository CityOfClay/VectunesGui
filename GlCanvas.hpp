#ifndef GlCanvas_hpp
#define GlCanvas_hpp

#include<iostream>
#include <chrono>
#include <vector>
#include <typeinfo>
#include <wx/wx.h>
#include <wx/glcanvas.h>

#include "./Vectunes/Vectunes.hpp"
#include "GlDrawingContext.hpp"

#undef UseArtist // deprecating Artist pattern for now.

#ifdef UseArtist // deprecating Artist pattern for now.
#include "Artists.hpp"
#endif // FALSE

#ifndef WIN32
#include <unistd.h> // FIXME: This work/necessary in Windows?
//Not necessary, but if it was, it needs to be replaced by process.h AND io.h
#endif

#if !wxUSE_GLCANVAS
#error "OpenGL required: set wxUSE_GLCANVAS to 1 and rebuild the library"
#endif

using namespace std;

/* ********************************************************************************* */

class GlCanvas: public wxGLCanvas { // Like DrawingPanel in Voices.java
public:
  //AudProject *MyProject = nullptr;
  //MainGui BigApp;// parent
  //Grabber Query; Grabber.DestinationGrabber DestQuery;
  FishHook SelectedFish;
  FishHook DestinationHook;
  GroupSong::Group_OffsetBox *DestinationGroup = nullptr;
  DestinationGrabber DestinationClaw;
  GroupSong *PossibleDestination = nullptr;
  Grabber Query;
  Point2D MouseOffset;// Mouse offset from selected object's origin, in audio root space.
  UndoStack History;

  int ScreenMouseX = 0, ScreenMouseY = 0;
  bool MouseDragging = false;
  bool DragStart = false;
  bool ShiftDown = false;
  bool ControlDown = false;
//  double MouseStartX = 0, MouseStartY = 0;
//  double MouseFinishX, MouseFinishY;
  Point2D MouseStart, MouseFinish;
  double SearchRadius = 2.0;//1.5;//0.1;//10; // 0.1;//100.0;//

  //public IDrawable.IMoveable Floater = nullptr;// copy we are dragging around (in hover mode, no mouse buttons)
  //Point2D.Double results = new Point2D.Double();// used in multiple places as a return parameter for mapping

  // opengl space limits
  double OglLeft = -1.0, OglRight = 1.0;
  double OglBottom = -1.0, OglTop = 1.0;

//  double OglLeft = 0.0, OglRight = 5.0;
//  double OglBottom = 0.0, OglTop = 5.0;

  double OglNear = -1.0, OglFar = 1.0;
  double OglWidth = OglRight-OglLeft;
  double OglHeight = OglTop-OglBottom;
  double OglMouseX = 0.0, OglMouseY = 0.0;

  AudProject *audproj;
  GraphicBox *GraphicBody;
  GraphicBox::Graphic_OffsetBox *GraphicRoot;
  GoLiveSync *LivePlayer;
  wxTimer AnimationTimer;
  OffsetBoxBase* PlayObx = nullptr;// for playing selected songlets
  //CajaDelimitadora MouseBox(-100, -100, 100, 100);

  //int AnimationIntervalMs = 100;//63;// 75;// 100;// 125;//128;//250;//250/2;//500;
  int AnimationIntervalMs = 50;// 75;// 100;// 125;//128;//250;//250/2;//500;
  //int AnimationIntervalMs = 25;// 75;// 100;// 125;//128;//250;//250/2;//500;
  TimeFloat AnimationIntervalSec = AnimationIntervalMs/Globals::Thousand;//128;//250;//250/2;//500;
  TimeFloat PlayerStartTimeFloat = 0.0;
  TimeFloat SongStartLocation = 0.0;// SongStartLocation is the difference between songlet time=0 and where in the songlet we are actually starting.
  TimeFloat SongFinishLocation = 0.0;// SongFinishLocation is always greater than SongStartLocation, and is the stopping place.
  TimeFloat SongLocation;

  // Automatic pan/zoom animation.
  const bool DoLivePan = true;
  TimeFloat PrevPanTime;// Drag timestamp value of previous animation frame.
  TimeFloat TargetTime;
  CajaDelimitadora TargetExtents;// DoLivePan

  // wxDefaultSize
  /* ********************************************************************************* */
  GlCanvas(wxWindow *parent) : wxGLCanvas(parent, wxID_ANY, NULL,  wxDefaultPosition, wxSize(250, 250), 0, wxT("GLCanvas"), wxNullPalette) {
    int argc = 1;
    char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };

    Bind(wxEVT_PAINT, &GlCanvas::PaintIt, this, wxID_ANY);
    Bind(wxEVT_MOTION, &GlCanvas::MouseMoved, this, wxID_ANY);
    Bind(wxEVT_LEFT_DOWN, &GlCanvas::MouseLeftDown, this, wxID_ANY);
    Bind(wxEVT_LEFT_UP, &GlCanvas::MouseLeftUp, this, wxID_ANY);
    Bind(wxEVT_RIGHT_DOWN, &GlCanvas::MouseRightDown, this, wxID_ANY);
    Bind(wxEVT_MOUSEWHEEL, &GlCanvas::MouseWheelMoved, this, wxID_ANY);
    Bind(wxEVT_MIDDLE_DOWN, &GlCanvas::MouseMiddleClick, this, wxID_ANY);
    Bind(wxEVT_ENTER_WINDOW, &GlCanvas::MouseEnterWindow, this, wxID_ANY);
    Bind(wxEVT_LEAVE_WINDOW, &GlCanvas::MouseLeaveWindow, this, wxID_ANY);
    Bind(wxEVT_KEY_DOWN, &GlCanvas::KeyPressed, this, wxID_ANY);
    //Bind(wxEVT_CHAR_HOOK, &GlCanvas::KeyPressed, this);
    Bind(wxEVT_KEY_UP, &GlCanvas::KeyReleased, this, wxID_ANY);
    Bind(wxEVT_SIZE, &GlCanvas::Resized, this, wxID_ANY);

    Bind(wxEVT_CLOSE_WINDOW, &GlCanvas::OnClose, this);// does not work right

    // this->AnimationTimer.Bind(wxEVT_TIMER, &GlCanvas::OnTimer, this);

#ifdef UseArtist // deprecating Artist pattern for now.
    Init_Artists();
#endif // UseArtist
    Init_Composition();

    LivePlayer = new GoLiveSync();
  }
  /* ********************************************************************************* */
  virtual ~GlCanvas() {
    std::cout << "Destructor!" << "\n";
    delete this->PlayObx;
    delete LivePlayer;
    Delete_Composition();
#ifdef UseArtist // deprecating Artist pattern for now.
    Delete_Artists();
#endif // UseArtist
  }
  /* ********************************************************************************* */
  void OnClose(wxCloseEvent& event) {
    this->StopMusic();
    this->StopAnimation();
    Destroy();
  }
  /* ********************************************************************************* */
  void TimerCalcLocation() {
    TimeFloat TimerNow = Globals::GetTimeInSecondsFloat();
    printf("TimerNow:%Lf\n", TimerNow);
    TimeFloat FullTimeElapsed = TimerNow - this->PlayerStartTimeFloat;
    this->SongLocation = this->SongStartLocation + FullTimeElapsed;
    this->SongLocation += this->AnimationIntervalSec;// Advance to future 1 cycle to keep up. Warning: Audio.hpp ringbuf resolution must be very fine for this.
    this->SongLocation = Math::min(this->SongLocation, this->SongFinishLocation);// snox hack cures bug, but kind of redundant with exit comparison.
  }
  /* ********************************************************************************* */
  void TimerPlay() {
    bool KeepGoing = this->LivePlayer->Play_To(this->SongLocation);
    if ((!KeepGoing) || (this->SongFinishLocation <= this->SongLocation)) {
      this->AnimationTimer.Stop();// does bind automatically call stop anyway?
      this->AnimationTimer.Bind(wxEVT_TIMER, &GlCanvas::DelayedEnding, this);
      this->AnimationTimer.StartOnce(AnimationIntervalMs);// Give the singer one last time interval to display its final state.
    }
    Refresh();
  }
  /* ********************************************************************************* */
  void OnTimerPlain(wxTimerEvent& event) {
    TimerCalcLocation();
    TimerPlay();
  }
  /* ********************************************************************************* */
  void OnTimerMovie(wxTimerEvent& event) {
    TimerCalcLocation();
    this->LivePan();
    TimerPlay();
  }
  /* ********************************************************************************* */
  void DelayedEnding(wxTimerEvent& event) {// Give the singer one last time interval to display its final state.
    this->StopShow();
  }
  /* ********************************************************************************* */
  void Moment(TimeFloat TimerNow) {
    printf("*************************************** Moment!  %f\n", TimerNow);
    printf("TimerNow:%Lf\n", TimerNow);
    TimeFloat FullTimeElapsed = TimerNow - this->PlayerStartTimeFloat;
    TimeFloat SongLocation = this->SongStartLocation;// + FullTimeElapsed;
    SongLocation += this->AnimationIntervalSec;
    SongLocation = Math::min(SongLocation, this->SongFinishLocation);// snox hack cures bug, but kind of redundant with exit comparison.
    bool KeepGoing = LivePlayer->Play_To(SongLocation);
    Refresh();
  }
  /* ********************************************************************************* */
  void StartShow(TimeFloat SongStartLocation0, TimeFloat SongFinishLocation0) {
    this->StartMusic(SongStartLocation0, SongFinishLocation0);
    this->StartAnimation();
  }
  /* ********************************************************************************* */
  void StartShowMovie(TimeFloat SongStartLocation0, TimeFloat SongFinishLocation0) {
    this->AssignPanTarget();
    this->StartMusic(SongStartLocation0, SongFinishLocation0);
    this->StartAnimationMovie();
  }
  /* ********************************************************************************* */
  void StopShow() {
    if (this->LivePlayer->Playing) {
      this->StopMusic();
      this->StopAnimation();
    }
  }
  /* ********************************************************************************* */
  void StartAnimation() {
    this->AnimationTimer.Bind(wxEVT_TIMER, &GlCanvas::OnTimerPlain, this);
    this->AnimationTimer.Start(AnimationIntervalMs);
    this->Refresh();
  }
  /* ********************************************************************************* */
  void StartAnimationMovie() {
    this->AnimationTimer.Bind(wxEVT_TIMER, &GlCanvas::OnTimerMovie, this);
    this->AnimationTimer.Start(AnimationIntervalMs);
    this->Refresh();
  }
  /* ********************************************************************************* */
  void StopAnimation() {
    this->AnimationTimer.Stop();
    this->Refresh();
  }
  /* ********************************************************************************* */
  void StartMusic(TimeFloat SongStartLocation0, TimeFloat SongFinishLocation0) {
    this->SongStartLocation = SongStartLocation0;
    this->SongFinishLocation = SongFinishLocation0;
    this->PlayerStartTimeFloat = Globals::GetTimeInSecondsFloat();
    // this->LivePlayer->Start(SongStartLocation0);
    SingerBase* Singer = this->GraphicRoot->Spawn_Singer();
    this->LivePlayer->Start(Singer, SongStartLocation0);

//    TimeFloat TimerNow = Globals::GetTimeInSecondsFloat();// Play first sound segment before timer starts.
//    this->Moment(TimerNow + this->AnimationIntervalSec);// buggy, not working.
  }
  /* ********************************************************************************* */
  void AssignPanTarget() {// Do this once at the beginning  // DoLivePan
    this->PrevPanTime = Globals::GetTimeInSecondsFloat();// Once before starting.
    this->TargetTime = this->PrevPanTime + 0.6;// define target// start time plus some seconds.
    // this->TargetTime = this->PrevPanTime + AnimationIntervalSec;// + 0.6;// define target// start time plus some seconds.
  }
  /* ********************************************************************************* */
  void LivePan() {// WIP, not finished.  DoLivePan
    TimeFloat NowTime = Globals::GetTimeInSecondsFloat();
    TimeFloat NowDeltaTime = NowTime - this->PrevPanTime;
    TimeFloat PrevDeltaTime = this->TargetTime - this->PrevPanTime;
    TimeFloat FractAlong = (NowDeltaTime / PrevDeltaTime);
    if (FractAlong > 1.0) { FractAlong = 1.0; }
    else if (FractAlong < 0.0) { FractAlong = 0.0; }

    double PixWdt = GetSize().x;
    double PixHgt = GetSize().y;

    if (true) {
      TimeFloat Wdt = this->TargetExtents.GetWidth();
      TimeFloat HalfWdt = Wdt/2.0;
      if (false) {// X time tracking movement
        TimeFloat Shift = 4.0;
        Shift = 0.0;
        TargetExtents.Min.X = this->SongLocation - (HalfWdt+Shift);
        TargetExtents.Max.X = this->SongLocation + (HalfWdt-Shift);
      }

      if (true) {// Y pitch tracking movement
        CajaDelimitadora Box, AudBox;
        bool exists = this->LivePlayer->RootSinger->GetSingerBounds(Box);// CHANGE THIS to return in audio space units, not pixels.
        if (exists) {
          this->GraphicRoot->MapTo(Box, AudBox);
          TargetExtents.Min.Y = AudBox.Min.Y;
          TargetExtents.Max.Y = AudBox.Max.Y;
          double Wdt = TargetExtents.GetWidth();
          double Hgt = TargetExtents.GetHeight();
          double AspRatio = PixWdt/PixHgt;// wrong AspRatio.
          Wdt = Hgt * AspRatio;

          HalfWdt = Wdt/2.0;
          TargetExtents.Min.X = this->SongLocation - (HalfWdt);
          TargetExtents.Max.X = this->SongLocation + (HalfWdt);

          TargetExtents.Multiply(2.0);
        }
      }
      this->TargetTime += NowDeltaTime;
    }

    this->GraphicRoot->MorphToExtents(this->TargetExtents, PixWdt, PixHgt, FractAlong);
    this->PrevPanTime = NowTime;
  }
  /* ********************************************************************************* */
  void StartSelectedMusic() {// WIP, not finished.
    Handle *Leaf;
    OffsetBoxBase *obx;
    if (nullptr != (Leaf = this->SelectedFish.VerCaptura())) {
      if (nullptr != (obx = dynamic_cast<OffsetBoxBase*>(Leaf))) {// another cast!
        delete this->PlayObx;
        this->PlayObx = obx->Clone_Me();// to do: refactor out this memory leak. snox
        Transformer::Copy_Transform(this->SelectedFish.RootMap, *PlayObx);// Now PlayObx maps between audio root and the obx's space/parent space.

        this->PlayObx->Compound(*obx);
//        PlayObx->AddXY(obx->TimeX, obx->OctaveY);
        this->SongStartLocation = PlayObx->TimeX;// Map from local space to audio space.
        this->SongFinishLocation = PlayObx->UnMap_EndTime();
        this->PlayerStartTimeFloat = Globals::GetTimeInSecondsFloat();
        // Spawn RootSinger at start.
        TimeFloat BeginTime = PlayObx->TimeX;
        SingerBase* Singer = this->GraphicRoot->Spawn_Singer(*PlayObx);
        Singer->Compound();
        this->LivePlayer->Start(Singer, BeginTime);// now do we determine BeginTime?
        this->StartAnimation();
      } else {
        VoicePoint* vp = nullptr;
        VoicePoint::LoudnessHandle* lh;
        if (nullptr != (vp = dynamic_cast<VoicePoint*>(Leaf)) ) { }
        else if (nullptr != (lh = dynamic_cast<VoicePoint::LoudnessHandle*>(Leaf)) ) { vp = lh->ParentPoint; }
        if (vp != nullptr) {// If a VoicePoint or LoudnessHandle was touched, play the surrounding time band.
          TimeFloat TimeRadius = 0.125;
          TimeFloat PntTime = this->SelectedFish.RootMap.UnMapTime(vp->TimeX);
          TimeFloat BeginTime = Math::max(0.0, PntTime - TimeRadius);
          TimeFloat EndTime = PntTime + TimeRadius;
          this->StartShow(BeginTime, EndTime);
        }
      }
    }
  }
  /* ********************************************************************************* */
  void StopMusic() {
    this->LivePlayer->Stop();// Block LivePlayer from launching a new play if previous play is still going.
  }
  /* ********************************************************************************* */
  void PaintIt(wxPaintEvent& WXUNUSED(event)) {
    wxPaintDC(this);
    Render();
  }
  /* ********************************************************************************* */
  bool Load_Composition(const String& FilePath) {
    cout << "Load_Composition()" << "\n";
    this->Delete_Composition();
    this->audproj = new AudProject();
    Config conf;// snox possible memory leak
    MetricsPacket metrics;
    metrics.MaxDuration = 0.0;
    metrics.MyProject = &conf;
    metrics.FreshnessTimeStamp = 2;
    bool success = false;
    if (audproj->ReadFile(FilePath)) {
      std::cout << "FilePath:" << FilePath << "\n";
      this->GraphicBody = audproj->GrSong;
      this->GraphicRoot = audproj->GraphicRoot;
      this->GraphicRoot->Update_Guts(metrics);
      this->GraphicRoot->Randomize_Color();// to do: delete this when we start saving color.
      success = true;
    }
    return success;
  }
  /* ********************************************************************************* */
  bool Load_Default_Composition(){
    cout << "Load_Default_Composition()" << "\n";
    std::vector<String> FileList;
    String FilePath;
    AudProject::Find_Jsong_Dir("./", FileList);
    bool success = false;
    if (FileList.size()>0){
      FilePath = FileList.at(0);
      return Load_Composition(FilePath);
    }
    return success;
  }
  /* ********************************************************************************* */
  void Generate_Default_Composition() {
    // this->audproj = new AudProject();
    Config conf;
    MetricsPacket metrics;
    metrics.MaxDuration = 0.0;
    metrics.MyProject = &conf;
    metrics.FreshnessTimeStamp = 2;

    {// Default pattern
      GroupSong *groupsong;
      GroupSong::Group_OffsetBox *Group_Handle;
      groupsong = new GroupSong();// parent group

      if (true) {
        LoopSong::Loop_OffsetBox *Loop_Handle;
        // Loop_Handle = PatternMaker::Simple_Loop();// keep this one.
        Loop_Handle = PatternMaker::Create_Spooky_Loops();
        Loop_Handle->OctaveY = 3.0;
        groupsong->Add_SubSong(Loop_Handle);
      } else {// Same pattern but with no LoopBox.
        GroupSong::Group_OffsetBox *Loop_Handle;
        Loop_Handle = PatternMaker::Create_Spooky();
        Loop_Handle->OctaveY = 3.0;
        groupsong->Add_SubSong(Loop_Handle);
      }

      Group_Handle = groupsong->Spawn_OffsetBox();

      audproj->Wrap_For_Graphics(Group_Handle);
    }
    {
      this->GraphicBody = audproj->GrSong;
      this->GraphicRoot = audproj->GraphicRoot;

      // this->GraphicRoot->Assign(109.57200186242139, 392.077713298938, 60.628662660420645, 60.628662660420645);
      // this->GraphicRoot->Assign(258.8627, -628.0639, 229.4328, 229.4328);// good for simple sines.
      this->GraphicRoot->Assign(75.7107575, -480.5047667877, 229.43283968, 229.43283968);// Good for default LoopSong.
      this->GraphicRoot->Update_Guts(metrics);
      this->GraphicRoot->Randomize_Color();
    }
    this->Refresh();
  }
  /* ********************************************************************************* */
  void Init_Composition() {
    this->audproj = new AudProject();
    if (true && Load_Default_Composition()){
      this->Refresh();
      return;
    }
    Generate_Default_Composition();
  }
  /* ********************************************************************************* */
  void Delete_Composition() {
    std::cout << "Delete_Composition start" << "\n";
    delete this->audproj; this->audproj = nullptr;
    std::cout << "Delete_Composition end" << "\n";
  }
  /* ********************************************************************************* */
  void PlotPolygon(std::vector<wxPoint> &points) {
    glBegin(GL_POLYGON);
    glColor3f(1.0, 1.0, 1.0);
    for (int cnt=0; cnt<points.size(); cnt++) {
      wxPoint *pnt = &(points.at(cnt));
      std::cout << "x:" << pnt->x << ", y:" << pnt->y << "\n";
      glVertex2f(pnt->x, pnt->y);
    }
    glEnd();
  }
  /* ********************************************************************************* */
  inline double FlipY(double YRaw) {// to consolidate all the Y-flipping in one place.
    return GetSize().y - YRaw;
  }
  /* ********************************************************************************* */
  // events
  void FocusHack(wxMouseEvent &event) {
    if(event.Entering()) {
      printf("event.Entering\n");
      //SetFocus();
    }
    event.Skip();
  }
  /* ********************************************************************************* */
  void MouseMoved(wxMouseEvent &event) {
    // printf("MouseMoved\n");
    this->ScreenMouseX = event.GetX(); this->ScreenMouseY = event.GetY();
    this->MouseDragging = event.Dragging();
    if (this->MouseDragging) {
      printf("Mouse Dragging\n");
      if (this->SelectedFish.VerCaptura() != nullptr) {
        if (DragStart){
          Backup();
        }
        Point2D AudioRootPnt;
        this->GraphicRoot->MapTo(this->ScreenMouseX, FlipY(this->ScreenMouseY), AudioRootPnt);// Map from pixel space to root audio space.
        AudioRootPnt.Subtract(MouseOffset);
        this->SelectedFish.MapMove(AudioRootPnt.X, AudioRootPnt.Y);
        this->audproj->Update_Guts();// snox Update_Guts for every mouse move is probably really expensive.
        //wxWindow::Refresh();
      }
    }
    MoveFloater(this->ScreenMouseX, FlipY(this->ScreenMouseY));
    DragStart=false;
    wxWindow::Refresh();// snox only for testing
  }
  /* ********************************************************************************* */
  void MouseLeftDown(wxMouseEvent &event) {
    printf("MouseLeftDown\n");
    DragStart = true;
    this->ScreenMouseX = event.GetX(); this->ScreenMouseY = event.GetY();
    SetFocus();
    this->MouseStart.Assign(event.GetX(), event.GetY());

    this->StopShow();// Stop play as soon as you click on anything. Good idea or not?
//    if (this->LivePlayer->Playing) {// Stop play as soon as you click on anything. Good idea or not?
//      cout << "MouseLeftDown: this->LivePlayer->Playing\n";
//      this->StopMusic();
//      this->StopAnimation();
//    }

    printf("Control Click:%d\n", this->ControlDown);
    if (this->ControlDown) {// Detect Control Click.
    }
    if (this->ShiftDown) {// Detect Shift Click.
    }

    SoundFloat FlippedMouseY = FlipY(this->ScreenMouseY);
    if (DestinationGroup!=nullptr) {// Click on destination to drop floater there.
      Point2D DestHandleLoc, DestInsideLoc;
      Backup();
      OffsetBoxBase *floater = this->GetFloater();
      // Translate floater location to be inside the DestinationGroup.
      DestinationHook.RootMap.MapTo(floater->TimeX, floater->OctaveY, DestHandleLoc);
      DestinationGroup->MapTo(DestHandleLoc, DestInsideLoc);
      floater->TimeX = DestInsideLoc.X; floater->OctaveY = DestInsideLoc.Y;
      DestinationGroup->GetContent()->Add_Child(floater);
      // Reset all drag and drop activity.
      DestinationGroup->GetContent()->SetSpineHighlight(false);
      this->SetFloater(nullptr);
      DestinationGroup = nullptr;
      this->audproj->Update_Guts();
      if (true) {
        StartSelectedMusic();// to do: instant playback when drag and drop is done.
      }
    } else {//
      if (this->TrySelect(this->ScreenMouseX, FlippedMouseY)) {
        Point2D OriginAudioRoot, MouseAudioRoot;
        this->GraphicRoot->MapTo(this->ScreenMouseX, FlippedMouseY, MouseAudioRoot);
        double XOrg = this->SelectedFish.Captura->GetX();
        double YOrg = this->SelectedFish.Captura->GetY();
        this->SelectedFish.UnMap(XOrg, YOrg, OriginAudioRoot);// Handle origin in audio root space.
        // Get difference between current mouse loc and object origin, in audio root space.
        double MouseOffsetX = MouseAudioRoot.X - OriginAudioRoot.X;
        double MouseOffsetY = MouseAudioRoot.Y - OriginAudioRoot.Y;
        MouseOffset.Assign(MouseOffsetX, MouseOffsetY);
      }
      this->AbortFloater();
    }
    wxWindow::Refresh();
  }
  /* ********************************************************************************* */
  void MouseLeftUp(wxMouseEvent &event) {
    printf("MouseLeftUp\n");
    if (this->SelectedFish.VerCaptura() != nullptr) {// Item selected, play selected.
      if (this->MouseDragging) {// Close out any dragging state.
        printf("Mouse Select Drag Up\n");
        this->MouseFinish.Assign(event.GetX(), event.GetY());// Close out any dragging state.
        this->MouseDragging = false;
        this->audproj->Update_Guts();
      }
      StartSelectedMusic();
      Refresh();
    } else {// Nothing selected.
      if (this->MouseDragging) {// Dragging but not selected, play swipe range.
        printf("Mouse Drag Up\n");
        this->MouseFinish.Assign(event.GetX(), event.GetY());// Close out any dragging state.
        this->MouseDragging = false;

        // Now play range of MouseStartX to MouseFinishX.
        double BeginTime = GraphicRoot->MapTime(this->MouseStart.X);// Map from graphic space to audio space.
        double EndTime = GraphicRoot->MapTime(this->MouseFinish.X);
        if (BeginTime>EndTime) {// if time is backward, swap
          double temp = BeginTime;
          BeginTime = EndTime; EndTime = temp;
        }
        printf("BeginTime:%f, EndTime:%f\n", BeginTime, EndTime);
        this->StartShow(BeginTime, EndTime);
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  bool TrySelect(TimeFloat XLoc, SoundFloat YLoc) {
    double radius = 0.1;//5;//20;// search box radius in pixels
    radius = SearchRadius;
    Grabber claw;
    claw.Clear();
    this->SelectedFish.Reset();

    {
      CajaDelimitadora PixelBox(XLoc-radius, YLoc-radius, XLoc+radius, YLoc+radius);
      //this->GraphicRoot->MapTo(PixelBox, claw.AudioRootSearchBox);// zoomdrag

      CajaDelimitadora AudioRootBox;
      this->GraphicRoot->MapTo(PixelBox, AudioRootBox);// zoomdrag
      //this->GraphicRoot->MapTo(CajaDelimitadora(XLoc-radius, YLoc-radius, XLoc+radius, YLoc+radius), AudioRootBox);// can replace need for PixelBox.
      claw.AssignAudioRootSearchBox(AudioRootBox);
    }

    FishingStackContext StackContext;// identity matrix in pixel space
    this->GraphicRoot->GoFishing(claw, StackContext);
    // this->GraphicRoot->GoFishing(claw, FishingStackContext()); this works too.

    this->SelectedFish = claw.GetMostDistal();

    Handle* obx2 = this->SelectedFish.VerCaptura();

//      this->Query.Leaf->SetSelected(false);
    return (obx2 != nullptr);
  }
  /* ********************************************************************************* */
  void MouseRightDown(wxMouseEvent &event) {
    printf("MouseRightDown\n");

    this->ScreenMouseX = event.GetX(); this->ScreenMouseY = event.GetY();

    // wxWindow::Refresh();

    SoundFloat XLoc = event.GetX();
    SoundFloat YLoc = FlipY(event.GetY());// Invert mouse Y because opengl origin is at bottom left.

    this->TrySelect(XLoc, YLoc);
    Handle* obx = this->SelectedFish.VerCaptura();

    /*
if numfound>0
once mouse is down, and something is selected, look for mouse move.
if mouse is moving and dragging, move SelectedFish.Captura, using SelectedFish.Map.
then updateguts?
the refresh for animation.

when mouse goes up, stop dragging but don't deselect.
    */

    //  bool IsGroup = typeid(obx) == typeid(GroupSong::Group_OffsetBox*);
    bool IsGroup = typeid(*obx) == typeid(GroupSong::Group_OffsetBox);
    cout << "a is B: " << boolalpha << IsGroup << endl;

    VoicePoint::LoudnessHandle* lh = dynamic_cast<VoicePoint::LoudnessHandle*>(obx);
    GroupSong::Group_OffsetBox* grobx = dynamic_cast<GroupSong::Group_OffsetBox*>(obx);
    Voice::Voice_OffsetBox* vobx = dynamic_cast<Voice::Voice_OffsetBox*>(obx);

//    claw.MoveAll(0.1, 0.1);
//    this->audproj->Update_Guts();
//    // this->GraphicRoot->Update_Guts()
//    Refresh();

    /*

for starters, we just pick most distal object in list.
. must be a *direct hit* before being even a consideration for distal comparison.
. single click, just start moving that.
. second click, compare with previous list and move that.

FishHook* Pick_Best(Grabber& claw) { }

all cases:

on every click down, list all for other options.

single click down on one thing, then drag
. move that thing

single click down on multiple siblings, then drag
. legit group select, move those things
. list all for other options.

single click down on parents and children, then drag
. only select the most distal thing, then move that thing
. list all for other options.

click AND unclick on multiple siblings
. select all, wait for second click to move
. but list all for other options.
. second click must hit something in currently selected, else unselect all and select whatever else you clicked.

click AND unclick on parents and children
. select most distal
. list all for other options.
. second click must hit something in currently selected, else unselect all and select whatever else you clicked.

box select?
. always 2 clicks.
. same issues as below.


two problems:
. need automatic test for parent child, true for *any* multi select.


. need click-detect for things already in list.

list detect may be easy.  on second click, create second list.
. if anything in the second list is in the first list, keep using the first list, and move that.


to do:
user clicks handle, lets go.  handle is selected.
user can click again.

https://stackoverflow.com/questions/351845/finding-the-type-of-an-object-in-c/4325139
TYPE& dynamic_cast<TYPE&> (object);
TYPE* dynamic_cast<TYPE*> (object);

#include <typeinfo>
cout << "a is B: " << boolalpha << (typeid(a) == typeid(B)) << endl;

    */

    SetFocus();
  }
  /* ********************************************************************************* */
  FishHook* Pick_Best(Grabber& claw) {
    int len = claw.Stringer.size();
    int MaxDepth = 0;
    FishHook *hook = nullptr, *best = nullptr;
    for (int cnt=0;cnt<len;cnt++) {
      hook = &(claw.Stringer.at(cnt));
      /*
      look for direct hit here.
      oh no, hitsme x y?
      no just make the search box 1 pixel in size. good enough.

      */
      if (MaxDepth <= hook->RecurseDepth) {
        MaxDepth = hook->RecurseDepth;
        best = hook;
      }
    }
    return best;
  }
  /* ********************************************************************************* */
  void Change_Loop_Count(int NumBeatsDelta) {
    Handle *Leaf = this->SelectedFish.VerCaptura();
    if (Leaf != nullptr) {
      LoopSong::Loop_OffsetBox *lobx;
      if (nullptr != (lobx = dynamic_cast<LoopSong::Loop_OffsetBox*>(Leaf))) {// another cast!
        printf("MainGui Change_Loop_Count\n");
        this->StopShow();
        Backup();
        LoopSong *loop = lobx->GetContent();
        loop->Delta_Beats(NumBeatsDelta);
        this->audproj->Update_Guts();
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  void Make_Even() {
    Handle *Leaf = this->SelectedFish.VerCaptura();
    if (Leaf != nullptr) {
      GroupSong::Group_OffsetBox *grobx;
      if (nullptr != (grobx = dynamic_cast<GroupSong::Group_OffsetBox*>(Leaf))) {// another cast!
        printf("MainGui Make_Even\n");
        this->StopShow();
        Backup();
        grobx->GetContent()->Space_Evenly();
        this->SelectedFish.Reset();
        this->audproj->Update_Guts();
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  void Align_Kids() {
    Handle *Leaf = this->SelectedFish.VerCaptura();
    if (Leaf != nullptr) {
      GroupSong::Group_OffsetBox *grobx;
      if (nullptr != (grobx = dynamic_cast<GroupSong::Group_OffsetBox*>(Leaf))) {// another cast!
        printf("MainGui Align_Kids\n");
        this->StopShow();
        Backup();
        grobx->GetContent()->Align_Kids();
        this->SelectedFish.Reset();
        this->audproj->Update_Guts();
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  void Burn_Scale() {
    Handle *Leaf = this->SelectedFish.VerCaptura();
    if (Leaf != nullptr) {
      OffsetBoxBase *obx;
      if (nullptr != (obx = dynamic_cast<OffsetBoxBase*>(Leaf))) {// another cast!
        printf("MainGui Burn_Scale\n");
        this->StopShow();
        Backup();
        TimeFloat ReScaleX, ReScaleY;
        ReScaleX = obx->ScaleX; ReScaleY = obx->ScaleY;
        // first get
        // OffsetBoxBase will need a burn scale function.
        // every ISonglet will need a burn scale function.
        // groups will call it on all of their child oboxes.
        // voices will call it on all of their voicepoints.
        CollisionLibrary HitTable;
        obx->BreakFromHerd(HitTable);
        obx->Randomize_Color();
        obx->ScaleX = 1.0; obx->ScaleY = 1.0;
        int TimeStamp = ++this->audproj->UpdateCounter;// Increment UpdateCounter every time it is used.
        obx->GetContent()->Burn_Scale(TimeStamp, ReScaleX, ReScaleY);
        // obx->Burn_Scale(TimeStamp, ReScaleX, ReScaleY);
        this->SelectedFish.Reset();
        this->audproj->Update_Guts();
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  void CopyBranch(double XDisp, double YDisp) {
    if (this->SelectedFish.VerCaptura() != nullptr) {
      Handle *Leaf = this->SelectedFish.VerCaptura();
      OffsetBoxBase *obx;
      VoicePoint *vpnt;
      OffsetBoxBase *FloatObx = nullptr;
      if (nullptr != (obx = dynamic_cast<OffsetBoxBase*>(Leaf))) {// another cast!
        printf("MainGui CopyBranch\n");

        Point2D MouseAudioRoot;
        this->GraphicRoot->MapTo(XDisp, YDisp, MouseAudioRoot);// map from pixel space to audio root space.

        this->AbortFloater();

        // FloatObx = obx->GetContent()->Spawn_OffsetBox();
        FloatObx = obx->Clone_Me();
//        FloatObx->Compound(this->SelectedFish.RootMap);// Now FloatObx maps between audio root and the obx's space/parent space.
        Transformer::Copy_Transform(this->SelectedFish.RootMap, *FloatObx);// Now FloatObx maps between audio root and the obx's space/parent space.
        // FloatObx->Compound(*obx);// Do this just to get the inner scale of obx.
        // We want FloatObx to have the same inner scale values as the selected obox, but the xy position of the mouse in audioroot space.
        FloatObx->MoveTo(MouseAudioRoot);
        this->SetFloater(FloatObx);
        this->audproj->Update_Guts();
        this->SelectedFish.Reset();
        Refresh();
      } else if (nullptr != (vpnt = dynamic_cast<VoicePoint*>(Leaf))) {// another cast!
        // to do: copy a voicepoint and hang it to the mouse.
        Point2D MouseAudioRoot, LocalPoint;
        this->GraphicRoot->MapTo(XDisp, YDisp, MouseAudioRoot);// map from pixel space to audio root space.
        this->SelectedFish.RootMap.MapTo(MouseAudioRoot, LocalPoint);
        vpnt->MyParentVoice->Add_Note(LocalPoint.X, LocalPoint.Y, vpnt->LoudnessFactor);
        // vpnt->MyParentVoice->Sort_Me();
        this->audproj->Update_Guts();
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  void Backup() {// Save state to UndoStack.
    printf("MainGui Backup\n");
    OffsetBoxBase *obx = this->GraphicBody->Get_Content();
    CollisionLibrary HitTable;
    OffsetBoxBase *snapshot = obx->Deep_Clone_Me(HitTable);
    History.BackUp(snapshot);
  }
  /* ********************************************************************************* */
  void Undo() {
    printf("MainGui Undo\n");
    OffsetBoxBase *obxprev = this->GraphicBody->Get_Content();
    OffsetBoxBase *obx = History.Undo();
    if (obx != nullptr) {
      if (this->LivePlayer->Playing) {// Stop play as soon as you click on anything. Good idea or not?
        cout << "Undo: this->LivePlayer->Playing\n";
        this->StopMusic();
        this->StopAnimation();
      }
      this->audproj->Wrap_For_Graphics(obx);
      delete obxprev;
      // to do: make sure everything else is connected correctly.
      this->audproj->Update_Guts();
      Refresh();
    }
  }
  /* ********************************************************************************* */
  void BreakFromHerd() {
    // just kinda works for now. probably a lot of memory leaks and dangerous side effects as-is.
    OffsetBoxBase* FloatObx = this->GetFloater();
    if (FloatObx!=nullptr) { // to do: check if floater is in play, and if so break that instead.
      CollisionLibrary HitTable;
      FloatObx->BreakFromHerd(HitTable);
      FloatObx->Randomize_Color();
      this->SetFloater(FloatObx);
      Refresh();
    } else if (this->SelectedFish.VerCaptura() != nullptr) {
      Handle *Leaf = this->SelectedFish.VerCaptura();
      OffsetBoxBase *obx;
      if (nullptr != (obx = dynamic_cast<OffsetBoxBase*>(Leaf))) {// another cast!
        printf("MainGui BreakFromHerd\n");
        Backup();
        this->SelectedFish.Reset();
        CollisionLibrary HitTable;
        obx->BreakFromHerd(HitTable);
        obx->Randomize_Color();
        this->audproj->Update_Guts();
        Refresh();
      }
    }
  }
  /* ********************************************************************************* */
  void MouseWheelMoved(wxMouseEvent &mwevent) {
    printf("MouseWheelMoved\n");
    double XCtrRaw, YCtrRaw;
    double XCtr, YCtr, Rescale = 1.0;
    double Width = GetSize().x, Height = GetSize().y;

    XCtr = XCtrRaw = mwevent.GetX(); YCtr = YCtrRaw = mwevent.GetY();
    this->ScreenMouseX = XCtrRaw; this->ScreenMouseY = YCtrRaw;
    // MouseBox.Assign(XCtrRaw-100, YCtrRaw-100, XCtrRaw+100, YCtrRaw+100);// for testing
    XCtr/=Width; YCtr/=Height;
    XCtr = (XCtr*this->OglWidth)+OglLeft;// Fit to boundaries of opengl viewport.
    YCtr = this->OglHeight - (YCtr*this->OglHeight);// Flip vertical for bottom-origin of opengl window
    YCtr = YCtr + this->OglBottom;
    cout << "XCtr:" << XCtr << ", YCtr:" << YCtr << "\n";
    OglMouseX = XCtr; this->OglMouseY = YCtr;

    double finerotation = mwevent.GetWheelRotation();// getPreciseWheelRotation
    finerotation = finerotation * 0.003;//0.2;// magic number from trial and error
    Rescale = std::pow(2.0, finerotation);//  Math::pow(2, finerotation);// range 0 to 1 to positive infinity
    //GraphicBox.Graphic_OffsetBox *gb = this->MyProject.GraphicRoot;
    this->GraphicRoot->Zoom(XCtr, YCtr, Rescale);

    this->GraphicRoot->Print_Me("GraphicRoot:");
    //event.Skip();
    wxWindow::Refresh();
  }
  /* ********************************************************************************* */
  void MouseMiddleClick(wxMouseEvent &event) {
    printf("MouseMiddleClick\n");
    //event.Skip();
  }
  void MouseEnterWindow(wxMouseEvent &event) {
    printf("MouseEnterWindow\n");
    //SetFocus();
    //event.Skip();
  }
  void MouseLeaveWindow(wxMouseEvent &event) {
    printf("MouseLeaveWindow\n");
    //event.Skip();
  }
  /* ********************************************************************************* */
  void HighlightTarget(bool Highlight) {
    if (this->PossibleDestination != nullptr) {
      this->PossibleDestination->SetSpineHighlight(Highlight);
    }
  }
  /* ********************************************************************************* */
  OffsetBoxBase* DropOnTarget() {
    IMoveable *floater = this->GetFloater();
    OffsetBoxBase *obox = dynamic_cast<OffsetBoxBase*>(floater);
    if (obox != nullptr) {
      if (this->PossibleDestination != nullptr) {
      }
    }
    return obox;
  }
  /* ********************************************************************************* */
  void LaunchFloater() {
    this->AbortFloater();
    // OffsetBoxBase *obx = PatternMaker::Create_Seed();
    OffsetBoxBase *obx = PatternMaker::Create_Seed_Long();
    this->SetFloater(obx);
  }
  /* ********************************************************************************* */
  void LaunchLoopFloater() {
    this->AbortFloater();
    OffsetBoxBase *obx = PatternMaker::Create_Loop();
    this->SetFloater(obx);
  }
  /* ********************************************************************************* */
  OffsetBoxBase* GetFloater() {
    // return this->MyProject->GraphicRoot->Content->Floater;
    return this->GraphicRoot->Content->Floater;
  }
  /* ********************************************************************************* */
  void AbortFloater() {
    if (this->GraphicRoot->Content->Floater != nullptr) {// Abort previous copy.
      delete this->GraphicRoot->Content->Floater;// Never do this here if floater has been re-added to composition.
      this->GraphicRoot->Content->Floater = nullptr;
    }
  }
  /* ********************************************************************************* */
  void SetFloater(OffsetBoxBase* floater) {
    if (floater != nullptr) {
      DestinationClaw.Reset(++this->audproj->UpdateCounter);// Increment UpdateCounter every time it is used.
      floater->GetInventory(DestinationClaw.Forbidden);// Get all unique containers in floater.
      DestinationClaw.Sort();
      printf("floater->GetInventory\n");
    }
    this->GraphicRoot->Content->Floater = floater;
  }
  /* ********************************************************************************* */
  void MoveFloater(double ScreenX, double ScreenY) {
    OffsetBoxBase* FloatObx = this->GetFloater();
    if (FloatObx!=nullptr) {
      Point2D MouseAudioRoot;
      this->GraphicRoot->MapTo(ScreenX, ScreenY, MouseAudioRoot);// map from pixel space to audio root space.
      FloatObx->MoveTo(MouseAudioRoot);
/*
big picture
if floater is dropped on something, GraphicRoot->Content->Floater is set to null without deletion.
if floating is aborted and floater is not null, delete it.
find the closest container on every MoveFloater and only highlight it.
keep a persistent pointer to the destination container as we move, and un-highlight it at the start of every movement.

so first we need a test for container closeness.
. somehow pass distance upward through the fish hook, I guess.
. every container on the stringer has a map to audio root.
. could re-read every container and re-calculate distance.

graphicbox.mapto(xloc, yloc, audiospace);
fish.rootmap.mapto(audiospace, groupobxloc);
groupobx.mapto(groupobxloc, insidegrouploc);

XLoc = insidegrouploc.X; YLoc = insidegrouploc.Y;
dist = groupsong.GetDistanceFromSpline(double XLoc, double YLoc);


for dropping the floater on a group
. unmap the floater to audio root space
. remap those coords to local group inner space.
. assign the floater's obx to those coords and attach it to the group.

DestinationClaw
*/

      {
        // Find IContainers nearby to join.
        double radius = 20.0;// search box radius in pixels
        DestinationClaw.Clear();
        CajaDelimitadora AudioRootBox;

        this->GraphicRoot->MapTo(CajaDelimitadora(ScreenX-radius, ScreenY-radius, ScreenX+radius, ScreenY+radius), AudioRootBox);// can replace need for PixelBox. zoomdrag
        DestinationClaw.AssignAudioRootSearchBox(AudioRootBox);
        this->GraphicRoot->GoFishing(DestinationClaw, FishingStackContext());

        int len = DestinationClaw.Stringer.size();
        GroupSong::Group_OffsetBox *grobx;

        if (DestinationGroup!=nullptr) {  // Test if previous Best Group exists and if so, un-highlight it.
          DestinationGroup->GetContent()->SetSpineHighlight(false);
          DestinationGroup = nullptr;
        }

        // to do: here we want a pick best group, to get just 1. use most distal for now, xy closest later.
        int MaxDepth = 0;
        FishHook hook;//, besthook;
        // int dragcnt = 0;
        for (int cnt=0;cnt<len;cnt++) {
          hook = DestinationClaw.Stringer.at(cnt);
          grobx = dynamic_cast<GroupSong::Group_OffsetBox*>(hook.Captura);
          if (grobx!=nullptr) {

            if (MaxDepth <= hook.RecurseDepth) {
              MaxDepth = hook.RecurseDepth;
              DestinationHook = hook; DestinationGroup = grobx;
            }
          }
        }

        if (DestinationGroup!=nullptr) {
          DestinationGroup->GetContent()->SetSpineHighlight(true);
        }
      }

      this->audproj->Update_Guts();
      Refresh();
    } else {
      //DestinationGroup = nullptr;
    }
  }
  /* ********************************************************************************* */
  void DeleteSelected() {
    if (this->SelectedFish.VerCaptura() != nullptr) {
      Handle *handle = this->SelectedFish.VerCaptura();
      VoicePoint::LoudnessHandle* loud = nullptr;
      GroupSong::Group_OffsetBox* grobx = nullptr;
      LoopCellSong::LoopCell_OffsetBox* loopcellbx = nullptr;
      Voice::Voice_OffsetBox* vobx = nullptr;
      VoicePoint* vp = nullptr;
      GraphicBox* gfx = nullptr;

      Voice *voz, *ParentVoice = nullptr;
      IContainer *ParentContainer = nullptr;

      if (nullptr != (loopcellbx = dynamic_cast<LoopCellSong::LoopCell_OffsetBox*>(handle))) {// loopsong unit cell, DO NOT delete!
        printf("LoopCell_OffsetBox delete attempt.\n");
      } else if (nullptr != (grobx = dynamic_cast<GroupSong::Group_OffsetBox*>(handle))) {// group
        printf("Group_OffsetBox delete attempt.\n");
        ParentContainer = grobx->GetParentSonglet();
        if (nullptr != (gfx = dynamic_cast<GraphicBox*>(ParentContainer))) {// GraphicBox
          printf("Parent is GraphicBox.\n");
        } else {
          this->Backup();
          ParentContainer->Remove_Child(grobx);//  Remove_Child deletes obx internally.
        }
        this->SelectedFish.Reset();
      } else if (nullptr != (vobx = dynamic_cast<Voice::Voice_OffsetBox*>(handle))) {// voice
        printf("Voice_OffsetBox delete attempt.\n");
        ParentContainer = vobx->GetParentSonglet();
        if (nullptr != (gfx = dynamic_cast<GraphicBox*>(ParentContainer))) {// GraphicBox
          printf("Parent is GraphicBox.\n");
        } else {
          this->Backup();
          ParentContainer->Remove_Child(vobx);//  Remove_Child deletes obx internally.
        }
        this->SelectedFish.Reset();
      } else if (nullptr != (vp = dynamic_cast<VoicePoint*>(handle))) {// voice point
        printf("VoicePoint delete attempt.\n");
        ParentVoice = vp->GetParentVoice();
        this->Backup();
        ParentVoice->Remove_Note(vp);
        this->SelectedFish.Reset();
      } else if (nullptr != (loud = dynamic_cast<VoicePoint::LoudnessHandle*>(handle))) {// loudness handle
        printf("LoudnessHandle delete attempt.\n");
        ParentVoice = loud->GetParentVoice();
        this->Backup();
        ParentVoice->Remove_Note(loud);
        this->SelectedFish.Reset();
      }

      this->audproj->Update_Guts();// snox Update_Guts for every mouse move is probably really expensive.
      wxWindow::Refresh();

    /*

    Remove_Me() would be great, but vp can't ref anything inside voice.
    vobx.Remove_Me(); // ask generic song parent to remove me, a generic obx.
    grobx.Remove_Me(); // ask generic song parent to remove me, a generic obx.
    vp.Remove_Me(); // ask voice parent to remove me, a vp.
    loud.Remove_Me();// ask my vp parent to remove itself and me.

    delete the selected.

    first firgure out what type selected is and cast it.

    after cast, IF it is a voice or group, it points up to a group or container.

    if it is a voicepoint, delete it from the voice that it points up to.
    if it is a loudness handle, find its pareant voidepoint and delete that.
    vps are really deleted?
    anything higher than a vp can be just cut looseand maybe saved.

    but, when we get parent pointers, do we cast them too?  yes because each parent has a different remove_child?
    well with group child is always an obox.
    inside voice child is either a vp, or a lh.

    so every handle has a generic Handle* GetParent(), which we then cast up.
    or MonkeyBox* GetParent()

    */
    }
  }
  /* ********************************************************************************* */
  void StartNewProject() {
    this->Delete_Composition();
    this->audproj = new AudProject();
    this->Generate_Default_Composition();
    // this->SetTitle(ProgName + " - " + OpenFileDialog.GetFilename());
  }
  /* ********************************************************************************* */
  void KeyPressed(wxKeyEvent& event) {
    int KeyCode = event.GetKeyCode();// 32767 for ctrl
    this->ShiftDown = event.ShiftDown();
    this->ControlDown = event.ControlDown();
    int mods = event.GetModifiers();
    if (event.GetModifiers() == wxMOD_CONTROL) {
    }
    if (this->ControlDown) {
      printf("ControlDown:%d, KeyCode:%d\n",this->ControlDown, KeyCode);
      switch (KeyCode) {// to do: switch to this pattern.
        case 'X': {
          printf("Ctrl-X KeyCode:%d\n", KeyCode);
          this->BreakFromHerd();
          break;
        }
        case 'C': {
          printf("Ctrl-C KeyCode:%d\n", KeyCode);
          this->CopyBranch(this->ScreenMouseX, FlipY(this->ScreenMouseY));
          break;
        }
        case 'E': {
          printf("Ctrl-E KeyCode:%d\n", KeyCode);
          this->Make_Even();
          break;
        }
        case 'R': {// Render to wav file.
          printf("Ctrl-R KeyCode:%d\n", KeyCode);
          this->audproj->SaveWav("Backup.wav");
          break;
        }
        case 'S': {// save here.
          printf("Ctrl-S KeyCode:%d\n", KeyCode);
          //this->audproj->WriteFile("Backup.jsong");
          this->audproj->WriteFile("Backup.vt.json");
          break;
        }
        case 'V': {
          this->GraphicRoot->Randomize_Color();
          Refresh();
          break;
        }
        case 'Z': {
          this->Undo();
          break;
        }
        case 'P': {
          Profile_Test2();// to do: remove this, temporary test.
          break;
        }
        case 'B': {
          Burn_Scale();
          break;
        }
        case 'M': {
          Align_Kids();
          break;
        }
        case 'Q': {
          printf("Ctrl-Q KeyCode:%d\n", KeyCode);
          if (this->LivePlayer->Playing) {// Space bar toggles play on and off.
            this->StopMusic();
            this->StopAnimation();
          }
          break;
        }
        case '-': {
          // reduce loop cycles by 1
          this->Change_Loop_Count(-1);
          break;
        }
        case '=': { // '+'
          // increase loop cycles by 1
          this->Change_Loop_Count(+1);
          break;
        }
        case '+': {
          this->Change_Loop_Count(+1);
          break;
        }
        case WXK_SPACE: {// ctrl spacebar
          if (this->LivePlayer->Playing) {// Space bar toggles play on and off.
            cout << "this->LivePlayer->Playing\n";
            this->StopShow();
          } else {// Play whole composition beginning to end.
            cout << "NOT this->LivePlayer->Playing\n";
            this->StartShowMovie(0.0, this->GraphicRoot->Get_Duration());// DoLivePan
          }
          break;
        }
      }
    } else {// non-control keys
      if(KeyCode == WXK_SPACE) {
         cout << "GlCanvas Spacebar!\n";
         if (this->LivePlayer->Playing) {// Space bar toggles play on and off.
           cout << "this->LivePlayer->Playing\n";
           this->StopShow();
         } else {// Play whole composition beginning to end.
           cout << "NOT this->LivePlayer->Playing\n";
           this->StartShow(0.0, this->GraphicRoot->Get_Duration());
         }
         cout << "GlCanvas Spacebar exited.\n";
      }
    }
    if(event.GetKeyCode() == WXK_BACK && event.GetModifiers() == wxACCEL_CTRL) {
    }
    /* ********************************************************************************* */
    if(event.GetKeyCode() == WXK_DELETE) {
      if (this->LivePlayer->Playing) {// redundant check, remove
        this->StopShow();// Halt all playing before edit.
      }
      this->DeleteSelected();
    }
    wxChar ch = event.GetUnicodeKey();
    wxChar keycode = event.GetKeyCode();
    wxObject *evtobj = event.GetEventObject();
    cout << "GlCanvas KeyPressed:" << ch << ": evtobj:" << evtobj << ":\n";
    event.Skip(true);
    return;
    if (event.GetKeyCode() == WXK_ESCAPE) {
      //SetCurrent(*m_glContext);
      //do your task here
      event.Skip(false); //not further process
      Refresh(false); //fire a paint-event which updates GL stuff
    }
  }
  void KeyReleased(wxKeyEvent& event) {
    printf("GlCanvas KeyReleased\n");
    this->ShiftDown = event.ShiftDown();;
    this->ControlDown = event.ControlDown();
    //event.Skip(true);
  }
  void Resized(wxSizeEvent& event) {
    printf("Resized\n");
    //this->Render();// assert "xid" failed in SwapBuffers(): window must be shown
    Refresh();
  }
  /* ********************************************************************************* */
  void Profile_Test(){
    cout << "Profile_Test()" << "\n";
    AudProject aup;
    String FilePath = "./jsong/horn_chords_long01.jsong";
    GlDrawingContext ParentDC;
    if (aup.ReadFile(FilePath)) {
      Init_Profilers();
      {
        ParentDC.RecurseDepth = 1;//0;
        ParentDC.ClipBounds.Make_Unlimited();//.Assign(OglLeft, OglBottom, OglRight, OglTop);
        aup.GraphicRoot->MyBounds.Make_Unlimited();
        aup.GraphicRoot->Print_Me("Render GraphicRoot:");
        aup.GraphicRoot->Draw_Me(ParentDC);
      }
      Print_Profilers();
    }
  }
  /* ********************************************************************************* */
  void Profile_Test2(){
    Init_Profilers();
    for (int cnt=0;cnt<10;cnt++){
      Render();
    }
    Print_Profilers_File();
  }
private:
  /* ********************************************************************************* */
  void Render() {
    wxGLContext wxcon(this);
    wxGLCanvasBase::SetCurrent(wxcon);
    SetCurrent(wxcon);
    //wxPaintDC(this);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    double Width = GetSize().x, Height = GetSize().y;
    //double Width = 500, Height = 500;

/*
What to do:
get largest dimension
AspRatio = ogl size / size of that dim

transform is
( (Loc - Win0) * (OglSz/WinSz) ) + OBase

OMinor
WinMinor/WinMajor * OMinorSz

Scale = OMajorSz/WinMajor

OminorSz = 2.0

lets say X is the scaler
so all things X are the same
but

OglWidth = OglRight-OglLeft;
OglHeight = OglTop-OglBottom;

so
OglHeight = 2.0 * (WinHeight/WinWidth)
OglTop = OglBottom+OglHeight;

*/
    if (true) {// Alternative where viewport inner scale is in pixels, which factors out the viewport.
      OglLeft=0; OglRight=Width; OglBottom=0; OglTop=Height;
      OglWidth = Width; OglHeight = Height;
    } else {
      OglHeight = OglWidth * (Height/Width);// keep aspect ratio 1:1
      OglTop = OglBottom+OglHeight;
    }

    double AspRatio = 1.0;
    double BigWinSize;
    if (Width>Height){
      AspRatio = 1.0;
      BigWinSize = Width;
    } else {
      AspRatio = 1.0;
      BigWinSize = Height;
    }
    double Factor = 0.25;

    glViewport(0, 0, Width, Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(OglLeft, OglRight, OglBottom, OglTop, OglNear, OglFar);
    //glOrtho(0, OglRight, 0, OglTop, OglNear, OglFar);
    //glScaled(Factor, Factor, Factor);
    {
//      const GLubyte *glversion = glGetString(GL_VERSION);
//      char next[256];
//      memcpy(next, glversion, 256);
//      printf(next);
      //https://www.khronos.org/opengl/wiki/Performance look into swapbuffers
      //to do: use glFinish to profile more accurately
    }
    if (true) {// Floating image in the background.
      double HalfLeft = OglLeft/2.0;
      double HalfRight = OglRight/2.0;
      double HalfBottom = OglBottom/2.0;
      double HalfTop = OglTop/2.0;

      glBegin(GL_POLYGON);
      glColor3f(1.0, 1.0, 1.0);
      glVertex2f(HalfLeft, HalfBottom);// bottom left corner
      glVertex2f(HalfLeft, HalfBottom + 1.0);// top left corner   // glVertex2f(HalfLeft, HalfTop);// top left corner

      glVertex2f(0.0, 0.0);// concave vertex

      glVertex2f(HalfRight, HalfBottom + 1.0);// glVertex2f(HalfRight, HalfTop);
      glVertex2f(HalfRight, HalfBottom);
      //glColor4f(1.0, 1.0, 1.0, 0.1);// differently colored vertex with alpha
      glColor3f(0.4, 0.5, 0.4);// differently colored vertex
      glVertex2f(0.0, OglBottom * 0.8);
      glEnd();

      // small red rectangle
      double RedBoxBottom = OglBottom * 0.1;
      double RedBoxTop = RedBoxBottom + 0.2;
      glBegin(GL_POLYGON);
      glColor4f(1.0, 0.0, 0.0, 0.75);
      glVertex2f(OglRight * 0.1, RedBoxTop);
      glVertex2f(OglLeft * 0.1, RedBoxTop);
      glVertex2f(OglLeft * 0.1, RedBoxBottom);
      glVertex2f(OglRight * 0.1, RedBoxBottom);
      glEnd();
    } else {
      glBegin(GL_POLYGON);
      glColor3f(1.0, 1.0, 1.0);
      glVertex2f(-0.5, -0.5);// bottom left corner
      glVertex2f(-0.5, 0.5);// top left corner

      glVertex2f(0.0, 0.0);// concave vertex

      glVertex2f(0.5, 0.5);
      glVertex2f(0.5, -0.5);
      //glColor4f(1.0, 1.0, 1.0, 0.1);// differently colored vertex with alpha
      glColor3f(0.4, 0.5, 0.4);// differently colored vertex
      glVertex2f(0.0, -0.8);
      glEnd();

      // small red rectangle
      glBegin(GL_POLYGON);
      glColor4f(1.0, 0.0, 0.0, 0.75);
      glVertex2f(0.1, 0.1);
      glVertex2f(-0.1, 0.1);
      glVertex2f(-0.1, -0.1);
      glVertex2f(0.1, -0.1);
      glEnd();
    }

    glColor4f(1.0, 0.0, 0.0, 1.0);
    GlDrawingContext::DrawCircle2(OglMouseX, OglMouseY, 0.01);

    //glColor4f(0,0,1,1);
    glColor4f(1.0, 0, 1, 1);
    {
      GlDrawingContext dc;
      dc.RecurseDepth = 1;//0;
      //this->ResetPlaybackCursor();
      dc.ClipBounds.Assign(OglLeft, OglBottom, OglRight, OglTop);
      this->GraphicRoot->Print_Me("Render GraphicRoot:");
      this->GraphicRoot->Draw_Me(dc);
      this->LivePlayer->Draw_Me(dc);

      // MouseBox;
      dc.DrawCenteredSquare(this->ScreenMouseX, FlipY(this->ScreenMouseY), SearchRadius);

      if (this->LivePlayer->Playing) {
        TimeFloat PixelTime = this->GraphicRoot->UnMapTime(this->SongLocation);
        dc.SetColor(Color::Red());
        dc.DrawLine(PixelTime, 0, PixelTime, OglTop);
        // printf("SongLocation:%Lf\n", this->SongLocation);

//        if (DoLivePan) {
//          CajaDelimitadora ExtPix;
//          this->GraphicRoot->UnMap(this->TargetExtents, ExtPix);
//          ExtPix.Swell(-10);
//          dc.SetColor(Color::Green());
//          dc.DrawRectangleHollow(ExtPix);
//        }
      }

    }

    glFlush();
    SwapBuffers();
  }
};

#endif // GlCanvas_hpp

#if FALSE

/*
crash on undo:
UpdateBoundingBoxLocal
UpdateBoundingBoxLocal
GraphicBox UpdateBoundingBoxLocal
GlCanvas KeyPressed:90: evtobj:0x55955ded0b80:
PaCallback, KeepGoing:1, FramesPerBuffer:882, StatusFlags:0
PLAYING:852
PaCallback, KeepGoing:1, FramesPerBuffer:882, StatusFlags:0
this->RootSinger->Draw_Me(dc);
*************************************** Timer!  31
TimerNow:0.000000
NextTime:12.5923
12.5923
Segmentation fault (core dumped)
john@FatFreddy:~/Documents/Projects/VectunesGui$

*/

// junkyard
void Keys() {
  ConnectKeyDownEvent(this);
}
void ConnectKeyDownEvent(wxWindow* pclComponent) {
  if(pclComponent) { // https://wiki.wxwidgets.org/Catching_key_events_globally
    if (true) {
      pclComponent->Bind(wxEVT_KEY_DOWN, &GlCanvas::KeyPressed, this, wxID_ANY);
    } else {
      pclComponent->Connect(wxID_ANY,
                            wxEVT_KEY_DOWN,
                            wxKeyEventHandler(GlCanvas::KeyPressed),
                            (wxObject*) NULL,
                            this);
    }
    wxWindowListNode* pclNode = pclComponent->GetChildren().GetFirst();
    while(pclNode) {
      wxWindow* pclChild = pclNode->GetData();
      this->ConnectKeyDownEvent(pclChild);
      pclNode = pclNode->GetNext();
    }
  }
}

#endif // FALSE
