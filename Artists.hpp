#ifndef Artists_hpp
#define Artists_hpp

//#include <GL3/gl3.h> // "GL3/gl3.h: No such file or directory"

#include "./Vectunes/Vectunes.hpp"
#include "GlDrawingContext.hpp"

/*
Artists.hpp injects graphics handlers into the Vectunes biz logic library at runtime.
This awkward ugly pattern is so that Vectunes biz logic has no deep dependencies on GUI graphics.

Hmm, generic Arist/ArtistFactory base class underneath Vectunes library?
Then all the songlets and oboxes can just have an instance artist pointer and a static factory pointer.
We would still have room for custom Artists later on.
*/

/* ********************************************************************************* */
class GrArtist : public GraphicBox::Artist {
public:
  GraphicBox *MySonglet = nullptr;
  GrArtist() { }
  GrArtist(GraphicBox *MySonglet) {
    this->MySonglet = MySonglet;
  }
  void Draw_Me(DrawingContext& ParentDC) override {
    Draw_Grid(ParentDC);
  }
  /* ********************************************************************************* */
  void Draw_Grid(DrawingContext& ParentDC) { // to do: put this in GraphicBox.
    double xloc, yloc;
    double MinX, MinY, MaxX, MaxY;// in audio coordinates
    double ScreenMinX, ScreenMinY, ScreenMaxX, ScreenMaxY;// in screen coordinates
    double XOrigin, YOrigin;
    Transformer *GlobalTransform = &(ParentDC.GlobalTransform);

    MinX = Math::floor(ParentDC.ClipBounds.Min.X);
    MinY = Math::floor(ParentDC.ClipBounds.Min.Y);
    MaxX = Math::ceil(ParentDC.ClipBounds.Max.X);
    MaxY = Math::ceil(ParentDC.ClipBounds.Max.Y);

    ScreenMinX = GlobalTransform->UnMapTime(MinX);
    ScreenMinY = GlobalTransform->UnMapPitch(MinY);
    ScreenMaxX = GlobalTransform->UnMapTime(MaxX);
    ScreenMaxY = GlobalTransform->UnMapPitch(MaxY);
    GlobalTransform->Print_Me("GlobalTransform:");

    if (ScreenMaxY < ScreenMinY) {// swap
      double temp = ScreenMaxY; ScreenMaxY = ScreenMinY; ScreenMinY = temp;
    }

    double Alpha = 0.25;
    glLineWidth(0.25);
    glColor4f(0.7, 0.7, 0.7, Alpha);
    //ParentDC.gr.setColor(Globals.ToAlpha(Color.lightGray, 100));// draw minor horizontal pitch lines
    for (double ysemi = MinY; ysemi < MaxY; ysemi += 1.0 / 12.0) {// semitone lines
      yloc = GlobalTransform->UnMapPitch(ysemi);
      glBegin(GL_LINES);
      glVertex2d(ScreenMinX, yloc); glVertex2d(ScreenMaxX, yloc);
      glEnd();
      //glVertex3f(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc, 0.0);
      //ParentDC.gr.drawLine(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc);
    }
    glColor4f(0.7, 0.7, 0.7, Alpha);
    //ParentDC.gr.setColor(Globals.ToAlpha(Color.lightGray, 100));// draw minor vertical time lines
    for (double xsemi = MinX; xsemi < MaxX; xsemi += 1.0 / 4.0) {// 1/4 second time lines
      xloc = GlobalTransform->UnMapTime(xsemi);
      glBegin(GL_LINES);
      glVertex2d(xloc, ScreenMinY); glVertex2d(xloc, ScreenMaxY);
      glEnd();
      //ParentDC.gr.drawLine((int) xloc, ScreenMinY, (int) xloc, ScreenMaxY);
    }

    Alpha = 0.75;
    glLineWidth(1.0);
    //ParentDC.gr.setColor(Globals.ToAlpha(Color.darkGray, 100));// draw major horizontal pitch lines
    glColor4f(0.3, 0.3, 0.3, Alpha);
    for (double ycnt = MinY; ycnt < MaxY; ycnt++) {// octave lines
      yloc = GlobalTransform->UnMapPitch(ycnt);
      glBegin(GL_LINES);
      glVertex2d(ScreenMinX, yloc); glVertex2d(ScreenMaxX, yloc);
      glEnd();
      //ParentDC.gr.drawLine(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc);
    }

    //ParentDC.gr.setColor(Globals.ToAlpha(Color.darkGray, 100));// draw major vertical time lines
    glColor4f(0.3, 0.3, 0.3, Alpha);
    for (double xcnt = MinX; xcnt < MaxX; xcnt++) {// 1 second time lines
      xloc = GlobalTransform->UnMapTime(xcnt);
      glBegin(GL_LINES);
      glVertex2d(xloc, ScreenMinY); glVertex2d(xloc, ScreenMaxY);
      glEnd();
      //ParentDC.gr.drawLine((int) xloc, ScreenMinY, (int) xloc, ScreenMaxY);
    }

    // draw origin lines
    Alpha = 1.0;
    //ParentDC.gr.setColor(Globals.ToAlpha(Color.red, 255));
    glColor4f(1.0, 0.0, 0.0, Alpha);
    XOrigin = GlobalTransform->UnMapTime(0.0);
    glBegin(GL_LINES);
    glVertex2d(XOrigin, ScreenMinY); glVertex2d(XOrigin, ScreenMaxY);
    glEnd();
    //ParentDC.gr.drawLine(XOrigin, ScreenMinY, XOrigin, ScreenMaxY);

    YOrigin = GlobalTransform->UnMapPitch(0.0);
    glBegin(GL_LINES);
    glVertex2d(ScreenMinX, YOrigin); glVertex2d(ScreenMaxX, YOrigin);
    glEnd();
    //ParentDC.gr.drawLine(ScreenMinX, YOrigin, ScreenMaxX, YOrigin);
  }
  bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override { return false; }
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {}// Look for mouse clicks on me or my children. to do: replace previous GoFishing
};
/* ********************************************************************************* */
class GrArtistFactory : public GraphicBox::ArtistFactory { //GraphicBox::ArtistFactory { // single-instance for all of this type of songlet
public:
  GrArtist* SpawnArtist() override { return new GrArtist(); }
};

// Work in progress - this is getting ugly
/* ********************************************************************************* */
class OffsetBoxArtist : public OffsetBoxBase::Artist {
public:
  OffsetBoxArtist() { }
  OffsetBoxArtist(OffsetBoxBase *MyOBox) {
    this->MyOBox = MyOBox;
  }
  void Draw_Me(DrawingContext& ParentDC) override {
    if (ParentDC.ClipBounds.Intersects(MyOBox->MyBounds)) {
      //GlDrawingContext *ChildDC = new GlDrawingContext(ParentDC, *MyOBox);// map to child (my) internal coordinates
      DrawingContext *ChildDC = ParentDC.Compound_Clone(*MyOBox);// map to child (my) internal coordinates
      this->MyOBox->Print_Me("OffsetBoxArtist:");
      //ChildDC->MyOBox->Print_Me("ChildDC:");
      this->MyOBox->GetContent()->Draw_Me(*ChildDC);
      delete ChildDC;
    }
  }
  bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override { return false; }
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {}// Look for mouse clicks on me or my children. to do: replace previous GoFishing
};
class OffsetBoxArtistFactory : public OffsetBoxBase::ArtistFactory { // single-instance for all of this type of songlet
public:
  OffsetBoxArtist* SpawnArtist() override {
    return new OffsetBoxArtist();
  }
};

void Init_Artists(){
  OffsetBoxBase::ArtistMaker = new OffsetBoxArtistFactory();
  GraphicBox::ArtistMaker = new GrArtistFactory();
}
void Delete_Artists(){
  std::cout << "Delete_Artists start" << "\n";
  delete GraphicBox::ArtistMaker; GraphicBox::ArtistMaker = nullptr;
  delete OffsetBoxBase::ArtistMaker; OffsetBoxBase::ArtistMaker = nullptr;
  std::cout << "Delete_Artists end" << "\n";
}

Object* DelNull(Object *obj){
  delete obj;
  return nullptr;
}

#endif // Artists_hpp
