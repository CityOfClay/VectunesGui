#ifndef Line2D_hpp
#define Line2D_hpp

#include "Globals.hpp"
#include "IDeletable.hpp"
#include "Point2D.hpp"

/* ********************************************************************************* */
class Line2D {
private:
  Point2D pnt[2];
  Point2D delta;// delta X, delta Y
public:
  Line2D() {}
  Line2D(Point2D& p0, Point2D& p1) {
    this->Assign(p0, p1);
  }
  void Assign(SoundFloat X0, SoundFloat Y0, SoundFloat X1, SoundFloat Y1) {
    this->pnt[0].Assign(X0, Y0); this->pnt[1].Assign(X1, Y1);
    this->CompDelta();
  }
  void Assign(const Point2D& p0, const Point2D& p1) {
    this->pnt[0].Assign(p0); this->pnt[1].Assign(p1);
    this->CompDelta();
  }
  void CompDelta() {
    this->delta.Assign(this->pnt[1].X - this->pnt[0].X, this->pnt[1].Y - this->pnt[0].Y);
  }
  void FractAlong(SoundFloat Fract, Point2D& results) {
    SoundFloat dx = this->delta.X * Fract;
    SoundFloat dy = this->delta.Y * Fract;
    results.X = this->pnt[0].X + dx;
    results.Y = this->pnt[0].Y + dy;
    //System.out.println("Fract:" + Fract + " dx:" + this->delta.X + " dy:" + this->delta.Y);
    //System.out.println("Fract:" + Fract + " dx:" + dx + " dy:" + dy);
  }
};

#endif // Line2D_hpp
