#ifndef GoLive_hpp
#define GoLive_hpp

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <stdalign.h>
#include <cstring>
#include <vector>
#include <thread> // std::thread  TO DO: Change this to pthreads for cross-platform compatibility.
#include <sstream>
#include <mutex>
#include "AudProject.hpp"

/*
GoLive handles automatic audio play in response to user edits.
*/

/* ********************************************************************************* */
class GoLiveSync : IDeletable {
public:
  /* ********************************************************************************* */
  SingerBase* RootSinger = nullptr;
  bool Playing;// probably only need one of these booleans
  Wave wave_render;
  double Current_Amp_Factor = 0.1;//0.2;// Scale the amplitude down arbitrarily to avoid audio clipping.
  DefaultAudioFeed audfeed;
  /* ********************************************************************************* */
  GoLiveSync() {
    this->Playing = false;
  }
  /* ********************************************************************************* */
  virtual ~GoLiveSync() {
    this->Stop();// Halt any further playing.
  }
  /* ********************************************************************************* */
  bool Play_To(double NextTime) {// single-chunk render for non-threaded approach
    // Render the song in chunks and pass them to Feed iteratvely.
    // All UI edit actions should cause rendering and playing to stop.
    // this->NextTime = NextTime0;
    std::cout << "NextTime:" << NextTime << "\n";
    this->RootSinger->Render_To(NextTime, this->wave_render);
    this->wave_render.Amplify(Current_Amp_Factor);
    this->audfeed.Feed(this->wave_render);
    if (this->RootSinger->IsFinished) {// this belongs inside if anywhere.  RootSinger should be private.
      return false;
    }
    return true;
    //return (!this->RootSinger->IsFinished);
  }
  /* ********************************************************************************* */
//  void Start() {// Play the whole project.
//    double BeginTime = MyProject->AudioRoot->TimeX;// Root.TimeX really should be zero all the time.
//    this->Start(BeginTime);
//    std::cout << "Start Non Threaded!\n";
//  }
  /* ********************************************************************************* */
//  void Start(TimeFloat BeginTime) {// Play to the end from an arbitrary beginning.
//    this->Start(*(MyProject->GraphicRoot), BeginTime);
//    std::cout << "Start Non Threaded!\n";
//  }
//  /* ********************************************************************************* */
//  void Start(GraphicBox::Graphic_OffsetBox& obx, TimeFloat BeginTime) {
//    std::cout << "GoLiveSync Start Non Threaded!\n";
//
//    // Spawn RootSinger at start.
//    SingerBase* Singer = obx.Spawn_Singer();
//    Singer->Compound();
//
//    this->Start(Singer, BeginTime);
//  }
  /* ********************************************************************************* */
  void Start(SingerBase* Singer, TimeFloat BeginTime) {// Play to the end from an arbitrary beginning of a specific branch.
    this->RootSinger = Singer;
    // this->RootSinger->Compound();// do this inside or before the call?

    this->RootSinger->Start();
    this->RootSinger->Skip_To(BeginTime);

    this->Playing = true;
    this->audfeed.Start();
  }
  /* ********************************************************************************* */
  void Stop() {
    std::cout << "GoLiveSync.Stop() \n";
    this->audfeed.Stop();
    this->Playing = false;
    delete this->RootSinger; this->RootSinger = nullptr;
  }
  /* ********************************************************************************* */
  void Skip_To(double Time) {
  }
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& dc) {
    if (this->RootSinger!=nullptr) {
      printf("this->RootSinger->Draw_Me(dc);\n");
      this->RootSinger->Draw_Me(dc);
      if (false) {// DoLivePan
        CajaDelimitadora Box;
        this->RootSinger->GetSingerBounds(Box);
        dc.DrawRectangle(Box);
        // dc.DrawRectangle(*(this->RootSinger->GetBoundingBox()));
      }
    }
  }
};

#endif // GoLive_hpp

