#ifndef IDrawable_hpp
#define IDrawable_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "CajaDelimitadora.hpp"
#include "ICloneable.hpp"
#include "DrawingContext.hpp"
// #include "IGrabber.hpp"
#include "ITree.hpp"

/**
 *
 * @author MultiTool
 *
 * This IDrawable approach is dubious. We will have to think it through a lot more.
 */
class IGrabber;// forward
class IContainer;// forward

/* ********************************************************************************* */
class FishingStackContext: public Transformer {
public:
  int RecurseDepth = 0;
};

/* ********************************************************************************* */
class InventoryList: public ArrayList<IContainer*> {
public:
  int TimeStamp = 0;
};

/* ********************************************************************************* */
class IDrawable: public ICloneable, public ITree {
public:
  //class IMoveable;// forward
  int mytype = 0;
  CajaDelimitadora MyBounds;
  virtual ~IDrawable() {}
  /* ********************************************************************************* */
  virtual void Draw_Me(DrawingContext& ParentDC) {}
  /* ********************************************************************************* */
  virtual CajaDelimitadora* GetBoundingBox() {// IDrawable, not needed for monkeybox, fix this
    return &(this->MyBounds);
  }
  virtual void UpdateBoundingBoxLocal() {}
  virtual void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) {}// Look for mouse clicks on me or my children. to do: replace previous GoFishing
  virtual void GetInventory(InventoryList& Hallazgos) {}
  virtual void Burn_Scale(int TimeStamp, TimeFloat ReScaleX, SoundFloat ReScaleY) {}// Burn_Scale may be replaced with a generalized Burn_Transform()

  IDrawable* Clone_Me() override { return nullptr; };
  IDrawable* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; };
  /* ********************************************************************************* */
  void Copy_From(const IDrawable& donor) {
    this->MyBounds.Copy_From(donor.MyBounds);
  }
};

/* ********************************************************************************* */
class IMoveable: public IDrawable {// IMoveable is for things that can be selected, dragged, copied, pasted, deleted etc. through the UI.
public:
  boolean IsSelected = false;
  virtual ~IMoveable() {}
  virtual void MoveTo(TimeFloat XLoc, SoundFloat YLoc) {};
  virtual void MoveTo(const Point2D& Loc) {};
  virtual void MoveBy(TimeFloat XMove, SoundFloat YMove) {};
  virtual bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) { return false; };// click detection
  virtual SoundFloat GetX() { return Double::NEGATIVE_INFINITY; };
  virtual SoundFloat GetY() { return Double::NEGATIVE_INFINITY; };
  virtual void SetSelected(bool Selected) { this->IsSelected = Selected; }
  IMoveable* Clone_Me() override { return nullptr; };
  IMoveable* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; };
};

/* ********************************************************************************* */
class ArtistBase {
public:
  ArtistBase() { }
  virtual void Draw_Me(DrawingContext& ParentDC) {}
  virtual bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) { return false; }
  virtual void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) {}// Look for mouse clicks on me or my children. to do: replace previous GoFishing
};
/* ********************************************************************************* */
class ArtistFactoryBase { // single-instance for all of this type of songlet
public:
  virtual ArtistBase* SpawnArtist() { return new ArtistBase(); }
  virtual ~ArtistFactoryBase(){}
};

#endif
