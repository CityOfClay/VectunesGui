#ifndef Globals_hpp
#define Globals_hpp

// C++ stuff
#define _USE_MATH_DEFINES
#include <cmath>
#include <climits>
#include <limits>       // std::numeric_limits
#include <algorithm>    // std::min
#include <stdlib.h>     // srand, rand
#include <cstdlib> // RAND_MAX
#include <string>
#include <vector>
#include <map>
#include <string_view>
#include <chrono>
#include <thread>
#include <fstream>

#if true
 #define ldouble long double
#else
 #define ldouble double
#endif // false

#define TimeFloat long double

#if false
 #define SoundFloat long double
#else
 #define SoundFloat double
#endif // false

#if true // string_view is the right way, but it messes up debugging for some reason
  #define StringConst static inline const std::string
#else
  #define StringConst static constexpr std::string_view
#endif
//static constexpr auto PtrPrefix = "ptr:";// never do this, crashes with no log as to why

#ifdef _WIN32
  #define realpath(N, R) _fullpath((R),(N),_MAX_PATH)
#endif // _WIN32

#define Math_min(a, b) (std::min(a, b))
#define Math_max(a, b) (std::max(a, b))
#define Math_ceil(a) (std::ceil(a))
#define Math_abs(a) (std::abs(a))
#define Math_hypot(a, b) (std::hypot(a, b))

#define Integer_MIN_VALUE INT_MIN
#define Integer_MAX_VALUE INT_MAX

#define PIdef 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270L
#define PI2def (PIdef*2.0) //M_PI_2;

/**
 *
 * @author MultiTool
*/

#define PROFILE_START auto start = std::chrono::system_clock::now()
#define PROFILE_END(counter) counter += Globals::TimeDiff(start)

class Globals {
public:
  constexpr static long double Thousand = 1000.0;// For milliseconds
  constexpr static long double Million = 1000000.0;// For microseconds
  constexpr static long double Billion = 1000000000.0;// For nanoseconds
  const static int SampleRate = 44100;
  //const int SampleRate = 44100;
  const static int SampleRateTest = 100;
  static constexpr SoundFloat BaseFreqC0 = 16.3516;// hz
  static constexpr SoundFloat BaseFreqA0 = 27.5000;// hz
  static constexpr SoundFloat MiddleC4Freq = 261.626;// hz
  //static constexpr SoundFloat BaseFreqC0;// hz
  //const constexpr BaseFreqC0 = 16.3516;// hz
  //static SoundFloat BaseFreqA0;// = 27.5000;// hz
  //constexpr static SoundFloat usPerSec = 1000000.0;

  static constexpr auto ObjectTypeName = "ObjectTypeName";// for serialization

  // http://stackoverflow.com/questions/2777541/static-const-SoundFloat-in-c
  static SoundFloat BaseFreqA0_G() {
    return 27.5000;  // hz
  }
  static SoundFloat BaseFreqC0_G() {
    return 16.3516;  // hz
  }

  static constexpr long double Math_PI = PIdef;
  static constexpr long double TwoPi = PI2def; //M_PI_2; // hz
  static constexpr SoundFloat Fudge = 0.00000000001;

  class Random {
  public:
    static SoundFloat NextDouble() {
      double test = std::rand();
      return ((SoundFloat)test) / (SoundFloat)RAND_MAX;
    }
  };
  const static Random RandomGenerator();
  /* ********************************************************************************* */
  static double TimeDiff(std::chrono::system_clock::time_point &start) {
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    return elapsed_seconds.count();
  }
  /* ********************************************************************************* */
  static TimeFloat GetTimeInSecondsFloat() {// https://stackoverflow.com/questions/16177295/get-time-since-epoch-in-milliseconds-preferably-using-c11-chrono
    using namespace std::chrono;
    int64_t nanoseconds_since_epoch = system_clock::now().time_since_epoch() / nanoseconds(1);
    return ((TimeFloat)nanoseconds_since_epoch) / Billion;
  }
  /* ********************************************************************************* */
  //static std::chrono::duration<int64_t, std::chrono::nanoseconds> FloatToDuration(long double FloatTime) {
//  static std::chrono::duration<std::chrono::nanoseconds> FloatToDuration(long double FloatTime) {
//    using namespace std::chrono;
//    return nanoseconds((int64_t)(FloatTime * Billion));
//    // return DurationResult;
//  }
  // could not convert ‘std::chrono::duration<long int, std::ratio<1, 1000000000> >((int64_t)(FloatTime * (long double)Globals::Billion))’ from
  // ‘std::chrono::duration<long int, std::ratio<1, 1000000000> >’ to
  // ‘std::chrono::duration<std::chrono::duration<long int, std::ratio<1, 1000000000> > >’|

  // could not convert ‘std::chrono::duration<long int, std::ratio<1, 1000000000> >((int64_t)(FloatTime * (long double)Globals::Billion))’ from
  // ‘std::chrono::duration<long int, std::ratio<1, 1000000000> >’ to
  // ‘std::chrono::duration<long int, std::chrono::duration<long int, std::ratio<1, 1000000000> > >’|
  /* ********************************************************************************* */
  static void SaveTextFile(const std::string &FileName, const std::string &Contents){// http://www.cplusplus.com/doc/tutorial/files/
    std::ofstream myfile;
    myfile.open (FileName);
    myfile << Contents;
    myfile.close();
  }
  /* ********************************************************************************* */
  static double GraphicBox_Obx_Hits;
  static double GraphicBox_Hits;
  static double GroupSong_Obx_Hits;
  static double GroupSong_Hits;
  static double IDrawable_Obx_Hits;
  static double IDrawable_Hits;
  static double MonkeyBox_Obx_Hits;
  static double OffsetBoxBase_Hits;
  static double Voice_Hits;
  static double LoudnessHandle_Hits;
  static double VoicePoint_Hits;
  static double Dc_Compound_Clone_Hits;
};

double Globals::GraphicBox_Obx_Hits = 0.0;
double Globals::GraphicBox_Hits = 0.0;
double Globals::GroupSong_Obx_Hits = 0.0;
double Globals::GroupSong_Hits = 0.0;
double Globals::IDrawable_Obx_Hits = 0.0;
double Globals::IDrawable_Hits = 0.0;
double Globals::MonkeyBox_Obx_Hits = 0.0;
double Globals::OffsetBoxBase_Hits = 0.0;
double Globals::Voice_Hits = 0.0;
double Globals::LoudnessHandle_Hits = 0.0;
double Globals::VoicePoint_Hits = 0.0;
double Globals::Dc_Compound_Clone_Hits = 0.0;

/* ********************************************************************************* */
void Init_Profilers(){// redundant unless we want to profile a second time
  Globals::GraphicBox_Obx_Hits = 0.0;
  Globals::GraphicBox_Hits = 0.0;
  Globals::GroupSong_Obx_Hits = 0.0;
  Globals::GroupSong_Hits = 0.0;
  Globals::IDrawable_Obx_Hits = 0.0;
  Globals::IDrawable_Hits = 0.0;
  Globals::MonkeyBox_Obx_Hits = 0.0;
  Globals::OffsetBoxBase_Hits = 0.0;
  Globals::Voice_Hits = 0.0;
  Globals::LoudnessHandle_Hits = 0.0;
  Globals::VoicePoint_Hits = 0.0;
  Globals::Dc_Compound_Clone_Hits = 0.0;
}
/* ********************************************************************************* */
void Print_Profilers(){
  std::cout << "GraphicBox_Obx_Hits:" << Globals::GraphicBox_Obx_Hits << "\n";
  std::cout << "GraphicBox_Hits:" << Globals::GraphicBox_Hits << "\n";
  std::cout << "GroupSong_Obx_Hits:" << Globals::GroupSong_Obx_Hits << "\n";
  std::cout << "GroupSong_Hits:" << Globals::GroupSong_Hits << "\n";
  std::cout << "IDrawable_Obx_Hits:" << Globals::IDrawable_Obx_Hits << "\n";
  std::cout << "IDrawable_Hits:" << Globals::IDrawable_Hits << "\n";
  std::cout << "MonkeyBox_Obx_Hits:" << Globals::MonkeyBox_Obx_Hits << "\n";
  std::cout << "OffsetBoxBase_Hits:" << Globals::OffsetBoxBase_Hits << "\n";
  std::cout << "Voice_Hits:" << Globals::Voice_Hits << "\n";
  std::cout << "LoudnessHandle_Hits:" << Globals::LoudnessHandle_Hits << "\n";
  std::cout << "VoicePoint_Hits:" << Globals::VoicePoint_Hits << "\n";
  std::cout << "Dc_Compound_Clone_Hits:" << Globals::Dc_Compound_Clone_Hits<< "\n";
}
/* ********************************************************************************* */
void Print_Profilers_File(){
  std::string txt;
  txt += "GraphicBox_Obx_Hits:" + std::to_string(Globals::GraphicBox_Obx_Hits) + "\n";
  txt += "GroupSong_Obx_Hits:" + std::to_string(Globals::GroupSong_Obx_Hits) + "\n";
  txt += "GroupSong_Hits:" + std::to_string(Globals::GroupSong_Hits) + "\n";
  txt += "IDrawable_Obx_Hits:" + std::to_string(Globals::IDrawable_Obx_Hits) + "\n";
  txt += "IDrawable_Hits:" + std::to_string(Globals::IDrawable_Hits) + "\n";
  txt += "MonkeyBox_Obx_Hits:" + std::to_string(Globals::MonkeyBox_Obx_Hits) + "\n";
  txt += "OffsetBoxBase_Hits:" + std::to_string(Globals::OffsetBoxBase_Hits) + "\n";
  txt += "Voice_Hits:" + std::to_string(Globals::Voice_Hits) + "\n";
  txt += "LoudnessHandle_Hits:" + std::to_string(Globals::LoudnessHandle_Hits) + "\n";
  txt += "VoicePoint_Hits:" + std::to_string(Globals::VoicePoint_Hits) + "\n";
  txt += "Dc_Compound_Clone_Hits:" + std::to_string(Globals::Dc_Compound_Clone_Hits) + "\n";
  Globals::SaveTextFile("AudLog.txt", txt);
}
/* ********************************************************************************* */
struct Example {
  static SoundFloat usPerSec() {
    return 1000000.0;
  }
};

#endif
