#ifndef Splines_hpp
#define Splines_hpp

#include <vector>
#include "Globals.hpp"
#include "CajaDelimitadora.hpp"
#include "Point2D.hpp"
#include "Line2D.hpp"

/**
 *
 * @author MultiTool
 */
class Splines {
public:
//  static constexpr SoundFloat Sin90 = Math::sin(Math::PI / 2.0);// 90 degrees
//  static SoundFloat Cos90 = Math::cos(Math::PI / 2.0);// 90 degrees
//  static SoundFloat Sin270 = Math::sin(Math::PI * 3.0 / 2.0);// 270 degrees
//  static SoundFloat Cos270 = Math::cos(Math::PI * 3.0 / 2.0);// 270 degrees
  static const int NumSubLines = 10;
  static ArrayList<Point2D> GlobalSplinePoints;// = Generate_Static_Spline(NumSubLines);
  /* ********************************************************************************* */
  static void Cubic_Spline_Chunk(const Point2D& p0, const Point2D& p1, const Point2D& p2, const Point2D& p3, SoundFloat FractAlong, Point2D& result) {
    // B(t) = (1-t)^3*P0  +  3*(1-t)^2*t*P1  +  3*(1-t)*t^2*P2  +  t^3*P3
    SoundFloat MinusFract = 1.0 - FractAlong;
    SoundFloat Wgt0 = Math::pow(MinusFract, 3);// * P0;
    SoundFloat Wgt1 = 3.0 * FractAlong * Math::pow(MinusFract, 2);// * P1;
    SoundFloat Wgt2 = 3.0 * MinusFract * Math::pow(FractAlong, 2);// * P2;
    SoundFloat Wgt3 = Math::pow(FractAlong, 3);// * P3;

    SoundFloat XLoc = (p0.X * Wgt0) + (p1.X * Wgt1) + (p2.X * Wgt2) + (p3.X * Wgt3);
    SoundFloat YLoc = (p0.Y * Wgt0) + (p1.Y * Wgt1) + (p2.Y * Wgt2) + (p3.Y * Wgt3);
    result.Assign(XLoc, YLoc);
  }
  /* ********************************************************************************* */
  static void Cubic_Spline_Boxes(ArrayList<OffsetBoxBase*>& raw, int NumSubLines, ArrayList<Point2D>& SplinePoints) {// Splines that only go forward in time
    SoundFloat CtrlPntLength = 1.0 / 2.0;
    Point2D Prev(0, 0), Next(0, 0);
    Point2D CtrlPrev, CtrlNext;
    Point2D result;
    int len = raw.size();
    SoundFloat FractAlong;
    SoundFloat XDist;
    OffsetBoxBase *NowBox;

    int pcnt = 0;
    int rescnt = 0;
    while (pcnt < len) {
      Prev.Copy_From(Next);// rollover
      NowBox = raw.at(pcnt);
      NowBox->CopyTo_Point2D(Next);
      XDist = Next.X - Prev.X;

      CtrlPrev.Copy_From(Prev);
      CtrlPrev.X += (XDist * CtrlPntLength);

      CtrlNext.Copy_From(Next);
      CtrlNext.X -= (XDist * CtrlPntLength);

      for (int subdex = 0; subdex < NumSubLines; subdex++) {
        FractAlong = ((SoundFloat) subdex) / (SoundFloat) NumSubLines;
        Cubic_Spline_Chunk(Prev, CtrlPrev, CtrlNext, Next, FractAlong, result);
        SplinePoints[rescnt].Copy_From(result);
        rescnt++;
      }
      pcnt++;
    }
    SplinePoints[rescnt].Copy_From(Next);// pin last segment to final point
  }
  /* ********************************************************************************* */
  static ArrayList<Point2D> Generate_Static_Spline(int NumSubLines) {// Create and cache 1 spline connector array of points.
    ArrayList<Point2D> SplinePoints;
    Generate_Static_Spline(NumSubLines, SplinePoints);
    return SplinePoints;// snox is this going to delete all of its children and so be unusable outside this scope?
  }
  /* ********************************************************************************* */
  static void Generate_Static_Spline(int NumSubLines, ArrayList<Point2D>& SplinePoints) {// Create and cache 1 spline connector array of points.
    SoundFloat CtrlPntLength = 1.0 / 2.0;
    const double Width = 1.0;
    Point2D Prev(0, 0), Next(Width, Width), result; // using Width as height too.
    Point2D CtrlPrev, CtrlNext;
    SoundFloat FractAlong;
    SplinePoints.resize(NumSubLines);//+1);
    // The starting point is assumed to be 0,0 so we never have to include that one.
    // So the number of points in this array always equals the number of line segments.
    CtrlPrev.Copy_From(Prev); CtrlPrev.X += (Width * CtrlPntLength);
    CtrlNext.Copy_From(Next); CtrlNext.X -= (Width * CtrlPntLength);
    for (int subdex = 0; subdex < NumSubLines; subdex++) {
      FractAlong = ((SoundFloat) (subdex+1)) / (SoundFloat) NumSubLines;
      Cubic_Spline_Chunk(Prev, CtrlPrev, CtrlNext, Next, FractAlong, result);
      SplinePoints[subdex].Copy_From(result);
    }
    //SplinePoints[NumSubLines].Copy_From(Next);// pin last segment to final point
  }
  /* ********************************************************************************* */
  static bool CrossVert(const CajaDelimitadora& SearchBox, double XCross, double YCross, double PrevX, double CurrentX) {
    if (SearchBox.Min.Y <= YCross && YCross <= SearchBox.Max.Y) {// intercept crosses box vertical wall.
      if (PrevX <= XCross && XCross <= CurrentX) { return true; }// intercept is between line endpoints.
    }
    return false;
  }
  /* ********************************************************************************* */
  static bool CrossHoriz(const CajaDelimitadora& SearchBox, double XCross, double YCross, double PrevX, double CurrentX) {
    if (SearchBox.Min.X <= XCross && XCross <= SearchBox.Max.X) {// intercept crosses box horizontal wall.
      if (PrevX <= XCross && XCross <= CurrentX) { return true; }// intercept is between line endpoints.
    }
    return false;
  }
  /* ********************************************************************************* */
  static bool BoxHitOneLine(const CajaDelimitadora& SearchBox, const Point2D& Prev, const Point2D& Current) {// A pretty horrible box-line collision detector.
    if (SearchBox.Contains(Current)){ return true; } // use this?  If an endpoint is in the box then we are done.
    // Large exclusion zones.
    if (Current.X<SearchBox.Min.X && Prev.X<SearchBox.Min.X){ return false; }
    if (Current.X>SearchBox.Max.X && Prev.X>SearchBox.Max.X){ return false; }
    if (Current.Y<SearchBox.Min.Y && Prev.Y<SearchBox.Min.Y){ return false; }
    if (Current.Y>SearchBox.Max.Y && Prev.Y>SearchBox.Max.Y){ return false; }

    // Prev.X is always assumed to be less than or equal to Current.X.
    double LineDx = Current.X - Prev.X;
    double LineDy = Current.Y - Prev.Y;
    double LineSlope = LineDy/LineDx;
    double LineBase = Prev.Y - (LineSlope * Prev.X);

    // Test for any line intersection, and if found return true.
    // 2 vertical lines.
    double CrossLeftY = (LineSlope*SearchBox.Min.X)+LineBase;
    if (CrossVert(SearchBox, SearchBox.Min.X, CrossLeftY, Prev.X, Current.X)) { return true; }
    double CrossRightY = (LineSlope*SearchBox.Max.X)+LineBase;
    if (CrossVert(SearchBox, SearchBox.Max.X, CrossRightY, Prev.X, Current.X)) { return true; }

    // 2 horizontal lines.
    double CrossBottomX = (SearchBox.Min.Y - LineBase) / LineSlope;
    if (CrossHoriz(SearchBox, CrossBottomX, SearchBox.Min.Y, Prev.X, Current.X)) { return true; }
    double CrossTopX = (SearchBox.Max.Y - LineBase) / LineSlope;
    if (CrossHoriz(SearchBox, CrossTopX, SearchBox.Max.Y, Prev.X, Current.X)) { return true; }
    return false;
  }
  /* ********************************************************************************* */
  static bool BoxHit(const CajaDelimitadora& SearchBox, const ArrayList<Point2D>& SplineBridge) {
    int len = SplineBridge.size();// iterative search
    Point2D Prev(0, 0), Current(0, 0);
    if (SearchBox.Contains(Prev)){ return true; }// if any endpoint is in box.
    int cnt;// Run up to search area with minimal comparisons.
    for (cnt=0; cnt<len; cnt++) {
      Current = SplineBridge.at(cnt);
      if (SearchBox.Min.X <= Current.X) { break; }
      Prev = Current;
    }
    while (cnt<len) {
      Current = SplineBridge.at(cnt);// One redundant dereference but it should work.
      // if (SearchBox.Contains(Current)){ return true; } // Redudant with call in BoxHitOneLine.
      if (Splines::BoxHitOneLine(SearchBox, Prev, Current)) { return true; }
      if (SearchBox.Max.X < Current.X) { return false; }// we have left the search box.
      Prev = Current;
      cnt++;
    }
    return false;
  }
  /* ********************************************************************************* */
  static bool LineLine(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
    // calculate the direction of the lines.  from http://www.jeffreythompson.org/collision-detection/line-rect.php
    float uA = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
    float uB = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
    //  boolean left = lineLine(x1,y1,x2,y2, rx,ry,rx, ry+rh);
    // if uA and uB are between 0-1, lines are colliding
    if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
      // optionally, draw a circle where the lines meet
//      float intersectionX = x1 + (uA * (x2-x1));
//      float intersectionY = y1 + (uA * (y2-y1));
//       fill(255,0,0); noStroke(); ellipse(intersectionX, intersectionY, 20, 20);
      return true;
    }
    return false;
  }
  /* ********************************************************************************* */
  static bool Collides(double MinX, double MinY, double MaxX, double MaxY, double x, double y, double vx, double vy) {
    // Liang–Barsky algorithm stolen from https://github.com/ChickenProp/breakpitch/blob/master/src/MyParticle.hx
    // Example call: // bool hits = Collides(SearchBox.Min.X, SearchBox.Min.Y, SearchBox.Max.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X-Prev.X, Current.Y-Prev.Y);
		double LineDelta[4] = {-vx, vx, -vy, vy};
		double q[4] = {x - MinX, MaxX - x, y - MinY, MaxY - y};
		double u1 = Double::NEGATIVE_INFINITY;
		double u2 = Double::POSITIVE_INFINITY;

		for (int i=0; i<4; i++) {
			if (LineDelta[i] == 0) {// Line is vertical or horizontal.
				if (q[i] < 0) {
					return false;
				}
			} else {
				double t = q[i] / LineDelta[i];
				if (LineDelta[i] < 0 && u1 < t) {
					u1 = t;
				} else if (LineDelta[i] > 0 && u2 > t) {
					u2 = t;
				}
			}
		}

		if (u1 > u2 || u1 > 1 || u1 < 0) {
			return false;
		}
//    Point2D collision;
//		collision.X = x + u1*vx;
//		collision.Y = y + u1*vy;

		return true;
	}
  /* ********************************************************************************* */
  static bool CrossOrtho(double WallX0, double WallY0, double WallX1, double WallY1,
    double LineX0, double LineY0, double LineX1, double LineY1) {// Assumes 'Wall' is vertical or horizontal.

    double LineDx = LineX1 - LineX0;// Delete CrossOrtho, it was never any good.
    double LineDy = LineY1 - LineY0;
    double LineSlope = LineDy/LineDx;
    double LineBase = LineY0 - (LineSlope * LineX0);

    double XCross = WallX0;
    double YCross = (LineSlope*WallX0)+LineBase;

    if (WallY0 <= YCross && YCross <= WallY1) {// intercept crosses box vertical wall.
      if (LineX0 <= XCross && XCross <= LineX1) { return true; }// intercept is between line endpoints.
    }
    return false;
  }
};

//Splines::Generate_Static_Spline(Splines::NumSubLines);

// Splines::GlobalSplinePoints = Splines::Generate_Static_Spline(Splines::NumSubLines);

//ArrayList<Point2D> Splines::Generate_Static_Spline(int NumSubLines);// Create and cache 1 spline connector array of points.

//CirclePoints GlDrawingContext::circlepoints;

/*
 real cubic spline:
 B(t) = (1-t)^3*P0  +  3*(1-t)^2*P1  +  3*(1-t)*t^2*P2  +  t^3*P3

 quadratic:
 B(t) = (1-t)^2*P0  +  2*(1-t)*t*P1  +  t^2*P2

 */


#endif
