#include <iostream>

#ifndef Audio_hpp
#define Audio_hpp

#include <stdio.h>
#include <math.h>
#include <ctime>
#include <cstdlib>
#include <atomic>
#include "Wave.hpp"

//#ifdef PortAudio // define this directive in compiler script
#if __has_include("portaudio.h") // https://en.cppreference.com/w/cpp/preprocessor/include
  #include "portaudio.h"
#else
  #undef PortAudio // If no port audio is available, build without it.
#endif // __has_include
//#endif // PortAudio

// #define PORTAUDIO_H
#ifdef PORTAUDIO_H
#else
#endif // PORTAUDIO_H

using namespace std;

// to do: put base class in own file, anybody can use that as a dummy.
// only pa enabled builds know about PortAudioFeed.

/* ********************************************************************************* */
class AudioFeed {
public:
  virtual void Start(){}
  virtual void Stop(){}
  virtual void Feed(const Wave &wave){}
};

#ifdef PortAudio // define this directive in compiler script
/* ********************************************************************************* */
class PortAudioFeed : public AudioFeed {// // PortAudio callback with ring buffer, that can be fed wave sample vectors of any length.
public:
  /*******************************************************************/
  static const int Num_Channels = 1;
  static const int SampleRate = 44100;
  static const int ChunksPerSecond = 50;//20;//100;//60;// 2 // 10
  static const int ChunkSize = SampleRate/ChunksPerSecond;// 100;
  static const int NumRingChunks = ChunksPerSecond;//20;//11;//3;//2;//5;//7;//11;
//  static const int ChunkSize = 44100/100;// snox this buf size is very wasteful, 1/100 of a second.
//  static const int NumRingChunks = 10*10;// snox this buf size is very wasteful, 1/100 of a second.
  static const int RingBufSz = ChunkSize * Num_Channels * NumRingChunks;// RingBufSz is 1 second long.
  float RingBuf[RingBufSz][Num_Channels];
  std::atomic_int RingGetDex, RingPutDex; // rough-grained indexes, always mod chunksize == 0.
  std::atomic_int RingGetDexPrev, RingPutDexNext;// fine-grained put index.
  std::atomic_bool FailFlag;

  int WavNext;
  bool KeepGoing;

  PaStream *OutStream;
  PaError OutErr;

  /* This routine will be called by the PortAudio engine when audio is needed.
  ** It may called at interrupt level on some machines so don't do anything
  ** that could mess up the system like calling malloc() or free().
  */

  /*******************************************************************/
  static int PaCallback(const void *InBuf, void *OutBuf, unsigned long FramesPerBuffer, const PaStreamCallbackTimeInfo* TimeInfo, PaStreamCallbackFlags StatusFlags, void *UserData) {
    (void) TimeInfo, StatusFlags, InBuf;/* Prevent unused variable warnings. */
    int RingGetDexNext;

    PortAudioFeed *self = static_cast<PortAudioFeed*>(UserData);
    float *OutBufFloat = (float*)OutBuf;

    std::stringstream Bruce; Bruce << "PaCallback" << ", KeepGoing:" << self->KeepGoing << ", FramesPerBuffer:" << FramesPerBuffer << ", StatusFlags:" << StatusFlags << "\n"; std::cout << Bruce.str();

    if ((StatusFlags & paOutputUnderflow) > 0LLU) {
      self->FailFlag = true;
      std::cout << "std::cout BUFFER UNDERRUN! \n";
      printf(" PaCallback BUFFER UNDERRUN!  \n");
      // exit(1);
      return paContinue;
    }

    while (self->RingGetDex == self->RingPutDex) {// stall
      if (!self->KeepGoing) {
        std::cout << "PaCallback abort\n" << std::flush;
        printf("PaCallback abort printf\n");
        return paComplete;
      }
    }

    std::stringstream Bruce2; Bruce2 << "PLAYING:" << self->WavNext << "\n"; std::cout << Bruce2.str();
    std::memcpy(OutBufFloat, (self->RingBuf[self->RingGetDex]), ChunkSize*Num_Channels*sizeof(float));

    self->RingGetDexPrev = self->RingGetDex.load();
    RingGetDexNext = self->RingGetDex + ChunkSize;// Increase by chunk size.

    //RingGetDexNext = self->RingGetDexPrev + ChunkSize;// Increase by chunk size.
    //std::stringstream Bruce2; Bruce2 << "PLAYING:" << self->WavNext << "\n"; std::cout << Bruce2.str();
    //std::memcpy(OutBufFloat, (self->RingBuf[self->RingGetDexPrev]), ChunkSize*Num_Channels*sizeof(float));

    if ((RingGetDexNext) >= RingBufSz){RingGetDexNext=0;}// wrap
    self->RingGetDex = RingGetDexNext;
    return paContinue;
  }
  /*******************************************************************/
  static void StreamFinished(void* UserData) {/* This routine is called by portaudio when playback is done. */
    PortAudioFeed *self = (PortAudioFeed*)UserData;
    self->KeepGoing = false;
    printf("Stream Completed: %d\n", self->KeepGoing);
  }
  /*******************************************************************/
  void Start() override {
    signed long writeable = 0;
    this->Init();

    PaStreamParameters OutputParameters;

    OutErr = Pa_Initialize();
    if( OutErr != paNoError ) { ReportError(OutErr); return; }

    OutputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (OutputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      ReportError(OutErr); return;
    }
    OutputParameters.channelCount = Num_Channels; /* stereo output */
    OutputParameters.sampleFormat = paFloat32;    /* 32 bit floating point output */
    OutputParameters.suggestedLatency = Pa_GetDeviceInfo( OutputParameters.device )->defaultLowOutputLatency;
    OutputParameters.hostApiSpecificStreamInfo = NULL;

    OutErr = Pa_OpenStream(
            &OutStream, NULL, /* Output, no input */
            &OutputParameters, SampleRate, ChunkSize,
            paClipOff,      /* we won't output out of range samples so don't bother clipping them */
            PaCallback, this);

    if( OutErr != paNoError ) { ReportError(OutErr); return; }

    writeable = Pa_GetStreamWriteAvailable(OutStream);
    printf("writeable:%ld\n", writeable);

    OutErr = Pa_SetStreamFinishedCallback(OutStream, &StreamFinished);
    if(OutErr != paNoError) { ReportError(OutErr); return; }

    OutErr = Pa_StartStream(OutStream);
    if(OutErr != paNoError) { ReportError(OutErr); return; }
  }
  static int DrainCnt;// snox this only exists to find a bug.
  /*******************************************************************/
  void Init() {
    this->KeepGoing = true;
    this->WavNext = ChunkSize;
    this->RingPutDex = 0; // rough-grained indexes, always mod chunksize == 0.
    this->RingGetDex = 0; // rough-grained indexes, always mod chunksize == 0.
    this->RingGetDexPrev = 0;// rough-grained dragging index.
    this->RingPutDexNext = 0;// fine-grained put index.
    for (int SampleCnt=0;SampleCnt<RingBufSz; SampleCnt++) {
      for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
        this->RingBuf[SampleCnt][chcnt] = 0.0;
      }
    }
    DrainCnt = 0;
    this->FailFlag = false;
    std::cout << "Init() \n";
  }
  /*******************************************************************/
  void Stop() override {
    this->Drain();
    this->UnInit();
    PaError err;
    err = Pa_StopStream(OutStream);
    if (err != paNoError) {
      printf("Pa_StopStream error! %d\n", err);
      exit(1);
    }
    err = Pa_Terminate();
    if (err != paNoError) {
      printf("Pa_Terminate error! %d\n", err);
      exit(1);
    }
  }
  /*******************************************************************/
  void UnInit() {
    this->KeepGoing = false;
    std::cout << "UnInit() " << ", KeepGoing:" << this->KeepGoing << "\n";
  }
  /*******************************************************************/
  void Feed(const Wave &wave) override {// Think of this as a loop.  Integrates ring buffer and odd-sized wave vectors.
    // At this point WavNext should be the offset from the beginning of the array that we run up *to*
    // WavNext *can* be beyond the end of the next array, if next array length is < ChunkSize.
    double value;
    int RingCnt = this->RingPutDexNext;// A dereference for RingPutDexNext.
    int WavPrev = 0, WavDex;
    int WavSize = wave.wave.size();// Only need to dereference wave size once.
    std::cout << "WavSize:"<< WavSize <<" \n";
    while (this->WavNext <= WavSize) {
      for (WavDex = WavPrev; WavDex<this->WavNext; WavDex++) {// copy chunk, or chunklet inherited from previous call.
        value = wave.wave[WavDex];
        for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
          this->RingBuf[RingCnt][chcnt] = value;// Copy one channel to all.
        }
        RingCnt++;
//        if (this->FailFlag) {
//          printf("FailFlag\n");
//        }
      }
      WavPrev = this->WavNext;
      this->WavNext += ChunkSize;
      if (RingCnt >= RingBufSz) { RingCnt = 0; } // wrap
      //while (RingCnt == this->RingGetDex){// Stall until player is ready.
      while (RingCnt == this->RingGetDexPrev){// Stall until player is ready.
        // to do: put timeout condition to prevent lockup?
//        if (this->FailFlag) {
//          printf("FailFlag\n");
//        }
      }
//      if (this->FailFlag) {
//        printf("FailFlag\n");
//      }
      this->RingPutDex = RingCnt;
    }
    for (WavDex = WavPrev; WavDex<WavSize; WavDex++) {// copy remainder, last bit of array, which is less than ChunkSize long.
      value = wave.wave[WavDex];
      for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
        this->RingBuf[RingCnt][chcnt] = value;
//        if (this->FailFlag) {
//          printf("FailFlag\n");
//        }
      }
//      if (this->FailFlag) {
//        printf("FailFlag\n");
//      }
      RingCnt++;
    }
    this->RingPutDexNext = RingCnt;
    this->WavNext -= WavSize; // After end of remainder, set up index for next call to Feed().
  }
  /*******************************************************************/
  void Drain() {// Close out last bit of odd-sized remainder.
    printf("Drain.............\n");
    DrainCnt++;
    if (DrainCnt>1) {
      printf("DrainCnt:%d\n", DrainCnt);
    }
    if (this->WavNext < ChunkSize) {
      int End = this->RingPutDexNext + this->WavNext;
      for (int rcnt=this->RingPutDexNext; rcnt<End; rcnt++) {
        for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
          this->RingBuf[rcnt][chcnt] = 0.0;// Pad with zeros.
        }
      }
      if (End >= RingBufSz) { End=0; }// wrap
      while (this->RingGetDex == End) {// stall.  RingPutDex is not allowed to overtake RingGetDex.
        // to do: put timeout condition to prevent lockup?
      }
      this->RingPutDex = End;// This frees RingGetDex to read what we just wrote to the ring buf.
      // usleep(1 * 100 * 1000); // to do: somehow block until the callback finishes playing out what we just put in the buffer.
//      while (this->RingGetDex != End) {// stall until player catchs up.
//        printf("this->RingGetDex:%d, End:%d\n", this->RingGetDex, End);
//      }
      this->WavNext = ChunkSize;
    }
    // Pa_Terminate();
  }
  /*******************************************************************/
  void ReportError(PaError OutErr) {
    fprintf(stderr, "An error occured while using the portaudio stream\n" );
    fprintf(stderr, "Error number: %d\n", OutErr);
    fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(OutErr) );
    if (OutErr == paUnanticipatedHostError) {// Print more information about the error.
      const PaHostErrorInfo *hostErrorInfo = Pa_GetLastHostErrorInfo();
      fprintf( stderr, "Host API error = #%ld, hostApiType = %d\n", hostErrorInfo->errorCode, hostErrorInfo->hostApiType );
      fprintf( stderr, "Host API error = %s\n", hostErrorInfo->errorText );
    }
    Pa_Terminate();
    exit(EXIT_FAILURE);
  }
};
int PortAudioFeed::DrainCnt = 0.0;
#endif // PortAudio

/*******************************************************************/
#ifdef PortAudio // define this directive in compiler script
class DefaultAudioFeed : public PortAudioFeed { };
#else
class DefaultAudioFeed : public AudioFeed { };
#endif // PortAudio

#ifdef PortAudio // define this directive in compiler script
/*******************************************************************/
void TestFeeder_Pa() {// Test function, use in main. All this does is stream a sine wave to audio feed.
  int now = std::time(nullptr);
  std::cout << "now:" << now << "\n";
  std::srand(now);
  Wave wav;
  int SampleCnt = 0;
  float value;
  float TwoPi = M_PI * 2.0;
  float fraction = 440.0 / (float)(PortAudioFeed::SampleRate);// frequency (A440) over sample rate
  fraction *= TwoPi;
  int NumSamples;
  int MinSize = 100;
  int RandRange = 100;//7;//3;//
  int NumWaves = 1000;//11;//5;//9;//7;//5;//3;//
  value = 0;
  std::cout << "TestFeeder_Pa(); start ******************************************************************************************************\n";
  PortAudioFeed hilo;
  hilo.Start();// threaded
  for (int wcnt=0; wcnt<NumWaves; wcnt++) {
    int random_variable = MinSize + (std::rand() % RandRange);// get random number here
    NumSamples = random_variable;
    printf("Main NumSamples:%d\n", NumSamples);
    for (int scnt=0;scnt<NumSamples;scnt++) {
      value = sin(((float)SampleCnt) * fraction);
      wav.wave.push_back(value);
      SampleCnt++;
    }
    //usleep(1 * 100 * 1000);// to create buffer underflow
    hilo.Feed(wav);
    wav.wave.clear();
  }
  hilo.Stop();// threaded
  std::cout << "IN SampleCnt:" << SampleCnt << "\n";
  printf("TestFeeder_Pa(); done.\n");
  // usleep(1 * 100 * 1000);// see if this is needed to drain.
}
#endif // PortAudio

#endif // Audio_hpp
