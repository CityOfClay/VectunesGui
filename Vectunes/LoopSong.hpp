#ifndef LoopSong_hpp
#define LoopSong_hpp

#include "GroupSong.hpp"

/**
 *
 * @author MultiTool
   WORK IN PROGRESS - SOMEWHAT FUNCTIONAL
*/

/* ********************************************************************************* */
class LoopCell_OffsetBox_Base: public GroupSong::Group_OffsetBox {// used like a forward
public:
  int MyIndex;
  LoopCell_OffsetBox_Base(GroupSong *songlet) : GroupSong::Group_OffsetBox(songlet) { }// LoopCellSong *songlet
};

/* ********************************************************************************* */
class LoopSong_Base: public GroupSong {// used like a forward
public:
  virtual void Update_Rhythm(LoopCell_OffsetBox_Base& mbox) = 0;
  virtual void Update_OctaveY(SoundFloat OctaveY) = 0;
};

/*
LoopCellSong is the one and only immediate child of LoopSong.
LoopSong points to many dummy handles, but all the handles point back to the one LoopCellSong.

LoopCellSong is used as a special spacer, so that when the user moves 1 LoopCellSong handle, it calls back
to the LoopSong parent to move all sibling dummy handles proportionately and keep the rythm.
The actual musical content handle is pointed to by the one LoopCellSong.
*/
class LoopCellSong: public GroupSong {// to do: RythmCellSong
public:
  /* ********************************************************************************* */
  class LoopCell_OffsetBox: public LoopCell_OffsetBox_Base {
  public:
    LoopSong_Base *ParentLoop = nullptr;
    LoopCellSong *LoopCellContentSonglet = nullptr;
    StringConst ObjectTypeName = {"LoopCell_OffsetBox"};// for serialization, which we never do with this class.
    /* ********************************************************************************* */
    LoopCell_OffsetBox(LoopCellSong *songlet) : LoopCell_OffsetBox_Base(songlet) {
      this->LoopCellContentSonglet = songlet;
    }
    /* ********************************************************************************* */
//    virtual ~LoopCell_OffsetBox() {
//      if (this->LoopCellContentSonglet->UnRef_Songlet() <= 0) {// LoopCellSong is in stack memory, so don't delete it.
//        printf("~LoopCell_OffsetBox\n");
//        // this->LoopCellContentSonglet->Delete_Me();// Just delete its content and kids. snox kinda ugly this way, refactor somehow.
//        delete this->LoopCellContentSonglet;
//      }
//    }
    /* ********************************************************************************* */
    void MoveTo(TimeFloat XLoc, SoundFloat YLoc) override {
      if (XLoc <= 0.0) { XLoc = 0.0; }// don't go backward in time
      this->TimeX = XLoc;
      this->OctaveY = 0.0;//YLoc;
      this->ParentLoop->Update_Rhythm(*this);
    }
    /* ********************************************************************************* */
    GroupSong::Group_Singer* Spawn_Singer() override {// always always always override this
      GroupSong::Group_Singer *Singer = this->LoopCellContentSonglet->Spawn_Singer();// for render time
      Singer->MyOffsetBox = this;// Transfer all of this box's offsets to singer.
      return Singer;
    }
    /* ********************************************************************************* */
    LoopCellSong* GetContent() override {
      return this->LoopCellContentSonglet;
    }
    /* ********************************************************************************* */
    LoopCell_OffsetBox* Clone_Me() override {// ICloneable
      LoopCell_OffsetBox *child = this->GetContent()->Spawn_OffsetBox();
      child->Copy_From(*this);// Either put a throw here or make this pure virtual.
      return child;
    }
    /* ********************************************************************************* */
    LoopCell_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      LoopCellSong* DumFork = this->LoopCellContentSonglet->Deep_Clone_Me(HitTable);
      LoopCell_OffsetBox *child = DumFork->Spawn_OffsetBox();
      child->Copy_From(*this);
      return child;
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
      JsonParse::HashNode *SelfPackage = OffsetBoxBase::Export(HitTable);// ready for test?
      SelfPackage->AddSubPhrase(Globals::ObjectTypeName, PackStringField(LoopCell_OffsetBox::ObjectTypeName));
      return SelfPackage;
    }
  };
  /* ********************************************************************************* */
//  class LoopCell_OffsetBox_Zero: public LoopCell_OffsetBox {// First item in loop has different dragging behavior.
//  public:
//    /* ********************************************************************************* */
//    LoopCell_OffsetBox_Zero(LoopCellSong *songlet) : LoopCell_OffsetBox(songlet) {}
//    /* ********************************************************************************* */
//    LoopCell_OffsetBox_Zero* Clone_Me() override {// ICloneable
//      LoopCell_OffsetBox_Zero *child = this->GetContent()->Spawn_OffsetBox_Zero();
//      child->Copy_From(*this);// Either put a throw here or make this pure virtual.
//      return child;
//    }
//    /* ********************************************************************************* */
//    LoopCell_OffsetBox_Zero* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
//      LoopCellSong* DumFork = this->LoopCellContentSonglet->Deep_Clone_Me(HitTable);
//      LoopCell_OffsetBox_Zero *child = DumFork->Spawn_OffsetBox_Zero();
//      child->Copy_From(*this);
//      return child;
//    }
//    /* ********************************************************************************* */
//    void MoveTo(TimeFloat XLoc, SoundFloat YLoc) override {
////      if (XLoc < 0.0) { XLoc = 0.0; }// don't go backward in time
////      this->TimeX = XLoc; // this->TimeX = (XLoc <= 0.0) ? 0.0: XLoc;// don't go backward in time
//      this->OctaveY = YLoc;
//      // this->ParentLoop->Update_OctaveY(YLoc); //  Update all siblings' OctaveY values.
//    }
//  };
  /* ********************************************************************************* */
  class LoopCell_Singer: public GroupSong::Group_Singer {
  public:
    LoopCellSong *MyLoopCellSong;
    /* ********************************************************************************* */
    LoopCell_Singer(LoopCellSong *MyLoopCellSong0): GroupSong::Group_Singer(MyLoopCellSong0) {
      this->MyLoopCellSong = MyLoopCellSong0;
    }
    /* ********************************************************************************* */
    virtual ~LoopCell_Singer() {
    }
  };
  /* ********************************************************************************* */
  LoopSong_Base *GrandParentLoop = nullptr;// (LoopSong_Base *)12;//
  /* ********************************************************************************* */
  LoopCellSong() {
    this->mytype = 10;
    this->Create_Me();
  }
  /* ********************************************************************************* */
  LoopCellSong(LoopSong_Base *GrandParentLoop0) : LoopCellSong() {
    this->GrandParentLoop = GrandParentLoop0;
  }
  /* ********************************************************************************* */
  virtual ~LoopCellSong(){
    printf("~LoopCellSong\n");
    this->Delete_Me();
  }
  /* ********************************************************************************* */
  LoopCell_OffsetBox* Spawn_OffsetBox() override {// For compose time.
    LoopCell_OffsetBox *child = new LoopCell_OffsetBox(this);
    child->LoopCellContentSonglet = this;// redundant
    child->Assign_Content(this);// redundant
    child->ParentLoop = this->GrandParentLoop;
    this->Ref_Songlet();
    return child;
  }
  /* ********************************************************************************* */
//  LoopCell_OffsetBox_Zero* Spawn_OffsetBox_Zero() {//  For compose time. First item in sequence, has special needs.
//    LoopCell_OffsetBox_Zero *child = new LoopCell_OffsetBox_Zero(this);
//    this->Ref_Songlet();
//    // child->ContentSonglet = this;
//    // child->Assign_Content(this);// redundant
//    child->ParentLoop = this->GrandParentLoop;
//    return child;
//  }
  /* ********************************************************************************* */
  LoopCell_Singer* Spawn_Singer() override {// for render time
    LoopCell_Singer *singer = new LoopCell_Singer(this);
    if (this->MyProject!=nullptr){//to_do();// fix this bs.  we need to load sample rate at render time.
      singer->Set_Project(this->MyProject);// inherit project
    }
    return singer;
  }
  /* ********************************************************************************* */
  LoopCellSong* Clone_Me() override {// ICloneable
    LoopCellSong *clone = new LoopCellSong();
    clone->Copy_From(*this);
    return clone;
  }
  /* ********************************************************************************* */
  LoopCellSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    LoopCellSong *clone;
    clone = (LoopCellSong*)GroupSong::Deep_Clone_Me(HitTable);// another cast!
    return clone;
  }
  /* ********************************************************************************* */
  boolean Create_Me() override { return true; }
  void Delete_Me() override {
    printf("LoopCellSong Delete_Me;\n");
    GroupSong::Delete_Me();
    this->GrandParentLoop = nullptr;
  }
//  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
//  }
};// end of LoopCellSong

/* ********************************************************************************* */
/* ********************************************************************************* */

class LoopSong: public LoopSong_Base {
public:
/*
General plan:
Create a GroupSong where all children are the same songlet, spaced evenly in time.
Any update to the location of one child triggers a callback to keep them all evenly spaced/rhythmic.
LoopCellSong is the most rigorous approach but inherits too much interface junk to fill in.  Is it needed, can we strip it out?
*/
  /* ********************************************************************************* */
  class Loop_OffsetBox: public Group_OffsetBox {
  public:
    StringConst ObjectTypeName = "Loop_OffsetBox";
    LoopSong* LoopContent = nullptr;
    /* ********************************************************************************* */
    Loop_OffsetBox(LoopSong* songlet) : Group_OffsetBox(songlet) {
      this->LoopContent = songlet;
    }
    /* ********************************************************************************* */
    LoopSong* GetContent() override {
      return LoopContent;
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
      JsonParse::HashNode *SelfPackage = OffsetBoxBase::Export(HitTable);// ready for test?
      SelfPackage->AddSubPhrase(Globals::ObjectTypeName, ITextable::PackStringField(Loop_OffsetBox::ObjectTypeName));
      return SelfPackage;
    }
    /* ********************************************************************************* */
    Loop_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      LoopSong* LoopFork = this->LoopContent->Deep_Clone_Me(HitTable);
      Loop_OffsetBox *MyFork = LoopFork->Spawn_OffsetBox();
      MyFork->Copy_From(*this);
      return MyFork;
    }
    /* ********************************************************************************* */
//    Loop_OffsetBox* BreakFromHerd(CollisionLibrary& HitTable) override {// for compose time. detach from my songlet and attach to an identical but unlinked songlet
//      return this->Deep_Clone_Me(HitTable);// different approach. fork me and let the caller dispose of elder and reattach.// allows us to deprecate Attach_Songlet.
//    }
    /* ********************************************************************************* */
    void BreakFromHerd(CollisionLibrary& HitTable) override {// for compose time. detach from my songlet and attach to an identical but unlinked songlet
      LoopSong *clone = this->LoopContent->Deep_Clone_Me(HitTable);
      if (this->LoopContent->UnRef_Songlet() <= 0) {
        delete this->LoopContent;
      }
      this->Attach_Songlet(clone);
      this->LoopContent = clone;
    }
  };
  /* ********************************************************************************* */
  //LoopCellSong SingleSong;// The one child song that gets repeated, which in turn owns the custom song we are given.
  StringConst LoopSongName = "LoopSong";
  LoopCellSong *SingleSong;// = new LoopCellSong();// The one child song that gets repeated, which in turn owns the custom song we are given.
  SoundFloat Interval = 0.25;// time spacing
  SoundFloat FirstBeatTimeX = 0.0;
  int Beats = 0;
  // TimeFloat CntOffst = 0.0;//1.0;
  /* ********************************************************************************* */
  LoopSong(): LoopSong_Base() {
    this->mytype = 9;
    this->SingleSong = new LoopCellSong(this);
    this->SingleSong->GrandParentLoop = this;
    this->Create_Me();

    {// Make first beat special, as it cannot be moved horizontally.
//      this->Beats = 1;
//      this->SubSongs.resize(this->Beats);
//      this->Add_LoopCell_Link(this->SingleSong->Spawn_OffsetBox_Zero(), 0);
    }

  }
  /* ********************************************************************************* */
  virtual ~LoopSong(){
    // delete SingleSong;// SingleSong is deleted when its refcount runs out.
    this->Delete_Me();
  }
  /* ********************************************************************************* */
  LoopSong* Clone_Me() override {// ICloneable
    LoopSong *clone = new LoopSong();
    clone->Copy_From(*this);
    clone->Interval = this->Interval;
    clone->Beats = this->Beats;
    clone->FirstBeatTimeX = this->FirstBeatTimeX;
    return clone;
  }
  /* ********************************************************************************* */
//  LoopSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
//    LoopSong *clone;
//    clone = (LoopSong*)GroupSong::Deep_Clone_Me(HitTable);// another cast!
////    this->ParentLoop;
//    return clone;
//  }
  /* ********************************************************************************* */
  LoopSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    LoopSong *LoopClone;
    CollisionItem *ci = HitTable.GetItem(this);
    if (ci == nullptr) {
      LoopClone = this->Clone_Me();
      ci = HitTable.InsertUniqueInstance(this);
      ci->Hydrated = LoopClone;

      int len;
      OffsetBoxBase *SubSongHandle;

      // First copy the song cell handles.  Could we replace with with Set_Beats?
      LoopCellSong::LoopCell_OffsetBox *CellObox;
      len = this->SubSongs.size();//ArrayList<OffsetBoxBase*> CellSubSongs;
      LoopClone->SubSongs.resize(this->Beats);
      for (int CellNumber = 0; CellNumber < len; CellNumber++) {
        SubSongHandle = this->SubSongs.at(CellNumber);
        CellObox = LoopClone->SingleSong->Spawn_OffsetBox();
        // CellObox->ParentLoop = this;// redundant with SingleSong.Spawn_OffsetBox.
        Transformer::Copy_Transform(*(SubSongHandle), *CellObox);
        // LoopClone->Add_LoopCell_Link(CellObox, CellNumber);
        {
          CellObox->MyIndex = CellNumber;
          // CellObox->ParentLoop = LoopClone;// redundant
          LoopClone->SubSongs[CellNumber] = CellObox;
        }
      }

      // Finally copy the contents of the one cell.
      len = this->SingleSong->SubSongs.size();//ArrayList<OffsetBoxBase*> CellSubSongs;
      for (int cnt = 0; cnt < len; cnt++) {
        SubSongHandle = this->SingleSong->SubSongs.at(cnt);
        OffsetBoxBase *obox = SubSongHandle->Deep_Clone_Me(HitTable);// clone my *grandkids* and add them to me.
        LoopClone->Add_SubSong(obox);
      }
    } else {// pre exists
      LoopClone = (LoopSong*) ci->Hydrated;// another cast!
    }
    return LoopClone;
  }
  /* ********************************************************************************* */
//  LoopSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
//    LoopSong *LoopClone;
//    CollisionItem *ci = HitTable.GetItem(this);
//    if (ci == nullptr) {
//      LoopClone = this->Clone_Me();
//      ci = HitTable.InsertUniqueInstance(this);
//      ci->Hydrated = LoopClone;
//
//      int len;
//      OffsetBoxBase *SubSongHandle;
//
//      // Copy the contents of the one cell.
//      len = this->SingleSong->SubSongs.size();//ArrayList<OffsetBoxBase*> CellSubSongs;
//      for (int cnt = 0; cnt < len; cnt++) {
//        SubSongHandle = this->SingleSong->SubSongs.at(cnt);
//        OffsetBoxBase *obox = SubSongHandle->Deep_Clone_Me(HitTable);// clone my *grandkids* and add them to me.
//        LoopClone->Add_SubSong(obox);
//      }
//
//      // Copy the song cell handles.  Could we replace with with Set_Beats?
//      LoopCellSong::LoopCell_OffsetBox *CellObox;
//      len = this->SubSongs.size();//ArrayList<OffsetBoxBase*> CellSubSongs;
//      LoopClone->SubSongs.resize(this->Beats);
//      for (int CellNumber = 0; CellNumber < len; CellNumber++) {
//        SubSongHandle = this->SubSongs.at(CellNumber);
//        CellObox = LoopClone->SingleSong->Spawn_OffsetBox();
//        // CellObox->ParentLoop = this;// redundant with SingleSong.Spawn_OffsetBox.
//        Transformer::Copy_Transform(*(SubSongHandle), *CellObox);
//        LoopClone->Add_LoopCell_Link(CellObox, CellNumber);
//      }
//
//    } else {// pre exists
//      LoopClone = (LoopSong*) ci->Hydrated;// another cast!
//    }
//    return LoopClone;
//  }
  /* ********************************************************************************* */
  void Set_Interval(SoundFloat Interval0){
    this->Interval = Interval0;
    OffsetBoxBase *obox;
    int NumKids = this->SubSongs.size();
    if (NumKids==0) { return; }
    for (int CellNum=1;CellNum<NumKids;CellNum++){
      obox = this->SubSongs.at(CellNum);
      obox->TimeX = this->CalcCellTimeX(CellNum);
    }
  }
  /* ********************************************************************************* */
  void Set_Beats(int NumBeats) {
    LoopCellSong::LoopCell_OffsetBox *dsobox;
    OffsetBoxBase *RepeatHandle;// to do: use LoopCell_OffsetBox_Zero. and never allow NumBeats to be zero.
    int PrevSize = this->SubSongs.size();
    if (NumBeats>PrevSize){// add beats
      this->SubSongs.resize(NumBeats);
      for (int CellNum=PrevSize;CellNum<NumBeats;CellNum++) {
//        this->Add_LoopCell_Link(this->SingleSong->Spawn_OffsetBox(), CellNum);
        {
          LoopCellSong::LoopCell_OffsetBox* dsobox = this->SingleSong->Spawn_OffsetBox();
          dsobox->MyIndex = CellNum;
          dsobox->TimeX = this->CalcCellTimeX(CellNum);
          this->SubSongs[CellNum] = dsobox;
        }
      }
    } else if (NumBeats<PrevSize){// remove beats
      if (NumBeats <= 1) { NumBeats = 2; } // Never delete the first 2 beats. Need at least 2 beats to define an interval.
      //if (NumBeats <= 0) { NumBeats = 1; } // Never delete the first 2 beats. to do:fix bug that blocks us from displaying a 1-beat loop.
      for (int CellNum=NumBeats;CellNum<PrevSize;CellNum++){
        RepeatHandle = this->SubSongs.at(CellNum);
        delete RepeatHandle;
      }
      this->SubSongs.resize(NumBeats);
    }
    this->Beats = NumBeats;
    Refresh_Splines();
  }
  /* ********************************************************************************* */
  void Delta_Beats(int NumBeatsDelta) {
    this->Set_Beats(this->Beats + NumBeatsDelta);
  }
  /* ********************************************************************************* */
  SoundFloat CalcCellTimeX(int CellNum) {
    return this->FirstBeatTimeX + (this->Interval * ((SoundFloat)CellNum));
  }
  /* ********************************************************************************* */
  void Add_LoopCell_Link(LoopCellSong::LoopCell_OffsetBox* dsobox, int CellNum) {
    dsobox->MyIndex = CellNum;
    dsobox->TimeX = this->CalcCellTimeX(CellNum);
    dsobox->OctaveY = 0.0;// redundant
    // dsobox->ParentLoop = this;// redundant
    this->SubSongs[CellNum] = dsobox;
  }
  /* ********************************************************************************* */
  int Add_SubSong(OffsetBoxBase *obox) override {
    // When someone gives me an OffsetBox, I own it.  I must delete it.
    return this->SingleSong->Add_SubSong(obox);// It is my responsibility to delete the handle I was given.
  }
  /* ********************************************************************************* */
  void Wipe_SubSongs() override {
    this->SingleSong->Wipe_SubSongs();
  }
  /* ********************************************************************************* */
//  void Update_Rhythm(LoopCell_OffsetBox_Base& mbox) override {
//    int Index = mbox.MyIndex;// like Refresh_Me_From_Beneath but specific for LoopCell_OffsetBox.
//    this->Interval = mbox.TimeX / (((SoundFloat)Index) + CntOffst);
//    OffsetBoxBase *obox;// = this->SubSongs.at(Index);
//    for (int cnt=1;cnt<Index;cnt++) {// start at 1, don't adjust first beat.
//      obox = this->SubSongs.at(cnt);
//      //obox->OctaveY = mbox.OctaveY;
//      obox->TimeX = this->Interval * ((SoundFloat)cnt + CntOffst);
//    }
//    Index++;
//    for (int cnt=Index;cnt<this->SubSongs.size();cnt++) {
//      obox = this->SubSongs.at(cnt);
//      //obox->OctaveY = mbox.OctaveY;
//      obox->TimeX = this->Interval * ((SoundFloat)cnt + CntOffst);
//    }
//  }
  /* ********************************************************************************* */
  void Update_Rhythm(LoopCell_OffsetBox_Base& mbox) override {
    int HandleNum = mbox.MyIndex;
    int EndpointNum;
    TimeFloat HandleX = mbox.TimeX;
    int len = this->SubSongs.size();
    // TimeFloat LastTimeX = this->SubSongs.at(LastNum)->TimeX;// to do: optimize this.
    TimeFloat EndpointX;
    if (HandleNum == 0) {
      int LastNum = len - 1;
      EndpointX = this->SubSongs.at(LastNum)->TimeX;// to do: optimize this.
      EndpointNum = LastNum;
      this->FirstBeatTimeX = this->SubSongs.at(0)->TimeX;
    } else {
      EndpointX = this->FirstBeatTimeX;
      EndpointNum = 0;//FirstNum;
    }
    this->Interval = (HandleX - EndpointX) / (HandleNum-EndpointNum);
    OffsetBoxBase *obox;
    for (int CellNum=0;CellNum<len;CellNum++) {
      obox = this->SubSongs.at(CellNum);
      obox->TimeX =  this->CalcCellTimeX(CellNum);
    }
  }
  /* ********************************************************************************* */
  void Update_OctaveY(SoundFloat OctaveY) override {
    OffsetBoxBase *obox;// = this->SubSongs.at(Index);
    for (int cnt=0;cnt<this->SubSongs.size();cnt++){
      obox = this->SubSongs.at(cnt);
      obox->OctaveY = OctaveY;
    }
  }
  /* ********************************************************************************* */
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {
    SoundFloat XLoc = mbox.GetX();
    SoundFloat YLoc = mbox.GetY();
    int NumSubSongs = this->SubSongs.size();
    for (int cnt = 0; cnt < NumSubSongs; cnt++) {
      OffsetBoxBase *obx = this->SubSongs.at(cnt);
      ISonglet *songlet = obx->GetContent();
      //obx->TimeX =
      obx->OctaveY = YLoc;
    }
  }
  /* ********************************************************************************* */
  Loop_OffsetBox* Spawn_OffsetBox() override {
    Loop_OffsetBox *lbox = new Loop_OffsetBox(this);// Spawn an OffsetBox specific to this type of songlet.
    this->Ref_Songlet();
    return lbox;
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *phrase = new JsonParse::HashNode();
    phrase->AddSubPhrase(Globals::ObjectTypeName, PackStringField(LoopSong::LoopSongName));// self name
    phrase->AddSubPhrase("MyName", ITextable::PackStringField(this->MyName));
    phrase->AddSubPhrase("Beats", ITextable::PackIntegerField(this->Beats));
    phrase->AddSubPhrase("Interval", ITextable::PackNumberField(this->Interval));
    phrase->AddSubPhrase("FirstBeatTimeX", ITextable::PackNumberField(this->FirstBeatTimeX));
//    phrase->AddSubPhrase("Content", SingleSong.ChildObox->Export(HitTable));

    {// how we would do it with no Y variance between cells. pretend singlesong's kids are my kids.
      JsonParse::ArrayNode *CPointsPhrase = new JsonParse::ArrayNode();
      ITextable::MakeArray<OffsetBoxBase>(HitTable, this->SingleSong->SubSongs, *CPointsPhrase);
      phrase->AddSubPhrase(GroupSong::SubSongsName, CPointsPhrase);
    }
    /*  Save number of beats, but not all subsongs.  regenerate dummy songs on load, in filemapper. */
    return phrase;
  }
  /* ********************************************************************************* */
  void ShallowLoad(JsonParse::HashNode& phrase) override {
    this->MyName = phrase.GetField("MyName", "Loop Song");
    this->Beats = ITextable::GetIntegerField(phrase, "Beats", 2);
    this->Interval = ITextable::GetNumberField(phrase, "Interval", 0.5);
    this->FirstBeatTimeX = ITextable::GetNumberField(phrase, "FirstBeatTimeX", 0.0);
    // loop->Set_Beats(len);
  }
};

#endif// LoopSong_hpp
