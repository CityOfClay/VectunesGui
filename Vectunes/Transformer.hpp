#ifndef Transformer_hpp
#define Transformer_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "IDeletable.hpp"
#include "ICloneable.hpp"
#include "ITextable.hpp"

/**
 *
 * @author MultiTool
 *
 * Transformer is a base class for affine transformations.
 * Its insides could probably be replaced with a matrix.
 *
 */
class CollisionLibrary;// forward

class Transformer: public IDeletable, public ITextable, public ICloneable {// location box to transpose in pitch, move in time, etc.
public:
  SoundFloat TimeX = 0.0, OctaveY = 0.0, LoudnessFactor = 1.0;// time, octave, and loudness context. all of these are in parent coordinates
  SoundFloat ScaleX = 1.0, ScaleY = 1.0; // to be used for pixels per second, pixels per octave
  // ScaleY is 'temper' context, which we will NEVER use unless we want to make ugly anharmonic noise.
  StringConst TimeXName = "TimeX", OctaveYName = "OctaveY", LoudnessFactorName = "LoudnessFactor", ScaleXName = "ScaleX", ScaleYName = "ScaleY";// for serialization

  /* ********************************************************************************* */
  Transformer() {this->Create_Me();}
  /* ********************************************************************************* */
  Transformer(const Transformer &donor) : Transformer() {
    this->Copy_From(donor);
  }
  /* ********************************************************************************* */
  virtual ~Transformer(){this->Delete_Me();}
  /* ********************************************************************************* */
  Transformer* Clone_Me() override {
    Transformer *child = new Transformer();// clone
    child->Copy_From(*this);
    return child;
  }
  Transformer* Deep_Clone_Me(CollisionLibrary& HitTable) override {return nullptr;};// snox does not do anything and should not be in Transformer
  /* ********************************************************************************* */
  void Assign(TimeFloat TimeX0, SoundFloat OctaveY0, TimeFloat ScaleX0, SoundFloat ScaleY0) {
    this->TimeX = TimeX0; this->OctaveY = OctaveY0;
    this->ScaleX = ScaleX0; this->ScaleY = ScaleY0;
  }
  /* ********************************************************************************* */
  void Copy_From(const Transformer& donor) {
    Copy_Transform(donor, *this);
//    this->TimeX = donor.TimeX; this->OctaveY = donor.OctaveY;
//    this->LoudnessFactor = donor.LoudnessFactor;
//    this->ScaleX = donor.ScaleX; this->ScaleY = donor.ScaleY;
  }
  /* ********************************************************************************* */
  static void Copy_Transform(const Transformer& donor, Transformer& recipient) {
    recipient.TimeX = donor.TimeX; recipient.OctaveY = donor.OctaveY;
    recipient.LoudnessFactor = donor.LoudnessFactor;
    recipient.ScaleX = donor.ScaleX; recipient.ScaleY = donor.ScaleY;
  }
  /* ********************************************************************************* */
  virtual void Clear() {// set all coordinates to identity, no transformation for content
    TimeX = OctaveY = 0.0;
    LoudnessFactor = 1.0;
    ScaleX = ScaleY = 1.0;
  }
  /* ********************************************************************************* */
  void CopyTo_Point2D(Point2D& pnt) {
    pnt.X = TimeX; pnt.Y = OctaveY;
  }
  /* ********************************************************************************* */
  void CopyFrom_Point2D(Point2D& pnt) {
    this->TimeX = pnt.X;
    this->OctaveY = pnt.Y;
  }
  /* ********************************************************************************* */
  virtual SoundFloat Get_Max_Amplitude() {// always override this
    return this->LoudnessFactor;
  }
  /* ********************************************************************************* */
  void Compound(const Transformer& donor) {
    this->TimeX += (this->ScaleX * donor.TimeX);// to do: combine matrices here.
    this->OctaveY += (this->ScaleY * donor.OctaveY);
    this->LoudnessFactor *= donor.LoudnessFactor;
    this->ScaleX *= donor.ScaleX;
    this->ScaleY *= donor.ScaleY;
  }
  /* ********************************************************************************* */
  void CreateBackMap(Transformer& target) const {
    target.BecomeBackMap(*this);
    return;

    Point2D pnt;
    this->MapTo(0.0, 0.0, pnt);
    target.TimeX = pnt.X; target.OctaveY = pnt.Y;
    target.LoudnessFactor = this->LoudnessFactor;
    // target.LoudnessFactor = 1.0/this->LoudnessFactor;// ??
    target.ScaleX = 1.0/this->ScaleX; target.ScaleY = 1.0/this->ScaleY;
  }
  /* ********************************************************************************* */
  void BecomeBackMap(const Transformer& source) {
    Point2D pnt;
    source.MapTo(0.0, 0.0, pnt);
    this->TimeX = pnt.X; this->OctaveY = pnt.Y;
    this->LoudnessFactor = source.LoudnessFactor;
    // this->LoudnessFactor = 1.0/source.LoudnessFactor;// ??
    this->ScaleX = 1.0/source.ScaleX; this->ScaleY = 1.0/source.ScaleY;
  }
  /* ********************************************************************************* */
  void MorphLocTo(const Point2D& Target, double FractAlong) {// Just move location, not scale.
    double FractionComp = 1.0 - FractAlong;
    this->TimeX = (this->TimeX * FractionComp) + (Target.X * FractAlong);
    this->OctaveY = (this->OctaveY * FractionComp) + (Target.Y * FractAlong);
  }
  /* ********************************************************************************* */
  void MorphTo(const Transformer& Target, double FractAlong) {
    double FractionComp = 1.0 - FractAlong;

    this->TimeX = (this->TimeX * FractionComp) + (Target.TimeX * FractAlong);
    this->OctaveY = (this->OctaveY * FractionComp) + (Target.OctaveY * FractAlong);

    this->LoudnessFactor = (this->LoudnessFactor * FractionComp) + (Target.LoudnessFactor * FractAlong);
    this->ScaleX = (this->ScaleX * FractionComp) + (Target.ScaleX * FractAlong);
    this->ScaleY = (this->ScaleY * FractionComp) + (Target.ScaleY * FractAlong);
  }
  /* ********************************************************************************* */
  void MorphToExtents(const Transformer& Target, double PixWdt, double PixHgt, double FractAlong) {// Convert to extents, morph, then convert back. Linear in Extents-world, hyperbolic in Transformer world.
    CajaDelimitadora TargetExtent;
    Target.ToExtents(TargetExtent, PixWdt, PixHgt);
    this->MorphToExtents(TargetExtent, PixWdt, PixHgt, FractAlong);
  }
  /* ********************************************************************************* */
  void MorphToExtents(const CajaDelimitadora& TargetExtent, double PixWdt, double PixHgt, double FractAlong) {// Convert to extents, morph, then convert back. Linear in Extents-world, hyperbolic in Transformer world.
    CajaDelimitadora MyExtent;
    this->ToExtents(MyExtent, PixWdt, PixHgt);;
    MyExtent.MorphTo(TargetExtent, FractAlong);
    this->FromExtents(MyExtent, PixWdt, PixHgt);
  }
  /* ********************************************************************************* */
  void FromExtents(const CajaDelimitadora &Extent) {
    double ExtentWdt = Extent.Max.X - Extent.Min.X;
    this->ScaleX = 1.0/ExtentWdt;
    this->TimeX = -this->ScaleX * Extent.Min.X;

    double ExtentHgt = Extent.Max.Y - Extent.Min.Y;
    this->ScaleY = 1.0/ExtentHgt;
    this->OctaveY = -this->ScaleY * Extent.Min.Y;
  }
  /* ********************************************************************************* */
  void ToExtents(CajaDelimitadora &Extent) const {
    double ExtentWdt = 1.0/this->ScaleX;
    Extent.Min.X = -this->TimeX/this->ScaleX;
    Extent.Max.X = Extent.Min.X + ExtentWdt;

    double ExtentHgt = 1.0/this->ScaleY;
    Extent.Min.Y = -this->OctaveY/this->ScaleY;
    Extent.Max.Y = Extent.Min.Y + ExtentHgt;
  }
//  [Extent1D] ToRealExtents([double]$PixWdt) {
//    [Extent1D] $retval = [Extent1D]::new(); # convert graph scale to real extents
//    $retval.MinX = $this.MapTime(0.0);
//    $retval.MaxX = $this.MapTime($PixWdt);
//    return $retval;
//  }
//  [void] FromRealExtents([Extent1D] $Extent, [double]$PixWdt) {
//    $this.ScaleX = $PixWdt / $Extent.GetWidth(); # convert real extents to graph scale
//    $this.TimeX = -($Extent.MinX * $this.ScaleX);
//  }
  /* ********************************************************************************* */
  void FromExtents(const CajaDelimitadora &Extent, double PixWdt, double PixHgt) {
    double ExtentWdt = Extent.GetWidth();
    //double ExtentWdt = Extent.Max.X - Extent.Min.X;
    this->ScaleX = PixWdt/ExtentWdt;
    this->TimeX = -this->ScaleX * Extent.Min.X;

    double ExtentHgt = Extent.GetHeight();
    //double ExtentHgt = Extent.Max.Y - Extent.Min.Y;
    this->ScaleY = PixHgt/ExtentHgt;
    this->OctaveY = -this->ScaleY * Extent.Min.Y;
  }
  /* ********************************************************************************* */
  void ToExtents(CajaDelimitadora &Extent, double PixWdt, double PixHgt) const {
    this->MapTo(0.0, 0.0, Extent.Min);
    this->MapTo(PixWdt, PixHgt, Extent.Max);

//    double ExtentWdt = PixWdt/this->ScaleX;
//    Extent.Min.X = -this->TimeX/this->ScaleX;
//    Extent.Max.X = Extent.Min.X + ExtentWdt;
//
//    double ExtentHgt = PixHgt/this->ScaleY;
//    Extent.Min.Y = -this->OctaveY/this->ScaleY;
//    Extent.Max.Y = Extent.Min.Y + ExtentHgt;
  }
  /* ********************************************************************************* */
  void RescaleTimeX(SoundFloat Factor) {
    this->ScaleX = Factor;
  }
  // <editor-fold defaultstate="collapsed" desc="Mappings and Unmappings">
  /* ********************************************************************************* */
  Point2D Scale(const Point2D& ParentPnt) const {// scale coordinates from my parent's space to my child's space.
    return Point2D(this->ScaleTime(ParentPnt.X), this->ScalePitch(ParentPnt.Y));// Only scale from 0,0, not move.
  }
  /* ********************************************************************************* */
  Point2D UnScale(const Point2D& ChildPnt) const {// scale coordinates from my parent's space to my child's space.
    return Point2D(this->UnScaleTime(ChildPnt.X), this->UnScalePitch(ChildPnt.Y));// Only scale from 0,0, not move.
  }
  /* ********************************************************************************* */
  SoundFloat ScaleTime(SoundFloat ParentTime) const {// scale time coordinate from my parent's space to my child's space.
    return (ParentTime / ScaleX);// Only scale from 0,0, not move.
  }
  /* ********************************************************************************* */
  SoundFloat UnScaleTime(SoundFloat ChildTime) const {// scale time coordinate from my child's space to my parent's space.
    return (ChildTime * ScaleX);// Only scale from 0,0, not move.
  }
  /* ********************************************************************************* */
  SoundFloat ScalePitch(SoundFloat ParentPitch) const {
    return (ParentPitch / ScaleY);// Only scale from 0,0, not move.
  }
  /* ********************************************************************************* */
  SoundFloat UnScalePitch(SoundFloat ChildPitch) const {
    return (ChildPitch * ScaleY);// Only scale from 0,0, not move.
  }
  /* ********************************************************************************* */
  void AddXY(TimeFloat IncrementX, SoundFloat IncrementY) {
    this->TimeX += IncrementX; this->OctaveY += IncrementY;
  }
  /* ********************************************************************************* */
  SoundFloat MapTime(SoundFloat ParentTime) const {// convert time coordinate from my parent's frame to my child's frame
    return ((ParentTime - this->TimeX) / this->ScaleX); // in the long run we'll probably use a matrix
  }
  /* ********************************************************************************* */
  SoundFloat UnMapTime(SoundFloat ChildTime) const {// convert time coordinate from my child's frame to my parent's frame
    return this->TimeX + (ChildTime * ScaleX);
  }
  /* ********************************************************************************* */
  SoundFloat MapPitch(SoundFloat ParentPitch) const {// convert octave coordinate from my parent's frame to my child's frame
    return ((ParentPitch - this->OctaveY) / this->ScaleY);
  }
  /* ********************************************************************************* */
  SoundFloat UnMapPitch(SoundFloat ChildPitch) const {// convert octave coordinate from my child's frame to my parent's frame
    return this->OctaveY + ((ChildPitch) * ScaleY);
  }
  /* ********************************************************************************* */
  void MapTo(SoundFloat XLoc, SoundFloat YLoc, Point2D& results) const {
    results.SetLocation(this->MapTime(XLoc), this->MapPitch(YLoc));
  }
  /* ********************************************************************************* */
  void UnMap(SoundFloat XLoc, SoundFloat YLoc, Point2D& results) const {
    results.SetLocation(this->UnMapTime(XLoc), this->UnMapPitch(YLoc));
  }
  /* ********************************************************************************* */
  void MapTo(const Point2D& pnt, Point2D& results) const {
    results.X = this->MapTime(pnt.X);
    results.Y = this->MapPitch(pnt.Y);
  }
  /* ********************************************************************************* */
  void UnMap(const Point2D& pnt, Point2D& results) const {
    results.X = this->UnMapTime(pnt.X);
    results.Y = this->UnMapPitch(pnt.Y);
  }
  /* ********************************************************************************* */
  void MapTo(const CajaDelimitadora& source, CajaDelimitadora& results) const {
    this->MapTo(source.Min, results.Min);
    this->MapTo(source.Max, results.Max);
    results.Sort_Me();
  }
  /* ********************************************************************************* */
  void UnMap(const CajaDelimitadora& source, CajaDelimitadora& results) const {
    this->UnMap(source.Min, results.Min);
    this->UnMap(source.Max, results.Max);
    results.Sort_Me();
  }
  // </editor-fold>
  /* ********************************************************************************* */
  SoundFloat GetFrequencyFactor() const {
    return Math::pow(2.0, this->OctaveY);
  }
  /* ********************************************************************************* */
  static SoundFloat OctaveToFrequencyFactor(SoundFloat Octave) {
    return Math::pow(2.0, Octave);
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {// IDeletable
    return false;
  }
  void Delete_Me() override {// IDeletable
    this->TimeX = Double::NEGATIVE_INFINITY; this->OctaveY = Double::NEGATIVE_INFINITY;
    this->LoudnessFactor = Double::NEGATIVE_INFINITY;
    this->ScaleX = ScaleY = Double::NEGATIVE_INFINITY;
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    return nullptr;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    this->TimeX = GetNumberField(phrase, Transformer::TimeXName, 0);
    this->OctaveY = GetNumberField(phrase, Transformer::OctaveYName, 0);
    this->LoudnessFactor = GetNumberField(phrase, Transformer::LoudnessFactorName, 1.0);
    this->ScaleX = GetNumberField(phrase, Transformer::ScaleXName, 1.0);
    this->ScaleY = GetNumberField(phrase, Transformer::ScaleYName, 1.0);
  }
  /* ********************************************************************************* */
  virtual void Print_Me(const String &Title) {
    return;
    std::cout << Title << "";
    Print_Num("TimeX:", this->TimeX);
    Print_Num("OctaveY:", this->OctaveY);
    Print_Num("ScaleX:", this->ScaleX);
    Print_Num("ScaleY:", this->ScaleY);
    Print_Num("LoudnessFactor:", this->LoudnessFactor);
    std::cout << "\n ";
  }
  static void Print_Num(const String &Text, const double Number){
    std::cout << Text << Number << ", ";
  }
};

#endif // Transformer_hpp
