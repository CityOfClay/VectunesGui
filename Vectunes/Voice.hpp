#ifndef Voice_hpp
#define Voice_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include <algorithm>    // std::find
#include "Globals.hpp"
#include "Wave.hpp"
#include "CajaDelimitadora.hpp"
#include "ISonglet.hpp"
#include "IDrawable.hpp"
#include "VoicePoint.hpp"
#include "OffsetBoxBase.hpp"
#include "SingerBase.hpp"
#include "Config.hpp"
#include "IGrabber.hpp"

/**
 *
 * @author MultiTool
*/

bool Voice_Iterative = false;// temporary variable for benchmarking
typedef Voice* VoicePtr;
class Voice: public ISonglet{//, IDrawable {// collection of control points, each one having a pitch and a volume. rendering morphs from one cp to another.
public:
  //static bool Iterative;
  ArrayList<VoicePoint*> CPoints;
  StringConst VoiceName = "Voice", CPointsName = "ControlPoints", BaseFreqName = "BaseFreq";// for serialization
  SoundFloat MaxAmplitude;
  SoundFloat BaseFreq = Globals::BaseFreqC0;
  TimeFloat ReverbDelay = 0.125 / 4.0;// delay in seconds
  // graphics support
  Color FillColor;
  static constexpr SoundFloat Filler = 0.3;// to diagnose/debug dropouts in output wave
  /* ********************************************************************************* */
  class Voice_Singer: public SingerBase {
  public:
    Voice* MyVoice = nullptr;
    SoundFloat Phase, Cycles;// Cycles is the number of cycles we've rotated since the start of this voice. The fractional part is the phase information.
    TimeFloat SubTime;// Subjective time.
    SoundFloat Current_Octave, Current_Frequency;
    int Next_Point_Dex;
    VoicePoint Cursor_Point;
    TimeFloat Prev_Time_Absolute = Double::NEGATIVE_INFINITY;//  Initialize to broken value.
    int Sample_Start = Integer::MIN_VALUE;// Number of samples since time 0 absolute, not local. Initialize to broken value.
    SoundFloat BaseFreq;
    /* ********************************************************************************* */
    Voice_Singer(Voice* MyVoice0) {
      this->Current_Frequency = 440;
      this->MyVoice = MyVoice0;
    }
    /* ********************************************************************************* */
    virtual ~Voice_Singer() {
      Delete_Me();
    }
    /* ********************************************************************************* */
    void Start() override {
      this->SubTime = 0.0; this->Phase = 0.0; this->Cycles = 0.0;
      this->Next_Point_Dex = 1;
      this->Sample_Start = 0;
      this->Prev_Time_Absolute = 0;
      if (this->MyVoice->CPoints.size() < 2 || this->InheritedMap.LoudnessFactor == 0.0) {
        this->IsFinished = true;// muted, so don't waste time rendering
      } else {
        this->IsFinished = false;
        VoicePoint *pnt = this->MyVoice->CPoints.at(0);
        this->Cursor_Point.CopyFrom(*pnt);
        this->Prev_Time_Absolute = this->InheritedMap.UnMapTime(pnt->TimeX);// get start time in global coordinates
        //this->Sample_Start = (int)(Prev_Time_Absolute * (double)this->SampleRate);
      }
    }
    /* ********************************************************************************* */
    void Skip_To(TimeFloat EndTime) override {// to do: rewrite this to match bug-fixed render_to
      if (this->IsFinished) { return; }
      VoicePoint *Prev_Point = null, *Next_Point = null;

      EndTime = this->MyOffsetBox->MapTime(EndTime);// EndTime is now time internal to voice's own coordinate system
      EndTime = this->ClipTime(EndTime);
      SoundFloat EndTime_Absolute = this->InheritedMap.UnMapTime(EndTime);
      //this->Sample_Start = EndTime_Absolute * (SoundFloat)this->SampleRate;
      this->Prev_Time_Absolute = EndTime_Absolute;

      Prev_Point = &this->Cursor_Point;
      int pdex = this->Next_Point_Dex;
      Next_Point = this->MyVoice->CPoints.at(pdex);
      if (false){// to do: put treesearch here
        pdex = this->MyVoice->Tree_Search(EndTime, this->Next_Point_Dex, this->MyVoice->CPoints.size());
        Next_Point = this->MyVoice->CPoints.at(pdex);
        // what to do with Prev_Point?
      }else{
        while (Next_Point->TimeX < EndTime) {// this loop ends with EndTime between Prev_Point and Next_Point.
          pdex++;
          Prev_Point = Next_Point;
          Next_Point = this->MyVoice->CPoints.at(pdex);
        }
      }
      this->Next_Point_Dex = pdex;

      if (EndTime <= Next_Point->TimeX) {// deal with loose end.
        if (Prev_Point->TimeX <= EndTime) {// EndTime is inside this box.
          VoicePoint End_Cursor;// this section should always be executed, due to time clipping
          End_Cursor.CopyFrom(*Prev_Point);
          Interpolate_ControlPoint(*Prev_Point, *Next_Point, EndTime, End_Cursor);
          this->Cursor_Point.CopyFrom(End_Cursor);
        }
      }
    }
    /* ********************************************************************************* */
    void Render_To(TimeFloat EndTime, Wave& wave) override { // ready for test
      int SampleRateLocal = this->SampleRate = wave.SampleRate;
      this->Sample_Start = (int)(this->Prev_Time_Absolute * (double)SampleRateLocal);
      if (this->IsFinished) {
        wave.Init_Sample(this->Sample_Start, this->Sample_Start, SampleRateLocal, Filler);// we promise to return a blank wave
        return;
      }
      VoicePoint *Prev_Point = null, *Next_Point = null;
      EndTime = this->MyOffsetBox->MapTime(EndTime);// EndTime is now time internal to voice's own coordinate system
      EndTime = this->ClipTime(EndTime);
      SoundFloat EndTime_Absolute = this->InheritedMap.UnMapTime(EndTime);
      int Sample_End = EndTime_Absolute * (SoundFloat)SampleRateLocal;
      wave.Init_Sample(this->Sample_Start, Sample_End, SampleRateLocal, Filler);// wave times are in global coordinates because samples are always real time

      Prev_Point = &this->Cursor_Point;
      int pdex = this->Next_Point_Dex;
      Next_Point = this->MyVoice->CPoints.at(pdex);
      while (Next_Point->TimeX < EndTime) {// this loop ends with EndTime between Prev_Point and Next_Point.
        Render_Segment(*Prev_Point, *Next_Point, wave);
        pdex++;
        Prev_Point = Next_Point;
        Next_Point = this->MyVoice->CPoints.at(pdex);
      }
      this->Next_Point_Dex = pdex;

      if (EndTime <= Next_Point->TimeX) {// render loose end.
        if (Prev_Point->TimeX <= EndTime) {// EndTime is inside this box.
          VoicePoint End_Cursor;// this section should always be executed, due to time clipping
          End_Cursor.CopyFrom(*Prev_Point);
          Interpolate_ControlPoint(*Prev_Point, *Next_Point, EndTime, End_Cursor);
          Render_Segment(*Prev_Point, End_Cursor, wave);
          this->Cursor_Point.CopyFrom(End_Cursor);
        }
      }
      wave.Amplify(this->MyOffsetBox->LoudnessFactor);
      this->Sample_Start = Sample_End;
      this->Prev_Time_Absolute = EndTime_Absolute;
    }
    /* ********************************************************************************* */
    SoundFloat virtual GetWaveForm(TimeFloat SubTimeAbsolute) {// not used currently
      return Math::sin(SubTimeAbsolute * this->MyVoice->BaseFreq * Globals::TwoPi);
    }
    /* ********************************************************************************* */
    TimeFloat ClipTime(SoundFloat EndTime) {
      if (EndTime < Cursor_Point.TimeX) {
        EndTime = Cursor_Point.TimeX;// clip time
      }
      int FinalIndex = this->MyVoice->CPoints.size() - 1;
      VoicePoint *Final_Point = this->MyVoice->CPoints.at(FinalIndex);
      if (EndTime > Final_Point->TimeX) {
        this->IsFinished = true;
        EndTime = Final_Point->TimeX;// clip time
      }
      return EndTime;
    }
    /* ********************************************************************************* */
    OffsetBoxBase* Get_OffsetBox() {
      return this->MyOffsetBox;
    }
    /* ********************************************************************************* */
    void Draw_Me(DrawingContext& ParentDC) override {// Animate what the singer is singing.
      ParentDC.RecurseDepth++;
      if (this->MyVoice->CPoints.size() > 0) {
        VoicePoint *first = this->MyVoice->CPoints.at(0);
        // first->TimeX to do: we need to clip at the first actual sound point of the voice.
        this->MyOffsetBox->Draw_Me(ParentDC);
        if (true) {// Draw circle with radius based on loudness.
          double Radius = this->Cursor_Point.LoudnessFactor * this->Cursor_Point.OctavesPerLoudness;
          double RadiusPixels = Math::abs(ParentDC.GlobalTransform.ScaleY) * (Radius);

          Point2D pnt0, pnt;
          this->MyOffsetBox->UnMap(this->Cursor_Point.TimeX, this->Cursor_Point.OctaveY, pnt0);// wasteful step redundant with voice draw_me mapping
          ParentDC.To_Screen(pnt0, pnt);

          // Draw moving bubble as sound plays.
          ParentDC.SetColor(0.7, 1.0, 0.7, 0.3);
          ParentDC.DrawCircle(pnt.X, pnt.Y, RadiusPixels);
        }
      }
    }
    /* ********************************************************************************* */
    const CajaDelimitadora* GetBoundingBox() override {// DoLivePan
      return &(this->Get_OffsetBox()->MyBounds);
    }
    /* ********************************************************************************* */
    const bool GetSingerBounds(CajaDelimitadora& Box) override {// DoLivePan
      Box.Copy_From(this->Get_OffsetBox()->MyBounds);
      if (Box.UnInit()) {
        printf("Voice GetSingerBounds UnInit\n");
      }
      if (!std::isfinite(Box.Min.X)) {
        printf("Voice GetSingerBounds\n");
      }
      return true;
    }
    /* ********************************************************************************* */
    void Render_Range(int dex0, int dex1, Wave& wave) {// written for testing
      VoicePoint *pnt0, *pnt1;
      for (int pcnt = dex0; pcnt < dex1; pcnt++) {
        pnt0 = this->MyVoice->CPoints.at(pcnt);
        pnt1 = this->MyVoice->CPoints.at(pcnt + 1);
        Render_Segment(*pnt0, *pnt1, wave);
      }
    }
    /* ********************************************************************************* */
    static void Interpolate_ControlPoint(VoicePoint& pnt0, VoicePoint& pnt1, SoundFloat RealTime, VoicePoint& PntMid) {
      SoundFloat FrequencyFactorStart = pnt0.GetFrequencyFactor();
      SoundFloat TimeRange = pnt1.TimeX - pnt0.TimeX;
      SoundFloat TimeAlong = RealTime - pnt0.TimeX;
      SoundFloat OctaveRange = pnt1.OctaveY - pnt0.OctaveY;
      SoundFloat OctaveRate = OctaveRange / TimeRange;// octaves per second
      SoundFloat SubTimeLocal;
      if (OctaveRate == 0.0){
        SubTimeLocal = TimeAlong;
      }else{
        SubTimeLocal = Voice::Integral(OctaveRate, TimeAlong);
      }
      PntMid.TimeX = RealTime;
      PntMid.SubTime = pnt0.SubTime + (FrequencyFactorStart * SubTimeLocal);

      // not calculus here
      PntMid.OctaveY = pnt0.OctaveY + (TimeAlong * OctaveRate);
      SoundFloat LoudRange = pnt1.LoudnessFactor - pnt0.LoudnessFactor;
      SoundFloat LoudAlong = TimeAlong * LoudRange / TimeRange;
      PntMid.LoudnessFactor = pnt0.LoudnessFactor + LoudAlong;
    }
    /* ********************************************************************************* */
    void Render_Segment(VoicePoint& pnt0, VoicePoint& pnt1, Wave& wave) {// Render a straight pitch line between two points (bend/chirp).
      if (Voice_Iterative){// In the long run, if we use Render_Segment_Iterative at all, it will be to fill in spaces for the integral approach.
        Render_Segment_Iterative(pnt0, pnt1, wave);
      }else{
        Render_Segment_Integral(pnt0, pnt1, wave);
      }
    }
    /* ********************************************************************************* */
    void Render_Segment_Iterative(VoicePoint& pnt0, VoicePoint& pnt1, Wave& wave) {// stateful iterative approach, ready for test
      SoundFloat SRate = this->SampleRate;
      SoundFloat Time0 = this->InheritedMap.UnMapTime(pnt0.TimeX);
      SoundFloat Time1 = this->InheritedMap.UnMapTime(pnt1.TimeX);
      SoundFloat FrequencyFactorInherited = this->InheritedMap.GetFrequencyFactor();// inherit transposition
      int EndSample = (int) (Time1 * (SoundFloat)SRate);// absolute

      SoundFloat SubTime0 = pnt0.SubTime * this->InheritedMap.ScaleX;// tempo rescale
      SoundFloat TimeRange = pnt1.TimeX - pnt0.TimeX;
      SoundFloat Octave0 = this->InheritedMap.OctaveY + pnt0.OctaveY;
      SoundFloat Octave1 = this->InheritedMap.OctaveY + pnt1.OctaveY;

      SoundFloat OctaveRange = Octave1 - Octave0;
      SoundFloat OctaveRate = OctaveRange / TimeRange;// octaves per second bend
      OctaveRate += this->Inherited_OctaveRate;// inherit note bend
      SoundFloat LoudnessRange = pnt1.LoudnessFactor - pnt0.LoudnessFactor;
      SoundFloat LoudnessRate = LoudnessRange / TimeRange;
      int NumSamples = EndSample - this->Sample_Start;

      SoundFloat TimeAlong, CurrentLoudness, Amplitude;

      SoundFloat CurrentOctaveLocal, CurrentFrequency, CurrentFrequencyFactorAbsolute, CurrentFrequencyFactorLocal;
      ldouble SubTimeIterate;

      ldouble FrequencyFactor0 = MonkeyBox::OctaveToFrequencyFactor(Octave0);
      ldouble FrequencyFactor1 = MonkeyBox::OctaveToFrequencyFactor(Octave1);
      ldouble FrequencyRatio = FrequencyFactor1/FrequencyFactor0;
      ldouble Root = std::pow(FrequencyRatio, 1.0/(ldouble)NumSamples);

      SubTimeIterate = pnt0.SubTime * FrequencyFactorInherited * this->InheritedMap.ScaleX;// tempo rescale
      ldouble Snowball = 1.0;// frequency, pow(anything, 0)
      for (int SampleCnt = this->Sample_Start; SampleCnt < EndSample; SampleCnt++){
        TimeAlong = (SampleCnt / SRate) - Time0;
        CurrentLoudness = pnt0.LoudnessFactor + (TimeAlong * LoudnessRate);
        CurrentFrequencyFactorAbsolute = (FrequencyFactor0 * Snowball);
        Amplitude = this->GetWaveForm(SubTimeIterate);
        wave.Set_Abs(SampleCnt, Amplitude * CurrentLoudness);
        SubTimeIterate += CurrentFrequencyFactorAbsolute / SRate;
        Snowball *= Root;
      }
      this->Sample_Start = EndSample;
    }
    /* ********************************************************************************* */
    virtual void Render_Segment_Integral(VoicePoint& pnt0, VoicePoint& pnt1, Wave& wave) {// stateless calculus integral approach
      SoundFloat SRate = this->SampleRate;
      SoundFloat Time0 = this->InheritedMap.UnMapTime(pnt0.TimeX);
      SoundFloat Time1 = this->InheritedMap.UnMapTime(pnt1.TimeX);
      SoundFloat FrequencyFactorInherited = this->InheritedMap.GetFrequencyFactor();// inherit transposition
      int EndSample = (int) (Time1 * (SoundFloat)SRate);// absolute

      SoundFloat SubTime0 = pnt0.SubTime * this->InheritedMap.ScaleX;// tempo rescale
      SoundFloat TimeRange = Time1 - Time0;
      SoundFloat FrequencyFactorStart = pnt0.GetFrequencyFactor();
      SoundFloat Octave0 = this->InheritedMap.OctaveY + pnt0.OctaveY, Octave1 = this->InheritedMap.OctaveY + pnt1.OctaveY;

      SoundFloat OctaveRange = Octave1 - Octave0;
      SoundFloat OctaveRate = OctaveRange / TimeRange;// octaves per second bend
      OctaveRate += this->Inherited_OctaveRate;// inherit note bend
      SoundFloat LoudnessRange = pnt1.LoudnessFactor - pnt0.LoudnessFactor;
      SoundFloat LoudnessRate = LoudnessRange / TimeRange;
      SoundFloat SubTimeLocal, SubTimeAbsolute;

      SoundFloat TimeAlong;
      SoundFloat CurrentLoudness;
      SoundFloat Amplitude;
      int SampleCnt;
      if (OctaveRate == 0.0) {// no bends, don't waste time on calculus// disabled due to possible bug
//      if (false) { // disabled due to possible bug
        for (SampleCnt = this->Sample_Start; SampleCnt < EndSample; SampleCnt++) {
          TimeAlong = (SampleCnt / SRate) - Time0;
          CurrentLoudness = pnt0.LoudnessFactor + (TimeAlong * LoudnessRate);
          SubTimeAbsolute = (SubTime0 + (FrequencyFactorStart * TimeAlong)) * FrequencyFactorInherited;
          Amplitude = this->GetWaveForm(SubTimeAbsolute);
          wave.Set_Abs(SampleCnt, Amplitude * CurrentLoudness);
        }
      }else{
//        printf("OctaveRate:%f\n", OctaveRate);
//        printf("OctaveRange:%f,  TimeRange:%f\n", OctaveRange, TimeRange);
//        printf("Time0:%f,  Time1:%f\n", Time0, Time1);
//        exit(10);
        SoundFloat PreCalc0 = (SubTime0 * FrequencyFactorInherited);// evaluate this outside the loop to optimize
        SoundFloat PreCalc1 = (FrequencyFactorStart * FrequencyFactorInherited);
        for (SampleCnt = this->Sample_Start; SampleCnt < EndSample; SampleCnt++) { // look into -ffast-math
          TimeAlong = (SampleCnt / SRate) - Time0;
          CurrentLoudness = pnt0.LoudnessFactor + (TimeAlong * LoudnessRate);
          SubTimeLocal = Voice::Integral(OctaveRate, TimeAlong);
          SubTimeAbsolute = (SubTime0 + (FrequencyFactorStart * SubTimeLocal)) * FrequencyFactorInherited;
          //SubTimeAbsolute = (PreCalc0 + (PreCalc1 * SubTimeLocal));// optimized, hardly notice the difference
          Amplitude = this->GetWaveForm(SubTimeAbsolute);
          wave.Set_Abs(SampleCnt, Amplitude * CurrentLoudness);
        }
      }
      this->Sample_Start = EndSample;
    }
    /* ********************************************************************************* */
    boolean Create_Me() override {
      return true;
    }
    void Delete_Me() override {// IDeletable
      SingerBase::Delete_Me();
      this->Cursor_Point.Delete_Me();
      this->MyVoice = nullptr;// wreck everything so we crash if we try to use a dead object
      this->Phase = this->Cycles = this->SubTime = Double::NEGATIVE_INFINITY;// Double::NEGATIVE_INFINITY;
      this->Current_Octave = this->Current_Frequency = Double::NEGATIVE_INFINITY;// Double::NEGATIVE_INFINITY;
      this->BaseFreq = Double::NEGATIVE_INFINITY;// Double::NEGATIVE_INFINITY;
      this->Next_Point_Dex = Integer::MIN_VALUE;
      this->Sample_Start = Integer::MIN_VALUE;
    }
  };
  /* ********************************************************************************* */
  class Voice_OffsetBox: public OffsetBoxBase {// location box to transpose in pitch, move in time, etc.
  public:
    Voice* VoiceContent = nullptr;
    StringConst ObjectTypeName = {"Voice_OffsetBox"};// for serialization
    /* ********************************************************************************* */
    Voice_OffsetBox(Voice *voice) : OffsetBoxBase() {
      mytype = 2;
      this->VoiceContent = voice; voice->Ref_Songlet();
      this->Create_Me();
      this->Clear();
    }
    virtual ~Voice_OffsetBox(){ this->Delete_Me(); }
    /* ********************************************************************************* */
    Voice* GetContent() override {
      return VoiceContent;
    }
    /* ********************************************************************************* */
    void Attach_Songlet(Voice *songlet) {// for serialization
      this->Assign_Content(this->VoiceContent = songlet);
      songlet->Ref_Songlet();
    }
    /* ********************************************************************************* */
//    void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
//      printf("Voice_OffsetBox::GoFishing\n");// this function only exists for debugging.
//      OffsetBoxBase::GoFishing(Scoop, StackContext);
//    }
    /* ********************************************************************************* */
    Voice_Singer* Spawn_Singer() override {// for render time.  always always always override this
      Voice_Singer *Singer = this->VoiceContent->Spawn_Singer();
      Singer->MyOffsetBox = this;
      return Singer;
    }
    /* ********************************************************************************* */
    Voice_OffsetBox* Clone_Me() override {// always override this thusly
      Voice_OffsetBox *child = this->VoiceContent->Spawn_OffsetBox();
      child->Copy_From(*this);
      return child;
    }
    /* ********************************************************************************* */
    Voice_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      Voice *VoiceFork = this->VoiceContent->Deep_Clone_Me(HitTable);
      Voice_OffsetBox *MyFork = VoiceFork->Spawn_OffsetBox();
      MyFork->Copy_From(*this);
      return MyFork;
    }
    /* ********************************************************************************* */
    void BreakFromHerd(CollisionLibrary& HitTable) override {// for compose time. detach from my songlet and attach to an identical but unlinked songlet
      Voice *clone = this->VoiceContent->Deep_Clone_Me(HitTable);
      if (this->VoiceContent->UnRef_Songlet() <= 0) {
        delete this->VoiceContent;
        this->VoiceContent = nullptr;
      }
      this->Attach_Songlet(clone);
    }
    /* ********************************************************************************* */
    boolean Create_Me() override {// IDeletable
      return true;
    }
    void Delete_Me() override {// IDeletable
      OffsetBoxBase::Delete_Me();
      if (this->VoiceContent != null) {
        if (this->VoiceContent->UnRef_Songlet() <= 0) {
          delete this->VoiceContent;
          this->VoiceContent = null;
        }
      }
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
      JsonParse::HashNode *SelfPackage = OffsetBoxBase::Export(HitTable);// ready for test?
      SelfPackage->AddSubPhrase(Globals::ObjectTypeName, PackStringField(ObjectTypeName));
      return SelfPackage;
    }
    void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
      OffsetBoxBase::ShallowLoad(phrase);
    }
  };
  /* ********************************************************************************* */
  Voice() {
    mytype = 3;
    FreshnessTimeStamp = 0;
    FillColor.Assign(0.1, 0.2, 0.3, 0.5);
    this->Randomize_Color();
    // this->FillColor.AlphaChannel = 0.5;
  }
  virtual ~Voice(){this->Delete_Me();}
  /* ********************************************************************************* */
  void Copy_Note(VoicePoint& donor) {
    VoicePoint* pnt = new VoicePoint();
    pnt->CopyFrom(donor);
    this->Add_Note(pnt);
  }
  /* ********************************************************************************* */
  void Add_Note(VoicePoint* pnt) {
    if (pnt->TimeX<0) {pnt->TimeX=0;}
    pnt->RefParent(this);
    this->CPoints.push_back(pnt);// to do: treesearch the right place and insert it there.
  }
  /* ********************************************************************************* */
  VoicePoint* Add_Note(SoundFloat RealTime, SoundFloat Octave, SoundFloat Loudness) {
    VoicePoint *pnt = new VoicePoint();// decide whether vp is created/deleted by the voice, or someone else.
    pnt->TimeX = RealTime;
    pnt->OctaveY = Octave;
    pnt->SubTime = 0.0;
    pnt->LoudnessFactor = Loudness;
    this->Add_Note(pnt);
    return pnt;
  }
  /* ********************************************************************************* */
  void Remove_Note(VoicePoint *pnt) {
    this->CPoints.remove(pnt);// to do: VoicePoint must be created and deleted by the voice that owns it.
    delete pnt;
  }
  /* ********************************************************************************* */
  void Remove_Note(VoicePoint::LoudnessHandle *handle) {
    VoicePoint *pnt = handle->ParentPoint;
    this->Remove_Note(pnt);// decide whether vp is created/deleted by the voice, or someone else.
  }
  /* ************************************************************************************************************************ */
  int Tree_Search(SoundFloat Time, int minloc, int maxloc) {// finds place where time would be inserted or replaced
    int medloc;
    while (minloc < maxloc) {
      medloc = (minloc + maxloc) >> 1; // >>1 is same as div 2, only faster.
      if (Time <= this->CPoints.at(medloc)->TimeX) {
        maxloc = medloc;/* has to go through here to be found. */
      } else {
        minloc = medloc + 1;
      }
    }
    return minloc;
  }
  /* ************************************************************************************************************************ */
  int Tree_Search_Last(SoundFloat Time, int minloc, int maxloc) {// finds END OF place where time would be inserted or replaced
    int medloc;
    while (minloc < maxloc) {
      medloc = (minloc + maxloc) >> 1; // >>1 is same as div 2, only faster.
      if (Time < this->CPoints.at(medloc)->TimeX) {
        maxloc = medloc;/* has to go through here to be found. */
      } else {
        minloc = medloc + 1;
      }
    }
    return minloc;
  }
  /* ********************************************************************************* */
  SoundFloat Get_Duration() override {
    int len = this->CPoints.size();
    if (len <= 0) {
      return 0;
    }
    VoicePoint *Final_Point = this->CPoints.at(len - 1);
    return Final_Point->TimeX + this->ReverbDelay;
  }
  /* ********************************************************************************* */
  SoundFloat Get_Max_Amplitude() override {
    return this->MaxAmplitude;
  }
  /* ********************************************************************************* */
  void Randomize_Color() override {
    double RandFract = Math::frand();// Globals::Random::NextDouble();
    Color::ToColorWheel(RandFract, this->FillColor);
    this->FillColor.AlphaChannel = 0.5;
  }
  /* ********************************************************************************* */
  void Update_Max_Amplitude() {
    int len = this->CPoints.size();
    VoicePoint* pnt;
    SoundFloat MaxAmp = 0.0;
    for (int pcnt = 0; pcnt < len; pcnt++) {
      pnt = this->CPoints.at(pcnt);
      if (MaxAmp < pnt->LoudnessFactor) {
        MaxAmp = pnt->LoudnessFactor;
      }
    }
    this->MaxAmplitude = MaxAmp;
  }
  /* ********************************************************************************* */
  void Update_Guts(MetricsPacket& metrics) override {
    if (this->FreshnessTimeStamp != metrics.FreshnessTimeStamp) {// don't hit the same songlet twice on one update
      this->MyProject = metrics.MyProject;
      this->Sort_Me();
      this->Recalc_Line_SubTime();
      this->Update_Max_Amplitude();
      {// Feb1
        VoicePoint *pnt;
        //this->MyBounds.Reset();
        int len = this->CPoints.size();
        for (int pcnt = 0; pcnt < len; pcnt++) {
          pnt = this->CPoints.at(pcnt);
          pnt->Update_Guts(metrics);
          //this->MyBounds.Include(pnt->MyBounds);// Don't have to UnMap in this case because my points are already in my internal coordinates.
        }
      }
      this->UpdateBoundingBoxLocal();
      this->FreshnessTimeStamp = metrics.FreshnessTimeStamp;
    }
    metrics.MaxDuration = this->Get_Duration();
  }
  /* ********************************************************************************* */
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {}
  /* ********************************************************************************* */
  void Sort_Me() {//override {// sorting by TimeX
    std::sort(this->CPoints.begin(), this->CPoints.end(), ComparePoints);
  }
  /* ********************************************************************** */
  static bool ComparePoints(VoicePoint* vp0, VoicePoint* vp1) { return vp0->TimeX < vp1->TimeX; }
  /* ********************************************************************************* */
  void Recalc_Line_SubTime() {
    SoundFloat SubTimeLocal;// run this function whenever this voice instance is modified, e.g. control points moved, added, or removed.
    int len = this->CPoints.size();
    if (len < 1) { return; }
    this->Sort_Me();
    VoicePoint Dummy_First, *Prev_Point, *Next_Point;
    Next_Point = this->CPoints.at(0);
    Dummy_First.CopyFrom(*Next_Point);
    Dummy_First.SubTime = Dummy_First.TimeX = 0.0;// Times must both start at 0, even though user may have put the first audible point at T greater than 0.
    Next_Point = &Dummy_First;
    for (int pcnt = 0; pcnt < len; pcnt++) {
      Prev_Point = Next_Point;
      Next_Point = this->CPoints.at(pcnt);
      SoundFloat FrequencyFactorStart = Prev_Point->GetFrequencyFactor();
      SoundFloat TimeRange = Next_Point->TimeX - Prev_Point->TimeX;
      SoundFloat OctaveRange = Next_Point->OctaveY - Prev_Point->OctaveY;
      if (TimeRange == 0.0) {
        TimeRange = Globals::Fudge;// Fudge to avoid div by 0
      }
      SoundFloat OctaveRate = OctaveRange / TimeRange;// octaves per second
      if (OctaveRate == 0.0){
        SubTimeLocal = TimeRange;// snox is using TimeRange right?
      }else{
        SubTimeLocal = Voice::Integral(OctaveRate, TimeRange);
      }
      Next_Point->SubTime = Prev_Point->SubTime + (FrequencyFactorStart * SubTimeLocal);
    }
  }
  /* ********************************************************************************* */
  static SoundFloat Integral(SoundFloat OctaveRate, SoundFloat TimeAlong) {// to do: optimize this!
    SoundFloat SubTimeCalc;// given realtime passed and rate of octave change, use integration to get the sum of all subjective time passed.
    //return TimeAlong;// snox for testing.  removing integral about doubles speed
    //if (OctaveRate == 0.0) { return TimeAlong; }// do we really have to check this for every sample? more efficient to do it once up front.
    // Yep calling log and pow functions for every sample generated is expensive. We will have to optimize later.
    SoundFloat Denom = (std::log(2.0) * OctaveRate);// returns the integral of (2 ^ (TimeAlong * OctaveRate))
    //SoundFloat Denom = 1.0;// snox for testing.  doing this might maybe be about 0.05(?) seconds faster for a ~1.4 second render
    //SubTimeCalc = (Math::pow(2.0, (TimeAlong * OctaveRate)) / Denom) - (1.0 / Denom);
    //SubTimeCalc = Denom;// snox for testing
    //SubTimeCalc = ((Math::pow(2.0, (TimeAlong * OctaveRate)) - 1.0) / Denom);
    SubTimeCalc = ((std::pow(2.0, (TimeAlong * OctaveRate)) - 1.0) / Denom);// This one line eats more than half our speed.
    return SubTimeCalc;
  }
  /* ********************************************************************************* */
  void Draw_Spine(DrawingContext& ParentDC, int StartDex, int EndDex) {
    double XlocPrev, YlocPrev, Xloc, Yloc;
    int Range, SpineRange;
    Range = EndDex - StartDex;
    SpineRange = Range + 1;
    //double Spine[SpineRange][2];
    VoicePoint *pnt;

    int pcnt = StartDex;
    int CntSpine = 0;

    // if StartDex==0, drag from true origin, voice handle
    Xloc = ParentDC.GlobalTransform.UnMapTime(0);// map to pixels
    Yloc = ParentDC.GlobalTransform.UnMapPitch(0);// map to pixels
    //Spine[CntSpine][0] = Xloc; Spine[CntSpine][1] = Yloc;
    CntSpine++;
    XlocPrev = Xloc; YlocPrev = Yloc;

    while (pcnt < EndDex){
      pnt = this->CPoints.at(pcnt);
      Xloc = ParentDC.GlobalTransform.UnMapTime(pnt->TimeX);// map to pixels
      Yloc = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY);// map to pixels
      //Spine[CntSpine][0] = Xloc; Spine[CntSpine][1] = Yloc;
      ParentDC.DrawLine(XlocPrev, YlocPrev, Xloc, Yloc);// draw spine
      XlocPrev = Xloc; YlocPrev = Yloc;
      CntSpine++;
      pcnt++;
    }
    //ParentDC.gr.DrawPolyline(Spine, SpineRange);// draw spine
  }
  /* ********************************************************************************* */
  void Draw_Outline(DrawingContext& ParentDC, int StartDex, int EndDex) {
    double Xloc, YlocLow, YlocHigh;
    double XlocPrev, YlocLowPrev, YlocHighPrev;
    double OutlineQuad[4][2];
    VoicePoint *pnt;

    Color Llenar(this->FillColor);
    Llenar.AlphaChannel = ParentDC.Brightness;

    double LoudnessHgt;
    int pcnt = StartDex;

    pnt = this->CPoints.at(pcnt);
    // LoudnessHgt = pnt->LoudnessFactor * pnt->OctavesPerLoudness;
    LoudnessHgt = pnt->LoudnessFactor * ParentDC.GlobalTransform.LoudnessFactor * pnt->OctavesPerLoudness;
    Xloc = ParentDC.GlobalTransform.UnMapTime(pnt->TimeX);// map to pixels
    YlocLow = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY - LoudnessHgt);// map to pixels
    YlocHigh = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY + LoudnessHgt);

    Color Emerald(0, 0.5, 0, 1.0);// rgba

    ParentDC.SetColor(Emerald);// voice outline
    ParentDC.DrawLine(Xloc, YlocLow, Xloc, YlocHigh);// left wall

    pcnt++;

    while (pcnt < EndDex){
      XlocPrev = Xloc; YlocLowPrev = YlocLow; YlocHighPrev = YlocHigh;

      pnt = this->CPoints.at(pcnt);
      LoudnessHgt = pnt->LoudnessFactor * pnt->OctavesPerLoudness;
      Xloc = ParentDC.GlobalTransform.UnMapTime(pnt->TimeX);// map to pixels
      YlocLow = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY - LoudnessHgt);// map to pixels
      YlocHigh = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY + LoudnessHgt);

      {
        OutlineQuad[0][0] = XlocPrev; OutlineQuad[0][1] = YlocLowPrev;
        OutlineQuad[1][0] = XlocPrev; OutlineQuad[1][1] = YlocHighPrev;
        OutlineQuad[2][0] = Xloc; OutlineQuad[2][1] = YlocHigh;
        OutlineQuad[3][0] = Xloc; OutlineQuad[3][1] = YlocLow;

        //ParentDC.SetColor(this->FillColor);
        ParentDC.SetColor(Llenar);
        ParentDC.FillPolygon(OutlineQuad, 4);// voice fill

        ParentDC.SetColor(Emerald);// voice outline
        ParentDC.DrawLine(XlocPrev, YlocLowPrev, Xloc, YlocLow);// bottom wall
        ParentDC.DrawLine(XlocPrev, YlocHighPrev, Xloc, YlocHigh);// top wall
      }
      pcnt++;
    }
    ParentDC.SetColor(Emerald);// voice outline
    ParentDC.DrawLine(Xloc, YlocLow, Xloc, YlocHigh);// right wall
  }
  /* ********************************************************************************* */
  void Draw_Handles(DrawingContext& ParentDC, int StartDex, int EndDex) {
    VoicePoint *pnt;
    CajaDelimitadora *ChildrenBounds = &(ParentDC.ClipBounds);// parent is already transformed by my offsetbox
    for (int pcnt = StartDex; pcnt < EndDex; pcnt++) {
      pnt = this->CPoints.get(pcnt);
      if (ChildrenBounds->Intersects(*(pnt->GetBoundingBox()))) {
        pnt->Draw_Me(ParentDC);
      }
    }
  }
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& ParentDC) override {// IDrawable
    PROFILE_START;
    int len = this->CPoints.size();// snox this is a hack. fix it for clip bounds.
    if (len<=0){ return; }
    // It might be less efficient to draw these separately but it is easier to maintain.
    this->Draw_Outline(ParentDC, 0, len);
    this->Draw_Spine(ParentDC, 0, len);
    this->Draw_Handles(ParentDC, 0, len);
    return;

    CajaDelimitadora *ChildrenBounds = &(ParentDC.ClipBounds);// parent is already transformed by my offsetbox
    VoicePoint *pnt;
    double Xloc, Yloc, YlocLow, YlocHigh;
    // work in progress. to do: make a ribbon-shaped polygon whose width is based on point loudness.
    // do we have to go around and then go backward? if pgon were a real array we could fill both sides at once.
    int StartDex, EndDex, Range, SpineRange;
    StartDex = 0;
    EndDex = len;
    Range = EndDex - StartDex;
    int NumDrawPoints = Range * 2;
    double Outline[NumDrawPoints][2];
    SpineRange = Range + 1;
    double Spine[SpineRange][2];
    Xloc = ParentDC.GlobalTransform.UnMapTime(0);// map to pixels
    Yloc = ParentDC.GlobalTransform.UnMapPitch(0);// map to pixels

    Spine[0][0] = Xloc; Spine[0][1] = Yloc;
    double LoudnessHgt;
    int CntUp = Range, CntDown = Range - 1, CntSpine = 1;
    for (int pcnt = StartDex; pcnt < EndDex; pcnt++) {
      pnt = this->CPoints.at(pcnt);
      LoudnessHgt = pnt->LoudnessFactor * ParentDC.GlobalTransform.LoudnessFactor * pnt->OctavesPerLoudness;
      Xloc = ParentDC.GlobalTransform.UnMapTime(pnt->TimeX);// map to pixels
      Yloc = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY);// map to pixels
      Spine[CntSpine][0] = Xloc; Spine[CntSpine][1] = Yloc;
      YlocLow = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY - LoudnessHgt);
      YlocHigh = ParentDC.GlobalTransform.UnMapPitch(pnt->OctaveY + LoudnessHgt);

      //Outline[CntUp][1] = Yloc;// should the loudness envelope just be flat on the bottom, with one control point?
      Outline[CntUp][0] = Xloc; Outline[CntUp][1] = YlocLow;
      Outline[CntDown][0] = Xloc; Outline[CntDown][1] = YlocHigh;

      CntUp++;
      CntDown--;
      CntSpine++;
    }
    //ParentDC.gr.FillPolygon(Outline, NumDrawPoints);// voice fill
    ParentDC.DrawPolygon(Outline, NumDrawPoints);// voice outline
    ParentDC.DrawPolyline(Spine, SpineRange);// draw spine
    for (int pcnt = 0; pcnt < len; pcnt++) {
      pnt = this->CPoints.get(pcnt);
      if (ChildrenBounds->Intersects(*(pnt->GetBoundingBox()))) {
        pnt->Draw_Me(ParentDC);
      }
    }
    PROFILE_END(Globals::Voice_Hits);
  }
  /* ********************************************************************************* */
  void UpdateBoundingBoxLocal() override {// IDrawable
    VoicePoint *pnt;
    this->MyBounds.Reset();
    int len = this->CPoints.size();
    for (int pcnt = 0; pcnt < len; pcnt++) {
      pnt = this->CPoints.at(pcnt);
      this->MyBounds.Include(pnt->MyBounds);// Don't have to UnMap in this case because my points are already in my internal coordinates.
    }
  }
  /* ********************************************************************************* */
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
    CajaDelimitadora PixelBounds;
    StackContext.UnMap(this->MyBounds, PixelBounds);// Map local bounds to pixel space.
    if (Scoop.Intersects(PixelBounds)) {
      int len = this->CPoints.size();
      VoicePoint *pnt;
      for (int pcnt = 0; pcnt < len; pcnt++) {// search my children
        printf("%d,", pcnt);
        pnt = this->CPoints.at(pcnt);
        pnt->GoFishing(Scoop, StackContext);
      }
    }
  }
  /* ********************************************************************************* */
  void Burn_Scale(int TimeStamp, TimeFloat ReScaleX, SoundFloat ReScaleY) override {
    if (this->FreshnessTimeStamp < TimeStamp) { // stale, so add me.
      this->FreshnessTimeStamp = TimeStamp;
      int len = this->CPoints.size();
      VoicePoint *pnt;
      for (int pcnt = 0; pcnt < len; pcnt++) {// search my children
        pnt = this->CPoints.at(pcnt);
        pnt->Burn_Scale(TimeStamp, ReScaleX, ReScaleY);
      }
    }
  }
  /* ********************************************************************************* */
  Voice* Clone_Me() override {// ICloneable
    Voice *child = new Voice();// clones
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  Voice* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    Voice *child;
    /*
     the idea is for hit table to be keyed by my actual identity (me), while the value
     is my first and only clone.
     */
    CollisionItem *ci = HitTable.GetItem(this);
    if (ci == nullptr) {// not seen before, create a neuvo instance
      child = new Voice();// clone
      ci = HitTable.InsertUniqueInstance(this);
      ci->Hydrated = child;
      child->Copy_From(*this);
      child->Copy_Children(*this, HitTable);
    } else {// pre exists
      child = (VoicePtr) ci->Hydrated;// another cast!
    }
    return child;
  }
  /* ********************************************************************************* */
  void Copy_Children(Voice& donor, CollisionLibrary& HitTable) {
    VoicePoint* vpnt;
    int len = donor.CPoints.size();
    for (int cnt = 0; cnt < len; cnt++) {
      vpnt = donor.CPoints.at(cnt);
      this->Add_Note(vpnt->Deep_Clone_Me(HitTable));
    }
  }
  /* ********************************************************************************* */
  void Copy_From(const Voice& donor) {
    this->BaseFreq = donor.BaseFreq;
    this->MyProject = donor.MyProject;
    this->MaxAmplitude = donor.MaxAmplitude;
    this->FillColor = donor.FillColor;
    this->FreshnessTimeStamp = 0;
    IDrawable::Copy_From(donor); // this->MyBounds.Copy_From(donor.MyBounds);
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {// IDeletable
    return true;
  }
  void Delete_Me() override {// IDeletable
    this->BaseFreq = Double::NEGATIVE_INFINITY;
    this->MyProject = nullptr;
    this->MaxAmplitude = Double::NEGATIVE_INFINITY;
    this->FreshnessTimeStamp = Integer_MIN_VALUE;
    this->MyBounds.Delete_Me();
    this->Wipe_CPoints();
    this->ReverbDelay = Double::NEGATIVE_INFINITY;
    this->RefCount = Integer_MIN_VALUE;
    //delete this->FillColor; this->FillColor = nullptr;
  }
  void Wipe_CPoints() {
    int len = this->CPoints.size();
    for (int cnt = 0; cnt < len; cnt++) {
      delete this->CPoints.at(cnt);
    }
    this->CPoints.clear();
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode* phrase = new JsonParse::HashNode();
    this->SerializeMyContents(HitTable, *phrase);
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    this->BaseFreq = GetNumberField(phrase, BaseFreqName, Globals::BaseFreqC0);
    // this.MaxAmplitude = GetNumberField(phrase, "MaxAmplitude", 0.125);// can be calculated
  }
  /* ********************************************************************************* */
  void SerializeMyContents(CollisionLibrary& HitTable, JsonParse::HashNode &Parent) {// Take hashmap as a parameter and modify it.
    Parent.AddSubPhrase(Globals::ObjectTypeName, PackStringField(Voice::VoiceName));// self name
    Parent.AddSubPhrase(BaseFreqName, ITextable::PackNumberField(this->BaseFreq));

    /*

    to do: save and load serialized colors
    int ColorPrecision = 3;
    Double::ToString(FillColor.GetRed(), ColorPrecision);
    Double::ToString(FillColor.GetGreen(), ColorPrecision);
    Double::ToString(FillColor.GetBlue(), ColorPrecision);
    Double::ToString(FillColor.GetAlpha(), ColorPrecision);

    Parent.AddSubPhrase("FillColor", ITextable::PackNumberField(this->BaseFreq));
    {
      // color to json
      { r:0.0, g:0,0, b:0.0, a:0.0 }
    }
    //FillColor.Assign(0.1, 0.2, 0.3, 0.5);

    */

    JsonParse::ArrayNode *CPointsPhrase = new JsonParse::ArrayNode();// Save my array of control points.
    ITextable::MakeArray<VoicePoint>(HitTable, this->CPoints, *CPointsPhrase);
    Parent.AddSubPhrase(Voice::CPointsName, CPointsPhrase);
  }
  /* ********************************************************************************* */
//  public HashMap<String, JsonParse.Node> SerializeMyContents(CollisionLibrary HitTable) {// sort of the counterpart to ShallowLoad
//    HashMap<String, JsonParse.Node> Fields = new HashMap<String, JsonParse.Node>();
//    Fields.put("BaseFreq", IFactory.Utils.PackField(this.BaseFreq));
//    Fields.put("MaxAmplitude", IFactory.Utils.PackField(this.MaxAmplitude));
//    // Fields.put("MyBounds", MyBounds.Export(HitTable)); // can be calculated
//    JsonParse.ArrayNode CPointsPhrase = new JsonParse.ArrayNode();// Save my array of control points.
//    CPointsPhrase.ChildrenArray = IFactory.Utils.MakeArray(HitTable, this.CPoints);
//    Fields.put(Voice.CPointsName, CPointsPhrase);
//    return Fields;
//  }
  /* ********************************************************************************* */
  Voice_OffsetBox* Spawn_OffsetBox() override {// for compose time
    Voice_OffsetBox *vbox = new Voice_OffsetBox(this);// Spawn an OffsetBox specific to this type of phrase.
    this->Ref_Songlet();
    // vbox->Attach_Songlet(this);
    return vbox;
  }
  /* ********************************************************************************* */
  Voice_Singer* Spawn_Singer() override {// for render time
    // Deliver one of my singers while exposing specific object class.
    // Handy if my parent's singers know what class I am and want special access to my particular type of singer.
    Voice_Singer *singer = new Voice_Singer(this);// Spawn a singer specific to this type of phrase.
    singer->Set_Project(this->MyProject);// inherit project
    singer->BaseFreq = this->BaseFreq;
    return singer;
  }
};

#endif // Voice_hpp
