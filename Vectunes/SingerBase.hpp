#ifndef SingerBase_hpp
#define SingerBase_hpp

#include "Wave.hpp"
#include "OffsetBoxBase.hpp"
#include "IDeletable.hpp"
#include "Config.hpp"

/* ********************************************************************************* */
class SingerBase: public IDeletable {// Cantante
public:// Cantante
  Config *MyProject = nullptr;
  SoundFloat Inherited_OctaveRate = 0.0;// bend context, change dynamically while rendering. not used yet.
  /*
  InheritedMap breakdown:
  Inherited_Time = 0.0, Inherited_Octave = 0.0, Inherited_Loudness = 1.0;// time, octave, and loudness context
  Inherited_ScaleX = 1.0;// tempo rescale context
  Inherited_ScaleY = 1.0;// 'temper' context, which we will NEVER use unless we want to make ugly anharmonic noise.
  */
  Transformer InheritedMap;// InheritedMap is transformation to and from samples. Replacement for Inherited_*
  int SampleRate;

  boolean IsFinished = false;
  SingerBase* ParentSinger = nullptr;
  OffsetBoxBase* MyOffsetBox = nullptr;
  /* ********************************************************************************* */
  SingerBase(){ this->Create_Me(); }
  /* ********************************************************************************* */
  virtual ~SingerBase(){ this->Delete_Me(); }
  /* ********************************************************************************* */
  virtual void Start() = 0;
  /* ********************************************************************************* */
  virtual void Skip_To(TimeFloat EndTime) = 0;
  /* ********************************************************************************* */
  virtual void Render_To(TimeFloat EndTime, Wave& wave) = 0;
  /* ********************************************************************************* */
  virtual void Inherit(SingerBase& parent) {// accumulate transformations
    this->ParentSinger = &parent;
    this->SampleRate = parent.SampleRate;
    this->InheritedMap.Copy_From(parent.InheritedMap);
    this->Compound();
  }
  /* ********************************************************************************* */
  virtual void Set_Project(Config *config){
    if (config!=nullptr){
      this->MyProject = config;
      this->SampleRate = config->SampleRate;
    }else{
      printf("Singer Set_Project config is null!\n");
    }
  }
  /* ********************************************************************************* */
  virtual void Compound() {// accumulate my own transformation
    this->Compound(*(this->Get_OffsetBox()));
  }
  /* ********************************************************************************* */
  virtual void Compound(MonkeyBox& donor) {// accumulate my own transformation
    this->InheritedMap.Compound(donor);// to do: combine matrices here.
  }
  /* ********************************************************************************* */
  virtual OffsetBoxBase* Get_OffsetBox() {
    return this->MyOffsetBox;
  }
  /* ********************************************************************************* */
  virtual void Draw_Me(DrawingContext& ParentDC) = 0;// Animate what the singers are singing.
  /* ********************************************************************************* */
//  virtual void Draw_Me(DrawingContext& ParentDC) {// Animate what the singers are singing.
//    ParentDC.RecurseDepth++;
//    //this->MyOffsetBox->Draw_Me(ParentDC);
//  }
  /* ********************************************************************************* */
  virtual const CajaDelimitadora* GetBoundingBox() = 0;// DoLivePan
  /* ********************************************************************************* */
//  virtual const CajaDelimitadora* GetBoundingBox() {// DoLivePan
//    return &(this->Get_OffsetBox()->MyBounds);
//  }
  /* ********************************************************************************* */
  virtual const bool GetSingerBounds(CajaDelimitadora& Box) = 0;// DoLivePan
  /* ********************************************************************************* */
  bool Create_Me() override {// IDeletable
    return true;
  }
  void Delete_Me() override {// IDeletable
    this->MyProject = nullptr;// wreck everything
    this->Inherited_OctaveRate = Double::NEGATIVE_INFINITY;
    this->InheritedMap.Delete_Me();// should be redundant
    this->IsFinished = true;
    this->ParentSinger = nullptr;
    this->MyOffsetBox = nullptr;
  }
};

#endif // SingerBase_hpp
