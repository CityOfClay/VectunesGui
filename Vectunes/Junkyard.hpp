#ifndef Junkyard_hpp
#define Junkyard_hpp

#include <iostream>
#include <time.h>
#include <chrono>
#include <ctime>

#include "ICloneable.hpp"
#include "ISonglet.hpp"
#include "JsonParse.hpp"
#include "CajaDelimitadora.hpp"

#include "IDrawable.hpp"
#include "MonkeyBox.hpp"
#include "OffsetBoxBase.hpp"
#include "Voice.hpp"

#include "Line2D.hpp"
#include "Splines.hpp"

#include "Grabber.hpp"

#include "GroupSong.hpp"
#include "LoopSong.hpp"
#include "PatternMaker.hpp"

#include "SampleVoice.hpp"

#include "AudProject.hpp"

using namespace std;

/* ********************************************************************************* */
// perfect example
class Outer {
public:
  static const String HornSampleName;// = "Horn";
  int NumberOut;
  Outer(){
    this->NumberOut = 999;
  }
  class Inner{
  public:
    class Eener{
    public:
      Outer *afuera;
      void Test(){
        this->afuera->LastOne();
      }
    };
    int NumberIn = 12;
    Outer *oot;
    void Test(){
      this->NumberIn = this->oot->NumberOut;
      this->oot->LastOne();
    }
  };
  Inner* Spawn(){
    Inner *ins = new Inner();
    ins->oot = this;
    ins->Test();
    return ins;
  }
  void LastOne(){
    cout << "LastOne" << endl;
  }
};

const string Outer::HornSampleName = "Horn";

/* ********************************************************************************* */
void TestVector(){// crashes
  std::vector<int> vint = {0, 1, 2};
  std::vector<int> vgo = {10, 11, 12};

  int StartPlace = 10;
  vint.insert(std::begin(vint) + StartPlace, std::begin(vgo), std::end(vgo));
  for (int cnt=0;cnt<vint.size();cnt++){
    printf("cnt:%i, ", vint.at(cnt));
  }
  printf("\n");
  // cnt:10, cnt:11, cnt:12, cnt:0, cnt:1, cnt:2,
}
/* ********************************************************************************* */
void Chop_Test(SingerBase *singer, const String& FileName){// test random chops
  printf("Chop_Test\n");
  Wave Chunk, Chopped, Whole;
  Chunk.Assign_SampleRate(Globals::SampleRate);
  Chopped.Assign_SampleRate(Globals::SampleRate);
  Whole.Assign_SampleRate(Globals::SampleRate);
  singer->Start();
  SoundFloat Time = 0;
  printf("Chopped render...\n");
  while (!singer->IsFinished){
    Time += 0.01;
    //Time += 0.01 * Math::frand();
    //Time += 0.02;// * Math::frand();
    printf("singer->Render_To(%f); ", Time);
    singer->Render_To(Time, Chunk);
    //Chopped.Append2(Chunk);
    Chopped.OverdubUnion(Chunk);
  }

  Chopped.SaveToWav(FileName + ".Chopped.wav");

  printf("Whole render...\n");
  singer->Start();
  singer->Render_To(Time, Whole);

  Whole.SaveToWav(FileName + ".Whole.wav");

  Chopped.Amplify(-1.0);
  Whole.Overdub(Chopped);// If everything works, the 'whole' file waveform should be a flat line - no difference between whole and chopped.
  Whole.SaveToWav(FileName + ".Diff.wav");

  printf("Chop_Test done\n");
}
/* ********************************************************************************* */
void Skip_Test(SingerBase *singer, const String& FileName){// test skip_to
  printf("Skip_Test\n");
  SoundFloat Duration = singer->Get_OffsetBox()->GetContent()->Get_Duration();
  Wave Chunk, Chopped, Whole;
  Chunk.SampleRate = Globals::SampleRate;
  //Duration = 0;
  Chopped.Init_Time(0.0, Duration, Globals::SampleRate, 0.0);
  Whole.SampleRate = Globals::SampleRate;
  singer->Start();
  SoundFloat Time = 0;
  while (!singer->IsFinished){
    Time += 0.01;
    printf("singer->Skip_To(%f); ", Time);
    singer->Skip_To(Time);
    Time += 0.01;
    singer->Render_To(Time, Chunk);
//    Chopped.OverdubUnion(Chunk);
//    Chopped.Append2(Chunk);
    Chopped.Overdub(Chunk);
  }

  Chopped.SaveToWav(FileName + ".Skipped.wav");

  printf("\nRendering Whole\n");
  singer->Start();
  singer->Render_To(Time, Whole);
  printf("Rendering Whole done\n");

  Whole.SaveToWav(FileName + ".Whole.wav");

  Chopped.Amplify(-1.0);
  Whole.Overdub(Chopped);// If everything works, the 'whole' file waveform should be a flat line - no difference between whole and chopped.
  Whole.SaveToWav(FileName + ".Diff.wav");
  printf("Skip_Test done\n");
}
/* ********************************************************************************* */
void FillVoice(Voice *voz, SoundFloat Duration){// add voice bend points
  VoicePoint *vp0 = new VoicePoint();
  vp0->OctaveY = 5.0; vp0->TimeX = 0;
  voz->Add_Note(vp0);

  VoicePoint *vp1 = new VoicePoint();
  vp1->OctaveY = 8.0; vp1->TimeX = Duration * 0.531;
  voz->Add_Note(vp1);

  VoicePoint *vp2 = new VoicePoint();
  vp2->OctaveY = 2.0; vp2->TimeX = Duration;
  voz->Add_Note(vp2);

  voz->Recalc_Line_SubTime();
}
/* ********************************************************************************* */
void FillVoice(Voice *voz){// add voice bend points
  FillVoice(voz, 0.2);
}
/* ********************************************************************************* */
GroupSong* MakeChord(ISonglet *songlet){
  OffsetBoxBase *handle;
  GroupSong *gsong = new GroupSong();

  handle = songlet->Spawn_OffsetBox();
  handle->OctaveY = 0.0;
  gsong->Add_SubSong(handle);

  handle = songlet->Spawn_OffsetBox();
  handle->OctaveY = 0.5;
  gsong->Add_SubSong(handle);

  handle = songlet->Spawn_OffsetBox();
  handle->OctaveY = 1.0;
  gsong->Add_SubSong(handle);

  return gsong;
}
/* ********************************************************************************* */
void TestSpeed(ISonglet& song, int NumTrials){
  using namespace std::chrono;
  SoundFloat Time, Duration;
  Wave Chunk, Chopped;

  Duration = song.Get_Duration();
  std::cout << "Song Duration:" << Duration << " seconds\n";

  OffsetBoxBase *obox = song.Spawn_OffsetBox();
  SingerBase *singer = obox->Spawn_Singer();
  singer->SampleRate = Globals::SampleRate;

  system_clock::time_point start = system_clock::now();// https://en.cppreference.com/w/cpp/chrono
  for (int tcnt=0;tcnt<NumTrials;tcnt++){
    cout << "Trial Number:" << tcnt << "\n";
    Time = 0;
    bool TimeRender = true;
    singer->Start();
    while (!singer->IsFinished){
      Time += 0.1;
      singer->Render_To(Time, Chunk);
      Chopped.Append2(Chunk);
    }
  }
  system_clock::time_point end = system_clock::now();

  delete singer;
  delete obox;

  duration<double> elapsed_seconds = end-start;
  // elapsed_seconds /= (SoundFloat)NumTrials;
  SoundFloat TestTime = elapsed_seconds.count() / (SoundFloat)NumTrials;
  std::time_t end_time = system_clock::to_time_t(end);
  std::cout << "finished speed test at " << std::ctime(&end_time) << "render time: " << TestTime << "s\n";
  std::cout << "Ratio:" << (TestTime / Duration) << " seconds per second\n";
}
/* ********************************************************************************* */
void TestSpeed(ISonglet& song){
  TestSpeed(song, 16);
}
/* ********************************************************************************* */
void CreateRandom(){// create a random composition
}
/* ********************************************************************************* */
void CreateBentTriad(){
  printf("CreateBentTriad\n");
  Config conf;
  MetricsPacket metrics;
  metrics.MaxDuration = 0.0; metrics.MyProject = &conf; metrics.FreshnessTimeStamp = 1;

  double NoteDuration = 2.0, Octave = 5.0;
  Voice *voz;
  voz = PatternMaker::Create_Bent_Note(0.0, NoteDuration, 0.0, 1.0);

  LoopSong *loop = PatternMaker::Create_Unbound_Triad_Rhythm_Loops(*voz);

  GroupSong *grsong = new GroupSong();// put in a group to raise it by 5 octaves.
  OffsetBoxBase *obox = grsong->Add_SubSong(*loop, 0.0, Octave, 1.0);
  //obox->ScaleX = 2.0;
  //obox->ScaleX = 1.0;
  //grsong->Update_Guts(metrics);// finalize
  obox->Update_Guts(metrics);// finalize

  GroupSong::Group_OffsetBox *grobox = grsong->Spawn_OffsetBox();
  GroupSong::Group_Singer *gsing = grobox->Spawn_Singer();
  Wave wave;
  wave.Assign_SampleRate(Globals::SampleRate);

  if (true){
    Chop_Test(gsing, "BentTriad");
    //Skip_Test(gsing, "BTSkip");
  }else {
    gsing->Start();
    printf("Render_To\n");
    gsing->Render_To(grsong->Duration, wave);
    printf("Render_To done\n");
    delete gsing;
    printf("SaveToWav\n");
    wave.SaveToWav("BentTriad.wav");
    printf("SaveToWav done\n");
  }

  if (false){
    Voice_Iterative = true;// temporary for testing
    TestSpeed(*grsong, 16);
    Voice_Iterative = false;// temporary for testing
    TestSpeed(*grsong, 16);
  }
  delete grobox;
  printf("CreateBentTriad done\n");
}
/* ********************************************************************************* */
void ComparePowers(){// Test for iterative render, faster than integral but accumulates errors
  int Divisions = 12;
  Divisions = 44100;
  Divisions = 100;
  ldouble OctaveStart = 0.2;//0.0;//
  ldouble OctaveEnd = OctaveStart+2.0; OctaveEnd = OctaveStart+1.5;
  ldouble OctaveRange = OctaveEnd - OctaveStart;

  ldouble FrequencyStart = std::pow(2.0, OctaveStart);
  ldouble FrequencyEnd = std::pow(2.0, OctaveEnd);
  ldouble FrequencyRatio = FrequencyEnd/FrequencyStart;
  ldouble Root = std::pow(FrequencyRatio, 1.0/(ldouble)Divisions);

  ldouble Snowball = 1.0;// frequency, pow(anything, 0)
  for (int cnt=0;cnt<=Divisions;cnt++){
    ldouble FractAlong = ((ldouble)cnt)/(ldouble)Divisions;
    ldouble OctAlong = OctaveStart + (OctaveRange * FractAlong);
    ldouble Power = std::pow(2.0, OctAlong);
    ldouble Iterative = FrequencyStart*Snowball;
    ldouble Error = Power - Iterative;
    //printf("FractAlong:%f, Power:%f, Iterative:%f, Error:%e\n", FractAlong, Power, Iterative, Error);
    printf("FractAlong:%Lf, Power:%Lf, Iterative:%Lf, Error:%Le\n", FractAlong, Power, Iterative, Error);
    //cout << "FractAlong:"<< FractAlong <<", Power:"<< Power <<", Iterative:"<< Iterative <<", Error:"<< Error <<"\n";
    Snowball *= Root;
  }
  /*
  So adding octave means multiplying frequency.
  Freq always starts at 1.0, octave at 0.0?
  Then we apply an offset by multiplying FrequencyStart by our current frequency, and
  adding OctaveStart to the current octave.
  */
  printf("\n");
}
/* ********************************************************************************* */
struct Functor{// https://stackoverflow.com/questions/9568150/what-is-a-c-delegate
  // for use in binding Draw_Me callbacks
  int operator()(double d){ // Arbitrary return types and parameter list
    return (int) d + 1;
  }
};
/*
        if (false) {// serial/deserial test.
          String JsonTxt = audproj->Textify();
          audproj->UnTextify(JsonTxt);
        }
*/

//
//      if (NowPlaying->MyOffsetBox->UnMap_EndTime() < EndTime) {// to do: if EndTime is beyond end of NowPlaying, return empty wave.
//        this->IsFinished = true;
//        return;
//      }

//      if (NowPlaying->MyOffsetBox->UnMap_EndTime() < EndTime) {// to do: if EndTime is beyond end of NowPlaying, return empty wave.
//        wave.Init_Sample(Sample_Start, Sample_Start, this->SampleRate, 0.0);
//        this->IsFinished = true;
//        return;
//      }

/*
  From Groupsong HitsMyVineSpline
    CajaDelimitadora SearchBox;// in same, local space as SplineBridge.
    TimeFloat BoxLeft = SearchBox.Min.X, BoxRight = SearchBox.Max.X;

    // if right wall is less than 0 OR left wall is greater than last point, then bail.  no match.
    // next, what if left wall is less than 0 but right wall is in range?

    {
      int len = SplineBridge.size();// iterative search
      Point2D Prev(0, 0), Current(0, 0);
      if (SearchBox.Contains(Prev)){ return true; }// if any endpoint is in box.
      int cnt;
      for (cnt=0; cnt<len; cnt++) {
        Current = SplineBridge.at(cnt);
        if (SearchBox.Contains(Current)){ return true; }

        if (Splines::BoxHitOneLine(SearchBox, Prev, Current)) { return true; }

        // exclusion zones
//        if (Current.X<SearchBox.Min.X && Prev.X<SearchBox.Min.X){ return false; }
//        if (Current.X>SearchBox.Max.X && Prev.X>SearchBox.Max.X){ return false; }
//        if (Current.Y<SearchBox.Min.Y && Prev.Y<SearchBox.Min.Y){ return false; }
//        if (Current.Y>SearchBox.Max.Y && Prev.Y>SearchBox.Max.Y){ return false; }

        // to do: test for any line intersect, and if found return true

//        bool lef = LineLine(SearchBox.Min.X, SearchBox.Min.Y, SearchBox.Min.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X, Current.Y);
//        bool rgt = LineLine(SearchBox.Max.X, SearchBox.Min.Y, SearchBox.Max.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X, Current.Y);
//
//        bool bot = LineLine(SearchBox.Min.X, SearchBox.Min.Y, SearchBox.Max.X, SearchBox.Min.Y, Prev.X, Prev.Y, Current.X, Current.Y);
//        bool top = LineLine(SearchBox.Min.X, SearchBox.Max.Y, SearchBox.Max.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X, Current.Y);


//        bool hit0 = CrossOrtho(SearchBox.Min.X, SearchBox.Min.Y, SearchBox.Min.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X, Current.Y);
//        bool hit1 = CrossOrtho(SearchBox.Max.X, SearchBox.Min.Y, SearchBox.Max.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X, Current.Y);
//        bool hit2 = CrossOrtho(SearchBox.Min.Y, SearchBox.Min.X, SearchBox.Min.Y, SearchBox.Max.X, Prev.Y, Prev.X, Current.Y, Current.X);
//        bool hit3 = CrossOrtho(SearchBox.Max.Y, SearchBox.Min.X, SearchBox.Max.Y, SearchBox.Max.X, Prev.Y, Prev.X, Current.Y, Current.X);


//        double LineDx = Current.X - Prev.X;
//        double LineDy = Current.Y - Prev.Y;
//        double LineSlope = LineDy/LineDx;
//        double LineBase = Prev.Y - (LineSlope * Prev.X);
//
//        double YCrossLeft = (LineSlope*SearchBox.Min.X)+LineBase;
//        if (CrossVert(SearchBox, SearchBox.Min.X, YCrossLeft, Prev.X, Current.X)) { return true; }
//        double YCrossRight = (LineSlope*SearchBox.Max.X)+LineBase;
//        if (CrossVert(SearchBox, SearchBox.Max.X, YCrossRight, Prev.X, Current.X)) { return true; }
//
//        // 2 horizontal lines.
//        double XCrossBottom = (SearchBox.Min.Y - LineBase) / LineSlope;
//        if (CrossHoriz(SearchBox, XCrossBottom, SearchBox.Min.Y, Prev.X, Current.X)) { return true; }
//        double XCrossTop = (SearchBox.Max.Y - LineBase) / LineSlope;
//        if (CrossHoriz(SearchBox, XCrossTop, SearchBox.Max.Y, Prev.X, Current.X)) { return true; }

        // bool hit3 = CrossHoriz(SearchBox, XCross, YCross, PrevY, CurrentY);//SearchBox.Min.?

        // bool hits = Collides(SearchBox.Min.X, SearchBox.Min.Y, SearchBox.Max.X, SearchBox.Max.Y, Prev.X, Prev.Y, Current.X-Prev.X, Current.Y-Prev.Y);

        if (SearchBox.Max.X < Current.X) { return false; }// we have left the search box.
        Prev = Current;
      }
    }

    int len = SplineBridge.size();// iterative search
    Point2D Prev(0, 0), Current(0, 0);
    int cnt;
    for (cnt=0; cnt<len; cnt++) {
      Current = SplineBridge.at(cnt);
      if (BoxLeft <= Current.X) {
        break;
      }
      Prev = Current;
    }
    if (len <= cnt) {
      // gone beyond the end, no match. return false;
    } else {
      // if BoxLeft is less than first one, it could be X=0.
      do {
        if (SearchBox.Contains(Current)){ return true; }// check if Current point is inside box. if so return true.

        // else check for intersection of line Prev, Current with any edges of box. if so return true;
        //
        // if (LineCheck(Prev, Current)) { return true; }
        cnt++;
        Prev = Current;
        Current = SplineBridge.at(cnt);
      } while (Current.X < BoxRight);
    }
    // at this point line is Prev, Current.

    // if target is lower than first item, returns index of first item, which is 0.
    // what we want to return is start and end of the line.  so if first, 0,0 to point[0].  anything else, point[cnt-1] to point[cnt].
    //

    tree search segments up to, but left of, left wall of searchbox.
    given X value of left wall, find exact line Y at that wall.

    if Y is between top and bottom then return true;
    else
    iterate until right wall?
    or tree search to just outside right wall?

    with iterate across,
     if Y starts above box,
      loop across,
       any new Y below the top : return true.
     if Y starts below box,
      loop across,
       then any new Y above the bottom : return true.

    with treesearch both sides,
     if either side Y intercept is inside then return true;
     if first Y intercept starts above box top,
      if other side Y intercept is above top then return false;
      if other side Y intercept is below top then return true;
     if first Y starts below box bottom,
      if other side Y intercept is above bottom then return false;
      if other side Y intercept is below bottom then return true;

*/

#endif // Junkyard_hpp
