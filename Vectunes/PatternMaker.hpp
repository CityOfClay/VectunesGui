#ifndef PatternMaker_hpp
#define PatternMaker_hpp

#include "Voice.hpp"
#include "PluckVoice.hpp"
#include "GroupSong.hpp"

class PatternMaker {
public:
  static constexpr double NumNotes = 12;
  static constexpr double SemitoneFraction = (1.0 / NumNotes);
  static constexpr double OffsetTime = 0.03;
  /* ********************************************************************************* */
  static void FillVoice(Voice *voz, SoundFloat Duration) { // add voice bend points
    VoicePoint *vp0 = new VoicePoint();
    vp0->TimeX = 0.0;
    vp0->OctaveY = 0.0;//5.0;
    vp0->LoudnessFactor = 0.25;
    voz->Add_Note(vp0);

    VoicePoint *vp1 = new VoicePoint();
    vp1->TimeX = Duration * 0.531;
    vp1->OctaveY = 3.0;//8.0;
    vp1->LoudnessFactor = 1.0;
    voz->Add_Note(vp1);

    VoicePoint *vp2 = new VoicePoint();
    vp2->TimeX = Duration;
    vp2->OctaveY = 2.0;
    vp2->LoudnessFactor = 0.7;
    voz->Add_Note(vp2);

    voz->Recalc_Line_SubTime();
  }
  /* ********************************************************************************* */
  static void FillVoice(Voice *voz) { // add voice bend points
    FillVoice(voz, 1.0);
  }
  /* ********************************************************************************* */
  static GroupSong* MakeMajor(ISonglet& song, double Key) {
    Key /= SemitoneFraction;// convert from octaves to frets
    return Create_Triad(song, Key, (Key + 4), (Key + 7));
  }
  /* ********************************************************************************* */
  static GroupSong* MakeMinor(ISonglet& song, double Key) {
    Key /= SemitoneFraction;// convert from octaves to frets
    return Create_Triad(song, Key, (Key + 3), (Key + 7));
  }
  /* ********************************************************************************* */
  static GroupSong* MakeAugmented(ISonglet& song, int Key) {
    Key /= SemitoneFraction;// convert from octaves to frets
    return Create_Triad(song, Key, (Key + 4), (Key + 8));
  }
  /* ********************************************************************************* */
  static GroupSong* MakeDiminished(ISonglet& song, int Key) {
    Key /= SemitoneFraction;// convert from octaves to frets
    return Create_Triad(song, Key, (Key + 3), (Key + 6));
  }
  /* ********************************************************************************* */
  static GroupSong* Create_Seventh(ISonglet& song, int NoteDex0, int NoteDex1, int NoteDex2, int NoteDex3) {
    double Loudness = 1.0;// NoteDex0 is usually the key
    double Duration = 2.0;
    GroupSong *cbx = Create_Triad(song, NoteDex0, NoteDex1, NoteDex2);
    cbx->Add_SubSong(song, 0, SemitoneFraction * NoteDex3, Loudness);
    return cbx;
  }
  /* ********************************************************************************* */
  static GroupSong* Create_Triad(ISonglet& song, double NoteDex0, double NoteDex1, double NoteDex2) {
    double Loudness = 1.0;// NoteDex0 is usually the key
    GroupSong *gsong = new GroupSong();
    gsong->Add_SubSong(song, 0.0000 + OffsetTime, SemitoneFraction * NoteDex0, Loudness);
    gsong->Add_SubSong(song, 0.0001 + OffsetTime, SemitoneFraction * NoteDex1, Loudness);
    gsong->Add_SubSong(song, 0.0002 + OffsetTime, SemitoneFraction * NoteDex2, Loudness);
    return gsong;
  }
  /* ********************************************************************************* */
  static void Create_Long_Note(Voice *voice, int NumPoints, double TimeOffset, double Duration, double OctaveOffset, double LoudnessOffset) {
    for (int cnt=0;cnt<NumPoints;cnt++) {
      double TimeAlong = ((double)cnt)/(double)NumPoints;
      voice->Add_Note(TimeOffset + (TimeAlong*Duration), OctaveOffset, LoudnessOffset);
    }
  }
  /* ********************************************************************************* */
  static void Create_Bent_Note(Voice *voice, double TimeOffset, double Duration, double OctaveOffset, double LoudnessOffset) {
    double midfrac0 = 0.03, midfrac1 = 0.5;
    voice->Add_Note(TimeOffset + 0, OctaveOffset + 0, LoudnessOffset * 0);
    voice->Add_Note(TimeOffset + Duration * midfrac0, OctaveOffset + 0, LoudnessOffset * 1);
    voice->Add_Note(TimeOffset + Duration * midfrac1, OctaveOffset - 0.07, LoudnessOffset * midfrac1);
    voice->Add_Note(TimeOffset + Duration, OctaveOffset + 0.0, LoudnessOffset * 0.0);
    //voice->Add_Note(TimeOffset + Duration, OctaveOffset + 0.1, LoudnessOffset * 0);
  }
  /* ********************************************************************************* */
  static Voice* Create_Bent_Note(double TimeOffset, double Duration, double OctaveOffset, double LoudnessOffset) {
    Voice *voice = new Voice();
    Create_Bent_Note(voice, TimeOffset, Duration, OctaveOffset, LoudnessOffset);
    return voice;
  }
  /* ********************************************************************************* */
  static GroupSong* Create_Triad_Pattern(ISonglet& song, double Delay) {
    GroupSong *CMinor = nullptr, *CMajor = nullptr, *DMajor = nullptr, *DMinor = nullptr;
    double Key = 0.0;//5;//0.0;// relative key, no offset from master group
    GroupSong *gsong = nullptr;

    double offx = OffsetTime;
    gsong = new GroupSong();
    CMajor = MakeMajor(song, Key);// C major
    gsong->Add_SubSong(*CMajor, 0 + offx, 0, 1.0);

    CMinor = MakeMinor(song, Key);// C minor
    gsong->Add_SubSong(*CMinor, Delay * 1 + offx, 0, 1.0);

    DMajor = MakeMajor(song, Key);// D major
    gsong->Add_SubSong(*DMajor, Delay * 2 + offx, 2.0 * SemitoneFraction, 1.0);

    DMinor = MakeMinor(song, Key);// D minor
    gsong->Add_SubSong(*DMinor, Delay * 3 + offx, 2.0 * SemitoneFraction, 1.0);

    return gsong;
  }
  /* ********************************************************************************* */
  static GroupSong* Create_Unbound_Triad_Rhythm(ISonglet& song) {
    double Delay = 1.5;
    GroupSong *gsong = nullptr;
    GroupSong *lsong = nullptr;
    OffsetBoxBase *gobx;

    gsong = Create_Triad_Pattern(song, Delay);

    lsong = new GroupSong();

    double Interval = Delay * 4.0;
    double cnt = 0.0;
    gobx = gsong->Spawn_OffsetBox();
    gobx->Assign(Interval*cnt++, 0.0, 1.0, 1.0);
    lsong->Add_SubSong(gobx);

    gobx = gsong->Spawn_OffsetBox();
    gobx->Assign(Interval*cnt++, 0.0, 1.0, 1.0);
    lsong->Add_SubSong(gobx);

    gobx = gsong->Spawn_OffsetBox();
    gobx->Assign(Interval*cnt++, 0.0, 1.0, 1.0);
    lsong->Add_SubSong(gobx);

    gobx = gsong->Spawn_OffsetBox();
    gobx->Assign(Interval*cnt++, 0.0, 1.0, 1.0);
    lsong->Add_SubSong(gobx);

    return lsong;
  }
  /* ********************************************************************************* */
  static LoopSong* Create_Unbound_Triad_Rhythm_Loops(ISonglet& song) {
    double Delay = 1.5;
    GroupSong *gsong = nullptr;
    LoopSong *lsong = nullptr;

    gsong = Create_Triad_Pattern(song, Delay);
    OffsetBoxBase *gobx = gsong->Spawn_OffsetBox();
    // gobx->OctaveY = 1;

    lsong = new LoopSong();
    lsong->Add_SubSong(gobx);

    lsong->Set_Interval(Delay * 4.0);// 4 * 1.5 seconds per cycle // lsong->Set_Delay(Delay * 4.0);
    lsong->Set_Beats(4);// lsong->Set_Duration(30.0);

    return lsong;
  }
  /* ********************************************************************************* */
  static LoopSong::Loop_OffsetBox* Create_Spooky_Loops() {
    double Duration=1.5, OctaveOffset=0.0, LoudnessOffset=1.0;
    Voice *voz = Create_Bent_Note(0.01, Duration, OctaveOffset, LoudnessOffset);// fill in voice
    LoopSong *loop = Create_Unbound_Triad_Rhythm_Loops(*voz);
    LoopSong::Loop_OffsetBox *Loop_Handle = loop->Spawn_OffsetBox();
    return Loop_Handle;
  }
  /* ********************************************************************************* */
  static GroupSong::Group_OffsetBox* Create_Spooky() {
    double Duration=1.5, OctaveOffset=0.0, LoudnessOffset=1.0;
    Voice *voz = Create_Bent_Note(0.01, Duration, OctaveOffset, LoudnessOffset);// fill in voice
    GroupSong *loop = Create_Unbound_Triad_Rhythm(*voz);
    GroupSong::Group_OffsetBox *Loop_Handle = loop->Spawn_OffsetBox();
    return Loop_Handle;
  }
  /* ********************************************************************************* */
  static LoopSong::Loop_OffsetBox* Create_Loop() {
    double Duration=1.0, OctaveOffset=0.0, LoudnessOffset=1.0;
    double XLoc = 0.1, YLoc = 0.1;
    int NumPoints = 5;
    LoopSong *loop = new LoopSong();

    Voice *voz = new Voice();
    Create_Long_Note(voz, NumPoints, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *vobx = voz->Spawn_OffsetBox();
    vobx->MoveTo(XLoc, YLoc);

    loop->Add_SubSong(vobx);

    loop->Set_Beats(8);
    loop->Set_Interval(Duration);

    LoopSong::Loop_OffsetBox *Loop_Handle = loop->Spawn_OffsetBox();
    return Loop_Handle;
  }
  /* ********************************************************************************* */
  static GroupSong::Group_OffsetBox* Create_Seed_Long() {
    double Duration=1.0, OctaveOffset=0.0, LoudnessOffset=1.0;
    double Increment = 1.0;
    double XLoc = 0.1, YLoc = 0.1;
    int NumPoints = 5;
    GroupSong *group = new GroupSong();

    // Do this 3 times, once for each voice type.
    Voice *voz = new Voice();
    Create_Long_Note(voz, NumPoints, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *vobx = voz->Spawn_OffsetBox();
    vobx->MoveTo(XLoc, YLoc); XLoc+=Increment;
    group->Add_SubSong(vobx);

    SampleVoice *svoz = new SampleVoice();
    svoz->Preset_Horn();
    Create_Long_Note(svoz, NumPoints, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *svobx = svoz->Spawn_OffsetBox();
    svobx->MoveTo(XLoc, YLoc); XLoc+=Increment;
    group->Add_SubSong(svobx);

    PluckVoice *pvoz = new PluckVoice();
    Create_Long_Note(pvoz, NumPoints, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *pvobx = pvoz->Spawn_OffsetBox();
    pvobx->MoveTo(XLoc, YLoc);
    group->Add_SubSong(pvobx);

    GroupSong::Group_OffsetBox *Group_Handle = group->Spawn_OffsetBox();
    return Group_Handle;
  }
  /* ********************************************************************************* */
  static GroupSong::Group_OffsetBox* Create_Seed() {
    double Duration=1.0, OctaveOffset=0.0, LoudnessOffset=1.0;
    double Increment = 0.1;
    double Loc = 0.1;
    GroupSong *group = new GroupSong();

    // Do this 3 times, once for each voice type.
    Voice *voz = new Voice();
    Create_Bent_Note(voz, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *vobx = voz->Spawn_OffsetBox();
    vobx->MoveTo(Loc, Loc); Loc+=Increment;
    group->Add_SubSong(vobx);

    SampleVoice *svoz = new SampleVoice();
    svoz->Preset_Horn();
    Create_Bent_Note(svoz, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *svobx = svoz->Spawn_OffsetBox();
    svobx->MoveTo(Loc, Loc); Loc+=Increment;
    group->Add_SubSong(svobx);

    PluckVoice *pvoz = new PluckVoice();
    Create_Bent_Note(pvoz, 0.01, Duration, OctaveOffset, LoudnessOffset);
    Voice::Voice_OffsetBox *pvobx = pvoz->Spawn_OffsetBox();
    pvobx->MoveTo(Loc, Loc);
    group->Add_SubSong(pvobx);

    GroupSong::Group_OffsetBox *Group_Handle = group->Spawn_OffsetBox();
    return Group_Handle;
  }
  /* ********************************************************************************* */
  static LoopSong* Create_LoopSong(ISonglet* song, int Beats) {
    OffsetBoxBase *obox = song->Spawn_OffsetBox();
    MetricsPacket mp;
    obox->Update_Guts(mp);
    double Delay = song->Get_Duration();
    LoopSong *lsong = new LoopSong();
    lsong->Add_SubSong(obox);

    lsong->Set_Interval(Delay);// 4 * 1.5 seconds per cycle // lsong->Set_Delay(Delay * 4.0);
    lsong->Set_Beats(Beats);

    return lsong;
  }
  /* ********************************************************************************* */
  static GroupSong* Create_GroupSong(ISonglet* song, int Beats) {
    OffsetBoxBase *obox = song->Spawn_OffsetBox();
    MetricsPacket mp;
    obox->Update_Guts(mp);
    GroupSong *gsong = new GroupSong();
    gsong->Add_SubSong(obox);
    return gsong;
  }
  /* ********************************************************************************* */
  static void Create_Tapered_Voice(Voice& voice, double TimeOffset, double Duration, double OctaveOffset, double LoudnessOffset, int numsteps) {
    double AttackTime = 0.01;
    Duration -= AttackTime;
    double midfrac;
    voice.Add_Note(TimeOffset, OctaveOffset, 0);
    for (int cnt = 0; cnt <= numsteps; cnt++) {
      midfrac = ((double) cnt) / (double) numsteps;
      voice.Add_Note(TimeOffset + AttackTime + (Duration * midfrac), OctaveOffset, LoudnessOffset * (1.0 - midfrac));
    }
  }
  /* ********************************************************************************* */
  static LoopSong::Loop_OffsetBox *Simple_Loop() {// under construction
    LoopSong *MainLoop = new LoopSong();
    Voice *voz;
    double Duration=1.0, OctaveOffset=0.0, LoudnessOffset=1.0; int numsteps = 3;
    voz = Create_Bent_Note(0.01, Duration, OctaveOffset, LoudnessOffset);// fill in voice
//    voz = new Voice();
//    Create_Tapered_Voice(*voz, 0.0, Duration, OctaveOffset, LoudnessOffset, numsteps);// fill in voice

    Voice::Voice_OffsetBox *vobox = voz->Spawn_OffsetBox(); MainLoop->Add_SubSong(vobox);
    vobox->TimeX = 0.1; vobox->OctaveY = 0.1;
    // vobox->Assign(0.1, 0.1, 1.0, 1.0);// better this way?
    LoopSong::Loop_OffsetBox *lobox = MainLoop->Spawn_OffsetBox();
    lobox->OctaveY = 4.0;

    SoundFloat LoopInterval = vobox->Get_Duration();// 0.123456;
    MainLoop->Set_Interval(LoopInterval);
    MainLoop->Set_Beats(16);
    printf("LoopInterval:%f\n", LoopInterval);

    //MainLoop->Add_SubSong(voz, (SoundFloat)0.0, (SoundFloat)4.0, (SoundFloat)1.0);// this should work! inherits from GroupSong.

    if (true) {
      Config conf;
      MetricsPacket metrics;
      metrics.MyProject = &conf;
      metrics.MaxDuration = 0.0;
      metrics.FreshnessTimeStamp = 1;
      printf("Simple_Loop Update_Guts:\n");
      MainLoop->Update_Guts(metrics);
      printf("Simple_Loop Update_Guts done.\n");
    }

    return lobox;
  }
  /* ********************************************************************************* */
  static OffsetBoxBase* Recurse(ArrayList<ISonglet*>& palette, int Depth){// Attempted random song maker, under construction.
    ISonglet *child=nullptr, *grandkid=nullptr;//  does this really not make circular references?
    Voice *voz; GroupSong *gsong; LoopSong *lsong;
    OffsetBoxBase *GrandkidHandle;
    int randex;
    // double PaletteOdds = Math.max(palette.size()), 0.001);
    double MajorMinorOdds = 0.5;
    double MajorMinorKey = 0.0;
    double NumWays = 0;
    double VoiceThresh = (NumWays += 0.1);// Voice
    // double PluckVoiceThresh = (NumWays += 0.1);// PluckVoice
    double PaletteThresh = (NumWays += palette.size());
    double LoopThresh = (NumWays += 1.0);// Loop
    double GroupThresh = (NumWays += 1.0);// Group
    double MajorThresh = (NumWays += MajorMinorOdds);// Major chord
    double MinorThresh = (NumWays += MajorMinorOdds);// Minor chord

    PaletteThresh /= NumWays; VoiceThresh /= NumWays; // PluckVoiceThresh /= NumWays;
    LoopThresh /= NumWays; GroupThresh /= NumWays; MajorThresh /= NumWays; MinorThresh /= NumWays;

    Depth++;

    double dice = Math::frand();
    if (dice<VoiceThresh || Depth>3){// end with a leaf
      if (Math::frand() < 0.5) {
        printf("Voice\n");
        child = voz = new Voice();
      } else {
        printf("PluckVoice\n");
        child = voz = new PluckVoice();
      }
      double Duration=0.25, OctaveOffset=0.0, LoudnessOffset=1.0; int numsteps = 3;
      Duration = (Math::frand() * 0.20) + 1.2;
      Create_Tapered_Voice(*voz, 0.0, Duration, OctaveOffset, LoudnessOffset, numsteps);// fill in voice
    } else if (dice<PaletteThresh){
      printf("Palette\n");
      if (palette.size()>0) {
        randex = std::rand() % palette.size();
        child = palette[randex];
      }
    } else if (dice<LoopThresh){
      printf("Loop\n");
      child = lsong = new LoopSong();
      GrandkidHandle = Recurse(palette, Depth);
      lsong->Add_SubSong(GrandkidHandle);
      grandkid = GrandkidHandle->GetContent();
      if (!palette.Contains(grandkid)){
        palette.Add(grandkid);
      }
      // Set interval and beats
      SoundFloat random = (Math::frand()*1.5) + 0.25;// 0.123456;//
      lsong->Set_Interval(random);
      randex = (std::rand() % 12)+2;
      lsong->Set_Beats(randex);
    } else if (dice<GroupThresh){
      printf("Group\n");
      child = gsong = new GroupSong();
      randex = (std::rand() % 12)+2;// arbitrary max members of a group
      for (int cnt=0; cnt<randex; cnt++){
        GrandkidHandle = Recurse(palette, Depth);
        gsong->Add_SubSong(GrandkidHandle);
        grandkid = GrandkidHandle->GetContent();
        if (!palette.Contains(grandkid)){
          palette.Add(grandkid);
        }
      }
    } else if (dice<MajorThresh){
      printf("Major\n");
      GrandkidHandle = Recurse(palette, Depth);
      grandkid = GrandkidHandle->GetContent();
      child = gsong = MakeMajor(*grandkid, MajorMinorKey);
      delete GrandkidHandle;// yuck, hacky
      if (!palette.Contains(grandkid)){
        palette.Add(grandkid);
      }
    } else if (dice<MinorThresh){
      printf("Minor\n");
      GrandkidHandle = Recurse(palette, Depth);
      grandkid = GrandkidHandle->GetContent();
      child = gsong = MakeMinor(*grandkid, MajorMinorKey);
      delete GrandkidHandle;
      if (!palette.Contains(grandkid)){
        palette.Add(grandkid);
      }
    }

    SoundFloat random_time = (Math::frand()*1.5) + 0.2;
    SoundFloat random_octave = (Math::frand()*2.0) + 0.1;
    SoundFloat random_loudness=1.0;
    OffsetBoxBase *obox = child->Spawn_OffsetBox();
    obox->TimeX = random_time;
    obox->OctaveY = random_octave;
    obox->LoudnessFactor = random_loudness;
    return obox;
  }
  /* ********************************************************************************* */
  static LoopSong* MakeRandom(){// under construction
    ArrayList<ISonglet*> palette; // things that we've created so far

    OffsetBoxBase *obox = Recurse(palette, 0);
    obox->OctaveY = 4.0;

    if (true) {
      Config conf;
      MetricsPacket metrics;
      metrics.MaxDuration = 0.0;
      metrics.MyProject = &conf;
      metrics.FreshnessTimeStamp = 1;
      ISonglet *songlet = obox->GetContent();
      printf("MakeRandom Update_Guts:\n");
      songlet->Update_Guts(metrics);
      printf("MakeRandom Update_Guts done.\n");
    }

    LoopSong *MainLoop = new LoopSong();
    MainLoop->Add_SubSong(obox);

    //Voice voz;
    //MainLoop->Add_SubSong(voz, (SoundFloat)0.0, (SoundFloat)4.0, (SoundFloat)1.0);// this should work! inherits from GroupSong.
    //MainLoop.Add_SubSong(*songlet, (SoundFloat)0.0, (SoundFloat)4.0, (SoundFloat)1.0);// this should work! inherits from GroupSong.
    SoundFloat LoopInterval = obox->GetContent()->Get_Duration();// 0.123456;
    //LoopInterval = 2.0;
    MainLoop->Set_Interval(LoopInterval);
    MainLoop->Set_Beats(2);
    printf("LoopInterval:%f\n", LoopInterval);
    return MainLoop;
  }
};

#endif // PatternMaker_hpp

/*
crash on undo
UpdateBoundingBoxLocal
UpdateBoundingBoxLocal
GraphicBox UpdateBoundingBoxLocal
GlCanvas KeyPressed:90: evtobj:0x558f0ee72ba0:
this->RootSinger->Draw_Me(dc);
Segmentation fault (core dumped)
john@FatFreddy:~/Documents/Projects/VectunesGui$
*/
