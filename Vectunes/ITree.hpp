#ifndef ITree_hpp
#define ITree_hpp

#include "Config.hpp"
/* ********************************************************************************* */
class MetricsPacket {// MetricsPacket is a ParamBlob
public:
  SoundFloat MaxDuration = 0.0;
  Config* MyProject = nullptr;
  int FreshnessTimeStamp = 1;
  Transformer GlobalTransform;// Global Transform is transformation to and from pixels
  //int RecurseDepth = 0;
  void Reset(){
    MaxDuration = 0.0;
    FreshnessTimeStamp = 1;
    //RecurseDepth = 0;
    GlobalTransform.Clear();
  }
};

/* ********************************************************************************* */
class ITree {
public:
  virtual void Update_Guts(MetricsPacket& metrics) = 0;
};

#endif // ITree_hpp
