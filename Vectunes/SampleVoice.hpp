#ifndef SampleVoice_hpp
#define SampleVoice_hpp

#include "Voice.hpp"

/**
 *
 * @author MultiTool
 */
class SampleVoice : public Voice {
public:
  Wave MySample;
public:
  boolean Looped = true;
  String SamplePreset = "Horn";
  StringConst HornSampleName = "Horn";//static constexpr auto HornSampleName = "Horn";
  StringConst SamplePresetName = "SamplePreset";
  StringConst LoopedName = "Looped";// for serialization
//  static constexpr std::vector<double> Horn = {
//    -0.030499, 0.019834, 0.073192, 0.130249, 0.191767, 0.257594, 0.32553, 0.394627, 0.465558, 0.5371, 0.607145, 0.676915, 0.74375, 0.806002, 0.861286, 0.909969, 0.950431, 0.978822, 0.995569, 1
//  };
  static constexpr double Horn[] = {// instrument waveform sample
    -0.030499, 0.019834, 0.073192, 0.130249, 0.191767, 0.257594, 0.32553, 0.394627, 0.465558, 0.5371, 0.607145, 0.676915, 0.74375, 0.806002, 0.861286, 0.909969, 0.950431, 0.978822, 0.995569, 1,
    0.991229, 0.968828, 0.932309, 0.882678, 0.819418, 0.745462, 0.661971, 0.569922, 0.472098, 0.372532, 0.273027, 0.174072, 0.078021, -0.011949, -0.094432, -0.168969, -0.235621, -0.29228, -0.338457, -0.374152,
    -0.399456, -0.41495, -0.420207, -0.416631, -0.404529, -0.384634, -0.35826, -0.326722, -0.290019, -0.25029, -0.209125, -0.168052, -0.126245, -0.084775, -0.046299, -0.01308, 0.016411, 0.041104, 0.05999, 0.071389,
    0.076248, 0.075118, 0.068394, 0.056109, 0.039179, 0.018611, -0.005256, -0.031508, -0.059012, -0.086547, -0.112982, -0.138806, -0.161023, -0.179696, -0.194579, -0.206192, -0.212731, -0.21429, -0.21212, -0.206497,
    -0.19684, -0.182263, -0.165363, -0.146629, -0.126398, -0.1047, -0.082636, -0.060785, -0.038537, -0.017756, 0.000428, 0.016014, 0.030285, 0.044435, 0.056078, 0.065002, 0.07142, 0.075729, 0.079213, 0.08071,
    0.081474, 0.081077, 0.079916, 0.078724, 0.076279, 0.074048, 0.070809, 0.067202, 0.062832, 0.058554, 0.054917, 0.051372, 0.047277, 0.042021, 0.037253, 0.032364, 0.027229, 0.01962, 0.011491, 0.003087,
    -0.005501, -0.015464, -0.02671, -0.039576, -0.053573, -0.068578, -0.083491, -0.099383, -0.11616, -0.133396, -0.150908, -0.168694, -0.186358, -0.202158, -0.218324, -0.234338, -0.249862, -0.261964, -0.271835, -0.280667,
    -0.288277, -0.29387, -0.297476, -0.299676, -0.301601, -0.303007, -0.302243, -0.300073, -0.297934, -0.297476, -0.297017, -0.295489, -0.293839, -0.291608, -0.289561, -0.286841, -0.28354, -0.278834, -0.271683, -0.261109,
    -0.246806, -0.228165, -0.204969, -0.177067, -0.144582, -0.108918, -0.069097
  };
  static constexpr size_t HornSize = (sizeof(Horn)/sizeof(double));
  /* ********************************************************************************* */
  SampleVoice() : Voice() {
    //this->FreshnessTimeStamp = 0;
    this->mytype = 8;
    this->MySample.Init_Time(0, 1.0, Globals::SampleRate, 0.6);
  }
  /* ********************************************************************************* */
  class SampleVoice_Singer : public Voice_Singer {
  public:
    SampleVoice *MySampleVoice;
    Wave *MySample = null;
    /* ********************************************************************************* */
    SampleVoice_Singer(SampleVoice *MySampleVoice0) : Voice_Singer(MySampleVoice0) {
      this->MySample = &(this->MySampleVoice->MySample);
      this->MySampleVoice = MySampleVoice0;
    }
    /* ********************************************************************************* */
    double GetWaveForm(TimeFloat SubTimeAbsolute) override {
      return this->MySample->GetResample(SubTimeAbsolute * BaseFreq);
    }
  };
  /* ********************************************************************************* */
  class SampleVoice_Looped_Singer : public SampleVoice_Singer {
  public:
    SampleVoice_Looped_Singer(SampleVoice *SVoice) : SampleVoice_Singer(SVoice) {
    }
    /* ********************************************************************************* */
    double GetWaveForm(TimeFloat SubTimeAbsolute) override {
      return this->MySample->GetResampleLooped(SubTimeAbsolute * BaseFreq);
    }
  };
  /* ********************************************************************************* */
  class SampleVoice_OffsetBox : public Voice_OffsetBox {// location box to transpose in pitch, move in time, etc.
  public:
    SampleVoice *SampleVoiceContent;
    StringConst ObjectTypeName = "SampleVoice_OffsetBox";//static //  static constexpr std::string_view
    /* ********************************************************************************* */
    SampleVoice_OffsetBox(SampleVoice *songlet) : Voice_OffsetBox(songlet) {
      this->mytype = 12;
      this->SampleVoiceContent = songlet;
    }
    /* ********************************************************************************* */
    SampleVoice* GetContent() override {
      return this->SampleVoiceContent;
    }
    /* ********************************************************************************* */
    void Attach_Songlet(SampleVoice *songlet) {// for serialization
      this->Assign_Content(this->VoiceContent = this->SampleVoiceContent = songlet); // to do: clean up this mess.
      songlet->Ref_Songlet();
    }
    /* ********************************************************************************* */
    SampleVoice_Singer* Spawn_Singer() override {// always always always override this
      SampleVoice_Singer *Singer = this->SampleVoiceContent->Spawn_Singer();
      Singer->MyOffsetBox = this;
      return Singer;
    }
    /* ********************************************************************************* */
    SampleVoice_OffsetBox* Clone_Me() override {// always override this thusly
      SampleVoice_OffsetBox *child = this->SampleVoiceContent->Spawn_OffsetBox();
      child->Copy_From(*this);
//      SampleVoice_OffsetBox *child = new SampleVoice_OffsetBox();
//      child->Copy_From(*this);
//      child->VoiceContent = child->SampleVoiceContent = this->SampleVoiceContent;// iffy, remove?
      return child;
    }
    /* ********************************************************************************* */
    SampleVoice_OffsetBox* Deep_Clone_Me(CollisionLibrary &HitTable) override {// ICloneable
      SampleVoice *VoiceFork = this->SampleVoiceContent->Deep_Clone_Me(HitTable);
      SampleVoice_OffsetBox *MyFork = VoiceFork->Spawn_OffsetBox();
      MyFork->Copy_From(*this);
      return MyFork;
    }
    /* ********************************************************************************* */
    void BreakFromHerd(CollisionLibrary &HitTable) override {// for compose time. detach from my songlet and attach to an identical but unlinked songlet
      SampleVoice *clone = this->SampleVoiceContent->Deep_Clone_Me(HitTable);
      if (this->SampleVoiceContent->UnRef_Songlet() <= 0) {
        delete this->SampleVoiceContent;
        this->SampleVoiceContent = nullptr;
      }
      this->Attach_Songlet(clone);
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
      //JsonParse::HashNode *SelfPackage = Voice_OffsetBox::Export(HitTable);// untested
      JsonParse::HashNode *SelfPackage = OffsetBoxBase::Export(HitTable);// ready for test?

//      JsonParse::LiteralNode *test;
//      test = (JsonParse::LiteralNode *)SelfPackage->Get(Globals::ObjectTypeName);// another cast!
//      std::cout << "Literal:" << test->ToJson() << "\n";

      SelfPackage->AddSubPhrase(Globals::ObjectTypeName, PackStringField(ObjectTypeName));

//      test = (JsonParse::LiteralNode *)SelfPackage->Get(Globals::ObjectTypeName);// another cast!
//      std::cout << "Literal:" << test->ToJson() << "\n";

      return SelfPackage;
    }
    void ShallowLoad(JsonParse::HashNode &phrase) override {// ITextable
      Voice_OffsetBox::ShallowLoad(phrase);
    }
  };
  /* ********************************************************************************* */
  void AttachWaveSample(Wave *Sample, double BaseFrequency) {
    this->MySample.Copy_From(*Sample);
    this->BaseFreq = BaseFrequency;
  }
  /* ********************************************************************************* */
  static Wave* Create_Horn_Sample() {// MEMORY LEAK ALL TO HELL the way this is called
    // to do: decide if SampleVoice owns its samples and must delete them, or shares them and uses refcount(?).
    Wave *MySample = new Wave();// a default
    int Length = HornSize;//std::size(Horn);// (sizeof(Horn)/sizeof(*Horn));
    MySample->Ingest(Horn, Length, Globals::SampleRate);
    return MySample;
  }
  /* ********************************************************************************* */
  void Preset_Horn() {
    //this->SamplePreset = HornSampleName; // snox
    if (true){
      this->AttachWaveSample(Create_Horn_Sample(), Globals::BaseFreqC0 / 265.0);
    }
  }
  /* ********************************************************************************* */
  SampleVoice_OffsetBox* Spawn_OffsetBox() override {// for compose time
    //printf("new SampleVoice_OffsetBox();\n");
    SampleVoice_OffsetBox *lbox = new SampleVoice_OffsetBox(this);// Deliver an OffsetBox specific to this type of phrase.
    this->Ref_Songlet();
    //printf("Attach_Songlet\n");
    //lbox->Attach_Songlet(this);
    return lbox;
  }
  /* ********************************************************************************* */
  SampleVoice_Singer* Spawn_Singer() override {// for render time
    // Deliver one of my singers while exposing specific object class.
    // Handy if my parent's singers know what class I am and want special access to my particular type of singer.
    SampleVoice_Singer *singer;
    if (this->Looped) {
      singer = new SampleVoice_Looped_Singer(this);
    } else {
      singer = new SampleVoice_Singer(this);
    }
    //singer->MyProject = this->MyProject;// inherit project
    singer->Set_Project(this->MyProject);// inherit project
    singer->BaseFreq = this->BaseFreq;
    singer->MySample = &(this->MySample);
    return singer;
  }
  /* ********************************************************************************* */
  SampleVoice* Clone_Me() override {// ICloneable
    SampleVoice *child = new SampleVoice();
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  SampleVoice* Deep_Clone_Me(CollisionLibrary &HitTable) override {// ICloneable
    SampleVoice *child;
    CollisionItem *ci = HitTable.GetItem(this);
    if (ci == nullptr) {
      child = new SampleVoice();
      ci = HitTable.InsertUniqueInstance(this);
      ci->Hydrated = child;
      child->Copy_From(*this);
      child->Copy_Children(*this, HitTable);
    } else {// pre exists
      child = (SampleVoice*) ci->Hydrated;// another cast!
    }
    return child;
  }
  /* ********************************************************************************* */
  void Copy_From(const SampleVoice &donor) {
    Voice::Copy_From(donor);
    this->MySample.Copy_From(donor.MySample);// maybe we should clone the sample too?
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *phrase = Voice::Export(HitTable);// to do: export my wave too
    this->SerializeMyContents(HitTable, *phrase);
    phrase->AddSubPhrase(LoopedName, ITextable::PackBoolField(this->Looped));
    if ( !(Ops::Trim(this->SamplePreset) == "") ) {
      phrase->AddSubPhrase(SamplePresetName, ITextable::PackStringField(this->SamplePreset));
    }
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode &phrase) override {// ITextable
    Voice::ShallowLoad(phrase);
    this->Looped = GetBoolField(phrase, LoopedName, true);
    this->SamplePreset = phrase.GetField(SamplePresetName, "");// default to no horn
    if (this->SamplePreset == HornSampleName) {
      this->Preset_Horn();
    }
  }
};

constexpr double SampleVoice::Horn[];

#endif // SampleVoice_hpp
