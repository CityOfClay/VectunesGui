#ifndef AudProject_hpp
#define AudProject_hpp

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fstream>

#include "LoopSong.hpp"
#include "FileMapper.hpp"
#include "Audio.hpp"

/**
 *
 * @author MultiTool
*/
class AudProject: public IDeletable {
public:
  OffsetBoxBase* AudioRoot = nullptr;
  GraphicBox::Graphic_OffsetBox* GraphicRoot = nullptr;
  GraphicBox* GrSong = nullptr;
  int UpdateCounter = 1;
  StringConst TreePhraseName = "Tree", LibraryPhraseName = "Library";// for serialization
  double VersionNum = 0.1;// song file version
  StringConst VersionNumName = "Version";// for serialization
  // int SampleRate = Globals::SampleRate;
  Config conf;
  DefaultAudioFeed aud;
  /* ********************************************************************************* */
  AudProject() {
    this->GrSong = new GraphicBox();
    this->GraphicRoot = this->GrSong->Spawn_OffsetBox();
  }
  /* ********************************************************************************* */
  virtual ~AudProject() {
    delete this->GraphicRoot;
    this->AudioRoot = nullptr; // unnecessary
  }
  /* ********************************************************************************* */
  void Wrap_For_Graphics(OffsetBoxBase* obox) {
    //obox->GetContent()->Set_Project(this);
    printf("GetContent\n");
    obox->GetContent()->MyProject = &(this->conf);
    printf("AudioRoot\n");
    this->AudioRoot = obox;
    printf("Attach_Content\n");
    std::cout << "GraphicRoot:" << this->GraphicRoot << "\n";
    this->GraphicRoot->Attach_Content(this->AudioRoot);
    printf("Update_Guts\n");
    this->Update_Guts();
    printf("done\n");
  }
  /* ********************************************************************************* */
  void Update_Guts() {
    MetricsPacket metrics;
    metrics.MaxDuration = 0.0;
    metrics.MyProject = &(this->conf);
    metrics.FreshnessTimeStamp = ++UpdateCounter;// Increment UpdateCounter every time it is used.
    this->GraphicRoot->Update_Guts(metrics);
  }
  /* ********************************************************************************* */
  void Create_For_Graphics() {}
  /* ********************************************************************************* */
  AudProject* Fork_Me() {// Used for UndoStack. not finished or functional.
    AudProject* fork = new AudProject();
    /*
    order
    shallow clone me.
    and graphicroot?  clone copies from my own graphic root, and copies from my own graphic body.

    maybe instead just fork the child of graphicbox and store that.

    problem: every songlet points to audproject. fork them and they point to the old audproject. maybe ok in one session?

    so maybe better to not fork audproject itself for undo, instead for the contents of graphicbox/graphic song.

    you can undo or redo all you want, up and down the stack.
    but if you undo and make an edit, the whole redo side of the stack should be deleted.

    */
    OffsetBoxBase *obx = this->GraphicRoot->Clone_Me();// snox this is an awkward way to do this. better to call deep_clone directly.
    printf("AudProject Fork_Me\n");
    CollisionLibrary HitTable;
    obx->BreakFromHerd(HitTable);
    // fork->Wrap_For_Graphics(obx);
    return fork;
  }
  /* ********************************************************************************* */
  void Compose_Test() {}
  /* ********************************************************************************* */
  void Audio_Test() {
    ISonglet *songlet = this->AudioRoot->GetContent();
    double Duration = songlet->Get_Duration();
    SingerBase *singer = this->AudioRoot->Spawn_Singer();
    singer->SampleRate = Globals::SampleRate;
    Wave wav(Globals::SampleRate);
    singer->Render_To(Duration, wav);
    wav.Normalize();

    cout << "Audio_Test Start.\n";
    aud.Start();
    cout << "Audio_Test Feed.\n";
    aud.Feed(wav);
    cout << "Audio_Test Stop.\n";
    aud.Stop();
    cout << "Audio_Test done.\n";
  }
  /* ********************************************************************************* */
  void SaveWav(const String& WavFileName) {
    ISonglet *songlet = this->GraphicRoot->GetContent();
    double Duration = songlet->Get_Duration();
    SingerBase *singer = this->GraphicRoot->Spawn_Singer();
    singer->SampleRate = Globals::SampleRate;
    Wave wav(Globals::SampleRate);
    singer->Render_To(Duration, wav);
    wav.SaveToWav(WavFileName);
  }
  /* ********************************************************************************* */
  double Get_Duration() {
    this->Update_Guts();
    ISonglet *songlet = this->AudioRoot->GetContent();
    double Duration = songlet->Get_Duration();
    return Duration;
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Jsonify() {// serialize to hash tree
    JsonParse::HashNode *MainPhrase = new JsonParse::HashNode();
    CollisionLibrary HitTable;
    JsonParse::HashNode *Tree = this->GraphicRoot->Export(HitTable);
    JsonParse::HashNode *Library = HitTable.ExportJson();
    MainPhrase->AddSubPhrase(VersionNumName, ITextable::PackNumberField(this->VersionNum));
    MainPhrase->AddSubPhrase(TreePhraseName, Tree);
    MainPhrase->AddSubPhrase(LibraryPhraseName, Library);
    return MainPhrase;
  }
  /* ********************************************************************************* */
  void UnJsonify(JsonParse::HashNode *RootJNode) {// deserialize from hash tree
    JsonParse jparse;
    GraphicBox::Graphic_OffsetBox* GraphicRootTemp = nullptr;
    this->VersionNum = ITextable::GetNumberField(*RootJNode, VersionNumName, -1.0);// -1 default means error
    FileMapper fmap;
    GraphicRootTemp = fmap.Create(RootJNode);
    if (GraphicRootTemp!=nullptr){
      delete this->GraphicRoot;
      this->GraphicRoot = GraphicRootTemp;
      this->GrSong = this->GraphicRoot->GetContent();
      this->AudioRoot = this->GrSong->Get_Content();
      this->Update_Guts();
    } else {
      std::cout << "Error parsing file.\n";
    }
  }
  /* ********************************************************************************* */
  String Textify() {// serialize to text
    String JsonTxt;
    JsonParse::HashNode *MainPhrase = Jsonify();
    JsonTxt = MainPhrase->ToJson();
    delete MainPhrase;
    return JsonTxt;
  }
  /* ********************************************************************************* */
  void UnTextify(const String& JsonTxt) {// deserialize from text
    JsonParse jparse;
    std::cout << this->AudioRoot << "\n";
    JsonParse::HashNode *RootJNode = jparse.Parse(JsonTxt);
    UnJsonify(RootJNode);
    delete RootJNode;
  }
  /* ********************************************************************************* */
  bool ReadFile(const String& FilePath) {
    using namespace std;
    String JsonTxt;
    string line;
    ifstream myfile(FilePath);
    if (myfile.is_open()){
      while (getline(myfile,line)) {
        cout << line << '\n';
        JsonTxt += line;
      }
      myfile.close();
      this->UnTextify(JsonTxt);
      return true;
    } else {
      cout << "Jsong file is not open!\n";
      return false;
    }
  }
  /* ********************************************************************************* */
  bool WriteFile(const String& FilePath) {
    using namespace std;
    String JsonTxt = this->Textify();
    ofstream myfile(FilePath);
    if (myfile.good()) {
      myfile << JsonTxt;
      myfile.close();// to do: do we do this even if the file is not good?
      return true;
    } else {
      cout << "Cannot create Jsong file!\n";
      return false;
    }
  }
  /* ********************************************************************************* */
  /*  Generic file/directory utilities - to do: move to its own namespace.             */
  /* ********************************************************************************* */
  static bool RealPath(const std::string& FPath, std::string& results) {// realpath for std::string
    int len = PATH_MAX;//256;//FPath.size() + 1;
    char resultbuf[len];
    char *resultptr = realpath(FPath.c_str(), resultbuf);
    if (resultptr==nullptr){// probably invalid path
      return false;
    }
    results.assign(resultptr);
    Ops::Replace(results, "\\", "/");// convert all paths to unix
    return true;
  }
  /* ********************************************************************************* */
  static void Get_Directory(const std::string& DirPath, std::vector<String>& txtvec) {
    String DirPathTemp(Standardize_Path(DirPath));
    DIR *dirp = opendir(DirPathTemp.c_str());// http://www.martinbroadhurst.com/list-the-files-in-a-directory-in-c.html
    struct dirent *dp;
    txtvec.clear();
    while ((dp = readdir(dirp)) != NULL) {
      txtvec.push_back(DirPathTemp + dp->d_name);
    }
    closedir(dirp);
  }
  /* ********************************************************************************* */
  static void Print_List(std::vector<String>& List) {
    std::cout << "******************************************************** \n";
    for (int cnt=0;cnt<List.size();cnt++){
      std::cout << List.at(cnt) << "\n";
    }
    std::cout << "\n";
  }
  /* ********************************************************************************* */
  static void Filter_List(std::vector<String> &List, const std::vector<String> &Pattern) {
    std::vector<String> results;
    for (int cnt=0;cnt<List.size();cnt++){
      std::cout << List.at(cnt) << "\n";
      for (int pcnt=0;pcnt<Pattern.size();pcnt++){
        if (Ops::EndsWith(List.at(cnt), Pattern.at(pcnt))){
          results.push_back(List.at(cnt)); break;
        }
      }
    }
    List.assign(results.begin(), results.end());
  }
  /* ********************************************************************************* */
  static void Filter_List(std::vector<String> &List, const std::string &Pattern) {
    std::vector<String> results;
    for (int cnt=0;cnt<List.size();cnt++){
      std::cout << List.at(cnt) << "\n";
      if (Ops::EndsWith(List.at(cnt), Pattern)){
        results.push_back(List.at(cnt));
      }
    }
    List.assign(results.begin(), results.end());
  }
  /* ********************************************************************************* */
  static std::string Standardize_Path(const std::string& DirPath) {// This would be unnecessary if std::filesystem worked.
    String DirPathTemp(DirPath);
    DirPathTemp = Ops::Trim(DirPathTemp);
    if (DirPathTemp.size()==0){// an empty dir "" becomes current dir "./"
      DirPathTemp="./";
    } else {// A problem is windows vs unix. could first search and replace \ with /, but legit unix file names can contain \.
  //#ifdef _WIN32 // This ifdef may be a bad idea. What if the OS is linux but the path is windows?
      Ops::Replace(DirPathTemp, "\\", "/");// convert all paths to unix
  //#endif // _WIN32
      if (!Ops::EndsWith(DirPathTemp, "/")){
        DirPathTemp += "/";
      }
    }
    return DirPathTemp;
  }
  /* ********************************************************************************* */
  static void Find_Jsong_Dir(const std::string& DirPath, std::vector<String>& Results) {
    String updir("../");// climb up directories until we find .jsong files, and render them to .wav
    std::vector<String> FoundList;
    String DirPathTemp = Standardize_Path(DirPath);
    String FilePath;
    int Limit = 4;// do not go up more than Limit directories
    int cnt = 0;
    for (cnt=0;cnt<Limit;cnt++){
      if (!RealPath(DirPathTemp, DirPathTemp)){ break; }
      DirPathTemp = Standardize_Path(DirPathTemp);
      Get_Directory(DirPathTemp, FoundList);
      // Filter_List(FoundList, "/jsong");
      Filter_List(FoundList, std::vector<String>{"/jsong"});
      if (FoundList.size()>0){ break; }
      DirPathTemp += updir;
    }
    Results.clear();
    if (FoundList.size()>0){// if we found the jsong dir
      DirPathTemp = FoundList.at(0);
      Get_Directory(DirPathTemp, Results);// Now search inside the jsong dir for *.jsong files.
      Filter_List(Results, std::vector<String>{".jsong", ".vt.json"});
    }
  }
  /* ********************************************************************************* */
  /*  End of generic file/directory utilities.                                         */
  /* ********************************************************************************* */

  /* ********************************************************************************* */
  boolean Create_Me() override { return 0; }
  void Delete_Me() override {}
};


#endif
