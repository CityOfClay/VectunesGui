#ifndef UndoStack_hpp
#define UndoStack_hpp

#include "AudProject.hpp"

// This will support an undo feature.
class UndoStack {
public:
  ArrayList<OffsetBoxBase*> Stack;
  int CurrentTop = 0;// CurrentTop is always 1 above the actual current snapshot entry.
  /* ********************************************************************************* */
  virtual ~UndoStack() {
    for (int cnt=0;cnt<Stack.size();cnt++) {
      delete Stack.get(cnt);
    }
    Stack.clear();
    CurrentTop = Integer::MIN_VALUE;// Wreck with impossible value.
  }
  /* ********************************************************************************* */
  void BackUp(OffsetBoxBase* Snapshot) { // Save a forked composition to stack.
    this->Truncate(CurrentTop);
    this->Stack.add(Snapshot);
    this->CurrentTop++;
  }
  /* ********************************************************************************* */
  void Truncate(int NextSize) {
    int len = Stack.size();
    for (int cnt=NextSize;cnt<len;cnt++) {
      delete Stack.get(cnt);// Here we rely on songlet refcounting to prevent disaster.
    }
    Stack.resize(NextSize);
  }
  /* ********************************************************************************* */
  OffsetBoxBase* Undo() {
    if (0 < CurrentTop) {
      OffsetBoxBase* Snapshot = Stack.get(--CurrentTop);// Here we rely on songlet refcounting to prevent disaster.
      return Snapshot->Clone_Me();
      // return Stack.get(--CurrentTop)->Clone_Me();// same idea. more compact?
    } else {
      return nullptr;
    }
  }
  /* ********************************************************************************* */
  OffsetBoxBase* Redo() {// not functional yet.
    if (CurrentTop < Stack.size()) {
      return Stack.get(CurrentTop++);
    } else {
      return nullptr;
    }
  }
  /* ********************************************************************************* */
  void SaveToFile(String& FileName) {// disk-persistent undo stack? maybe don't do this.
  }
};

#endif
