#ifndef DrawingContext_hpp
#define DrawingContext_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "KaffeeErsatz.hpp"
#include "IDeletable.hpp"
#include "CajaDelimitadora.hpp"
#include "Transformer.hpp"
/**
 *
 * @author MultiTool
 */

// Every IDrawable has a bounding box, and every DrawingContext also has a bounding box for clipping.
// Drawing will always be called from the top, and the bounding box will define what to draw.
/* ********************************************************************************* */
class DrawingContext: public IDeletable {// DrawingContext is a ParamBlob
private:
  SoundFloat Excitement;// to highlight animation, range 0 to 1.
public:
  Transformer GlobalTransform;// Global Transform is transformation to and from pixels
  CajaDelimitadora ClipBounds;
  double Brightness = 0.5;//1.0;// for highlighting while playing
  int RecurseDepth = 0;
  /* ********************************************************************************* */
  DrawingContext() { RecurseDepth = 0; }
  /* ********************************************************************************* */
  DrawingContext(const DrawingContext& Fresh_Parent, const Transformer& Fresh_Transform) {
    this->Create_Me();
    Fresh_Parent.Compound_To_Child(Fresh_Transform, *this);
  }
  /* ********************************************************************************* */
  virtual ~DrawingContext() {
    this->Delete_Me();
  }
  /* ********************************************************************************* */
  virtual void To_Screen(SoundFloat XLoc, SoundFloat YLoc, Point2D& ScreenPoint) const {
     this->GlobalTransform.UnMap(XLoc, YLoc, ScreenPoint);
  }
  /* ********************************************************************************* */
  virtual void To_Screen(const Point2D& AudioPoint, Point2D& ScreenPoint) const {
     this->GlobalTransform.UnMap(AudioPoint, ScreenPoint);
  }
  /* ********************************************************************************* */
  virtual void Compound(Transformer& other) {
    this->GlobalTransform.Compound(other);
  }
  /* ********************************************************************************* */
  virtual void Copy_From(const DrawingContext& donor) {
    //this->LocalTransform.Copy_From(donor.LocalTransform);
    this->GlobalTransform.Copy_From(donor.GlobalTransform);
    //this->gr = donor.gr;
    this->RecurseDepth = donor.RecurseDepth;
    // this->Excitement = donor.Excitement;// private
    this->ClipBounds.Copy_From(donor.ClipBounds);
  }
  /* ********************************************************************************* */
  virtual DrawingContext* Clone_Me() {
    DrawingContext* child = new DrawingContext();
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  virtual DrawingContext* Compound_Clone(const Transformer& Fresh_Transform) const {
    PROFILE_START;
    DrawingContext* child = new DrawingContext();
    Compound_To_Child(Fresh_Transform, *child);
    PROFILE_END(Globals::Dc_Compound_Clone_Hits);
    return child;
  }
  /* ********************************************************************************* */
  virtual void Compound_To_Child(const Transformer& Fresh_Transform, DrawingContext& child) const {
    //child->LocalTransform.Copy_From(Fresh_Transform);
    //child->LocalTransform.Print_Me("DC Copy_From child->LocalTransform:");
    child.GlobalTransform.Copy_From(this->GlobalTransform);
    child.GlobalTransform.Compound(Fresh_Transform);// inherit and further transform parent space
    child.GlobalTransform.Print_Me("DC Compound child->GlobalTransform:");
    // inherit and transform bounding box.
    Fresh_Transform.MapTo(this->ClipBounds, child.ClipBounds);// map to child (my) internal coordinates
    child.ClipBounds.Sort_Me();
    //child->gr = this->gr;
    child.Brightness = this->Brightness;
    child.RecurseDepth = this->RecurseDepth + 1;
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {return true;}// IDeletable
  void Delete_Me() override {// IDeletable
    this->ClipBounds.Delete_Me();// wreck everything
    //this->LocalTransform.Delete_Me();
    this->GlobalTransform.Delete_Me();
    //this->gr.Delete_Me();
  }
  // class Graphics2D
  virtual void SetLineWidth(double LineWidth){ }
  virtual void DrawLine(double X0, double Y0, double X1, double Y1){ }
  virtual void SetColor(const Color& color){ }
  virtual void SetColor(double Red, double Green, double Blue, double Alpha){
  }
  virtual void Render(){// https://en.wikibooks.org/wiki/OpenGL_Programming/GLStart/Tut3
  }
  ///////////////////////////////////////////////////////////////////////////////
  // draw a simple concave quad
  ///////////////////////////////////////////////////////////////////////////////
  virtual void DrawConcave(){
  }
  virtual void FillPolygon(double Outline[][2],int NumDrawPoints){// only convex polygons for now
    //std::this_thread::sleep_for (std::chrono::seconds(2));
  }
  virtual void DrawPolygon(double Outline[][2],int NumDrawPoints){
    //std::this_thread::sleep_for (std::chrono::seconds(2));
  }
  virtual void DrawPolygon(double *OutlineX,double *OutlineY,int NumDrawPoints){
    //std::this_thread::sleep_for (std::chrono::seconds(2));
  }
  virtual void DrawPolyline(double PolyLine[][2],int NumDrawPoints){
    //std::this_thread::sleep_for (std::chrono::seconds(2));
  }
  /* ********************************************************************************* */
  virtual void DrawCircle(double XCtr, double YCtr, double Radius){
  }
  /* ********************************************************************************* */
  virtual void DrawEllipse(double XCtr, double YCtr, double RadiusX, double RadiusY) {
  }
  /* ********************************************************************************* */
  virtual void DrawCenteredSquare(double x1, double y1, double Sidelength) {// https://stackoverflow.com/questions/38163188/openglc-draw-square
  }
  /* ********************************************************************************* */
  virtual void DrawRectangle(const CajaDelimitadora& box) {
  }
  /* ********************************************************************************* */
  virtual void DrawRectangleHollow(const CajaDelimitadora& box) {
  }
};

#endif
