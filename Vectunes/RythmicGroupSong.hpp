#ifndef RythmicGroupSong_hpp
#define RythmicGroupSong_hpp

#include "GroupSong.hpp"

// WORK IN PROGRESS - NOT FUNCTIONAL YET

// class RythmCellSong; // forward

/* ********************************************************************************* */
//class RythmCellSongBase: public GroupSong {};// used like a forward

/* ********************************************************************************* */
class RythmCell_OffsetBox_Base: public GroupSong::Group_OffsetBox {// used like a forward
public:// to do: inherit from GroupSong::Group_OffsetBox
  int MyIndex;
  RythmCell_OffsetBox_Base(GroupSong *songlet) : GroupSong::Group_OffsetBox(songlet) { }
  virtual ~RythmCell_OffsetBox_Base() { }
};

/* ********************************************************************************* */
class RythmicGroupSong_Base: public GroupSong {// used like a forward
public:
  /* ********************************************************************************* */
  RythmicGroupSong_Base() : GroupSong() { }
  virtual ~RythmicGroupSong_Base() {}
  /* ********************************************************************************* */
  virtual void Update_Rhythm(RythmCell_OffsetBox_Base& mbox) = 0;
  virtual void Update_OctaveY(SoundFloat OctaveY) = 0;
};


/* ********************************************************************************* */
/* After forwards */
/* ********************************************************************************* */

/* ********************************************************************************* */
class RythmCellSong: public GroupSong {
public:
  RythmicGroupSong_Base *GrandParentGroup = nullptr;
  /* ********************************************************************************* */
  class RythmCell_OffsetBox: public RythmCell_OffsetBox_Base {
  public:
    RythmicGroupSong_Base *ParentGroup = nullptr;
    RythmCellSong *CellContentSonglet = nullptr;
    StringConst ObjectTypeName = {"RythmCell_OffsetBox"};// For serialization.
    /* ********************************************************************************* */
    RythmCell_OffsetBox(RythmCellSong* songlet): RythmCell_OffsetBox_Base(songlet) {
      this->CellContentSonglet = songlet;
    }
    /* ********************************************************************************* */
    virtual ~RythmCell_OffsetBox() {}
    /* ********************************************************************************* */
    void MoveTo(TimeFloat XLoc, SoundFloat YLoc) override {
      if (XLoc <= 0.0) { XLoc = 0.0; }// don't go backward in time
      this->TimeX = XLoc; this->OctaveY = YLoc;
      this->ParentGroup->Update_Rhythm(*this);
    }
    /* ********************************************************************************* */
    RythmCellSong* GetContent() override { return CellContentSonglet; }
    /* ********************************************************************************* */
    RythmCell_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      RythmCellSong* CellFork = this->GetContent()->Deep_Clone_Me(HitTable);
      RythmCell_OffsetBox *clone = CellFork->Spawn_OffsetBox();
      clone->Copy_From(*this);
      return clone;
    }
  };
  /* ********************************************************************************* */
  class RythmCell_OffsetBox_Zero: public RythmCell_OffsetBox {// First item in loop has different dragging behavior.
  public:
    /* ********************************************************************************* */
    RythmCell_OffsetBox_Zero(RythmCellSong* songlet) : RythmCell_OffsetBox(songlet) {}
    /* ********************************************************************************* */
    virtual ~RythmCell_OffsetBox_Zero() {}
    /* ********************************************************************************* */
    RythmCell_OffsetBox_Zero* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      RythmCellSong* CellFork = this->GetContent()->Deep_Clone_Me(HitTable);
      RythmCell_OffsetBox_Zero *clone = CellFork->Spawn_OffsetBox_Zero();
      clone->Copy_From(*this);
      return clone;
    }
    /* ********************************************************************************* */
    void MoveTo(TimeFloat XLoc, SoundFloat YLoc) override {
//      if (XLoc < 0.0) { XLoc = 0.0; }// don't go backward in time
//      this->TimeX = XLoc; // this->TimeX = (XLoc <= 0.0) ? 0.0: XLoc;// don't go backward in time
      this->OctaveY = YLoc;
      // this->ParentLoop->Update_OctaveY(YLoc); //  Update all siblings' OctaveY values.
    }
  };
  /* ********************************************************************************* */
  RythmCell_OffsetBox* Spawn_OffsetBox() {//  For compose time. First item in sequence, has special needs.
    RythmCell_OffsetBox *obx = new RythmCell_OffsetBox(this);
    this->Ref_Songlet();
    obx->ParentGroup = this->GrandParentGroup;
    return obx;
  }
  /* ********************************************************************************* */
  RythmCell_OffsetBox_Zero* Spawn_OffsetBox_Zero() {//  For compose time. First item in sequence, has special needs.
    RythmCell_OffsetBox_Zero *obxz = new RythmCell_OffsetBox_Zero(this);
    this->Ref_Songlet();
    obxz->ParentGroup = this->GrandParentGroup;
    return obxz;
  }
  /* ********************************************************************************* */
  RythmCellSong* Clone_Me() override {// ICloneable
    RythmCellSong *clone = new RythmCellSong();
    clone->Copy_From(*this);
    return clone;
  }
  /* ********************************************************************************* */
  RythmCellSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    RythmCellSong *clone;
    clone = (RythmCellSong*)GroupSong::Deep_Clone_Me(HitTable);// another cast!
    return clone;

//    CollisionItem *ci = HitTable.GetItem(this);
//    if (ci == nullptr) {
//      clone = this->Clone_Me();
//      ci = HitTable.InsertUniqueInstance(this);
//      ci->Hydrated = clone;
//      OffsetBoxBase *SubSongHandle;
//      int len = this->SubSongs.size();
//      for (int cnt = 0; cnt < len; cnt++) {
//        SubSongHandle = this->SubSongs.at(cnt);
//        OffsetBoxBase *obox = SubSongHandle->Deep_Clone_Me(HitTable);
//        clone->Add_SubSong(obox);
//      }
//    } else {// pre exists
//      clone = (RythmCellSong*) ci->Hydrated;// another cast!
//    }
//    return clone;
  }
};


/* ********************************************************************************* */
/* ********************************************************************************* */

// This will be a GroupSong that is always evenly-spaced.
/* ********************************************************************************* */
class RythmicGroupSong: public RythmicGroupSong_Base {
  /* ********************************************************************************* */
  SoundFloat Interval = 0.25;// time spacing
  int Beats = 0;
  TimeFloat CntOffst = 0.0;//1.0;
  /* ********************************************************************************* */
  class RythmicGroup_OffsetBox: public GroupSong::Group_OffsetBox {
  public:
    RythmicGroupSong *MyContent = nullptr;
    RythmicGroup_OffsetBox(RythmicGroupSong* songlet) : GroupSong::Group_OffsetBox(songlet) {
      MyContent = songlet;
    }
    virtual ~RythmicGroup_OffsetBox() {}
  };
  /* ********************************************************************************* */
  RythmicGroupSong(): RythmicGroupSong_Base() {
    this->mytype = 13;
    this->Create_Me();

    {// Make first beat special, as it cannot be moved horizontally.
      this->Beats = 1;
      this->SubSongs.resize(this->Beats);
      // this->Add_Dummy_Link(this->SingleSong.Spawn_OffsetBox_Zero(), 0);
    }
  }
  /* ********************************************************************************* */
  virtual ~RythmicGroupSong() {}
  /* ********************************************************************************* */
  int Add_SubSong(OffsetBoxBase *obox) override {
    RythmCellSong *cell = new RythmCellSong();
    cell->Add_SubSong(obox);// When someone gives me an OffsetBox, I own it.  I must delete it.

    RythmCellSong::RythmCell_OffsetBox *cellobx = cell->Spawn_OffsetBox();
    // RythmCellSong::RythmCell_OffsetBox_Zero *cellobx = cell->Spawn_OffsetBox_Zero();
    cellobx->MyParentSong = this;

    int Loc = GroupSong::Tree_Search_Last(obox->TimeX, 0, this->SubSongs.size());// finds END OF place where time would be inserted or replaced

    if (Loc<=0) {
      // swap obox's cell with cell at index 0.
    } else {
      // just insert obox/cell where obox fits.
    }

    int dex = GroupSong::Add_SubSong(cellobx);
    // BIG PROBLEM: How do we know if the new subsong will be at Zero or normal?
    // would it be simpler to do the ugly if (index==0) { behave this way } else { behave that way } ?

    // first check if obox goes before index 0.
    // if so, swap with 0.

    // other problem, what about dragging? cannot drag before index zero, unless you are zero?
    // then what if I drag index zero forward?
    // no dragging is not a problem because here everything moves out of the way, accordion-style.

    // int Tree_Search_Last(SoundFloat Time, int minloc, int maxloc) {// finds END OF place where time would be inserted or replaced

    return dex;
  }
  /* ********************************************************************************* */
  void Remove_Child(OffsetBoxBase* child) override {
    // to do: if child is removed from index 0, convert the next one in line.
    // do not let list run less than 1.
    GroupSong::Remove_Child(child);
  }
  /* ********************************************************************************* */
  void Swap_Zero(RythmCellSong::RythmCell_OffsetBox_Zero* obxz, RythmCellSong::RythmCell_OffsetBox_Zero* obx) {
    RythmCellSong::RythmCell_OffsetBox *ObxNext = obxz->GetContent()->Spawn_OffsetBox();
    ObxNext->Copy_From(*obxz);
    RythmCellSong::RythmCell_OffsetBox_Zero *ObxZNext = obx->GetContent()->Spawn_OffsetBox_Zero();
    ObxZNext->Copy_From(*obx);
    // next, copy all coordinate values to children obxes.
    // then replace the old obxes in my subsong list with the new ones.
    // finally, delete the old obxes.
  }
  /* ********************************************************************************* */
  void Update_Rhythm_Zero(RythmCell_OffsetBox_Base& mbox) {
    int Index = mbox.MyIndex;// if
    int LastDex = this->SubSongs.size() - 1;
    TimeFloat FirstTimeX = this->SubSongs.at(0)->TimeX;
    TimeFloat LastTimeX = this->SubSongs.at(LastDex)->TimeX;
    TimeFloat PivotTimeX;
    // RythmCell_OffsetBox_Base *Anchor = this->SubSongs.at(LastDex);

    // now update everything around Anchor.
    int Dex0 = 1, Dex1 = this->SubSongs.size();

    if (mbox.MyIndex == 0) { PivotTimeX=LastTimeX; Dex0 = 0; Dex1 = LastDex; }
    else { PivotTimeX=FirstTimeX; Dex0 = 1; Dex1 = this->SubSongs.size(); }

    // ( (handle - PivotTimeX) / scale) + PivotTimeX // This line is junk.
    /*
    TimeFloat GrabX = mbox.TimeX;
    TimeFloat Interval = (GrabX - PivotTimeX) / Math::abs(GrabDex - PivotDex);
    for all
    me.TimeX = PivotTimeX + (me.MyIndex * Interval)
    */

    this->Interval = mbox.TimeX / (((SoundFloat)Index) + CntOffst);
    OffsetBoxBase *obox;// = this->SubSongs.at(Index);
    for (int cnt=Dex0;cnt<Index;cnt++) {// start at 1, don't adjust first beat.
      obox = this->SubSongs.at(cnt);
      //obox->OctaveY = mbox.OctaveY;
      obox->TimeX = this->Interval * ((SoundFloat)cnt + CntOffst);
    }
    Index++;
    for (int cnt=Index;cnt<Dex1;cnt++) {
      obox = this->SubSongs.at(cnt);
      //obox->OctaveY = mbox.OctaveY;
      obox->TimeX = this->Interval * ((SoundFloat)cnt + CntOffst);
    }
  }
  /* ********************************************************************************* */
  void Update_Rhythm(RythmCell_OffsetBox_Base& mbox) override {
    int Index = mbox.MyIndex;// like Refresh_Me_From_Beneath but specific for RythmCell_OffsetBox.
    this->Interval = mbox.TimeX / (((SoundFloat)Index) + CntOffst);
    OffsetBoxBase *obox;// = this->SubSongs.at(Index);
    for (int cnt=1;cnt<Index;cnt++) {// start at 1, don't adjust first beat.
      obox = this->SubSongs.at(cnt);
      //obox->OctaveY = mbox.OctaveY;
      obox->TimeX = this->Interval * ((SoundFloat)cnt + CntOffst);
    }
    Index++;
    for (int cnt=Index;cnt<this->SubSongs.size();cnt++) {
      obox = this->SubSongs.at(cnt);
      //obox->OctaveY = mbox.OctaveY;
      obox->TimeX = this->Interval * ((SoundFloat)cnt + CntOffst);
    }
  }
  /* ********************************************************************************* */
  void Update_OctaveY(SoundFloat OctaveY) override {
    OffsetBoxBase *obox;// = this->SubSongs.at(Index);
    for (int cnt=0;cnt<this->SubSongs.size();cnt++){
      obox = this->SubSongs.at(cnt);
      obox->OctaveY = OctaveY;
    }
  }
  /* ********************************************************************************* */
  RythmicGroup_OffsetBox* Spawn_OffsetBox() override {// For compose time.
    RythmicGroup_OffsetBox *child = new RythmicGroup_OffsetBox(this);
    this->Ref_Songlet();
    return child;
  }
  /* ********************************************************************************* */
  void Copy_From(const RythmicGroupSong& donor) {
    GroupSong::Copy_From(donor);
    this->Interval = donor.Interval;
    this->Beats = donor.Beats;
    this->CntOffst = donor.CntOffst;
  }
  /* ********************************************************************************* */
  RythmicGroupSong* Clone_Me() override {// ICloneable
    RythmicGroupSong *clone = new RythmicGroupSong();
    clone->Copy_From(*this);
    return clone;
  }
  /* ********************************************************************************* */
//  RythmicGroupSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
//    RythmicGroupSong *clone;
//    clone = (RythmicGroupSong*)GroupSong::Deep_Clone_Me(HitTable);// another cast!
//    return clone;
//
//    CollisionItem *ci = HitTable.GetItem(this);
//    if (ci == nullptr) {
//      clone = this->Clone_Me();
//      ci = HitTable.InsertUniqueInstance(this);
//      ci->Hydrated = clone;
//      OffsetBoxBase *SubSongHandle;
//      int len = this->SubSongs.size();
//      for (int cnt = 0; cnt < len; cnt++) {
//        SubSongHandle = this->SubSongs.at(cnt);
//        OffsetBoxBase *obox = SubSongHandle->Deep_Clone_Me(HitTable);
//        clone->Add_SubSong(obox);
//      }
//    } else {// pre exists
//      clone = (RythmicGroupSong*) ci->Hydrated;// another cast!
//    }
//    return clone;
//  }
};

#endif
