#ifndef MonkeyBox_hpp
#define MonkeyBox_hpp

//#include <std>
#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "IDrawable.hpp"
#include "CajaDelimitadora.hpp"
#include "IDeletable.hpp"
#include "ITextable.hpp"
#include "JsonParse.hpp"
#include "Transformer.hpp"
#include "Handle.hpp"

/**
 *
 * @author MultiTool
 *
 * MonkeyBox will be a base class for both OffsetBox and Voice Point.
 * It is any movable point that can contain other movable points.
 * The name is a place holder until something better comes to mind (PitchHandle?).
 *
 */
//class IDrawable;// forward
//class IDrawable::IMoveable; // forward
class CollisionLibrary;// forward
class ISonglet;// forward
/* ********************************************************************************* */
class MonkeyBox: public Transformer, public Handle {// location box to transpose in pitch, move in time, etc.  //IMonkeyBox,
public: // Rename MonkeyBox to PitchHandle?  As in any handle where Y is pitch.
  IContainer *MyParentSong = nullptr;// can do this but not used yet
  /* ********************************************************************************* */
  MonkeyBox() {
    this->Create_Me();
    this->AssignOctavesPerRadius(0.01);
  }
  virtual ~MonkeyBox(){this->Delete_Me();}
  /* ********************************************************************************* */
  virtual IContainer* GetParentSonglet() {
    return this->MyParentSong;
  }
  /* ********************************************************************************* */
  MonkeyBox* Clone_Me() override {// snox why would we ever do this?  maybe don't do this.
    MonkeyBox *child = new MonkeyBox();// clone
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  MonkeyBox* Deep_Clone_Me(CollisionLibrary& HitTable) {
    MonkeyBox *child = this->Clone_Me();
    return child;
  }
  /* ********************************************************************************* */
  void Copy_From(const MonkeyBox& donor) {
    Transformer::Copy_From(donor);
    Handle::Copy_From(donor);
    //this->MyParentSong = donor.MyParentSong;// snox violates pattern to copy a pointer in a Copy_From.  All Copy_Froms should be shallow.
    //this->AssignOctavesPerRadius(donor.OctavesPerRadius);// Redundant with Handle::Copy_From(donor);
    //this->MyBounds.Copy_From(donor.MyBounds);// Redundant with IDrawable::Copy_From(donor);
  }
  /* ********************************************************************************* */
  void Clear() override {// set all coordinates to identity, no transformation for content
    Transformer::Clear();
  }
  /* ********************************************************************************* */
  void RescaleTimeX(SoundFloat Factor) {
    this->ScaleX = Factor;
  }
  /* ********************************************************************************* */
  virtual void Rebase_Time(TimeFloat Time) {
    this->TimeX = Time;
    SoundFloat RelativeMinBound = this->MyBounds.Min.X;// preserve the relative relationship of my bounds and my origin.
    this->MyBounds.Rebase_Time(Time + RelativeMinBound);
  }
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& ParentDC) override {}
  /* ********************************************************************************* */
  void Draw_Dot(DrawingContext& ParentDC, Color& col) {}
  /* ********************************************************************************* */
  static void Draw_Dot2(DrawingContext& DC, SoundFloat XCtr, SoundFloat YCtr, SoundFloat OctavesPerRadius, boolean Selected, Color& col) {
    // #kludgey, hacky.  Need to create the gradient only once I guess, rather than every time a point is moved.
    Point2D pnt;
    DC.To_Screen(XCtr, YCtr, pnt);
    double RadiusPixels = Math::abs(DC.GlobalTransform.ScaleY) * (OctavesPerRadius);
    //RadiusPixels = Math::ceil(RadiusPixels);
    double DiameterPixels = RadiusPixels * 2.0;

    DC.SetColor(0.7, 1.0, 0.7, 0.5);
    DC.DrawCircle(XCtr, YCtr, RadiusPixels);
#if FALSE
    Paint oldpaint = DC.gr.getPaint();
    if (Selected) {// add glow for selected objects
      float[] dist = {0.0f, 0.49f, 0.5f, 1.0f};
      Color[] colors = {Globals.ToAlpha(Color.red, 0), Globals.ToAlpha(Color.red, 0), Color.red, Globals.ToAlpha(Color.red, 1)};
      double GradRadius = RadiusPixels * 2;
      double GradDiameter = GradRadius * 2;
      pnt.setLocation(XCtr, YCtr);
      RadialGradientPaint paint = new RadialGradientPaint(pnt, (int) GradRadius, dist, colors);
      DC.gr.setPaint(paint);
      DC.gr.fillOval((int) (XCtr - GradRadius), (int) (YCtr - GradRadius), (int) GradDiameter, (int) GradDiameter);
      DC.gr.setPaint(oldpaint);
    }
    DC.gr.setColor(Globals.ToAlpha(col, 200));// control point just looks like a dot
    DC.gr.fillOval((int) (XCtr - RadiusPixels), (int) (YCtr - RadiusPixels), (int) DiameterPixels, (int) DiameterPixels);
    DC.gr.setColor(Globals.ToAlpha(Color.darkGray, 200));
    DC.gr.drawOval((int) (XCtr - RadiusPixels), (int) (YCtr - RadiusPixels), (int) DiameterPixels, (int) DiameterPixels);
//    DC.gr.fillOval((int) (pnt.X - RadiusPixels), (int) (pnt.Y - RadiusPixels), (int) DiameterPixels, (int) DiameterPixels);
//    DC.gr.setColor(Globals.ToAlpha(Color.darkGray, 200));
//    DC.gr.drawOval((int) (pnt.X - RadiusPixels), (int) (pnt.Y - RadiusPixels), (int) DiameterPixels, (int) DiameterPixels);
#endif // FALSE
  }
  /* ********************************************************************************* */
  void Update_Guts(MetricsPacket& metrics) override {// ITree
 }
  void UpdateBoundingBoxLocal() override {// IDrawable
  }
  void MoveTo(TimeFloat XLoc, SoundFloat YLoc) override {// IMoveable
    if (XLoc <= 0.0) { XLoc = 0.0; }// don't go backward in time
    this->TimeX = XLoc;
    this->OctaveY = YLoc;
  }
  void MoveTo(const Point2D& Loc) override {
    this->MoveTo(Loc.X, Loc.Y);
  }
  SoundFloat GetX() override {return this->TimeX;}// IMoveable
  SoundFloat GetY() override {return this->OctaveY;}// IMoveable
  /* ********************************************************************************* */
  void SetSelected(boolean Selected) override {// IMoveable
    this->IsSelected = Selected;
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {// IDeletable
    return 0;
  }
  void Delete_Me() override {// IDeletable
    this->TimeX = Double::NEGATIVE_INFINITY; this->OctaveY = Double::NEGATIVE_INFINITY;
    this->LoudnessFactor = Double::NEGATIVE_INFINITY;
    this->ScaleX = ScaleY = Double::NEGATIVE_INFINITY;
    this->MyParentSong = nullptr;
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *phrase = new JsonParse::HashNode();// in the MonkeyBox base class, we export only shallow values, no songlet children
    phrase->AddSubPhrase(MonkeyBox::TimeXName, ITextable::PackNumberField(this->TimeX));
    phrase->AddSubPhrase(MonkeyBox::OctaveYName, ITextable::PackNumberField(this->OctaveY));
    phrase->AddSubPhrase(MonkeyBox::LoudnessFactorName, ITextable::PackNumberField(this->LoudnessFactor));
    phrase->AddSubPhrase(MonkeyBox::ScaleXName, ITextable::PackNumberField(this->ScaleX));
    phrase->AddSubPhrase(MonkeyBox::ScaleYName, ITextable::PackNumberField(this->ScaleY));
    if (false) {
      phrase->AddSubPhrase("OctavesPerRadius", ITextable::PackNumberField(this->OctavesPerRadius));
    }
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    this->TimeX = GetNumberField(phrase, MonkeyBox::TimeXName, 0);
    this->OctaveY = GetNumberField(phrase, MonkeyBox::OctaveYName, 0);
    this->LoudnessFactor = GetNumberField(phrase, MonkeyBox::LoudnessFactorName, 1.0);
    this->ScaleX = GetNumberField(phrase, MonkeyBox::ScaleXName, 1.0);
    this->ScaleY = GetNumberField(phrase, MonkeyBox::ScaleYName, 1.0);
    if (false){
      this->AssignOctavesPerRadius(ITextable::GetNumberField(phrase, "OctavesPerRadius", 0.01));//this->OctavesPerRadius = ITextable::GetNumberField(phrase, "OctavesPerRadius", 0.01);
    }
  }
  /* ********************************************************************************* */
  void Print_Me(const String &Title) override {}// comment this out for debugging
};

#endif // MonkeyBox_hpp
