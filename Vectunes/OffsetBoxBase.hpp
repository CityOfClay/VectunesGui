#ifndef OffsetBoxBase_hpp
#define OffsetBoxBase_hpp

#include "Globals.hpp"
#include "MonkeyBox.hpp"
#include "ISonglet.hpp"
#include "IGrabber.hpp"

class CajaDelimitadora;// forward
class SingerBase;// forward

class Factory;// forward
// A synonym for OffsetBox is 'handle'. We will probably rename all offsetboxes to handle someday, too.
class OffsetBoxBase: public MonkeyBox { // IMoveable, IDeletable {// location box to transpose in pitch, move in time, etc.
private:
  //ISonglet *MySonglet = nullptr;// to do: deprecate this.
public:
  StringConst ContentName = "Content";
  //ISonglet* MyParentSong = nullptr;// can do this but not used yet
  // CajaDelimitadora CircleBounds;
  // Begin Artist experimental pattern
  /* ********************************************************************************* */
  class Artist : public ArtistBase { public: OffsetBoxBase *MyOBox = nullptr; };
  /* ********************************************************************************* */
  class ArtistFactory : ArtistFactoryBase { // single-instance for all of this type of songlet
  public:
    Artist* SpawnArtist() override { return new Artist(); }
  };
  static ArtistFactory *ArtistMaker;// = nullptr;
  Artist *MyArtist = nullptr;
  // End Artist experimental pattern
  /* ********************************************************************************* */
  OffsetBoxBase() {
    this->Create_Me();
    this->MyBounds.Reset();
    if (OffsetBoxBase::ArtistMaker!=nullptr) {
      this->MyArtist = OffsetBoxBase::ArtistMaker->SpawnArtist();
      this->MyArtist->MyOBox = this;
    }
  }
  virtual ~OffsetBoxBase(){this->Delete_Me();}
  /* ********************************************************************************* */
  OffsetBoxBase* Clone_Me() override {// ICloneable
    OffsetBoxBase *child = this->GetContent()->Spawn_OffsetBox();
    child->Copy_From(*this);// Either put a throw here or make this pure virtual.
    return child;
  }
  /* ********************************************************************************* */
  OffsetBoxBase* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    return nullptr;// Either put a throw here or make this pure virtual.
  }
  /* ********************************************************************************* */
  virtual void BreakFromHerd(CollisionLibrary& HitTable) {}
  /* ********************************************************************************* */
  void Copy_From(const OffsetBoxBase& donor) {
    MonkeyBox::Copy_From(donor);
  }
  /* ********************************************************************************* */
  SoundFloat Get_Max_Amplitude() override {// Transformer // always override this
    return this->LoudnessFactor;
  }
  /* ********************************************************************************* */
  virtual SoundFloat Get_Duration() {
    return this->GetContent()->Get_Duration();
  }
  /* ********************************************************************************* */
  void Randomize_Color() {
    this->GetContent()->Randomize_Color();
  }
  /* ********************************************************************************* */
  virtual SingerBase* Spawn_Singer() {// always always always override this
    throw std::runtime_error("OffsetBoxBase::Spawn_Singer not supported, should be overridden.");
    return nullptr;
  }
  /* ********************************************************************************* */
  virtual void Rescale_TimeX(SoundFloat Factor) {
    this->ScaleX = Factor;
  }
  /* ********************************************************************************* */
  virtual SoundFloat UnMap_EndTime() {
    return this->UnMapTime(this->GetContent()->Get_Duration());
  }
  /* ********************************************************************************* */
  void Update_Guts(MetricsPacket &metrics) override {
    ISonglet *songlet = this->GetContent();
    songlet->Update_Guts(metrics);
    printf("UpdateBoundingBoxLocal\n");
    this->UpdateBoundingBoxLocal();
  }
  // virtual ISonglet* GetContent() = 0;// always always always override this
  /* ********************************************************************************* */
  virtual ISonglet* GetContent() {// always always always override this
    std::cout << "OffsetBoxBase::GetContent not supported, should be overridden.";
    throw std::runtime_error("OffsetBoxBase::GetContent not supported, should be overridden.");
    return nullptr;
    //return this->MySonglet;
  }
  /* ********************************************************************************* */
  void Assign_Content(ISonglet* songlet) {// Deprecated, can probably be removed now.
    //this->MySonglet = songlet;
  }
  // <editor-fold defaultstate="collapsed" desc="IDrawable and IMoveable">
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& ParentDC) override {// IDrawable
    PROFILE_START;
    if (false) {
    if (this->MyArtist!=nullptr) { // to do: remove Artist approach for now.
      this->MyArtist->Draw_Me(ParentDC);
      PROFILE_END(Globals::OffsetBoxBase_Hits);
      return;
    }
    }
    if (ParentDC.ClipBounds.Intersects(MyBounds)) {

      if (true) { // level of detail clipping
        SoundFloat BoundsWdtPixels = Math::abs(ParentDC.GlobalTransform.ScaleX) * MyBounds.GetWidth();
        SoundFloat BoundsHgtPixels = Math::abs(ParentDC.GlobalTransform.ScaleY) * MyBounds.GetHeight();
        double Limit = 1.0;
        if ((BoundsWdtPixels <= Limit) || (BoundsHgtPixels <= Limit)) {
          // std::cout << "************************* LOD CLIPPED *******" << "\n";
          PROFILE_END(Globals::OffsetBoxBase_Hits);
          return;
        }
      }

      Point2D pnt;
      ParentDC.To_Screen(this->GetX(), this->GetY(), pnt);
      ParentDC.SetColor(0.7, 1.0, 0.7, 0.5);

      // Draw circle handle of this offset box.
      Point2D RadiusPixels2D = this->GetEllipseSizePixels(ParentDC.GlobalTransform, ParentDC.RecurseDepth);
      ParentDC.DrawEllipse(pnt.X, pnt.Y, RadiusPixels2D.X, RadiusPixels2D.Y);

      // Maybe reduce ChildDC's clipbounds to intersect with my own bounds? after all none of my children should go outside my bounds.
      // eh, that would conflict with dragging my child outside of my bounds - it would stop being drawn during the drag.
      DrawingContext *ChildDC = ParentDC.Compound_Clone(*this);// In C++ ChildDC will be a local variable from the stack, not heap. except that DrawingContext is abstract, darn it.
      ISonglet *Content = this->GetContent();
      Content->Draw_Me(*ChildDC);
      delete ChildDC;
      // Draw_My_Bounds(ParentDC);// only for testing
    }
    PROFILE_END(Globals::OffsetBoxBase_Hits);
  }
  /* ********************************************************************************* */
  virtual void Draw_Dot(DrawingContext& DC, Color& col) {}
  /* ********************************************************************************* */
  virtual void Draw_My_Bounds(DrawingContext& ParentDC) {
    CajaDelimitadora PixelBounds;
    ParentDC.GlobalTransform.UnMap(this->MyBounds, PixelBounds);
    ParentDC.DrawRectangle(PixelBounds);
  }
  /* ********************************************************************************* */
  void UpdateBoundingBoxLocal() override {// IDrawable
    ISonglet *Content = this->GetContent();
//    Content->UpdateBoundingBoxLocal();// either this
    CajaDelimitadora *cd = Content->GetBoundingBox();
    this->UnMap(*(cd), this->MyBounds);// project child limits into parent (my) space

    // Include my bubble in bounds
    Point2D EllipseSize = GetEllipseSizeLocal(1.0);// Get ellipse size in local space. Fake recurse depth is good enough.
    this->MyBounds.IncludePoint(this->TimeX - EllipseSize.X, this->OctaveY - EllipseSize.Y);
    this->MyBounds.IncludePoint(this->TimeX + EllipseSize.X, this->OctaveY + EllipseSize.Y);
  }
  /* ********************************************************************************* */
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
    StackContext.RecurseDepth++;
    CajaDelimitadora AudioRootBounds;
    StackContext.UnMap(this->MyBounds, AudioRootBounds);// Map MyBounds from local handle space to root audio space.
    if (Scoop.Intersects(AudioRootBounds)) {// AudioRootBounds and SearchBox are in root audio space.

      bool hit = this->HitsMe(Scoop, StackContext);// Results and obox(me) are in immediate parent space.

      FishingStackContext ChildContext(StackContext);
      ChildContext.Compound(*this);// Make ChildContext matrix be a root-to-MyChild map.

      ISonglet* song = this->GetContent();
      if (Scoop.BranchFilter(song)) {// BranchFilter: If Scoop is customized to ignore some songs, let it have a say here.
        hit |= song->HitsMe(Scoop, ChildContext);// ask child if search box hits it anywhere.
        if (hit) {// if Scoop hits either obox (me) or my songlet, add me to Scoop Fish stringer.
          printf("Hit Circle or Child!");
          Scoop.AddFish(this, StackContext);
        }
        song->GoFishing(Scoop, ChildContext);// Ask child to search its own children for more hits, recursively.
      }
    }
  }
  /* ********************************************************************************* */
  bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override {// IDrawable.IMoveable
    printf("OffsetBoxBase HitsMe:");
    CajaDelimitadora SearchBoxLocal;// Do this all in local space, not pixel space.
    StackContext.MapTo(*Scoop.GetSearchBox(), SearchBoxLocal);// Map SearchBox from root audio space to obx local space.
    Point2D EllipseSize = GetEllipseSizeLocal(StackContext.RecurseDepth);// Get ellipse size in local space.
    return BoxHitsEllipse(SearchBoxLocal, this->TimeX, this->OctaveY, EllipseSize.X, EllipseSize.Y);
  }
  /* ********************************************************************************* */
  void GetInventory(InventoryList& Hallazgos) override {// get all unique containers in me
    this->GetContent()->GetInventory(Hallazgos);
  }
  /* ********************************************************************************* */
  void Burn_Scale(int TimeStamp, TimeFloat ReScaleX, SoundFloat ReScaleY) override {
    this->TimeX *= ReScaleX;
    this->OctaveY *= ReScaleY;
    this->GetContent()->Burn_Scale(TimeStamp, ReScaleX, ReScaleY);
  }
  /* ********************************************************************************* */
  Point2D GetEllipseSizeLocal(double RecurseDepth) override {// Handle
    double RadiusAbstract = this->GetRecursedRadius(RecurseDepth);
    Point2D EllipseSize(Math::abs(this->UnScaleTime(RadiusAbstract)), Math::abs(this->UnScalePitch(RadiusAbstract)));// Map my size from my child space to my obx space, for aesthetic reasons.
    return EllipseSize;// Now EllipseSize represents the full distortions I make to my children.
  }
  // </editor-fold>
  /* ********************************************************************************* */
  boolean Create_Me() override {// not needed probably
    return MonkeyBox::Create_Me();
  }
  void Delete_Me() override {
    MonkeyBox::Delete_Me();
    delete this->MyArtist;
    this->MyArtist = nullptr;
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *SelfPackage = MonkeyBox::Export(HitTable);// ready for test?
    JsonParse::Node *ChildPackage = nullptr;
    ISonglet *songlet = this->GetContent();
    if (songlet==nullptr){// this should never happen
      std::cout << "Child songlet null in OffsetBoxBase.Export()!\n";
    }else{
      if (songlet->GetRefCount() != 1) {// songlet exists in more than one place, use a pointer to library
        JsonParse::LiteralNode *litnode = new JsonParse::LiteralNode();// multiple references, use a pointer to library instead
        CollisionItem *colitem = nullptr;// songlet is already in library, just create a child phrase and assign its textptr to that entry key
        if ((colitem = HitTable.GetItem(songlet)) == nullptr) {
          colitem = HitTable.InsertUniqueInstance(songlet);// songlet is NOT in library, serialize it and add to library
          colitem->JsonPhrase = songlet->Export(HitTable);
          litnode->Literal = colitem->ItemTxtPtr;
        }
        litnode->Literal = colitem->ItemTxtPtr;
        ChildPackage = litnode;// multiple references, use a pointer to library instead
      } else {// songlet only exists in one place, make it inline.
        ChildPackage = songlet->Export(HitTable);
      }
      SelfPackage->AddSubPhrase(OffsetBoxBase::ContentName, ChildPackage);
    }
    return SelfPackage;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    MonkeyBox::ShallowLoad(phrase);
  }
};

OffsetBoxBase::ArtistFactory *OffsetBoxBase::ArtistMaker = nullptr;// for static declaration

#endif // OffsetBoxBase_hpp
