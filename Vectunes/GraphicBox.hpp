#ifndef GraphicBox_hpp
#define GraphicBox_hpp

#include "Globals.hpp"
#include "ICloneable.hpp"
#include "LoopSong.hpp"

/**
 *
 * @author MultiTool
*/
class AudProject;// forward

class GraphicBox: public IContainer  {
public:
  OffsetBoxBase* ContentOBox = nullptr;
  OffsetBoxBase* Floater = nullptr;//IMoveable

  int FreshnessTimeStamp = -1;
  StringConst ContentOBoxName = "ContentOBox";
  StringConst GraphicBoxName = "GraphicBox";
  TimeFloat Duration = Double::NEGATIVE_INFINITY;
  // Begin Artist experimental pattern
  /* ********************************************************************************* */
  class Artist : public ArtistBase { public: GraphicBox *MySonglet = nullptr; };
  /* ********************************************************************************* */
  class ArtistFactory : ArtistFactoryBase { // single-instance for all of this type of songlet
  public:
    Artist* SpawnArtist() override { return new Artist(); }
  };
  static ArtistFactory *ArtistMaker;// = nullptr;
#ifdef UseArtist
  Artist *MyArtist = nullptr;
  // End Artist experimental pattern
#endif // FALSE
  /* ********************************************************************************* */
  class Graphic_Singer: public SingerBase {
  public:
    GraphicBox *MyGraphic = nullptr;
    SingerBase *NowPlaying = nullptr;// currently playing songlet
    CajaDelimitadora SingerBounds;
    /* ********************************************************************************* */
//    Graphic_Singer(GraphicBox *MyGraphic0) {
//      this->MyGraphic = MyGraphic0;
//      // For audio, Graphic_Singer is just a wrapper for its content singer.
//      this->NowPlaying = this->MyGraphic->ContentOBox->Spawn_Singer(); // snox might be good
//      this->NowPlaying->Inherit(*this);
//    }
    /* ********************************************************************************* */
    Graphic_Singer(GraphicBox *MyGraphic0) : Graphic_Singer(MyGraphic0, *(MyGraphic0->ContentOBox)) { }// Fun with constructor chaining.
    /* ********************************************************************************* */
    Graphic_Singer(GraphicBox *MyGraphic0, OffsetBoxBase& Selected) {
      this->MyGraphic = MyGraphic0;
      this->NowPlaying = Selected.Spawn_Singer();
      this->NowPlaying->Inherit(*this);
    }
    /* ********************************************************************************* */
    virtual ~Graphic_Singer() {
      Delete_Me();
      delete this->NowPlaying;
    }
    /* ********************************************************************************* */
    void Start() override {
      NowPlaying->Start();
    }
    /* ********************************************************************************* */
    void Skip_To(TimeFloat EndTime) override {
      NowPlaying->Skip_To(EndTime);// Graphic does no mapping for audio, just pass through.
      this->IsFinished = NowPlaying->IsFinished;
      this->MyOffsetBox->UnMap(*(this->NowPlaying->GetBoundingBox()), this->SingerBounds);// map to pixels.
    }
    /* ********************************************************************************* */
    void Render_To(TimeFloat EndTime, Wave& wave) override {
      NowPlaying->Render_To(EndTime, wave);// Graphic does no mapping for audio, just pass through.
      this->IsFinished = NowPlaying->IsFinished;
      this->MyOffsetBox->UnMap(*(this->NowPlaying->GetBoundingBox()), this->SingerBounds);// map to pixels.
    }
    /* ********************************************************************************* */
    void Draw_Me(DrawingContext& ParentDC) override {// This will animate what the singers are singing.
      DrawingContext *ChildDC;// Map through my GraphicBox's handle.
      ChildDC = ParentDC.Compound_Clone(*(this->MyOffsetBox));
      ChildDC->Brightness = 1.0;// for highlighting while playing

      NowPlaying->Draw_Me(*ChildDC);

      delete ChildDC;
    }
    /* ********************************************************************************* */
    const CajaDelimitadora* GetBoundingBox() override {// DoLivePan
      return &(this->SingerBounds);
    }
    /* ********************************************************************************* */
    const bool GetSingerBounds(CajaDelimitadora& Box) override {// DoLivePan
      Box.Reset();
      if (!this->NowPlaying->GetSingerBounds(Box)) {
        return false;
      }
      if (!std::isfinite(Box.Min.X)) {
        printf("Graphic GetSingerBounds A\n");
      }
      if (Box.UnInit()) {
        printf("Graphic GetSingerBounds UnInit\n");
      }
      CajaDelimitadora Box2;
      this->MyOffsetBox->UnMap(Box, Box2);// map to pixels.
      if (!std::isfinite(Box2.Min.X)) {
        printf("Graphic GetSingerBounds B\n");
      }
      Box.Copy_From(Box2);
      return true;
    }
  };
  /* ********************************************************************************* */
  class Graphic_OffsetBox: public OffsetBoxBase {
  public:
    GraphicBox *Content = nullptr;
    StringConst ObjectTypeName = "Graphic_OffsetBox";
    /* ********************************************************************************* */
    Graphic_OffsetBox(GraphicBox *songlet) : OffsetBoxBase() {
      mytype = 4;
      this->Content = songlet;
      //this->AssignOctavesPerRadius(0.5);
      //this->AssignOctavesPerRadius(0.0001);
      //this->AssignOctavesPerRadius(0.00001);// vanishingly tiny
      if (false){
        this->ScaleX = 40;// pixels per second
        this->ScaleY = -40;// pixels per octave
        this->OctaveY = 400;
        this->TimeX = 20;
      }
      if (false){
        this->ScaleX = 211;// pixels per second
        this->ScaleY = -211;// pixels per octave
        this->OctaveY = 1118;
        this->TimeX = 392;
      }
    }
    /* ********************************************************************************* */
    virtual ~Graphic_OffsetBox() { this->Delete_Me(); }
    /* ********************************************************************************* */
    void Update_Guts(MetricsPacket &metrics) override {
      ISonglet *songlet = this->GetContent();
      songlet->Update_Guts(metrics);
      printf("GraphicBox UpdateBoundingBoxLocal\n");
      this->UpdateBoundingBoxLocal();
    }
    /* ********************************************************************************* */
    void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
      StackContext.RecurseDepth++;
      CajaDelimitadora AudioRootBounds;// zoomdrag
      this->MapTo(this->MyBounds, AudioRootBounds);
      if (Scoop.Intersects(AudioRootBounds)) {// SearchBox and AudioRootBounds is in AudioRoot space.
        ISonglet* song = this->GetContent();
        song->GoFishing(Scoop, StackContext);// Ask child to search its own children for more hits, recursively.
      }
    }
    /* ********************************************************************************* */
//    void UpdateBoundingBoxLocal() override {// IDrawable
//      ISonglet *Content = this->GetContent();
//      CajaDelimitadora *cd = Content->GetBoundingBox();
//      this->UnMap(*(cd), MyBounds);// project child limits into parent (my) space
//      // include my bubble in bounds
//      double CircleRadius = this->UnMapPitch(this->OctavesPerRadius);// Map from audio space to pixel space.
//
//      this->MyBounds.IncludePoint(this->TimeX - CircleRadius, this->OctaveY - CircleRadius);
//      this->MyBounds.IncludePoint(this->TimeX + CircleRadius, this->OctaveY + CircleRadius);
//    }
    /* ********************************************************************************* */
    void Attach_Content(OffsetBoxBase *content) {// snox this looks dangerous
      this->Content->Attach_Content(content);
    }
    /* ********************************************************************************* */
    GraphicBox* GetContent() override {
      return this->Content;
    }
    /* ********************************************************************************* */
    void Attach_Songlet(GraphicBox *songlet) {// for serialization
      this->Assign_Content(this->Content = songlet);
      songlet->Ref_Songlet();
    }
    /* ********************************************************************************* */
    Graphic_Singer* Spawn_Singer() override {// for render time.  always always always override this
      Graphic_Singer *ChildSinger = this->Content->Spawn_Singer();// for render time
      ChildSinger->MyOffsetBox = this;// Transfer all of this box's offsets to singer.
      return ChildSinger;
    }
    /* ********************************************************************************* */
    Graphic_Singer* Spawn_Singer(OffsetBoxBase& Selected) {// for render time.  always always always override this
      Graphic_Singer *ChildSinger = this->Content->Spawn_Singer(Selected);// for render time
      ChildSinger->MyOffsetBox = this;// Transfer all of this box's offsets to singer.
      return ChildSinger;
    }
    /* ********************************************************************************* */
    void Draw_Me(DrawingContext &ParentDC) override {// IDrawable
      PROFILE_START;
      //if (ParentDC.ClipBounds.Intersects(MyBounds)) {
        //if (this->MyArtist!=nullptr){
          DrawingContext *ChildDC = ParentDC.Compound_Clone(*this);// map to child (my) internal coordinates
          this->Print_Me("Graphic_OffsetBox:");
          ParentDC.GlobalTransform.Print_Me("ParentDC.GlobalTransform:");
          ChildDC->GlobalTransform.Print_Me("ChildDC.GlobalTransform:");
          this->Content->Draw_Me(*ChildDC);
          delete ChildDC;
        //}
      //}
      // OffsetBoxBase::Draw_Me(ParentDC);// snox for testing
      PROFILE_END(Globals::GraphicBox_Obx_Hits);
    }
//    void UpdateBoundingBoxLocal() override {// IDrawable
////      this->UnMap(this->Content->GetBoundingBox(), MyBounds);// project child limits into parent (my) space
////      this->MyBounds.Sort_Me();
//    }
    /* ********************************************************************************* */
    bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override {// IDrawable.IMoveable
      printf("Graphic_OffsetBox HitsMe:");
      return false;// GraphicBox obx handle is not displayed or selectable.
    }
    /* ********************************************************************************* */
    void MoveTo(TimeFloat  XLoc, SoundFloat YLoc) override {}
    /* ********************************************************************************* */
    Graphic_OffsetBox* Clone_Me() override { return nullptr; }
    /* ********************************************************************************* */
    Graphic_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; }
    /* ********************************************************************************* */
    void BreakFromHerd(CollisionLibrary& HitTable) override { }
    /* ********************************************************************************* */
    boolean Create_Me() override { return 0; }
    void Delete_Me() override {
      if (this->Content->UnRef_Songlet() <= 0) {
        delete this->Content;
      }
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) {// ITextable
      JsonParse::HashNode* SelfPackage = OffsetBoxBase::Export(HitTable);// ready for test?
      SelfPackage->AddSubPhrase(Globals::ObjectTypeName, PackStringField(ObjectTypeName));
      return SelfPackage;
    }
    void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
      OffsetBoxBase::ShallowLoad(phrase);
    }
    /* ********************************************************************************* */
    void Zoom(double XCtr, double YCtr, double Scale) {
      double XMov = XCtr - (Scale * XCtr);
      double YMov = YCtr - (Scale * YCtr);
      this->TimeX = XMov + (Scale * this->TimeX);
      this->OctaveY = YMov + (Scale * this->OctaveY);
      this->ScaleX *= Scale; this->ScaleY *= Scale;
      this->UpdateBoundingBoxLocal();
    }
  };// Graphic_OffsetBox
  /* ********************************************************************************* */
  GraphicBox() {
    mytype = 5;
#ifdef UseArtist// deprecating Artist pattern for now.
    if (GraphicBox::ArtistMaker!=nullptr) {
      this->MyArtist = GraphicBox::ArtistMaker->SpawnArtist();
      this->MyArtist->MySonglet = this;
    }
#endif // UseArtist
  }
  /* ********************************************************************************* */
  virtual ~GraphicBox() {
    this->Delete_Me();
  }
  /* ********************************************************************************* */
  void Attach_Content(OffsetBoxBase *content) {
    this->ContentOBox = content;
    content->MyParentSong = this;
  }
  /* ********************************************************************************* */
  OffsetBoxBase *Get_Content() {
    return this->ContentOBox;
  }
  /* ********************************************************************************* */
  Graphic_OffsetBox* Spawn_OffsetBox() override {
    Graphic_OffsetBox *grbox = new Graphic_OffsetBox(this);// Spawn an OffsetBox specific to this type of songlet.
    this->Ref_Songlet();
    return grbox;
  }
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& ParentDC) override {
    PROFILE_START;
#ifdef UseArtist// deprecating Artist pattern for now.
    if (this->MyArtist!=nullptr) {
      this->MyArtist->Draw_Me(ParentDC);
    }
#endif // UseArtist
    this->Draw_Grid(ParentDC);
    this->ContentOBox->Draw_Me(ParentDC);
    if (this->Floater != nullptr) {
      this->Floater->Draw_Me(ParentDC);
    }
    PROFILE_END(Globals::GraphicBox_Hits);
  }
  CajaDelimitadora* GetBoundingBox() override {// meant to crash
    return &(this->MyBounds);
    // throw std::runtime_error("GraphicBox GetBoundingBox MUST NOT be called."); // return nullptr;
  }
  /* ********************************************************************************* */
  void UpdateBoundingBoxLocal() override {// IDrawable
    this->MyBounds.Reset();
    CajaDelimitadora *ChildBBoxUnMapped = this->ContentOBox->GetBoundingBox();// project child limits into parent (my) space
    this->MyBounds.Include(*ChildBBoxUnMapped);
  }
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
    this->ContentOBox->GoFishing(Scoop, StackContext);
  }
  /* ********************************************************************************* */
  bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override {// maybe move this to IDrawable?
    return false;// Everything is inside graphicbox. to do: use bounds instead.
  }
  /* ********************************************************************************* */
  GraphicBox* Clone_Me() override { return nullptr; }
//  GraphicBox* Clone_Me() override {
//    return new GraphicBox();// nullptr;
//  }
//  IDrawable* Clone_Me() override {
//    return new GraphicBox();// nullptr;
//  }
  /* ********************************************************************************* */
  GraphicBox* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; }
  /* ********************************************************************************* */
  void Copy_From(GraphicBox& donor) {}
  /* ********************************************************************************* */
  void Draw_Grid(DrawingContext& ParentDC) {// to do: migrate this to a master drawing utility
    double xloc, yloc;
    double MinX, MinY, MaxX, MaxY;// in audio coordinates
    double ScreenMinX, ScreenMinY, ScreenMaxX, ScreenMaxY;// in screen coordinates
    double XOrigin, YOrigin;
    Transformer *GlobalTransform = &(ParentDC.GlobalTransform);

    MinX = Math::floor(ParentDC.ClipBounds.Min.X);
    MinY = Math::floor(ParentDC.ClipBounds.Min.Y);
    MaxX = Math::ceil(ParentDC.ClipBounds.Max.X);
    MaxY = Math::ceil(ParentDC.ClipBounds.Max.Y);

    ScreenMinX = GlobalTransform->UnMapTime(MinX);
    ScreenMinY = GlobalTransform->UnMapPitch(MinY);
    ScreenMaxX = GlobalTransform->UnMapTime(MaxX);
    ScreenMaxY = GlobalTransform->UnMapPitch(MaxY);
    GlobalTransform->Print_Me("GlobalTransform:");

    if (ScreenMaxY < ScreenMinY) {// swap
      double temp = ScreenMaxY; ScreenMaxY = ScreenMinY; ScreenMinY = temp;
    }

    double Alpha = 0.25;

    ParentDC.SetLineWidth(0.25);
    // draw minor horizontal pitch lines
    ParentDC.SetColor(0.7, 0.7, 0.7, Alpha);// Color.lightGray
    for (double ysemi = MinY; ysemi < MaxY; ysemi += 1.0 / 12.0) {// semitone lines
      yloc = GlobalTransform->UnMapPitch(ysemi);
      ParentDC.DrawLine(ScreenMinX, yloc, ScreenMaxX, yloc);
    }

    // draw minor vertical time lines
    ParentDC.SetColor(0.7, 0.7, 0.7, Alpha);// Color.lightGray
    for (double xsemi = MinX; xsemi < MaxX; xsemi += 1.0 / 4.0) {// 1/4 second time lines
      xloc = GlobalTransform->UnMapTime(xsemi);
      ParentDC.DrawLine(xloc, ScreenMinY, xloc, ScreenMaxY);
    }

    Alpha = 0.75;
    ParentDC.SetLineWidth(1.0);
    // draw major horizontal pitch lines
    ParentDC.SetColor(0.3, 0.3, 0.3, Alpha);// Color.darkGray
    for (double ycnt = MinY; ycnt < MaxY; ycnt++) {// octave lines
      yloc = GlobalTransform->UnMapPitch(ycnt);
      ParentDC.DrawLine(ScreenMinX, yloc, ScreenMaxX, yloc);
    }

    // draw major vertical time lines
    ParentDC.SetColor(0.3, 0.3, 0.3, Alpha);// Color.darkGray
    for (double xcnt = MinX; xcnt < MaxX; xcnt++) {// 1 second time lines
      xloc = GlobalTransform->UnMapTime(xcnt);
      ParentDC.DrawLine(xloc, ScreenMinY, xloc, ScreenMaxY);
    }

    // draw origin lines
    Alpha = 1.0;
    XOrigin = GlobalTransform->UnMapTime(0.0);
    ParentDC.SetColor(1.0, 0.0, 0.0, Alpha); // Color.red
    ParentDC.DrawLine(XOrigin, ScreenMinY, XOrigin, ScreenMaxY);

    YOrigin = GlobalTransform->UnMapPitch(0.0);
    ParentDC.DrawLine(ScreenMinX, YOrigin, ScreenMaxX, YOrigin);
  }
  /* ********************************************************************************* */
  void AntiAlias(DrawingContext& g2d) {}
  /* ********************************************************************************* */
  boolean Create_Me() override { return 0; }// IDeletable
  void Delete_Me() override {// IDeletable
#ifdef UseArtist// deprecating Artist pattern for now.
    delete this->MyArtist; this->MyArtist = nullptr;
#endif // UseArtist
    this->Duration = Double::NEGATIVE_INFINITY;
    this->MyBounds.Delete_Me();
    delete this->ContentOBox;//.Delete_Me();
    if (this->Floater != nullptr){
      delete this->Floater;
      this->Floater = nullptr;// wreck everything
    }
    this->RefCount = Integer::MIN_VALUE;
    this->FreshnessTimeStamp = Integer::MIN_VALUE;
  }
  /* ********************************************************************************* */
//  int Ref_Songlet() override { return 0; } // ISonglet
//  int UnRef_Songlet() override { return 0; }
//  int GetRefCount() override { return 0; }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode* phrase = new JsonParse::HashNode();
    phrase->AddSubPhrase(Globals::ObjectTypeName, PackStringField(GraphicBox::GraphicBoxName));// self name
    JsonParse::HashNode* ContentOBoxPhrase = this->ContentOBox->Export(HitTable);
    phrase->AddSubPhrase(ContentOBoxName, ContentOBoxPhrase);
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
  }
  /* ********************************************************************************* */
  Graphic_Singer* Spawn_Singer() override {// for render time
    // Graphic_Singer will have an identity matrix for rendering audio, but a true screen map for drawing itself.
    Graphic_Singer *singer = new Graphic_Singer(this);// Spawn a singer specific to this type of phrase.
    if (this->MyProject!=nullptr){//to_do();// fix this bs.  we need to load sample rate at render time.
      singer->Set_Project(this->MyProject);// inherit project
    }
    return singer;
  }
  /* ********************************************************************************* */
  Graphic_Singer* Spawn_Singer(OffsetBoxBase& Selected) {// for render time
    // Graphic_Singer will have an identity matrix for rendering audio, but a true screen map for drawing itself.
    Graphic_Singer *singer = new Graphic_Singer(this, Selected);// Spawn a singer specific to this type of phrase.
    if (this->MyProject!=nullptr){//to_do();// fix this bs.  we need to load sample rate at render time.
      singer->Set_Project(this->MyProject);// inherit project
    }
    return singer;
  }
  /* ********************************************************************************* */
//  Graphic_Selected_Singer* Spawn_Selected_Singer() {// for render time
//    // Graphic_Singer will have an identity matrix for rendering audio, but a true screen map for drawing itself.
//    Graphic_Selected_Singer *singer = new Graphic_Selected_Singer(this);// Spawn a singer specific to this type of phrase.
//    if (this->MyProject!=nullptr){//to_do();// fix this bs.  we need to load sample rate at render time.
//      singer->Set_Project(this->MyProject);// inherit project
//    }
//    return singer;
//  }
  /* ********************************************************************************* */
  double Get_Duration() override {// Duration here is for audio, so it is in content (AudioRoot) coordinates.
    return this->ContentOBox->UnMap_EndTime();
  }
  /* ********************************************************************************* */
  double Get_Max_Amplitude() override {// Amplitude here is for audio, so it punts to child AudioRoot for that info.
    return this->ContentOBox->Get_Max_Amplitude();
  }
  /* ********************************************************************************* */
  void Randomize_Color() override {
    this->Get_Content()->Randomize_Color();
  }
  /* ********************************************************************************* */
  void Update_Guts(MetricsPacket& metrics) override {
    if (this->FreshnessTimeStamp != metrics.FreshnessTimeStamp) {// don't hit the same songlet twice on one update
      this->MyProject = metrics.MyProject;
      this->ContentOBox->Update_Guts(metrics);

      if (this->Floater!=nullptr) {// Include Floater in graphic bounding box.
        this->Floater->Update_Guts(metrics);
        CajaDelimitadora *floatbox = this->Floater->GetBoundingBox();
        this->MyBounds.Include(*floatbox);// to do: include floater in duration?
      }

      this->Duration = this->ContentOBox->UnMap_EndTime();
      this->UpdateBoundingBoxLocal();
      this->FreshnessTimeStamp = metrics.FreshnessTimeStamp;
    }
    metrics.MaxDuration = this->Duration;
  }
  /* ********************************************************************************* */
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {};// should be just for IContainer types, but aren't all songs containers?
  /* ********************************************************************************* */
  void Sort_Me() {}
  AudProject* Get_Project() { return nullptr; }
  /* ********************************************************************************* */
  void SetMute(boolean Mute) {}
  /* ********************************************************************************* */
  int FreshnessTimeStamp_g() { return 0; }
  void FreshnessTimeStamp_s(int TimeStampNew) {}
};

#ifdef UseArtist// deprecating Artist pattern for now.
GraphicBox::ArtistFactory *GraphicBox::ArtistMaker = nullptr;// for static declaration
#endif // UseArtist

#endif
