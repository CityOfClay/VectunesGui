#ifndef IGrabber_hpp
#define IGrabber_hpp

#include "Globals.hpp"
#include "IDrawable.hpp"

/**
 *
 * @author MultiTool

 IGrabber is basically a spatial query which also carries back all the results data.
 *
 */
class Handle;// forward
class FishHook;// forward
/* ********************************************************************************* */
class IGrabber {
protected:
  CajaDelimitadora AudioRootSearchBox;// rename to SearchBox, zoomdrag
public:
  /* ********************************************************************************* */
  virtual CajaDelimitadora* GetSearchBox() = 0;
  virtual void Clear() = 0;
  virtual void AssignAudioRootSearchBox(double XMin, double YMin, double XMax, double YMax) = 0;
  virtual void AssignAudioRootSearchBox(CajaDelimitadora AudioRootBox) = 0;
  virtual bool BranchFilter(ISonglet* song) = 0;
  virtual void AddFish(Handle *NewFish, FishingStackContext& StackContext) = 0;
  virtual bool Intersects(CajaDelimitadora& OtherBounds) = 0;
  virtual size_t NumFound() = 0;
  virtual void MoveAll(TimeFloat XMove, SoundFloat YMove) = 0;
  virtual FishHook GetMostDistal() = 0;
};

#endif // IGrabber_hpp
