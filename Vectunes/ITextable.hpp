#ifndef ITextable_hpp
#define ITextable_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "JsonParse.hpp"

/**
 *
 * @author MultiTool
*/
class CollisionLibrary;// forward
class ITextable {// DIY Json ISerializable - more control
public:
  StringConst ObjectTypeName = "ObjectTypeName";// for serialization
  StringConst PtrPrefix = "ptr:";// for serialization
  virtual JsonParse::HashNode* Export(CollisionLibrary& HitTable) {// pass a collision table parameter.  to do: put a crasher here, to guarantee override.
    throw std::runtime_error("ITextable Export MUST be overridden."); // return nullptr;
  }
  // virtual JsonParse::HashNode* Export(CollisionLibrary& HitTable) = 0;// pass a collision table parameter.  to do: put a crasher here, to guarantee override.
  virtual void ShallowLoad(JsonParse::HashNode& phrase){};// just fill in primitive fields that belong to this object, don't follow up pointers.
  /* ********************************************************************************* */
  static double GetNumberField(JsonParse::HashNode &hnode, const String FieldName, double DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return Double::ParseDouble(FieldTxt, DefaultValue);
    }else{
      return DefaultValue;
    }
  }
  /* ********************************************************************************* */
  static int GetIntegerField(JsonParse::HashNode &hnode, const String FieldName, int DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return Integer::ParseInt(FieldTxt, DefaultValue);
    }else{
      return DefaultValue;
    }
  }
  /* ********************************************************************************* */
  static bool GetBoolField(JsonParse::HashNode &hnode, const String FieldName, bool DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return Boolean::ParseBoolean(FieldTxt, DefaultValue);
    }else{
      return DefaultValue;
    }
  }
  /* ********************************************************************************* */
  static String GetStringField(JsonParse::HashNode &hnode, const String FieldName, String DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return FieldTxt;
    }else{
      return DefaultValue;
    }
  }
  static JsonParse::LiteralNode* PackIntegerField(int Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    phrase->Literal = Integer::ToString(Value);
    return phrase;
  }
  static JsonParse::LiteralNode* PackNumberField(double Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    phrase->Literal = Double::ToString(Value);
    return phrase;
  }
  static JsonParse::LiteralNode* PackBoolField(bool Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    phrase->Literal = Boolean::ToString(Value);
    return phrase;
  }
  static JsonParse::LiteralNode* PackStringField(const String &Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    phrase->Literal = Value;
    return phrase;
  }
  template<typename TextableT>
  static void MakeArray(CollisionLibrary &HitTable, ArrayList<TextableT*> &Things, JsonParse::ArrayNode &Parent) {
    int len = Things.size();
    TextableT* thing = nullptr;
    JsonParse::Node *node = nullptr;
    for (int cnt = 0; cnt < len; cnt++) {
      thing = Things.get(cnt);
      node = thing->Export(HitTable);
      Parent.AddSubPhrase(node);
    }
  }
  /* ********************************************************************************* */
  static bool IsTxtPtr(const String& ContentTxt){// for deserialization
    String ContentTxtTrim = Ops::Trim(ContentTxt);
    return Ops::StartsWith(ContentTxtTrim, ITextable::PtrPrefix);
  }
};

/* ********************************************************************************* */
class CollisionItem {// do we really need this?
public:
  String ItemTxtPtr;// Key, usually
  ITextable *Hydrated = nullptr;//, ClonedItem = null;
  JsonParse::HashNode *JsonPhrase = nullptr;// serialization of the ITextable Item
};
/* ********************************************************************************* */
class CollisionLibrary {// contains twice-indexed list of instances of (usually) songlet/phrase pairs for serialization, DEserialization, and cloning
public:
  int ItemIdNum = 0;
  HashMap<ITextable*, CollisionItem*> Instances;// serialization and cloning
  HashMap<String, CollisionItem*> Items;// DEserialization
  /* ********************************************************************************* */
  virtual ~CollisionLibrary(){
    this->Clear();
  }
  /* ********************************************************************************* */
  void Clear(){// Iterate through my hashmaps and delete all CollisionItems, but not the Items they point to.
    //std::map<String, CollisionItem*>::iterator iter;
    for (auto iter=Items.begin(); iter!=Items.end(); ++iter){
      delete iter->second;
    }
    Items.clear();
    this->Instances.clear();
  }
  /* ********************************************************************************* */
  CollisionItem *InsertUniqueInstance(ITextable *KeyObj) {// for serialization
    CollisionItem *colitem = new CollisionItem();
    colitem->ItemTxtPtr = ITextable::PtrPrefix + Integer::ToString(ItemIdNum);
    colitem->Hydrated = KeyObj;
    this->Instances.put(KeyObj, colitem);// object is key
    this->Items.put(colitem->ItemTxtPtr, colitem);// string is key
    this->ItemIdNum++;
    return colitem;
  }
  /* ********************************************************************************* */
  void InsertTextifiedItem(const String &KeyTxt, JsonParse::HashNode *JsonNode) {// for deserialization, only on load
    CollisionItem *ci = new CollisionItem();
    ci->ItemTxtPtr = KeyTxt; ci->JsonPhrase = JsonNode;
    ci->Hydrated = nullptr;
    this->Items.put(KeyTxt, ci);// string is key
  }
  /* ********************************************************************************* */
  CollisionItem* GetItem(ITextable* KeyObj) {// for serialization
    int siz = this->Instances.size();
    return this->Instances.get(KeyObj);
  }
  CollisionItem* GetItem(String& KeyTxt) {
    int siz = this->Items.size();
    return this->Items.get(KeyTxt);
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* ExportJson() {// snox NOT TESTED YET
    JsonParse::HashNode *MainPhrase = new JsonParse::HashNode();
    CollisionItem *ci;
    for (auto iter=this->Items.begin(); iter!=this->Items.end(); ++iter){
      ci = (CollisionItem*)iter->second;// another cast!
      if (ci->JsonPhrase != nullptr) {
        //ChildPhrase = new JsonParse.Node();// should we clone the child phrase?
        MainPhrase->AddSubPhrase(ci->ItemTxtPtr, ci->JsonPhrase);
      }
    }
    return MainPhrase;
  }
  /* ********************************************************************************* */
  void ConsumeLibrary(JsonParse::HashNode& LibraryPhrase) {
    this->Clear();
    int siz = LibraryPhrase.ChildrenHash.size();
    //std::map<String, JsonParse::Node*>::iterator iter;
    for (auto iter=LibraryPhrase.ChildrenHash.begin(); iter!=LibraryPhrase.ChildrenHash.end(); ++iter){
      JsonParse::HashNode *JsonNode = (JsonParse::HashNode*)iter->second;// another cast!
      this->InsertTextifiedItem(iter->first, JsonNode);
    }
  }
  /* ********************************************************************************* */
  void Wipe_Songlets() {// for testing  snox NOT TESTED YET
    CollisionItem *ci;
    IDeletable *deletable;
    for (auto iter=this->Items.begin(); iter!=this->Items.end(); ++iter){
      ci = (CollisionItem*)iter->second;// another cast!
      if ((deletable = (IDeletable*) ci->Hydrated) != nullptr) {
        delete deletable;
        ci->Hydrated = nullptr;
      }
    }
  }
};

#endif
