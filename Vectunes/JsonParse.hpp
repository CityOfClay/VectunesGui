#ifndef JsonParse_hpp
#define JsonParse_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include <regex>
#include "KaffeeErsatz.hpp"

//#define System_println printf

StringConst Environment_NewLine {"\r\n"};//const

/* ********************************************************************************************************* */
class Ops{ // basic static string operations
public:
  // <editor-fold defaultstate="collapsed" desc="Literal finders, Is Word, Is Number, etc">
  /* ********************************************************************************************************* */
  static boolean IsWordChar(char ch) {// for alphanumerics, eg variable names, reserved words, etc.
    if ('a' <= ch && ch <= 'z') { return true; }
    if ('A' <= ch && ch <= 'Z') { return true; }
    if ('0' <= ch && ch <= '9') { return true; }
    if ('_' == ch || ch == '@') { return true; }
    if ('-' == ch || ch == '.') { return true; }// for numbers
    return false;
  }
  /* ********************************************************************************************************* */
  static boolean IsNumericChar(char ch) {// for numbers
    if ('0' <= ch && ch <= '9') { return true; }
    if ('-' == ch || ch == '.') { return true; }// currently we are sloppy and let gibberish like ".--99.00.-45..88" go through
    return false;
  }
  /* ********************************************************************************************************* */
  static boolean IsNumericPunctuationChar(char ch) {// for number punctuation such as '.' and '-' anything else?
    if ('-' == ch || ch == '.') { return true; }// currently we are sloppy and let gibberish like ".--99.00.-45..88" go through
    return false;
  }
  /* ********************************************************************************************************* */
  static boolean IsNumericSuffixChar(char ch) {
    ch = tolower(ch);
    if ('e' == ch || ch == 'f') { return true; }// currently we are sloppy and let gibberish like ".--99.00.-45..88" go through
    return false;
  }
  /* ********************************************************************************************************* */
  static boolean IsNumericString(const String& txt) {// for numbers
    if (txt.length()==0){return false;}// currently we are sloppy and let gibberish like ".--99.00.-45..88" go through
    uint64_t chcnt = 0;
    boolean isnum = true;
    while (chcnt < txt.length()) {
      if (!IsNumericChar(txt.at(chcnt))) { isnum = false; break; }
      chcnt++;
    }
    return isnum;
  }
  /* ********************************************************************************************************* */
  static boolean IsBlankChar(char ch) {// any whitespace
    if (' ' == ch || ch == '\t' || ch == '\n' || ch == '\r') { return true; }
    return false;
  }
  // </editor-fold>
  /* ********************************************************************************************************* */
  //https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
  static inline void ltrim(std::string &s) {// trim from start (in place)
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
  }
  static inline void rtrim(std::string &s) {// trim from end (in place)
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
  }
  static inline void Trim(const std::string &RawText, std::string &result) {// trim from both ends (in place)
    result = RawText;//https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    ltrim(result); rtrim(result);
  }
  static inline std::string Trim(const std::string &RawText) {// trim from both ends
    std::string result;
    result = RawText;//https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    ltrim(result); rtrim(result);
    return result;
  }
  /* ********************************************************************************************************* */
  static void Substring(const String &RawText, int Start, int End, String &result){
     result = RawText.substr(Start, End-Start);
  }
  /* ********************************************************************************************************* */
  static void DeQuote(const String& RawText, String &result) {
    const String Q1 = "\'", Q2 = "\"";
    String Text;
    Trim(RawText, Text);
    int TextLen = RawText.length();
    if (TextLen<2){// not really quoted, but maybe an error would be better
      result = RawText; return;
    }
    TextLen = Text.length();
    String firstchar, lastchar;
    Ops::Substring(Text, 0, 1, firstchar);// Text.at(0);
    Ops::Substring(Text, TextLen-1, TextLen, lastchar);// String lastchar = Text.at(TextLen-1);
    if (firstchar == Q1 && lastchar == Q1){ // single quotes
      Ops::Substring(Text, 1, TextLen-1, result);//result = Text.substr(1, TextLen-2);
    } else if (firstchar == Q2 && lastchar == Q2){ // double quotes
      Ops::Substring(Text, 1, TextLen-1, result);//result = Text.substr(1, TextLen-2);
    }
  }
  /* ********************************************************************************************************* */
  static void EnQuote(const String& RawText, String& result) {
    Replace(RawText, "\"", "\"\"", result);
    result = "\"" + result + "\"";
  }
  /* ********************************************************************************************************* */
  static void Replace(const String &RawText, const String &PrevTxt, const String &NextTxt, String &result){
    result = std::regex_replace(RawText, std::regex(PrevTxt), NextTxt);// https://stackoverflow.com/questions/1494399/how-do-i-search-find-and-replace-in-a-standard-string
  }
  /* ********************************************************************************* */
  static void Replace(std::string& RawText, const std::string& OldStr, const std::string& NewStr){
    std::string::size_type pos = 0u;//https://stackoverflow.com/questions/1494399/how-do-i-search-find-and-replace-in-a-standard-string
    while((pos = RawText.find(OldStr, pos)) != std::string::npos){
       RawText.replace(pos, OldStr.length(), NewStr);
       pos += NewStr.length();
    }
  }
  /* ********************************************************************************* */
  static bool StartsWith(std::string const &FullString, std::string const &beginning) {
    return (FullString.find(beginning, 0)) == 0;
  }
  /* ********************************************************************************* */
  static bool EndsWith(std::string const &FullString, std::string const &ending) {
    if (FullString.length() >= ending.length()) { // https://stackoverflow.com/questions/874134/find-if-string-ends-with-another-string-in-c
      return (0 == FullString.compare(FullString.length() - ending.length(), ending.length(), ending));
    } else {
      return false;
    }
  }
  /* ********************************************************************************************************* */
  static int CompareStartStay(const String& txt, int StartPlace, const String& target) {// look for any substring right at the StartPlace of the text
    int foundloc = StartPlace;// if not found, return the place where we started
    uint64_t cnt = StartPlace;
    uint64_t tcnt = 0;
    char ch;
    while (cnt < txt.length()){// && tcnt < target.length())
      ch = txt.at(cnt);
      if (ch != target.at(tcnt)) { break; }
      cnt++; tcnt++;
      if (tcnt >= target.length()) { foundloc = cnt; break; }// return the loc of just past the end of what we found
    }
    return foundloc;
  }
  /* ********************************************************************************************************* */
  static int CompareStart(const String& txt, int StartPlace, const String& target) {// look for any substring right at the StartPlace of the text
    int foundloc = -1;
    size_t cnt = StartPlace;
    size_t tcnt = 0;
    char ch;
    while (cnt < txt.length()){// && tcnt < target.length())
      ch = txt.at(cnt);
      if (ch != target.at(tcnt)) { break; }
      cnt++; tcnt++;
      if (tcnt >= target.length()) { foundloc = cnt; break; }// return the loc of just past the end of what we found
    }
    return foundloc;
  }
  /* ********************************************************************************************************* */
  static int CompareStartAny(const String& txt, int StartPlace, const ArrayList<String> &targets) {// look for one of any substrings right at the StartPlace of the text
    int foundloc = -1;
    int sz = targets.size();
    for (int cnt=0; cnt<sz; cnt++) {  // snox, replace foreach
      if ((foundloc = CompareStart(txt, StartPlace, targets.at(cnt))) >= 0) { break; }
    }
    return foundloc;
  }
};
/* ********************************************************************************************************* */
class Token {
public:
  enum class TokenType { NoType, CommentStar, CommentSlash, Word, Whitespace, SingleChar, TextString };
  String Text;
  TokenType BlockType;
  String SpecificType = "NoType";
  String DeQuoted() { String DeQ; Ops::DeQuote(this->Text, DeQ); return DeQ; }
};
/* ********************************************************************************************************* */
class Tokenizer {
public:
  ArrayList<Token*> Chunks;//TokenList;
  /* ********************************************************************************************************* */
  Tokenizer(){}
  /* ********************************************************************************************************* */
  virtual ~Tokenizer(){
    Clear_TokenList();
  }
  /* ********************************************************************************************************* */
  ArrayList<Token*>* Tokenize(int StartPlace, const String &txt) {
    Clear_TokenList();
    int len = txt.length();
    int MarkPrev = 0;
    int MarkNext = 0;
    while (MarkPrev < len){
      if ((MarkNext = Chomp_CommentStar(txt, MarkPrev)) > MarkPrev) { }
      else if ((MarkNext = Chomp_CommentSlash(txt, MarkPrev)) > MarkPrev) { }
      else if ((MarkNext = Chomp_Word(txt, MarkPrev)) > MarkPrev) { }
      else if ((MarkNext = Chomp_Whitespace(txt, MarkPrev)) > MarkPrev) { }
      else if ((MarkNext = Chomp_SingleChar(txt, MarkPrev)) > MarkPrev) { }
      else if ((MarkNext = Chomp_TextStringDoubleQuoted(txt, MarkPrev)) > MarkPrev) { }
      else if ((MarkNext = Chomp_TextStringSingleQuoted(txt, MarkPrev)) > MarkPrev) { }
      else { printf("Inconceivable!\n"); break; }//throw new Exception("Inconceivable!");
      MarkPrev = MarkNext;
    }
    return &Chunks;
  }
  /* ********************************************************************************************************* */
  void Clear_TokenList(){
    int Len = this->Chunks.size();
    for (int cnt=0;cnt<Len;cnt++){
      delete this->Chunks.at(cnt);
    }
    this->Chunks.clear();
  }
  /* ********************************************************************************************************* */
  void Print_Tokens(){
    int Len = Chunks.size();
    printf("Print_Tokens: %i tokens\n", Len);
    for (int cnt=0;cnt<Len;cnt++){
      Token *tkn = Chunks.at(cnt);
      printf("[%s]\n", tkn->Text.c_str());
    }
  }
  // Chunk Finders
  /* ********************************************************************************************************* */
  int Chomp_CommentStar(const String& txt, int StartPlace) {
    String ender = "*/";  // for / * comments
    //std::string ender = "*/";  // for / * comments
    int loc = StartPlace, loc1;
    Token *tkn = nullptr;
    if ((loc1 = Ops::CompareStart(txt, StartPlace, "/*")) >= 0) {
      loc = txt.find(ender, loc1);
      if (loc != std::string::npos) {//if (loc >= 0) {
        loc += ender.length();
      } else {
        loc = txt.length();
      }
      String chunk;// = txt.substr(StartPlace, loc-StartPlace);
      Ops::Substring(txt, StartPlace, loc, chunk);
      tkn = new Token();//  Text = chunk, BlockType = TokenType::CommentStar ;
      tkn->Text = chunk; tkn->BlockType = Token::TokenType::CommentStar;
      Chunks.add(tkn);
    }
    return loc;
  }
  /* ********************************************************************************************************* */
  int Chomp_CommentSlash(const String& txt, int StartPlace) {
    String ender0 = Environment_NewLine;// for // comments
    String ender1 = "\n";
    int loc = StartPlace, loc1;
    Token *tkn = nullptr;
    if ((loc1 = Ops::CompareStart(txt, StartPlace, "//")) >= 0) {
      loc = txt.find(ender0, loc1);
      if (loc != std::string::npos) { //if (loc >= 0) {
        loc += ender0.length();
      } else {
        loc = txt.find(ender1, loc1);
        if (loc != std::string::npos) {//if (loc >= 0) {
          loc += ender1.length();
        } else {
          loc = txt.length();
        }
        String chunk = txt.substr(StartPlace, loc-StartPlace);
        tkn = new Token();// { Text = chunk, BlockType = TokenType::CommentSlash };
        tkn->Text = chunk; tkn->BlockType = Token::TokenType::CommentSlash;
        Chunks.add(tkn);
      }
    }
    return loc;
  }
  /* ********************************************************************************************************* */
  int Chomp_Word(const String& txt, int StartPlace) {
    int loc = StartPlace;// for alphanumerics, eg variable names, reserved words, etc.
    Token *tkn = nullptr;
    char ch = txt.at(StartPlace);
    while (loc < txt.length() && Ops::IsWordChar(txt.at(loc))) {
      loc++;
    }
    if (StartPlace < loc) { // then we found something
      String chunk = txt.substr(StartPlace, loc-StartPlace);
      tkn = new Token();// { Text = chunk, BlockType = TokenType::Word };
      tkn->Text = chunk; tkn->BlockType = Token::TokenType::Word;
      Chunks.add(tkn);
    }
    return loc;
  }
  /* ********************************************************************************************************* */
  int Chomp_Whitespace(const String& txt, int StartPlace) {
    int loc = StartPlace;// for whitespace
    Token *tkn = nullptr; // by default whitespace ends where text ends
    while (loc < txt.length() && Ops::IsBlankChar(txt.at(loc))) {
      loc++;
    }
    if (StartPlace < loc) {// then we found something
      String chunk = txt.substr(StartPlace, loc-StartPlace);
      tkn = new Token();// { Text = chunk, BlockType = Token::TokenType::Whitespace };
      tkn->Text = chunk; tkn->BlockType = Token::TokenType::Whitespace;
      Chunks.add(tkn);
    }
    return loc;
  }
  /* ********************************************************************************************************* */
  int Chomp_TextString(const String& txt, const String& QuoteChar, int StartPlace) {
    int loc0 = StartPlace, loc1;// for text strings "blah blah"
    Token *tkn = nullptr;
    if ((loc1 = Ops::CompareStart(txt, StartPlace, QuoteChar)) >= 0) {
      loc0 = loc1;
      while (loc0 < txt.length()){// by default String ends where text ends
        char ch = txt.at(loc0);
        if (Ops::CompareStart(txt, loc0, "\\") >= 0) { loc0++; }// ignore slash-escaped characters
        else if (Ops::CompareStart(txt, loc0, ""+QuoteChar+""+QuoteChar+"") >= 0) { loc0++; }// ignore double-escaped quotes (only legal in some languages)
        else if (Ops::CompareStart(txt, loc0, QuoteChar) >= 0) { loc0++; break; }// we found a closing quote, break.
        //else { if (ch == QuoteChar.at(0)) { loc0++; break; } }
        loc0++;
      }
      if (StartPlace < loc0){// then we found something
        String chunk = txt.substr(StartPlace, loc0-StartPlace);
        tkn = new Token();// { Text = chunk, BlockType = Token::TokenType::TextString };
        tkn->Text = chunk; tkn->BlockType = Token::TokenType::TextString;
        Chunks.add(tkn);
      }
    }
    return loc0;
  }
  /* ********************************************************************************************************* */
  int Chomp_TextStringDoubleQuoted(const String& txt, int StartPlace) {
    return Chomp_TextString(txt, "\"", StartPlace);// for text strings "blah blah"
  }
  /* ********************************************************************************************************* */
  int Chomp_TextStringSingleQuoted(const String& txt, int StartPlace) {
    return Chomp_TextString(txt,"\'", StartPlace);// for text strings 'blah blah'
  }
  /* ********************************************************************************************************* */
  int Chomp_SingleChar(const String& txt, int StartPlace) {
    // for single char such as { or ] or ; etc.
    // String singles = "}{][)(*&^%$#@!~+=;:.>,<|\\?/-";
    String singles = "}{][)(*&^%$#@!~+=;:>,<|\\?/";
    Token *tkn = nullptr;
    if (StartPlace >= txt.length()) { return StartPlace; }
    char ch = txt.at(StartPlace);
    if (singles.find(ch, 0) != std::string::npos) {//if (singles.find(ch, 0) >= 0){// then we found something
      tkn = new Token();// { Text = ch.toString(), BlockType = Token::TokenType::SingleChar };
      tkn->Text = ch;// Character.toString(ch);
      tkn->BlockType = Token::TokenType::SingleChar;
      Chunks.add(tkn);
      StartPlace++;
    }
    return StartPlace;
  }
};
/* ********************************************************************************************************* */
class JsonParse {
public:
  const String PhraseEnd= {","};
  /* ********************************************************************************************************* */
  //typedef class Node *NodePtr;
  class Node {// a value that is a hashtable, an array, a literal, or a pointer to a multiply-used item
  public:
    enum class Types { NoType, IsHash, IsArray, IsLiteral, IsPointer };
    Types MyType = Types::NoType;
    String MyPhraseName = "***Nothing***";
    Node* Parent = nullptr;
    int ChunkStart,ChunkEnd;
    Node() {}
    virtual ~Node() {}
    virtual String ToJson() = 0;//{ return "Node ToJson should be overridden"; }
  };
  /* ********************************************************************************************************* */
  class LiteralNode: public Node {// a value that is a literal
  public:
    String Literal = "";
    LiteralNode(){ this->MyType = Types::IsLiteral; }
    virtual ~LiteralNode() { this->Literal = ""; }
    String Get() { return this->Literal; }
    String ToJson() override {
      String retval;
      Ops::EnQuote(this->Get(), retval);
      return retval;// maybe put quotes around this
    }
  };
  /* ********************************************************************************************************* */
  class HashNode: public Node {// a value that is a hashtable
  public:
    HashMap<String, Node*> ChildrenHash;
    HashNode(){ this->MyType = Types::IsHash; }
    virtual ~HashNode() {
      std::map<String, Node*>::iterator iter;
      for (iter=this->ChildrenHash.begin(); iter!=this->ChildrenHash.end(); ++iter){
        delete iter->second;
      }
      this->ChildrenHash.clear();
    }
    /* ********************************************************************************* */
    const bool TryGetField(const String &FieldName, String &retval) {
      bool GotSuccess;
      if (ChildrenHash.ContainsKey(FieldName)) {
        Node *phrase = ChildrenHash.get(FieldName);
        if (phrase != nullptr && phrase->MyType == Node::Types::IsLiteral){
          retval = ((LiteralNode*)phrase)->Literal;// another cast!
          GotSuccess = true;
        } else {
          GotSuccess = false;
        }
      } else {
        GotSuccess = false;
      }
      return GotSuccess;
    }
    /* ********************************************************************************* */
    String GetField(const String &FieldName, const String &DefaultValue) {
      String retval = "";
      if (!TryGetField(FieldName, retval)){
        retval = DefaultValue;
      }
      return retval;
    }
    /* ********************************************************************************* */
    void AddSubPhrase(const String& Name, Node *ChildPhrase) {
      this->ChildrenHash.put(Name, ChildPhrase); ChildPhrase->Parent = this;
      //this->ChildrenHash[Name] = ChildPhrase; ChildPhrase->Parent = this;
    }
    Node* Get(const String& Name) {
      if (this->ChildrenHash.ContainsKey(Name)) { return this->ChildrenHash[Name]; }
      else { return nullptr; }
    }
    void Clear(){ this->ChildrenHash.clear(); }
    String ToHash() {
      Node *child;
      String Text = "";
      Text.append("{");
      int len = this->ChildrenHash.size();
      String key, quoted;
      std::map<String, Node*>::iterator iter;
      if (0<len){
        int cnt=0;
        iter = this->ChildrenHash.begin();
        // std::cout << iter->first << " => " << iter->second << '\n';
        key = iter->first; child = iter->second;
        Ops::EnQuote(key, quoted);
        Text.append(quoted); Text.append(" : "); Text.append(child->ToJson());
        ++iter;
        while (iter!=this->ChildrenHash.end()){// https://stackoverflow.com/questions/3496982/printing-lists-with-commas-c
          // std::cout << iter->first << " => " << iter->second << '\n';
          key = iter->first; child = iter->second;
          Ops::EnQuote(key, quoted);
          Text.append(", ");
          Text.append(quoted); Text.append(" : "); Text.append(child->ToJson());
          ++iter;
        }
      }
      Text.append("}");
      return Text;
    }
    String ToJson() override { return this->ToHash(); }
  };
  /* ********************************************************************************************************* */
  class ArrayNode: public Node {// a value that is an array
  public:
    ArrayList<Node*> ChildrenArray;
    ArrayNode(){ this->MyType = Types::IsArray; }
    virtual ~ArrayNode() {
//      for (auto kid : this->ChildrenArray) {// to do: use this simpler approach
//        delete kid;// https://stackoverflow.com/questions/2395275/how-to-navigate-through-a-vector-using-iterators-c
//      }
      //ArrayList<Node*>::iterator iter;
      for (auto iter=this->ChildrenArray.begin(); iter!=this->ChildrenArray.end(); ++iter){
        delete *iter;
      }
      this->ChildrenArray.clear();
    }
    void AddSubPhrase(Node *ChildPhrase) {
      this->ChildrenArray.add(ChildPhrase);
      ChildPhrase->Parent = this;
    }
    Node* Get(int Dex) { return this->ChildrenArray.get(Dex);}
    void Clear(){ this->ChildrenArray.clear(); }
    String ToArray() {
      Node *child;
      String Text = "";
      Text.append("[");
      int len = this->ChildrenArray.size();
      if (0 < len) {
        int cnt = 0;
        child = this->ChildrenArray.get(cnt++); Text.append(child->ToJson());
        while (cnt < len) {
          Text.append(", ");
          child = this->ChildrenArray.get(cnt++); Text.append(child->ToJson());
        }
      }
      Text.append("]");
      return Text;
    }
    String ToJson() override { return this->ToArray(); }
  };
  /* ********************************************************************************************************* */
  HashNode *RootNode = nullptr;
  /* ********************************************************************************************************* */
  JsonParse(){}
  virtual ~JsonParse(){
    this->RootNode = nullptr;//delete this->RootNode;
  }
  /* ********************************************************************************************************* */
  LiteralNode* MakeLiteral(const String& Text, int ChunkStart, int ChunkEnd) {
    LiteralNode *OnePhrase = new LiteralNode();
    OnePhrase->Literal = Text; OnePhrase->ChunkStart=ChunkStart; OnePhrase->ChunkEnd=ChunkEnd;
    OnePhrase->MyType = Node::Types::IsLiteral;
    return OnePhrase;
  }
  /* ********************************************************************************************************* */
  LiteralNode* Chomp_Number(ArrayList<Token*>& Chunks, int Marker, int RecurDepth) {// this is wrong. need to re-think it before using.
    // chunks are a number if: all chunks are all numeric
    // but not if: numeric chunks end, but next chunk is non-whitespace, non-comma, non-semicolon, and what else? just non-delimiter?
    // 12.345blah is not a number. maybe let that pass anyway? 123.4f is a number sometimes.
    // hmm. valid numberenders: ; , []() etc. any single char thing that's not a .
    // how about a number can end with whitespace or any non-numeric punctation.
    LiteralNode *OnePhrase = nullptr;
    int FirstChunk, FinalChunk = Marker;
    Token *tkn = Chunks.get(Marker);
    char ch = tkn->Text.at(0);
    if (Ops::IsNumericPunctuationChar(ch) || Ops::IsNumericString(tkn->Text)) {
      FirstChunk = Marker;
      String WholeString = "";
      while (Marker < Chunks.size()) {// to do: fix this-> as-is will return true if text is empty.
        tkn = Chunks.get(Marker);
        ch = tkn->Text.at(0);
        if (!(Ops::IsNumericPunctuationChar(ch) || Ops::IsNumericString(tkn->Text) || Ops::IsNumericSuffixChar(ch))) { break; }
        FinalChunk = Marker;
        WholeString = WholeString + tkn->Text;
        Marker++;
      }
      OnePhrase = new LiteralNode(); OnePhrase->ChunkStart = FirstChunk; OnePhrase->ChunkEnd = FinalChunk;
      OnePhrase->Literal = WholeString;
      OnePhrase->MyType = Node::Types::IsLiteral;
    }
    return OnePhrase;
  }
  /* ********************************************************************************************************* */
  HashNode* Chomp_HashMap(ArrayList<Token*>& Chunks, int Marker, int RecurDepth) {
    HashNode *OnePhrase = nullptr;
    Node *SubPhrase = nullptr;
    String Starter="{", Ender="}";
    int MarkNext=Marker;
    int KeyOrValue=0;
    String Empty = "";
    String Key=Empty;
    RecurDepth++;
    Token *tkn = Chunks.get(Marker);
    if (tkn->Text == Starter){
      OnePhrase = new HashNode();
      OnePhrase->ChunkStart = Marker;
      OnePhrase->Clear();
      MarkNext = ++Marker;
      while (Marker<Chunks.size()) {
        tkn = Chunks.get(Marker);
        if (tkn->Text == Ender){break;}
        else if ((SubPhrase = Chomp_HashMap(Chunks,  Marker, RecurDepth))!=nullptr){
          OnePhrase->AddSubPhrase(Key, SubPhrase); Key=Empty; MarkNext = SubPhrase->ChunkEnd+1;
        } else if ((SubPhrase = Chomp_Array(Chunks,  Marker, RecurDepth))!=nullptr){
          OnePhrase->AddSubPhrase(Key, SubPhrase); Key=Empty; MarkNext = SubPhrase->ChunkEnd+1;
        } else if ((SubPhrase = Chomp_Number(Chunks, Marker, RecurDepth)) != nullptr) {
          OnePhrase->AddSubPhrase(Key, SubPhrase); Key=Empty; MarkNext = SubPhrase->ChunkEnd+1;
        } else if ((tkn->BlockType == Token::TokenType::TextString) || (tkn->BlockType == Token::TokenType::Word)) {
          if (KeyOrValue == 0) {
            Key = tkn->Text;
            if (tkn->BlockType == Token::TokenType::TextString) { Ops::DeQuote(Key, Key); }//Key = Ops::DeQuote(Key);
          } else {
            String UnQuoted;
            Ops::DeQuote(tkn->Text, UnQuoted);
            SubPhrase = MakeLiteral(UnQuoted, Marker, Marker);// inclusive
            OnePhrase->AddSubPhrase(Key, SubPhrase);
            Key = Empty;
          }
          MarkNext++;
        } else if (tkn->BlockType == Token::TokenType::SingleChar){
          if (tkn->Text == ":"){ /* Key=CurrentLiteral; */ KeyOrValue=1; }
          else if (tkn->Text == ","){ Key=Empty; KeyOrValue=0; }
          MarkNext++;
        } else if (tkn->BlockType == Token::TokenType::Whitespace){/* skip whitespace */ MarkNext++; }
        else {/* skip over everything else */ MarkNext++;}

        Marker = MarkNext;
      } // while (Marker<Chunks.size() && !(tkn = Chunks.get(Marker)).Text.equals(Ender));
      OnePhrase->ChunkEnd = Marker;// inclusive?
      OnePhrase->MyType = Node::Types::IsHash;
    }
    return OnePhrase;
  }
  /* ********************************************************************************************************* */
  ArrayNode* Chomp_Array(ArrayList<Token*>& Chunks, int Marker, int RecurDepth) {
    ArrayNode *OnePhrase = nullptr;
    Node *SubPhrase = nullptr;
    String Starter="[", Ender="]";
    String results;
    int MarkNext=Marker;
    RecurDepth++;
    Token *tkn = Chunks.get(Marker);
    if (tkn->Text == (Starter)){
      OnePhrase = new ArrayNode();
      OnePhrase->ChunkStart = Marker;
      OnePhrase->Clear();// = new ArrayList<Node>();
      Marker++;
      MarkNext=Marker;
      while (Marker<Chunks.size()) {
        tkn = Chunks.get(Marker);
        if (tkn->Text == Ender){break;}
        if ((SubPhrase = Chomp_HashMap(Chunks,  Marker, RecurDepth))!=nullptr){
          OnePhrase->AddSubPhrase(SubPhrase); MarkNext = SubPhrase->ChunkEnd+1;
        } else if ((SubPhrase = Chomp_Array(Chunks,  Marker, RecurDepth))!=nullptr){
          OnePhrase->AddSubPhrase(SubPhrase); MarkNext = SubPhrase->ChunkEnd+1;
        } else if ((SubPhrase = Chomp_Number(Chunks, Marker, RecurDepth)) != nullptr) {
          OnePhrase->AddSubPhrase(SubPhrase); MarkNext = SubPhrase->ChunkEnd+1;
        } else if ((tkn->BlockType == Token::TokenType::TextString) || (tkn->BlockType == Token::TokenType::Word)){
          Ops::DeQuote(tkn->Text, results);
          SubPhrase = MakeLiteral(results, Marker, Marker);// inclusive
          OnePhrase->AddSubPhrase(SubPhrase);
          MarkNext++;
        } else if (tkn->BlockType == Token::TokenType::SingleChar){
          if (tkn->Text == (",")){ }
          MarkNext++;
        } else if (tkn->BlockType == Token::TokenType::Whitespace){/* skip whitespace */ MarkNext++; }
        else {/* skip over everything else */ MarkNext++;}

        Marker = MarkNext;
      }// while (Marker<Chunks.size() && !(tkn = Chunks.get(Marker)).Text.equals(Ender));
      OnePhrase->ChunkEnd = Marker;// inclusive?
      OnePhrase->MyType = Node::Types::IsArray;
    }
    return OnePhrase;
  }
  /* ********************************************************************************************************* */
  HashNode* Parse(const String& JsonText) {
    this->RootNode = nullptr;//delete this->RootNode;
    Tokenizer tknizer;
    ArrayList<Token*> *Chunks = tknizer.Tokenize(0, JsonText);
    this->RootNode = Fold(*Chunks);
    return this->RootNode;
  }
  /* ********************************************************************************************************* */
  HashNode* Fold(ArrayList<Token*>& Chunks) {
    printf("\n");
    printf("-----------------------------------------------------\n");
    int Marker = 0;
    HashNode *parent;
    parent = Chomp_HashMap(Chunks, Marker, 0);
    printf("Done\n");
    String JsonTxt = parent->ToJson();
    // std::cout << "JsonTxt:" << JsonTxt << ":\n";
    return parent;
  }
};

#endif // JsonParse_hpp
