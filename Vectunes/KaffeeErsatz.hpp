#ifndef KaffeeErsatz_hpp
#define KaffeeErsatz_hpp

#include <random>
#include <iostream>
#include <chrono>

#include <vector>
#include "Globals.hpp"

/* Fake Java code */

#define boolean bool
#define jpublic
#define jprivate
#define implements :
//#define interface class
#define extends :
#define Object void*
#define null nullptr

#define String std::string

//#define Double_NEGATIVE_INFINITY std::numeric_limits<SoundFloat>::max()
//#define Double_NEGATIVE_INFINITY std::numeric_limits<SoundFloat>::min()

//class String : public std::string {
//public:
// //String substring(int startIndex, int endIndex)
//};
//class Color; // forward

template <typename T> std::string to_string_with_precision(const T a_value, const int n = 6) {
  std::ostringstream out;// https://stackoverflow.com/questions/16605967/set-precision-of-stdto-string-when-converting-floating-point-values
  out.precision(n);
  out << std::fixed << a_value;
  return out.str();
}
/* ********************************************************************************* */
class Double {// more simulated Java
public:
  static constexpr double POSITIVE_INFINITY = (std::numeric_limits<double>::max)();
  static constexpr double NEGATIVE_INFINITY = -(std::numeric_limits<double>::max)();
  /* ********************************************************************************* */
  static double ParseDouble(const String &NumTxt, double Default){
    size_t EndPtr;
    double Num = std::stod(NumTxt, &EndPtr);
    if (EndPtr==0){Num = Default;}
    return Num;
  }
  static inline String ToString(const double Value) {
    return to_string_with_precision(Value, 10);// was precision of 30
    //return std::to_string(Value);
  }
  static inline String ToString(const double Value, int Precision) {
    return to_string_with_precision(Value, Precision);
  }
};
/* ********************************************************************************* */
class Integer {// more simulated Java
public:
  static constexpr int MAX_VALUE = (std::numeric_limits<int>::max)();//  2147483647
  static constexpr int MIN_VALUE = (std::numeric_limits<int>::min)();// -2147483648
  /* ********************************************************************************* */
  static int ParseInt(const String &NumTxt, int Default){
    size_t EndPtr;
    int Num = std::stoi(NumTxt, &EndPtr);
    if (EndPtr==0){Num = Default;}
    return Num;
  }
  static inline String ToString(const int Value) {
    return std::to_string(Value);
  }
};
/* ********************************************************************************* */
class Boolean {
public:
  /* ********************************************************************************* */
  static bool ParseBoolean(const String &BoolTxt, bool Default){
    if (BoolTxt.size()<=0) {return Default; }
    char ch = BoolTxt.at(0);
    ch = tolower(ch);
    if (ch=='t') { return true; }
    if (ch=='f') { return false; }
    return Default;
  }
  static inline String ToString(const bool Value) {
    return Value ? "true" : "false";
  }
};
/* ********************************************************************************* */
class Math {// more simulated Java
public:
  static constexpr long double PI = PIdef;
  static constexpr long double E = 2.71828182845904523536028747135266249775724709369995;
  //  std::srand(std::time(nullptr)); // use current time as seed for random generator

  static SoundFloat min(SoundFloat a, SoundFloat b) {
    return std::min(a, b);
  }
  static SoundFloat max(SoundFloat a, SoundFloat b) {
    return std::max(a, b);
  }
//  static int max(int a, int b) {
//    return std::max(a, b);
//  }
  static SoundFloat ceil(SoundFloat a) {
    return std::ceil(a);
  }
  static SoundFloat floor(SoundFloat a) {
    return std::floor(a);
  }
  static SoundFloat round(SoundFloat a) {
    return std::round(a);
  }
  static SoundFloat abs(SoundFloat a) {
    return std::abs(a);
  }
  static SoundFloat hypot(SoundFloat a, SoundFloat b) {
    return std::hypot(a, b);
  }
  static SoundFloat sqrt(SoundFloat a) {
    return std::sqrt(a);
  }
  static SoundFloat sin(SoundFloat a) {
    return std::sin(a);
  }
  static SoundFloat cos(SoundFloat a) {
    return std::cos(a);
  }
  static SoundFloat log(SoundFloat a) {
    return std::log(a);
  }
  static SoundFloat pow(SoundFloat a, SoundFloat exp) {
    return std::pow(a, exp);
  }
  //static unsigned seed = 2;
  //static const unsigned long long myseed = 2;
  //std::vector<int> val{std::vector<int>(5,0)};
  // static std::vector<int> val2{std::vector<int>(5,0)};
  //static std::default_random_engine generatorstatic = std::default_random_engine(2LLU);//Math::myseed);
  std::default_random_engine generatorstatic = std::default_random_engine(2LLU);//Math::myseed);
  static void InitRand(){
    std::srand(std::time(nullptr)); // use current time as seed for random generator
    int limit = std::rand()%117;// prime number
    // We don't get enough variation from mingw gcc random seed, so we exaggerate it by advancing the sequence randomly.
    for (int cnt=0;cnt<limit;cnt++){ std::rand(); }// advance a random number of iterations

    // trying a LOT of combinations to find one that makes sense and works
    unsigned myseed = std::chrono::system_clock::now().time_since_epoch().count();
    //generatorstatic.seed(myseed);
    //unsigned long long int myseed = 2;
    std::default_random_engine generator(myseed);
    //std::default_random_engine generator(myseed);
    generator.seed(myseed);
    std::uniform_real_distribution<double> distribution(0.0, 1.0);
  }
  static SoundFloat frand() {
    int numer = std::rand();
    int denom = 9973;//117;// prime number
    int remainder = numer % denom;
    return ((SoundFloat)remainder) / (SoundFloat)denom;
    //return ((SoundFloat)std::rand()) / (SoundFloat)RAND_MAX;
  }
  void BetterRandom(){// to do: incorporate this instead. https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(1.0, 2.0);
    for (int n = 0; n < 10; ++n) {
      // Use dis to transform the random unsigned int generated by gen into a
      // double in [1, 2). Each call to dis(gen) generates a new random double
      std::cout << dis(gen) << ' ';
    }
    std::cout << '\n';
  }
  // uniform_real_distribution example
  static void BetterRandom2(){ // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    generator.seed(seed);
    //std::mt19937 generator(seed); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<double> distribution(0.0, 100.0);
    //distribution(0, 100.0);
    std::cout << "some random numbers between 0.0 and 100.0: " << std::endl;
    double num;//http://www.cplusplus.com/reference/random/uniform_real_distribution/operator()/
    for (int cnt=0; cnt<10; cnt++){
      num = distribution(generator);
      std::cout << num << std::endl;
    }
  }
};
/* ********************************************************************************* */
class Color {
public:
  double RedChannel, GreenChannel, BlueChannel, AlphaChannel;
  //static Color yellow(1.0, 1.0, 0.0);
  Color() {};
  Color(double R, double G, double B) : Color(R, G, B, 1.0) {
  }
  Color(double R, double G, double B, double A) {
    Assign(R, G, B, A);
  }
  Color(const Color &donor) {
    this->Assign(donor.RedChannel, donor.GreenChannel, donor.BlueChannel, donor.AlphaChannel);
  }
  void Assign(double Red, double Green, double Blue){
    this->Assign(Red, Green, Blue, 1.0);
  }
  void Assign(double Red, double Green, double Blue, double Alpha){
    this->RedChannel=Red; this->GreenChannel=Green; this->BlueChannel=Blue; this->AlphaChannel=Alpha;
  }
  double GetRed() const { return this->RedChannel; }
  double GetGreen() const { return this->GreenChannel; }
  double GetBlue() const { return this->BlueChannel; }
  double GetAlpha() const { return this->AlphaChannel; }

  //* ********************************************************************************* *
  static Color ToAlpha(const Color& col, int Alpha) {
    return Color(col.GetRed(), col.GetGreen(), col.GetBlue(), Alpha);// rgba
  }
  //* ********************************************************************************* *
//  static Color* ToAlpha(const Color& col, int Alpha) {
//    return new Color(col.GetRed(), col.GetGreen(), col.GetBlue(), Alpha);// rgba
//  }
  //* ********************************************************************************* *
  static void ToRainbow(double Fraction, Color &color) {
    if (Fraction < 0.5) {
      Fraction *= 2.0;
      color.Assign((1.0 - Fraction), Fraction, 0);
    } else {
      Fraction = Math::min((Fraction - 0.5) * 2, 1.0);
      color.Assign(0, (1.0 - Fraction), Fraction);
    }
  }
  /* ********************************************************************************* */
  static void ToColorWheel(double Fraction, Color &color) {
    Fraction = Fraction - Math::floor(Fraction); // remove whole number part if any
    double StartBig, DeadAir = 0.0;
    if (Fraction < (1.0 / 3.0)) {
      Fraction *= 3.0;// map to range 0 to 1
      StartBig = 1.0 - Fraction;
      color.Assign(StartBig, Fraction, DeadAir);// red to green
    } else if (Fraction < (2.0 / 3.0)) {
      Fraction = (Fraction - (1.0 / 3.0)) * 3.0;// map to range 0 to 1
      StartBig = 1.0 - Fraction;
      color.Assign(DeadAir, StartBig, Fraction);// green to blue
    } else {
      Fraction = (Fraction - (2.0 / 3.0)) * 3.0;// map to range 0 to 1
      StartBig = 1.0 - Fraction;
      color.Assign(Fraction, DeadAir, StartBig);// blue to red
    }
  }
  // Some color 'constants'
  static Color Red() { return Color(1.0, 0.0, 0.0); }
  static Color Green() { return Color(0.0, 1.0, 0.0); }
  static Color Blue() { return Color(0.0, 0.0, 1.0); }
};
/* ********************************************************************************* */
template <class Type>
class ArrayList: public std::vector<Type> {
public:
  void remove(const Type item){// remove by content
    typename ArrayList<Type>::iterator it;
    it = std::find(this->begin(), this->end(), item);
    if (it != this->end()){
      this->erase(it);
    }
  }
  void add(const Type item){
    this->push_back(item);
  }
  void Add(const Type item){
    this->push_back(item);
  }
  void Insert(int Dex, Type item){// insert by index
    this->insert(this->begin() + Dex, item);
  }
  Type get(int Dex){
    return this->at(Dex);
  }
  bool Contains(const Type item){// find by content
    typename ArrayList<Type>::iterator it;
    it = std::find(this->begin(), this->end(), item);
    if (it != this->end()){
      return true;
    }else{
      return false;
    }
  }
};
/* ********************************************************************************* */
template <class KeyT, class ValueT>
class HashMap: public std::map<KeyT, ValueT> {
public:
  ValueT get(const KeyT &Key){
    typename std::map<KeyT, ValueT>::iterator result;
    result = this->find(Key);
    if (result == this->end()){
      return nullptr;
    }else{
      return result->second;
    }
  }
  void put2(const KeyT &Key, const ValueT Value){
    this->insert(std::make_pair(Key, Value));
  }
  void put(const KeyT &Key, const ValueT Value){// Does not seem to work
    typename std::map<KeyT, ValueT>::iterator it; // http://www.cplusplus.com/reference/map/map/find/
    it = this->find(Key);
    if (it == this->end()){
      this->insert(std::make_pair(Key, Value));
    }else{
      it->second = Value;
    }
  }
  //typedef typename std::map<KeyT, ValueT>::const_iterator MapIterator;
  bool ContainsKey(const KeyT& Key){
    typename std::map<KeyT, ValueT>::iterator result;
    result = this->find(Key);
    if (result == this->end()){
      return false;
    }else{
      return true;
    }
//    int num = this->count(Key);// this is a horrid inefficient approach but the only one that compiles for generics.
//    if (num > 0) {return true;}
//    else { return false; }
  }
};

#endif // KaffeeErsatz_hpp
