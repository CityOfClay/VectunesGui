#ifndef Grabber_hpp
#define Grabber_hpp

#include <vector>
#include "Globals.hpp"
#include "Wave.hpp"
#include "CajaDelimitadora.hpp"
#include "IGrabber.hpp"
#include "ITextable.hpp"

/**

 @author MultiTool
 Grabber is basically a spatial query which also carries back all the results data.

*/

class GroupSong;// forward


/* ********************************************************************************* */
class FishHook {
//protected:
//  Transformer RootMap;
public:
  Transformer RootMap;
  Handle* Captura = nullptr;
  int RecurseDepth = -1;// bogus value
  SoundFloat Distance = Double::POSITIVE_INFINITY;// Distance of found object from center of search box.

  // if this handle has a parent and we are removing it, keep a pointer to the parent.
  // parent.RemoveChild(Handle child)?
  /* ********************************************************************************* */
  FishHook() { this->Captura = nullptr; }
  /* ********************************************************************************* */
  FishHook(Handle *NewFish, const FishingStackContext& StackContext) {
    this->Captura = NewFish;
    Transformer::Copy_Transform(StackContext, this->RootMap);// Copy StackContext to root map.
    this->RecurseDepth = StackContext.RecurseDepth;
  }
  /* ********************************************************************************* */
  Handle* VerCaptura() {
    return this->Captura;
  }
  /* ********************************************************************************* */
  void MapMove(TimeFloat XLoc, SoundFloat YLoc) {
    Point2D pnt;
    this->RootMap.MapTo(XLoc, YLoc, pnt);
    this->Captura->MoveTo(pnt.X, pnt.Y);
  }
  /* ********************************************************************************* */
  void UnMap(SoundFloat XLoc, SoundFloat YLoc, Point2D& results) const {
    this->RootMap.UnMap(XLoc, YLoc, results);
  }
  /* ********************************************************************************* */
  void Reset() {
    this->Captura = nullptr;
    RecurseDepth = -1;// bogus value
  }
  /* ********************************************************************************* */
  virtual ~FishHook() {
    this->Captura = nullptr;
    this->RootMap.Delete_Me();// wreck
  }
};

/* ********************************************************************************* */
class Grabber : public IGrabber {
public:
  ArrayList<FishHook> Stringer;
  /* ********************************************************************************* */
  void Clear() override {
    // to do: delete every handle in stringer.
    this->Stringer.clear();
  }
  /* ********************************************************************************* */
  CajaDelimitadora* GetSearchBox() override {
    return &(this->AudioRootSearchBox);
    //return &(this->SearchBox);
  }
  /* ********************************************************************************* */
  void AssignAudioRootSearchBox(double XMin, double YMin, double XMax, double YMax) override {
    AudioRootSearchBox.Assign(XMin, YMin, XMax, YMax);
  }
  /* ********************************************************************************* */
  void AssignAudioRootSearchBox(CajaDelimitadora AudioRootBox) override {
    AudioRootSearchBox.Copy_From(AudioRootBox);
  }
  /* ********************************************************************************* */
  bool BranchFilter(ISonglet* song) override { return true; }
  /* ********************************************************************************* */
  void AddFish(Handle *NewFish, FishingStackContext& StackContext) override {
    FishHook hook(NewFish, StackContext);
    this->Stringer.Add(hook);
  }
  /* ********************************************************************************* */
  bool Intersects(CajaDelimitadora& OtherBounds) override {
    return this->GetSearchBox()->Intersects(OtherBounds);
  }
  /* ********************************************************************************* */
  size_t NumFound() override {
    return Stringer.size();
  }
  /* ********************************************************************************* */
  void MoveAll(TimeFloat XMove, SoundFloat YMove) override {
    int len = Stringer.size();
    for (int cnt=0;cnt<len;cnt++) {
      Handle* grab = Stringer.at(cnt).Captura;
      grab->MoveBy(XMove, YMove);
    }
  }
  /* ********************************************************************************* */
  FishHook GetMostDistal() override {
    int len = Stringer.size();
    int MaxDepth = 0;
    FishHook hook, best;
    for (int cnt=0;cnt<len;cnt++) {
      hook = Stringer.at(cnt);
      if (MaxDepth <= hook.RecurseDepth) {
        MaxDepth = hook.RecurseDepth;
        best = hook;
      }
    }
    return best;
  }
};

/* ********************************************************************************* */
class DestinationGrabber : public Grabber {// Used for finding drag and drop destinations, containers such as GroupSong.
public:
  InventoryList Forbidden; // Prohibidios Sorted list of pointers to songlets that we DON'T want to drop into.
  /* ********************************************************************************* */
  bool BranchFilter(ISonglet* song) override {
    // This could also be set to ignore anything that is not IContainer.
    return !(std::binary_search(Forbidden.begin(), Forbidden.end(), song));
  }
  /* ********************************************************************************* */
  void Reset(int TimeStamp) {
    this->Forbidden.clear();
    this->Forbidden.TimeStamp = TimeStamp;
  }
  /* ********************************************************************************* */
  void Sort() {
    std::sort(this->Forbidden.begin(), this->Forbidden.end());
  }
};


///* ********************************************************************************* */
//class Hook : public ISonglet {
//public:
//  /* ********************************************************************************* */
//  class Hook_OffsetBox : public OffsetBoxBase {
//  };
//};


/* ********************************************************************************* */
//class StackItem: public IDeletable {
//public:
//  CajaDelimitadora SearchBounds;
//  OffsetBoxBase* OBox;
//  int HitDex;
//  Point2D Loc;
// /* so do we pass HitDex to all songlets, or to all oboxes? only songlets can use it directly.
//  and when do we use it? we use it when we move the thing we grabbed, in MapThroughStack
//  we would have to pass it through every mapto.  ack.
//  why not just give
//  */
//  /* ********************************************************************************* */
//  StackItem(){}
//  virtual ~StackItem(){this->Delete_Me();}
//  /* ********************************************************************************* */
//  void Copy_From(const StackItem& donor) {
//    this->SearchBounds.Copy_From(donor.SearchBounds);
//    this->OBox = donor.OBox;
//    this->HitDex = donor.HitDex;
//    this->Loc.SetLocation(donor.Loc);
//  }
//  /* ********************************************************************************* */
//  boolean Create_Me() override {// IDeletable
//    return true;
//  }
//  void Delete_Me() override {// IDeletable
//    this->OBox = nullptr;
//    this->Loc.Assign(Double::NEGATIVE_INFINITY, Double::NEGATIVE_INFINITY);
//    this->HitDex = Integer::MAX_VALUE;
//  }
//};

/* ********************************************************************************* */
//class Grabber3: public IGrabber3 {
//public:
//  SoundFloat XHit, YHit;// exact mouse click point
//  //StackItem *CurrentContext = nullptr;
//  int Stack_Depth = 0, Stack_Depth_Best = 0;
//  ArrayList<StackItem> Explore_Stack;
//  ArrayList<StackItem> Best_Stack;
//  IMoveable *Leaf = nullptr;// thing we hit and are going to move or copy or whatever
//  ArrayList<IMoveable*> Grabbed; // things we hit and are going to move or copy or whatever
//  SoundFloat Radius = 5;
//  /* ********************************************************************************* */
//  void ConsiderLeaf(IMoveable* CandidateLeaf) override {
//    if (CandidateLeaf->HitsMe2(this->CurrentContext->Loc.X, this->CurrentContext->Loc.Y)) {
//      printf(" Was Hit, ");
//      if (this->Stack_Depth_Best <= this->Stack_Depth) {// prefer the most distal
//        this->Stack_Depth_Best = this->Stack_Depth;// or if equal, prefer the last drawn (most recent hit)
//        this->Leaf = CandidateLeaf;
//        Copy_Stack(this->Explore_Stack, this->Best_Stack);
//      }
//      // this->Compare(this->Leaf, leaf);
//    }
//  }
//  /* ********************************************************************************* */
//  boolean KeepDigging(IMoveable* Candidate) override { // Determine whether we should look into the children of Candidate to select a songlet
//    return (this->CurrentContext->SearchBounds.Intersects(*Candidate->GetBoundingBox()));
//  }
//  /* ********************************************************************************* */
//  void Init(OffsetBoxBase& starter, SoundFloat XLoc, SoundFloat YLoc) {// add first space map at start of search
//    OffsetBoxBase *child = new OffsetBoxBase();// first layer place holder.  not a great solution.
//    child->MyBounds.Assign(Double::NEGATIVE_INFINITY, Double::NEGATIVE_INFINITY, Double::POSITIVE_INFINITY, Double::POSITIVE_INFINITY);
//    StackItem next;// = new StackItem();
//    next.OBox = child;
//    next.Loc.X = XLoc;// Next's location values exist in the space above it. At the top they are mouse coordinates.
//    next.Loc.Y = YLoc;
//    next.SearchBounds.Assign(XLoc - Radius, YLoc - Radius, XLoc + Radius, YLoc + Radius);
//    this->Explore_Stack.push_back(std::move(next));
//    this->CurrentContext = &next;
//    Stack_Depth = 1;// Now we have one element, whee!
//  }
//  /* ********************************************************************************* */
//  void AddFirstBox(OffsetBoxBase& starter, SoundFloat XLoc, SoundFloat YLoc) override {// add first space map at start of search
//    this->Leaf = nullptr;
//    this->Stack_Depth_Best = 0;
//    this->Stack_Depth = 0;
//    this->TruncateStack(this->Explore_Stack, 0);
//    StackItem next;
//    next.OBox = &starter;
////    next.Loc.X = XLoc;// Next's location values exist in the space above it. At the top they are mouse coordinates.
////    next.Loc.Y = YLoc;
////    next.SearchBounds.Assign(XLoc - Radius, YLoc - Radius, XLoc + Radius, YLoc + Radius);
//
//    // map to child space
//    //CajaDelimitadora SearchBoundsTemp; SearchBoundsTemp.Assign(XLoc - Radius, YLoc - Radius, XLoc + Radius, YLoc + Radius);
//    CajaDelimitadora SearchBoundsTemp(XLoc - Radius, YLoc - Radius, XLoc + Radius, YLoc + Radius);
//    starter.MapTo(SearchBoundsTemp, next.SearchBounds);// prev.SearchBounds.Map(child, next.SearchBounds);
//    starter.MapTo(XLoc, YLoc, next.Loc);
//
//    this->Explore_Stack.push_back(std::move(next));
//    this->CurrentContext = &next;
//    this->Stack_Depth = 1;// Now we have one element, whee!
//  }
//  /* ********************************************************************************* */
//  void AddBoxToStack(OffsetBoxBase* child) override {
//    StackItem *prev = this->CurrentContext;
//    StackItem next;// snox wrong, this will explode when it passes out of scope.
//    next.OBox = child;
//
//    // map to child space
//    child->MapTo(prev->SearchBounds, next.SearchBounds);// prev.SearchBounds.Map(child, next.SearchBounds);
//    child->MapTo(prev->Loc, next.Loc);
//    //Fresh_Parent.ClipBounds.Map(this->Offset, this->ClipBounds);// map to child (my) internal coordinates
//
//    this->Explore_Stack.push_back(std::move(next));
//    this->CurrentContext = &next;
//    this->Stack_Depth++;
//  }
//  void DecrementStack() override {
//    int resize = this->Stack_Depth - 1;
//    if (resize >= 0) {
//      this->TruncateStack(this->Explore_Stack, resize);
//      this->Stack_Depth--;
//      if (resize > 0) {
//        // this->CurrentContext = &(this->Explore_Stack.get(resize - 1));
//        this->CurrentContext = &(this->Explore_Stack.data()[resize - 1]);// snox this is ugly
//      } else {
//        this->CurrentContext = nullptr;
//      }
//    }
//  }
//  void TruncateStack(ArrayList<StackItem>& Stack, int resize) {
//    int len = Stack.size();
//    for (int cnt = resize; cnt < len; cnt++) {
//      Stack.get(cnt).Delete_Me();
//    }
//    // Globals::ShrinkList(Stack, resize);// does this really work?
//  }
//  /* ********************************************************************************* */
//  int Compare(IMoveable& thing0, IMoveable& thing1) { return 0; }
//  /* ********************************************************************************* */
//  void Copy_Stack(ArrayList<StackItem>& StackPrev, ArrayList<StackItem>& StackNext) {
//    TruncateStack(StackNext, 0);
//    int len = StackPrev.size();
//    StackItem siprev, sinext;
//    for (int cnt = 0; cnt < len; cnt++) {
////      siprev = StackPrev.get(cnt);
////      sinext.Copy_From(siprev);
////      StackNext.add(sinext);
//      StackNext.add(StackPrev.get(cnt));
//    }
//  }
//  /* ********************************************************************************* */
//  void UnMapThroughStack(SoundFloat XLoc, SoundFloat YLoc, Point2D& results) {
//    int FinalLoc = this->Best_Stack.size() - 1;
//    Point2D pntfrom, pntto;
//    pntfrom.SetLocation(XLoc, YLoc);
//    pntto.SetLocation(pntfrom);// in case of no mapping at all, default to original coordinates
//    StackItem si;
//    for (int cnt = FinalLoc; cnt >= 0; cnt--) {
//      si = this->Best_Stack.get(cnt);
//      si.OBox->UnMap(pntfrom, pntto);
//      pntfrom.SetLocation(pntto);
//    }
//    results.SetLocation(pntto);
//  }
//  /* ********************************************************************************* */
//  void MapThroughStack(SoundFloat XLoc, SoundFloat YLoc, Point2D& results) {}
//  /* ********************************************************************************* */
//  void MapThroughStack(SoundFloat XLoc, SoundFloat YLoc, MonkeyBox& startplace, Point2D& results) {
//    int len = this->Best_Stack.size();
//    StackItem si;
//    Point2D pntfrom, pntto;
//    pntfrom.SetLocation(XLoc, YLoc);
//    pntto.SetLocation(pntfrom);// in case of no mapping at all, default to original coordinates
//    int cnt = 0;
//    while (cnt < len) {
//      si = this->Best_Stack.get(cnt);
//      if (si.OBox == &startplace) {// start at startplace
//        break;
//      }
//      cnt++;
//    }
//    while (cnt < len) {
//      si = this->Best_Stack.get(cnt);
//      si.OBox->MapTo(pntfrom, pntto);
//      pntfrom.SetLocation(pntto);
//      cnt++;
//    }
//    results.SetLocation(pntto);
//  }
//  /* ********************************************************************************* */
//  void CompoundStack(MonkeyBox& startplace, MonkeyBox& results) {
//    results.Clear();// crush all transformations to one obox
//    int len = this->Best_Stack.size();
//    StackItem si;
//    int cnt = 0;
//    while (cnt < len) {
//      si = this->Best_Stack.get(cnt);
//      if (si.OBox == &startplace) {// start at startplace
//        break;
//      }
//      cnt++;
//    }
//    while (cnt < len) {
//      si = this->Best_Stack.get(cnt);
//      results.Compound(*(si.OBox));// this becomes a transformation from screen down to object.
//      cnt++;
//    }
//  }
//  /* ************StackItem********************************************************************* */
//  void UpdateBoundingBoxes() {
//    this->Leaf->UpdateBoundingBoxLocal();
//    int lastitem = this->Best_Stack.size() - 1;
//    StackItem si;
//    for (int cnt = lastitem; cnt >= 0; cnt--) {
//      si = this->Best_Stack.get(cnt);
//      si.OBox->UpdateBoundingBoxLocal();
//    }
//  }
//  /* ********************************************************************************* */
//  boolean Intersects(CajaDelimitadora& OtherBounds) override {
//    return this->CurrentContext->SearchBounds.Intersects(OtherBounds);
//  }
//  /* ********************************************************************************* */
//  TimeFloat GetX() override {
//    return this->CurrentContext->Loc.X;
//  }
//  SoundFloat GetY() override {
//    return this->CurrentContext->Loc.Y;
//  }
//};

/* ********************************************************************************* */
//class DestinationGrabber: public Grabber3 {// this class searches for containers in which to drop a floating, copied songlet
//public:
//  OffsetBoxBase* Floater = nullptr;
//  GroupSong* PossibleDestination = nullptr;
//  SoundFloat ClosestDistance = Double::POSITIVE_INFINITY;
//  /* ********************************************************************************* */
//  void AddFirstBox(OffsetBoxBase& starter, SoundFloat XLoc, SoundFloat YLoc) override {
//    this->PossibleDestination = nullptr;
//    this->ClosestDistance = Double::POSITIVE_INFINITY;
//    Grabber3::AddFirstBox(starter, XLoc, YLoc);
//  }
//  /* ********************************************************************************* */
//  void ConsiderLeaf(IMoveable* CandidateLeaf) override {
//#if false
//    if (CandidateLeaf instanceof OffsetBox) {// only one that works so far
//      OffsetBoxBase *obx = (OffsetBoxBase*) &(CandidateLeaf);// another cast!
//      ISonglet *songlet = obx->GetContent();
//      if (songlet instanceof GroupSong*) {
//        GroupSong *gbx = (GroupSong*) songlet;// other cast!
//        Point2D results;
//        obx->MapTo(this->CurrentContext.Loc.X, this->CurrentContext.Loc.Y, results);// we're hitting the songlet, not its offsetbox, so we have to map to obox child coordinates.
//        double FoundDistance;
//        FoundDistance = gbx->HitsMyVineSpline(results.X, results.Y);// WIP
////          if ((FoundDistance < this->ClosestDistance) && this->Stack_Depth_Best <= this->Stack_Depth) {// prefer the most distal
//        //if ((FoundDistance < this->ClosestDistance) || ((FoundDistance == this->ClosestDistance) && this->Stack_Depth_Best <= this->Stack_Depth)) {// prefer the most distal
//        if (FoundDistance < this->ClosestDistance) {// prefer the closest
//          this->Stack_Depth_Best = this->Stack_Depth;// or if equal, prefer the last drawn (most recent hit)
//          this->Leaf = &CandidateLeaf;
//          PossibleDestination = gbx;
//          this->ClosestDistance = FoundDistance;
//          this->Copy_Stack(this->Explore_Stack, this->Best_Stack);
//        }
//      }
//    }
//#endif // false
//  }
//  /* ********************************************************************************* */
//  boolean KeepDigging(IMoveable* Candidate) override {
//#if false
//    if (Candidate instanceof OffsetBox) {
//      OffsetBox *obx = (OffsetBox*) &Candidate;// another cast!
//      ISonglet *songlet = obx->GetContent();
//      if (this->Floater->GetContent() == songlet) {// Never drop a songlet inside of itself or you will get a stack overflow.
//        return false;
//      }
//    }
//    return (this->CurrentContext.SearchBounds.Intersects(Candidate.GetBoundingBox()));
//#else
//    return false;
//#endif // false
//  }
//};

#endif
