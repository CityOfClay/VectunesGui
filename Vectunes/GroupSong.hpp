#ifndef GroupSong_hpp
#define GroupSong_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "IDrawable.hpp"
#include "ISonglet.hpp"

/**
 *
 * @author MultiTool
*/

// class AudProject;// forward

class GroupSong: public IContainer {
public:
  ArrayList<OffsetBoxBase*> SubSongs;
  StringConst SubSongsName = "SubSongs";
  StringConst GroupSongName = "GroupSong";
  SoundFloat Duration = 0.0;
  String MyName;// for debugging
  SoundFloat MaxAmplitude = 0.0;

  // to do: These will belong to the Artist object after we code for that separation
  GlowType SpineGlow = GlowType::Nothing;
  // Color* LineColor = nullptr;
  Color LineColor;
  bool UsingSplines = true;
  int NumSubLines = 10;
  ArrayList<Point2D> SplinePoints;
  /* ********************************************************************************* */
  GroupSong() {
    mytype = 6;
    RefCount = 0;
    FreshnessTimeStamp = 0;
    this->Randomize_Color();
//    double RandFract = Math::frand();// Globals::Random::NextDouble();
//    Color::ToColorWheel(RandFract, this->LineColor);
  }
  virtual ~GroupSong() {this->Delete_Me();}
  /* ********************************************************************************* */
  virtual OffsetBoxBase* Add_SubSong(ISonglet& songlet, SoundFloat TimeOffset, SoundFloat OctaveOffset, SoundFloat LoudnessFactor) {
    OffsetBoxBase *obox = songlet.Spawn_OffsetBox();
    this->Add_SubSong(obox, TimeOffset, OctaveOffset, LoudnessFactor);
    return obox;// even though we return the offsetbox, it is groupsong's responsibility to delete the offsetbox.
  }
  /* ********************************************************************************* */
  virtual void Add_SubSong(OffsetBoxBase* obox, SoundFloat TimeOffset, SoundFloat OctaveOffset, SoundFloat LoudnessFactor) {// Add a songlet with its offsetbox already created.
    obox->TimeX = (TimeOffset);
    obox->OctaveY = (OctaveOffset);
    obox->LoudnessFactor = (LoudnessFactor);
    this->Add_SubSong(obox);
  }
  /* ********************************************************************************* */
  virtual int Add_SubSong(OffsetBoxBase* obox) {// Add a songlet with its offsetbox already created and filled out.
    obox->MyParentSong = this;
    int dex = this->Tree_Search(obox->TimeX, 0, SubSongs.size());
    SubSongs.insert(SubSongs.begin() + dex, obox);
    int sz = SubSongs.size();
    Refresh_Splines();// maybe sort_me and refresh_splines should be in update_guts instead?
    return dex;
  }
  /* ********************************************************************************* */
  void Remove_SubSong(OffsetBoxBase* obox) {
    this->SubSongs.remove(obox);
    obox->MyParentSong = nullptr;
    Refresh_Splines();
  }
  /* ********************************************************************************* */
  void Remove_Child(OffsetBoxBase* child) override {// IContainer
    Remove_SubSong(child);
    delete child;
  }
  void Add_Child(OffsetBoxBase* child) override {// IContainer
    Add_SubSong(child);
  }
  /* ********************************************************************************* */
  void Refresh_Splines() {
    int SplineSize = (this->SubSongs.size()) * NumSubLines + 1;
    this->SplinePoints.resize(SplineSize);
    Splines::Cubic_Spline_Boxes(this->SubSongs, NumSubLines, SplinePoints);
  }
  /* ********************************************************************************* */
  SoundFloat Get_Duration() override {
    return this->Duration;
  }
  /* ********************************************************************************* */
  SoundFloat Get_Max_Amplitude() override {
    return this->MaxAmplitude;
  }
  /* ********************************************************************************* */
  void Randomize_Color() override {
    double RandFract = Math::frand();
    Color::ToColorWheel(RandFract, this->LineColor);

    int len = this->SubSongs.size();
    for (int pcnt = 0; pcnt < len; pcnt++) {
      this->SubSongs.at(pcnt)->Randomize_Color();
    }
  }
  /* ********************************************************************************* */
  void Update_Max_Amplitude() {
    int len = this->SubSongs.size();
    OffsetBoxBase *pnt;
    SoundFloat MaxAmp = 0.0;
    for (int pcnt = 0; pcnt < len; pcnt++) {
      pnt = this->SubSongs.at(pcnt);
      SoundFloat Amp = pnt->Get_Max_Amplitude();
      MaxAmp += Amp;// this is overkill, we need to only sum those subsongs that overlap.
    }
    this->MaxAmplitude = MaxAmp;
  }
  /* ********************************************************************************* */
  void Update_Guts(MetricsPacket& metrics) override {
    if (this->FreshnessTimeStamp != metrics.FreshnessTimeStamp) {// don't hit the same songlet twice on one update
      this->MyProject = metrics.MyProject;
      //this->Sort_Me();
      this->Update_Max_Amplitude();
      metrics.MaxDuration = 0.0;// redundant
      SoundFloat MyMaxDuration = 0.0;
      SoundFloat DurBuf = 0.0;
      int NumSubSongs = this->SubSongs.size();
      for (int cnt = 0; cnt < NumSubSongs; cnt++) {
        OffsetBoxBase *ChildOffsetBox = this->SubSongs.at(cnt);
        // ISonglet *songlet = ChildOffsetBox->GetContent();
        metrics.MaxDuration = 0.0;
        ChildOffsetBox->Update_Guts(metrics);
        if (MyMaxDuration < (DurBuf = (ChildOffsetBox->UnMap_EndTime()))) {// snox experimental, seems to work
          MyMaxDuration = DurBuf;
        }
      }
      this->Duration = MyMaxDuration;
      this->UpdateBoundingBoxLocal();
      this->FreshnessTimeStamp = metrics.FreshnessTimeStamp;
      this->Sort_Me();
      this->Refresh_Splines();
    }
    metrics.MaxDuration = this->Duration;
  }
  /* ********************************************************************************* */
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {
    //System.out.println("Refresh_Me_From_Beneath");
    this->Sort_Me();
//    int Dex = this->Tree_Search(mbox.GetX(), 0, this->SubSongs.size());
//    this->Bubble_Right(Dex);// how do we get the right index? OffsetBoxes do not have that.
    if (UsingSplines) {
      Splines::Cubic_Spline_Boxes(this->SubSongs, NumSubLines, this->SplinePoints);
    }
  }
  /* ********************************************************************************* */
  void Bubble_Right(int Dex) {// when a point moves right in space, move it to the correct place in the collection.
    int len = this->SubSongs.size();
    OffsetBoxBase *mov = this->SubSongs.at(Dex);
    OffsetBoxBase *next;
    int PrevDex = Dex++;
    while (Dex < len) {
      next = this->SubSongs.at(Dex);
      if (mov->TimeX <= next->TimeX) {
        break;
      }
      this->SubSongs.at(PrevDex) = next;
      PrevDex = Dex++;
    }
    this->SubSongs.at(PrevDex) = mov;
  }
  /* ********************************************************************************* */
  void Sort_Me() {// override {// sorting by RealTime, TimeX
    std::sort(this->SubSongs.begin(), this->SubSongs.end(), CompareChildren);
  }
  /* ********************************************************************** */
  static bool CompareChildren(const OffsetBoxBase* obox0, const OffsetBoxBase* obox1) {
    return obox0->TimeX < obox1->TimeX;
  }
  /* ********************************************************************************* */
  void SetSpineHighlight(bool Highlight) {
    if (Highlight) {
      this->SpineGlow = GlowType::Yellow;
    } else {
      this->SpineGlow = GlowType::Nothing;
    }
  }
  /* ********************************************************************************* */
  void Draw_Lines(DrawingContext& ParentDC, int StartDex, int EndDex) {
    // Draw Group spine
    EndDex = this->SplinePoints.size() - 1;// inclusive
    if (EndDex<=0) { return; }
    Point2D pntprev, pnt;
    int pcnt;
    Point2D *SplinePoint;
    ParentDC.To_Screen(0, 0, pntprev);
    pcnt = StartDex;
    while (pcnt <= EndDex) {
      SplinePoint = &(this->SplinePoints.at(pcnt));
      ParentDC.To_Screen(SplinePoint->X, SplinePoint->Y, pnt);
      ParentDC.DrawLine(pntprev.X, pntprev.Y, pnt.X, pnt.Y);
      pntprev.Copy_From(pnt);
      if (ParentDC.ClipBounds.Max.X < SplinePoint->X) {// break from loop if subsong starts after MaxX.
        EndDex = pcnt;
        break;
      }
      pcnt++;
    }
    ParentDC.To_Screen(0, 0, pntprev);
    pcnt = StartDex;
  }
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& ParentDC) override {// IDrawable
    PROFILE_START;
    OffsetBoxBase *ChildOffsetBox;
    int len = this->SubSongs.size();
    int StartDex = 0;// not sure how to get the first within clip box without just iterating from 0. treesearch?
    int EndDex = len - 1;// inclusive
    int pcnt;
    if (UsingSplines) {
      switch (this->SpineGlow) {
        case GlowType::Nothing : {
        }
        break;
        case GlowType::Blue : {
        }
        break;
        case GlowType::Red : {
        }
        break;
        case GlowType::Yellow : {
          ParentDC.SetLineWidth(3.0);
          ParentDC.SetColor(1.0, 1.0, 0.0, 1.0);
          Draw_Lines(ParentDC, StartDex, EndDex);
        }
        break;
      }
      ParentDC.SetLineWidth(1.0);
      ParentDC.SetColor(this->LineColor);
      Draw_Lines(ParentDC, StartDex, EndDex);
    } else {
      // Draw Group spine
      Point2D pntprev, pnt;
    }
    // Draw children
    for (pcnt = StartDex; pcnt <= EndDex; pcnt++) {
      ChildOffsetBox = this->SubSongs.get(pcnt);
      ChildOffsetBox->Draw_Me(ParentDC);
    }
    PROFILE_END(Globals::GroupSong_Hits);
  }
  void UpdateBoundingBoxLocal() override {// IDrawable
    OffsetBoxBase *ChildOffsetBox;
    CajaDelimitadora *ChildBBoxUnMapped;
    this->MyBounds.Reset();
    int len = this->SubSongs.size();
    if (len == 0) {
      this->MyBounds.ClearZero();
    } else {
      for (int pcnt = 0; pcnt < len; pcnt++) {
        ChildOffsetBox = this->SubSongs.at(pcnt);
        ChildBBoxUnMapped = ChildOffsetBox->GetBoundingBox();// project child limits into parent (my) space
        this->MyBounds.Include(*ChildBBoxUnMapped);
      }
    }
  }
  /* ********************************************************************************* */
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
    CajaDelimitadora PixelBounds;
    StackContext.UnMap(this->MyBounds, PixelBounds);// Map local bounds to root pixel space.
    if (Scoop.Intersects(PixelBounds)) {
      int len = this->SubSongs.size();
      OffsetBoxBase *child;
      for (int pcnt = 0; pcnt < len; pcnt++) {
        child = this->SubSongs.at(pcnt);
        child->GoFishing(Scoop, StackContext);
      }
    }
  }
  /* ********************************************************************************* */
  void GetInventory(InventoryList& Hallazgos) override {// get all unique containers in me
    if (this->FreshnessTimeStamp < Hallazgos.TimeStamp) { // stale, so add me.
      this->FreshnessTimeStamp = Hallazgos.TimeStamp;
      Hallazgos.add(this);
      int len = this->SubSongs.size();
      OffsetBoxBase *child;
      for (int pcnt = 0; pcnt < len; pcnt++) {
        child = this->SubSongs.at(pcnt);
        child->GetInventory(Hallazgos);
      }
    } else {
      return; // Been here already, do nothing.
    }
  }
  /* ********************************************************************************* */
  void Align_Kids() {
    int len = this->SubSongs.size();
    if (len>0) {
      OffsetBoxBase *child = this->SubSongs.at(0);
      Move_Kids(-child->TimeX, -child->OctaveY);
    }
  }
  /* ********************************************************************************* */
  void Move_Kids(TimeFloat MoveX, SoundFloat MoveY) {
    int len = this->SubSongs.size();
    OffsetBoxBase *child;
    // to do: limit so earliest child can't go below 0.
    if (len>0) {
      child = this->SubSongs.at(0);
      if ((child->TimeX + MoveX) < 0) {
        MoveX = -child->TimeX;
      }
      for (int pcnt = 0; pcnt < len; pcnt++) {
        child = this->SubSongs.at(pcnt);
        child->MoveBy(MoveX, MoveY);
      }
    }
  }
  /* ********************************************************************************* */
  void Burn_Scale(int TimeStamp, TimeFloat ReScaleX, SoundFloat ReScaleY) override {
    if (this->FreshnessTimeStamp < TimeStamp) { // stale, so update.
      this->FreshnessTimeStamp = TimeStamp;
      int len = this->SubSongs.size();
      OffsetBoxBase *child;
      for (int pcnt = 0; pcnt < len; pcnt++) {
        child = this->SubSongs.at(pcnt);
        child->Burn_Scale(TimeStamp, ReScaleX, ReScaleY);
      }
    }
  }
  /* ********************************************************************************* */
  bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override {// maybe move this to IDrawable?
    // return false;// disabled by default, expand later.
    return HitsMyVineSpline(Scoop, StackContext);// work in progress for drag and drop support
  }
  /* ********************************************************************************* */
  GroupSong* Clone_Me() override {// ICloneable
    GroupSong *clone = new GroupSong();
    clone->Copy_From(*this);
    return clone;
  }
  /* ********************************************************************************* */
  GroupSong* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    GroupSong *clone;
    CollisionItem *ci = HitTable.GetItem(this);
    if (ci == nullptr) {
      clone = this->Clone_Me();
      //clone = new GroupSong(); clone->Copy_From(*this);// clone
      ci = HitTable.InsertUniqueInstance(this);
      ci->Hydrated = clone;
      OffsetBoxBase *SubSongHandle;
      int len = this->SubSongs.size();
      for (int cnt = 0; cnt < len; cnt++) {
        SubSongHandle = this->SubSongs.at(cnt);
        OffsetBoxBase *obox = SubSongHandle->Deep_Clone_Me(HitTable);
        clone->Add_SubSong(obox);
      }
    } else {// pre exists
      clone = (GroupSong*) ci->Hydrated;// another cast!
    }
    return clone;
  }
  /* ********************************************************************************* */
  GroupSong* Shallow_Clone_Me() {
    /*
     clone-me clones myself, clones all my children oboxes, but does NOT clone their songlets.
     */
    GroupSong *child;
    child = new GroupSong();// clone
    //child->TraceText = "I am a shallow clone";
    child->Copy_From(*this);
    OffsetBoxBase *SubSongHandle, *ChildSubSongHandle;
    ISonglet *songlet;
    int len = this->SubSongs.size();
    for (int cnt = 0; cnt < len; cnt++) {
      SubSongHandle = this->SubSongs.at(cnt);
      songlet = SubSongHandle->GetContent();
      ChildSubSongHandle = songlet->Spawn_OffsetBox();
      ChildSubSongHandle->Copy_From(*SubSongHandle);
      child->Add_SubSong(ChildSubSongHandle);
    }
    return child;
  }
  /* ********************************************************************************* */
  void Copy_From(const GroupSong& donor) {
    this->Duration = donor.Duration;
    this->MyProject = donor.MyProject;
    this->MyName = donor.MyName;// for debugging
    this->MaxAmplitude = donor.MaxAmplitude;
    this->LineColor = donor.LineColor;
    this->FreshnessTimeStamp = 0;// donor.FreshnessTimeStamp;
    IDrawable::Copy_From(donor); //this->MyBounds.Copy_From(donor.MyBounds);
  }
  /* ********************************************************************************* */
  void RescaleGroupTimeX(SoundFloat Factor) {
    int len = this->SubSongs.size();
    for (int cnt = 0; cnt < len; cnt++) {
      OffsetBoxBase *obox = this->SubSongs.at(cnt);
      obox->TimeX *= Factor;
    }
  }
  /* ********************************************************************************* */
  static SoundFloat DotProduct(SoundFloat X0, SoundFloat Y0, SoundFloat X1, SoundFloat Y1) {
    return X0 * X1 + Y0 * Y1;// length of projection from one vector onto another
  }
  /* ********************************************************************************* */
  static void LineClosestPoint(Point2D LineEnd0, Point2D LineEnd1, Point2D Pnt, Point2D& Intersection) {// Find dnd destination using dot product on line segments.
    Point2D Temp;
    if (LineEnd1.X < LineEnd0.X || (LineEnd1.X == LineEnd0.X && LineEnd1.Y < LineEnd0.Y)) {// sort endpoints
      Temp = LineEnd0;// swap ends
      LineEnd0 = LineEnd1;
      LineEnd1 = Temp;
    }
    SoundFloat XDif = LineEnd1.X - LineEnd0.X, YDif = LineEnd1.Y - LineEnd0.Y;
    SoundFloat Shrink = Globals::Fudge;// shrink is a cheat so consecutive lines have slightly separate endpoints. this makes mouse distance from their endpoints unequal.
    SoundFloat ShrinkX = XDif * Shrink, ShrinkY = YDif * Shrink;
    LineEnd0.X += ShrinkX;
    LineEnd0.Y += ShrinkY;
    LineEnd1.X -= ShrinkX;
    LineEnd1.Y -= ShrinkY;
    XDif -= ShrinkX * 2;
    YDif -= ShrinkY * 2;
    SoundFloat Magnitude = Math::hypot(XDif, YDif);

    SoundFloat DotProd = DotProduct(Pnt.X - LineEnd0.X, Pnt.Y - LineEnd0.Y, XDif, YDif);
    DotProd /= Magnitude;// now dotprod is the full length of the projection
    SoundFloat XLoc = ((XDif / Magnitude) * DotProd);// scale separate dimensions to length of shadow
    SoundFloat YLoc = ((YDif / Magnitude) * DotProd);
    if (XLoc > XDif) {// Test if the intersection is between the line's endpoints and cap them.
      //System.out.println("XLoc:" + XLoc + ", YLoc:" + YLoc);
      XLoc = XDif;
      YLoc = YDif;
    } else if (XLoc < 0) {
      XLoc = YLoc = 0;
    }
    XLoc += LineEnd0.X + ShrinkX;
    YLoc += LineEnd0.Y + ShrinkY;
    Intersection.SetLocation(XLoc, YLoc);
  }
  /* ********************************************************************************* */
  static void LineClosestPoint(SoundFloat LineX0, SoundFloat LineY0, SoundFloat LineX1, SoundFloat LineY1, SoundFloat XPnt, SoundFloat YPnt, Point2D& Intersection) {// Find dnd destination using dot product on line segments.
    SoundFloat Temp;
    if (LineX1 < LineX0 || (LineX1 == LineX0 && LineY1 < LineY0)) {// sort endpoints
      Temp = LineX0;// swap X
      LineX0 = LineX1;
      LineX1 = Temp;

      Temp = LineY0;// swap Y
      LineY0 = LineY1;
      LineY1 = Temp;
    }
    SoundFloat XDif = LineX1 - LineX0, YDif = LineY1 - LineY0;
    SoundFloat Shrink = Globals::Fudge;// shrink is a cheat so consecutive lines have slightly separate endpoints. this makes mouse distance from their endpoints unequal.
    SoundFloat ShrinkX = XDif * Shrink, ShrinkY = YDif * Shrink;
    LineX0 += ShrinkX;
    LineY0 += ShrinkY;
    LineX1 -= ShrinkX;
    LineY1 -= ShrinkY;
    XDif -= ShrinkX * 2;
    YDif -= ShrinkY * 2;
    SoundFloat Magnitude = Math::hypot(XDif, YDif);

    SoundFloat DotProd = DotProduct(XPnt - LineX0, YPnt - LineY0, XDif, YDif);
    DotProd /= Magnitude;// now dotprod is the full length of the projection
    SoundFloat XLoc = ((XDif / Magnitude) * DotProd);// scale separate dimensions to length of shadow
    SoundFloat YLoc = ((YDif / Magnitude) * DotProd);
    if (XLoc > XDif) {// Test if the intersection is between the line's endpoints and cap them.
      //System.out.println("XLoc:" + XLoc + ", YLoc:" + YLoc);
      XLoc = XDif;
      YLoc = YDif;
    } else if (XLoc < 0) {
      XLoc = YLoc = 0;
    }
    XLoc += LineX0 + ShrinkX;
    YLoc += LineY0 + ShrinkY;
    Intersection.SetLocation(XLoc, YLoc);
  }
  /* ********************************************************************************* */
  static SoundFloat DistanceFromLine(SoundFloat LineX0, SoundFloat LineY0, SoundFloat LineX1, SoundFloat LineY1, SoundFloat XPnt, SoundFloat YPnt) {// work in progress for drag and drop support
    SoundFloat XDif = LineX1 - LineX0, YDif = LineY1 - LineY0;
    SoundFloat DotProd = DotProduct(XDif, YDif, XPnt - LineX0, YPnt - LineY0);
    SoundFloat XLoc = LineX0 + (XDif * DotProd);// point of intersection
    SoundFloat YLoc = LineY0 + (YDif * DotProd);
    // to do: at this point we would like to test if the intersection is between the line's endpoints.
//    SoundFloat XLoc = (XDif * DotProd);// point of intersection
//    SoundFloat YLoc = (YDif * DotProd);
//    SoundFloat PntDX = (XLoc - XPnt); SoundFloat PntDY = (YLoc - YPnt);
    SoundFloat Distance = Math::hypot(XLoc - XPnt, YLoc - YPnt);
    return Distance;
  }
  /* ********************************************************************************* */
  static SoundFloat HeightFromLine(SoundFloat LineX0, SoundFloat LineY0, SoundFloat LineX1, SoundFloat LineY1, SoundFloat XPnt, SoundFloat YPnt) {// work in progress for drag and drop support
    SoundFloat YCross = LineYCross(LineX0, LineY0, LineX1, LineY1, XPnt);
    SoundFloat Distance = YPnt - YCross;
    return Distance;
  }
  /* ********************************************************************************* */
  static SoundFloat LineYCross(SoundFloat LineX0, SoundFloat LineY0, SoundFloat LineX1, SoundFloat LineY1, SoundFloat XPnt) {// work in progress for drag and drop support
    SoundFloat XDif = LineX1 - LineX0;// given a line and an X point, return the Y location of the intersect along that line
    SoundFloat YCross;
    if (XDif == 0) {// If slope is infinite, just return the halfway point between the top and bottom of the line.
      YCross = (LineY0 + LineY1) / 2.0;
    } else {
      SoundFloat YDif = LineY1 - LineY0;
      SoundFloat Slope = YDif / XDif;
      SoundFloat PointXOffset = (XPnt - LineX0);
      YCross = LineY0 + (PointXOffset * Slope);
    }
    return YCross;
  }
  /* ********************************************************************************* */
  SoundFloat HitsMyVineSpline(IGrabber& Scoop, FishingStackContext& StackContext) {// work in progress for drag and drop support
    // See if this spline intersects the search box in any way.
//    ArrayList<Point2D> SplineBridge;// Only 1 bridge connecting 2 voicepoints.
//    CajaDelimitadora SearchBox;
//    if (Splines::BoxHit(SearchBox, SplineBridge)) { return true; }

    CajaDelimitadora SearchBox;
    StackContext.MapTo(*(Scoop.GetSearchBox()), SearchBox);

    return (Splines::BoxHit(SearchBox, this->SplinePoints));
    // to do:
    // compound audiroot-to-local transform with space between two voicepoints.
    // next transform search box to the above extra-local space.
  }
  /* ********************************************************************************* */
  SoundFloat HitsMyVineSpline(SoundFloat XPnt, SoundFloat YPnt) {// work in progress for drag and drop support
    SoundFloat Limit = 0.1;// octaves.  hardcoded hack, need something better
    int len = this->SubSongs.size();
    Point2D Target(XPnt, YPnt);
    SoundFloat Dist;
    OffsetBoxBase *LastBox = this->SubSongs.at(len - 1);
    Point2D Intersection;
    if (0.0 <= XPnt && XPnt <= LastBox->TimeX) {// or this->MyBounds.Max.X) {
      int FoundDex = Tree_Search(XPnt, 0, len);// to do: tree search with buffer around click point
      int LinesPerSubSong = this->SplinePoints.size() / this->SubSongs.size();

      // Splines start to the left of subsong[0], so their indexes are higher than the subsongs they correspond to.
      int StartSplineDex = FoundDex * LinesPerSubSong;
      int EndSplineDex = (FoundDex + 1) * LinesPerSubSong;
      EndSplineDex = Math::min(EndSplineDex, this->SplinePoints.size());

      Point2D prevpnt, nowpnt(0, 0);// = Point2D.Zero;
      SoundFloat MinDist = Double::POSITIVE_INFINITY;
      for (int scnt = StartSplineDex; scnt < EndSplineDex; scnt++) {// roll through all segments in this subrange of this spline. look for closest segment.
        prevpnt = nowpnt;
        nowpnt = this->SplinePoints[scnt];
        //LineClosestPoint(prevpnt.X, prevpnt.Y, nowpnt.X, nowpnt.Y, XPnt, YPnt, Intersection);
        LineClosestPoint(prevpnt, nowpnt, Target, Intersection);
        Dist = Math::hypot(XPnt - Intersection.X, YPnt - Intersection.Y);
        //System.out.println("Dist:" + Dist + ", Intersection.X:" + Intersection.X + ", Intersection.Y:" + Intersection.Y);
        if (MinDist > Dist) {
          MinDist = Dist;
        }
      }
      if (MinDist < Limit) {// then we found one
        return MinDist;
      }
      // to do: need condition if FoundDex is greater than len. beyond-end insertion would be nice.
    }
    return Double::POSITIVE_INFINITY;// infinite if not found
  }
  /* ********************************************************************************* */
  SoundFloat HitsMyVine(SoundFloat XPnt, SoundFloat YPnt) {// work in progress for drag and drop support
    SoundFloat Limit = 0.1;// octaves.  hardcoded hack, need something better
    int len = this->SubSongs.size();
    OffsetBoxBase *OBox, *ClosestPoint = null;
    SoundFloat XPrev = 0, YPrev = 0, YCross, YDist, Dist;
    OffsetBoxBase *LastBox = this->SubSongs.at(len - 1);
    Point2D Intersection;
    if (0.0 <= XPnt && XPnt <= LastBox->TimeX) {// or this->MyBounds.Max.X) {
//      int FoundDex = Tree_Search(XPnt - Limit, 0, len);
      int FoundDex = Tree_Search(XPnt, 0, len);
      if (FoundDex == 0) {// X point equals first element in subsong array
        XPrev = YPrev = 0;
      } else {
        OffsetBoxBase *PrevBox = this->SubSongs.at(FoundDex - 1);
        XPrev = PrevBox->TimeX;
        YPrev = PrevBox->OctaveY;
      }
      // to do: need condition if FoundDex is greater than len. beyond-end insertion would be nice.
      OBox = this->SubSongs.at(FoundDex);

      if (true) {
        LineClosestPoint(XPrev, YPrev, OBox->TimeX, OBox->OctaveY, XPnt, YPnt, Intersection);
        Dist = Math::hypot(XPnt - Intersection.X, YPnt - Intersection.Y);
        //System.out.println("Dist:" + Dist + ", Intersection.X:" + Intersection.X + ", Intersection.Y:" + Intersection.Y);
        if (Dist < Limit) {// then we found one
          ClosestPoint = this->SubSongs.at(FoundDex);
          return Dist;
        }
      } else {
        YCross = LineYCross(XPrev, YPrev, OBox->TimeX, OBox->OctaveY, XPnt);
        YDist = Math::abs(YPnt - YCross);
        if (YDist < Limit) {// then we found one
          ClosestPoint = this->SubSongs.at(FoundDex);
          return YDist;
        }
      }
    }
    return Double::POSITIVE_INFINITY;// infinite if not found
  }
  /* ************************************************************************************************************************ */
  int Roll_Search_Spline(ArrayList<Point2D>& SplinePoints, SoundFloat TimeTarget) {// iterative search
    int len = SplinePoints.size();
    Point2D Prev(0, 0), Current(0, 0);
    int cnt;
    for (cnt=0; cnt<len; cnt++) {
      Current = SplinePoints.at(cnt);
      if (TimeTarget <= Current.X) {
        break;
      }
      Prev = Current;
    }
    // point[cnt].X will always be 1 past the location of target?
    // returns size (past end) if target is after last point.
    // returns 0 if len is 0.
    // if target is lower than first item, returns index of first item, which is 0.
    // what we want to return is start and end of the line.  so if first, 0,0 to point[0].  anything else, point[cnt-1] to point[cnt].
    //
    return cnt;
  }
  /* ************************************************************************************************************************ */
  int Tree_Search_Spline(ArrayList<Point2D>& SplinePoints, SoundFloat TimeTarget, int minloc, int maxloc) {// finds place where time would be inserted or replaced
    int medloc;// This function assumes SplinePoints is sorted by X, and never doubles back on itself.
    minloc = Math::max(minloc, 0);
    maxloc = Math::min(maxloc, SplinePoints.size());
    while (minloc < maxloc) {
      medloc = (minloc + maxloc) >> 1; // >>1 is same as div 2, only faster.
      if (TimeTarget <= SplinePoints.at(medloc).X) {
        maxloc = medloc;
      } else {
        minloc = medloc + 1;/* has to go through here to be found. */
      }
    }
    return minloc;
  }
  /* ************************************************************************************************************************ */
  int Tree_Search(SoundFloat TimeTarget, int minloc, int maxloc) {// finds place where time would be inserted or replaced
    int medloc;
    while (minloc < maxloc) {
      medloc = (minloc + maxloc) >> 1; // >>1 is same as div 2, only faster.
      if (TimeTarget <= this->SubSongs.at(medloc)->TimeX) {
        maxloc = medloc;
      } else {
        minloc = medloc + 1;/* has to go through here to be found. */
      }
    }
    return minloc;
  }
  /* ************************************************************************************************************************ */
  int Tree_Search_Last(SoundFloat TimeTarget, int minloc, int maxloc) {// finds END OF run where time would be inserted or replaced
    int medloc;
    while (minloc < maxloc) {
      medloc = (minloc + maxloc) >> 1; // >>1 is same as div 2, only faster.
      if (TimeTarget < this->SubSongs.at(medloc)->TimeX) {
        maxloc = medloc;
      } else {
        minloc = medloc + 1;/* has to go through here to be found. */
      }
    }
    return minloc;
  }
  /* ********************************************************************************* */
  void Space_Evenly() {
    int NumKids = this->SubSongs.size();
    if (NumKids == 0) { return; }
    int FirstKid = 0;
    int FinalKid = NumKids - 1;
    OffsetBoxBase *obox = this->SubSongs.at(FirstKid);
    SoundFloat XStart = obox->TimeX;
    obox = this->SubSongs.at(FinalKid);
    SoundFloat XLimit = obox->TimeX;
    SoundFloat Span = XLimit - XStart;
    SoundFloat FractAlong;
    for (int cnt = 0; cnt <= FinalKid; cnt++) {// do we want it to be  ---.---.---.  or  .---.---.  or  .---.---.---  ?
      obox = this->SubSongs.at(cnt);
      FractAlong = (((SoundFloat) cnt) / (SoundFloat) FinalKid);//  .---.---.  spacing
      obox->TimeX = XStart + (Span * FractAlong);
    }
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {// IDeletable
    return true;
  }
  void Delete_Me() override {// IDeletable
    this->MyBounds.Delete_Me();// wreck everything to prevent accidental re-use
    this->MyProject = nullptr;
    this->Wipe_SubSongs();
    this->Duration = Double::NEGATIVE_INFINITY;
    this->FreshnessTimeStamp = Integer_MAX_VALUE;
  }
  virtual void Wipe_SubSongs() {
    int len = this->SubSongs.size();
    OffsetBoxBase *obox;
    for (int cnt = 0; cnt < len; cnt++) {
      obox = this->SubSongs.at(cnt);
      delete obox;
    }
    this->SubSongs.clear();
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *phrase = new JsonParse::HashNode();
    phrase->AddSubPhrase(Globals::ObjectTypeName, PackStringField(GroupSong::GroupSongName));// self name
    phrase->AddSubPhrase("MyName", ITextable::PackStringField(this->MyName));
    // Save my array of songlets.
    JsonParse::ArrayNode *CPointsPhrase = new JsonParse::ArrayNode();
    ITextable::MakeArray<OffsetBoxBase>(HitTable, this->SubSongs, *CPointsPhrase);
    phrase->AddSubPhrase(GroupSong::SubSongsName, CPointsPhrase);
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    this->MyName = phrase.GetField("MyName", "GroupBoxName");
  }
  /* ********************************************************************************* */
  class Group_Singer: public SingerBase {
  public:
    GroupSong* MySonglet;
    ArrayList<SingerBase*> NowPlaying;// pool of currently playing voices
    int Current_Dex = 0;
    SoundFloat Prev_Time_Absolute = 0;
    CajaDelimitadora SingerBounds;
    /* ********************************************************************************* */
    Group_Singer(GroupSong *MySonglet0){
      this->Create_Me(); this->MySonglet = MySonglet0;
    }
    virtual ~Group_Singer(){ this->Delete_Me(); }
    /* ********************************************************************************* */
    void Start() override {
      IsFinished = false;
      Current_Dex = 0;
      this->Prev_Time_Absolute = 0;
      Delete_Kids();
      if (this->MySonglet->SubSongs.size() <= 0) {
        this->IsFinished = true;
      }
      if (this->InheritedMap.LoudnessFactor == 0.0) { this->IsFinished = true; }// muted, so don't waste time rendering
    }
    /* ********************************************************************************* */
    void Skip_To2(TimeFloat EndTime) {// override {// to do: rewrite this to match bug-fixed render_to
      if (this->IsFinished) { return; }
      TimeFloat Clipped_EndTime = this->MyOffsetBox->UnMap_EndTime();// Clipped_EndTime is now time internal to GroupSong's own coordinate system

      {
        if (EndTime < 0) { EndTime = 0; }// clip time
        int NumSonglets = MySonglet->SubSongs.size();
        int FinalSongletDex = NumSonglets - 1;
        SoundFloat Final_Start = this->MySonglet->SubSongs.at(FinalSongletDex)->TimeX;
        Final_Start = Math::min(Final_Start, EndTime);
        SoundFloat Final_Time = this->MySonglet->Get_Duration();
        if (EndTime > Final_Time) {
          this->IsFinished = true;
          EndTime = Final_Time;// clip time
        }
      }
//      EndTime = this->MyOffsetBox->MapTime(EndTime);// EndTime is now time internal to GroupSong's own coordinate system
//      TimeFloat Clipped_EndTime = this->Tee_Up_Skip(EndTime);

      int NumPlaying = this->NowPlaying.size();
      SingerBase *player = nullptr;
      int cnt = 0;
      int AliveCnt = 0;
      while (cnt < NumPlaying) {// Skip_To for all preexisting kids.
        player = this->NowPlaying.at(cnt);
        player->Skip_To(Clipped_EndTime);
        if (player->IsFinished) {
          delete player;
        } else {
          this->NowPlaying[AliveCnt++] = player;
        }
        cnt++;
      }
      this->NowPlaying.resize(AliveCnt);// now pack down the finished ones

      int NumSonglets = MySonglet->SubSongs.size();
      int FinalSongletDex = NumSonglets - 1;
      SoundFloat Final_Start = this->MySonglet->SubSongs.at(FinalSongletDex)->TimeX;
      Final_Start = Math::min(Final_Start, Clipped_EndTime);

      OffsetBoxBase *obox;
      SoundFloat Child_End_Time, ChildDuration;
      SingerBase *ChildSinger;

      while (this->Current_Dex < NumSonglets) {// first find neuvo songlets in this time range and add them to pool
        obox = MySonglet->SubSongs.at(this->Current_Dex);
        if (Final_Start < obox->TimeX) { break; }// repeat until obox start time overtakes EndTime
        Child_End_Time = obox->UnMap_EndTime();// Duration starts at time==0 so we can treat it as a coordinate.
        if (EndTime < Child_End_Time){// Only use songlets that cross into the next 'play zone'
          ChildSinger = obox->Spawn_Singer();
          ChildSinger->Inherit(*this);
          this->NowPlaying.add(ChildSinger);
          ChildSinger->Start();
          ChildSinger->Skip_To(Child_End_Time);
        }
        this->Current_Dex++;
      }
    }
    /* ********************************************************************************* */
    void Skip_To(TimeFloat EndTime) override {// to do: rewrite this to match bug-fixed render_to
      if (this->IsFinished) { return; }
      EndTime = this->MyOffsetBox->MapTime(EndTime);// EndTime is now time internal to GroupSong's own coordinate system
      TimeFloat Clipped_EndTime = this->Tee_Up_Skip(EndTime);
      int NumPlaying = this->NowPlaying.size();
      SingerBase *player = null;
      int cnt = 0;
      while (cnt < NumPlaying) {// Skip_To for the whole pool
        player = this->NowPlaying.at(cnt);
        player->Skip_To(Clipped_EndTime);
        cnt++;
      }
      cnt = 0;// now pack down the finished ones, probably redundant with Tee_Up_Skip now
      int AliveCnt = 0;
      for (cnt=0; cnt < NumPlaying; cnt++) {
        player = this->NowPlaying.at(cnt);
        if (player->IsFinished) {
          delete player;
        } else {
          this->NowPlaying[AliveCnt++] = player;
        }
      }
      this->NowPlaying.resize(AliveCnt);
      this->Prev_Time_Absolute = this->InheritedMap.UnMapTime(Clipped_EndTime);// get end time in absolute universal coordinates

      // this->UpdateSingerBounds();// DoLivePan
    }
    /* ********************************************************************************* */
    void Render_To(TimeFloat EndTime, Wave& wave) override {
      int SampleRateLocal = wave.SampleRate;// spoin
      this->SampleRate = SampleRateLocal;
      int Sample_Start = this->Prev_Time_Absolute * (TimeFloat)this->SampleRate;

      if (this->IsFinished) {
        wave.Init_Sample(Sample_Start, Sample_Start, this->SampleRate, 0.0);
        return;
      }

      double testtime = EndTime;
      std::cout << testtime << "\n";
      EndTime = this->MyOffsetBox->MapTime(EndTime);// EndTime is now time internal to GroupSong's own coordinate system
      SoundFloat Clipped_EndTime = this->Tee_Up_Render(EndTime);
      SoundFloat EndTime_Absolute = this->InheritedMap.UnMapTime(Clipped_EndTime);// get end time in absolute universal coordinates
      int Sample_End = EndTime_Absolute * (SoundFloat)this->SampleRate;

      wave.Init_Sample(Sample_Start, Sample_End, this->SampleRate, 0.0);// wave times are in parent coordinates because the parent will be reading the wave data.
      //wave.Init_Sample_NoRate(Sample_Start, Sample_End, 0.0);// wave times are in parent coordinates because the parent will be reading the wave data.
      Wave ChildWave;
      ChildWave.Assign_SampleRate(wave.SampleRate);// pass down sample rate
      int NumPlaying = this->NowPlaying.size();
      SingerBase *player = nullptr;
      int cnt = 0;
      while (cnt < NumPlaying) {// then play the whole pool
        player = this->NowPlaying.at(cnt);
        player->Render_To(Clipped_EndTime, ChildWave);
        wave.Overdub(ChildWave);// sum/overdub the waves
        cnt++;
      }
      cnt = 0;// now pack down the finished ones
      int AliveCnt = 0;
      while (cnt < NumPlaying) {
        player = this->NowPlaying[cnt];
        if (player->IsFinished) {
          delete player;
        } else {// still playing, so pack it down
          this->NowPlaying[AliveCnt++] = player;
        }
        cnt++;
      }
      this->NowPlaying.resize(AliveCnt);
      wave.Amplify(this->MyOffsetBox->LoudnessFactor);
      this->Prev_Time_Absolute = EndTime_Absolute;

      // this->UpdateSingerBounds();// DoLivePan
    }
    /* ********************************************************************************* */
    void UpdateSingerBounds() {// DoLivePan
      int NumPlaying = this->NowPlaying.size();
      CajaDelimitadora TempBounds;
      SingerBase *player;
      this->SingerBounds.Reset();
      for (int cnt=0; cnt<NumPlaying; cnt++) {
        player = this->NowPlaying[cnt];
        const CajaDelimitadora *cd = player->GetBoundingBox();
        this->MyOffsetBox->UnMap(*(cd), TempBounds);// project child limits into my parent space (my obox).
        this->SingerBounds.Include(TempBounds);
      }
    }
    /* ********************************************************************************* */
    const bool GetSingerBounds(CajaDelimitadora& Box) override {// DoLivePan
      int NumPlaying = this->NowPlaying.size();
      CajaDelimitadora TempBounds;
      SingerBase *player;
      bool BoxInit = false;
      Box.Reset();
      for (int cnt=0; cnt<NumPlaying; cnt++) {
        player = this->NowPlaying[cnt];
        if (player->GetSingerBounds(TempBounds)) {
          BoxInit = true;
          this->MyOffsetBox->UnMap(TempBounds, TempBounds);// project child limits into my parent space (my obox).
          Box.Include(TempBounds);
          if (!std::isfinite(Box.Min.X)) {
            printf("Group GetSingerBounds\n");
          }
        }
      }
      if (BoxInit && Box.UnInit()) {
        printf("Group GetSingerBounds UnInit\n");
      }
      return BoxInit;
//      return true;
    }
    /* ********************************************************************************* */
    boolean Create_Me() override {// IDeletable
      return SingerBase::Create_Me();
    }
    void Delete_Me() override {// IDeletable
      SingerBase::Delete_Me();
      Delete_Kids();
    }
    void Delete_Kids() {// delete the whole pool of sub-singers
      int len = this->NowPlaying.size();
      SingerBase *singer;
      for (int cnt = 0; cnt < len; cnt++) {
        singer = this->NowPlaying.at(cnt);
        delete singer;
      }
      this->NowPlaying.clear();
    }
    /* ********************************************************************************* */
    SoundFloat Tee_Up_Skip(TimeFloat EndTime) {// this works best for skip_to
      if (EndTime < 0) { EndTime = 0; }// clip time
      int NumSonglets = MySonglet->SubSongs.size();
      int FinalSongletDex = NumSonglets - 1;
      SoundFloat Final_Start = this->MySonglet->SubSongs.at(FinalSongletDex)->TimeX;
      Final_Start = Math::min(Final_Start, EndTime);
      SoundFloat Final_Time = this->MySonglet->Get_Duration();
      if (EndTime > Final_Time) {
        this->IsFinished = true;
        EndTime = Final_Time;// clip time
      }

      OffsetBoxBase *obox;
      SoundFloat Child_End_Time, ChildDuration;
      SingerBase *ChildSinger;
      if (false) {// this code may be useless.
        // Clear out any singers that finished before EndTime.
        int NumPlaying = this->NowPlaying.size();
        int AliveCnt = 0;
        for (int cnt=0;cnt<NumPlaying;cnt++){
          ChildSinger = this->NowPlaying[cnt];
          obox = ChildSinger->MyOffsetBox;
          ChildDuration = obox->GetContent()->Get_Duration();
          Child_End_Time = obox->UnMapTime(ChildDuration);// Duration starts at time==0 so we can treat it as a coordinate.
          if (Child_End_Time < EndTime){// Delete any singers whose songs end before the next 'play zone'.
            delete ChildSinger;
          }else{
            this->NowPlaying[AliveCnt++] = ChildSinger;// pack them down
          }
        }
        this->NowPlaying.resize(AliveCnt);
        //printf("NowPlaying:%i\n", NowPlaying.size());
      }

      while (this->Current_Dex < NumSonglets) {// first find neuvo songlets in this time range and add them to pool
        obox = MySonglet->SubSongs.at(this->Current_Dex);
        if (Final_Start < obox->TimeX) { break; }// repeat until obox start time overtakes EndTime
        ChildDuration = obox->GetContent()->Get_Duration();
        Child_End_Time = obox->UnMapTime(ChildDuration);// Duration starts at time==0 so we can treat it as a coordinate.
        if (EndTime < Child_End_Time){// Only use songlets that cross into the next 'play zone'
          ChildSinger = obox->Spawn_Singer();
          ChildSinger->Inherit(*this);
          this->NowPlaying.add(ChildSinger);
          ChildSinger->Start();
        }
        this->Current_Dex++;
      }
      return EndTime;
    }
    /* ********************************************************************************* */
    SoundFloat Tee_Up_Render(TimeFloat EndTime) {// this works best for render_to
      if (EndTime < 0) { EndTime = 0; }// clip time
      int NumSonglets = MySonglet->SubSongs.size();
      int FinalSongletDex = NumSonglets - 1;
      SoundFloat Final_Start = this->MySonglet->SubSongs.at(FinalSongletDex)->TimeX;
      Final_Start = Math::min(Final_Start, EndTime);
      SoundFloat Final_Time = this->MySonglet->Get_Duration();
      if (EndTime > Final_Time) {
        this->IsFinished = true;
        EndTime = Final_Time;// clip time
      }
      OffsetBoxBase *obox;
      while (this->Current_Dex < NumSonglets) {// first find neuvo songlets in this time range and add them to pool
        obox = MySonglet->SubSongs.at(this->Current_Dex);
        if (Final_Start < obox->TimeX) { break; }// repeat until obox start time overtakes EndTime
        SingerBase *ChildSinger = obox->Spawn_Singer();
        ChildSinger->Inherit(*this);
        this->NowPlaying.add(ChildSinger);
        ChildSinger->Start();
        this->Current_Dex++;
      }
      return EndTime;
    }
    /* ********************************************************************************* */
    void Draw_Me(DrawingContext& ParentDC) override {// This will animate what the singers are singing.
      DrawingContext *ChildDC;// Map through my GraphicBox's handle.
      ChildDC = ParentDC.Compound_Clone(*(this->MyOffsetBox));

      int NumPlaying = this->NowPlaying.size();
      SingerBase *player = nullptr;
      int cnt = 0;
      while (cnt < NumPlaying) {// then play the whole pool
        player = this->NowPlaying.at(cnt);
        player->Draw_Me(*ChildDC);
        cnt++;
      }

      delete ChildDC;
    }
    /* ********************************************************************************* */
    const CajaDelimitadora* GetBoundingBox() override {// DoLivePan
      return &(this->SingerBounds);
    }
  };
  /* ********************************************************************************* */
  class Group_OffsetBox: public OffsetBoxBase {// location box to transpose in pitch, move in time, etc.
  public:
    GroupSong* Content = nullptr;
    SoundFloat GroupScaleX = 1.0;
    StringConst GroupScaleXName = "GroupScaleX";// for serialization
    StringConst ObjectTypeName = "Group_OffsetBox";
    /* ********************************************************************************* */
    Group_OffsetBox(GroupSong *songlet) : OffsetBoxBase() {
      this->Clear();
      this->Create_Me();
      mytype = 7;
      // this->Assign_Content(this->Content = songlet);
      this->Content = songlet;
    }
    /* ********************************************************************************* */
    virtual ~Group_OffsetBox() {
      this->Delete_Me();
    }
    /* ********************************************************************************* */
    GroupSong* GetContent() override {
      return Content;
    }
    /* ********************************************************************************* */
    void Attach_Songlet(GroupSong* songlet) {// for serialization
      this->Assign_Content(this->Content = songlet);
      songlet->Ref_Songlet();
    }
    /* ********************************************************************************* */
    void RescaleGroupTimeX(SoundFloat Factor) {
      this->Content->RescaleGroupTimeX(Factor);
    }
    /* ********************************************************************************* */
    void Draw_Me(DrawingContext& ParentDC) override {// IDrawable
      PROFILE_START;
      OffsetBoxBase::Draw_Me(ParentDC);
      // here draw the Rescale_TimeX handle
      PROFILE_END(Globals::GroupSong_Obx_Hits);
    }
    /* ********************************************************************************* */
    Group_Singer* Spawn_Singer() override {// always always always override this
      Group_Singer *Singer = this->Content->Spawn_Singer();// for render time
      Singer->MyOffsetBox = this;// Transfer all of this box's offsets to singer.
      return Singer;
    }
    /* ********************************************************************************* */
    Group_OffsetBox* Clone_Me() override {// ICloneable, always override this thusly
      Group_OffsetBox *child = this->GetContent()->Spawn_OffsetBox();
      child->Copy_From(*this);
      return child;
    }
    /* ********************************************************************************* */
    Group_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      GroupSong* GroupFork = this->Content->Deep_Clone_Me(HitTable);
      Group_OffsetBox *child = GroupFork->Spawn_OffsetBox();
      child->Copy_From(*this);
      return child;
    }
    /* ********************************************************************************* */
    void BreakFromHerd(CollisionLibrary& HitTable) override {// for compose time. detach from my songlet and attach to an identical but unlinked songlet
      GroupSong *clone = this->Content->Deep_Clone_Me(HitTable);
      if (this->Content->UnRef_Songlet() <= 0) {
        delete this->Content;
        this->Content = nullptr;
      }
      this->Attach_Songlet(clone);
    }
    /* ********************************************************************************* */
    void BreakFromHerd_Shallow() {// for compose time. detach from my songlet and attach to an identical but unlinked songlet
      GroupSong *clone = this->Content->Shallow_Clone_Me();
      if (this->Content->UnRef_Songlet() <= 0) {
        this->Content->Delete_Me();
        delete this->Content;
      }
      this->Content = clone;
      this->Content->Ref_Songlet();
    }
    /* ********************************************************************************* */
    boolean Create_Me() {// IDeletable
      return true;
    }
    void Delete_Me() {// IDeletable
      OffsetBoxBase::Delete_Me();
      this->GroupScaleX = Double::NEGATIVE_INFINITY;
      //ISonglet::Unref(&(this->Content));
      if (this->Content != nullptr) {
        if (this->Content->UnRef_Songlet() <= 0) {
          delete this->Content;
          this->Content = nullptr;
        }
      }
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
      JsonParse::HashNode *SelfPackage = OffsetBoxBase::Export(HitTable);// ready for test?
      SelfPackage->AddSubPhrase(Globals::ObjectTypeName, ITextable::PackStringField(ObjectTypeName));
      SelfPackage->AddSubPhrase(GroupScaleXName, ITextable::PackNumberField(this->GroupScaleX));// GroupScaleX is probably deprecated field
      if (false) {
//        JsonParse.Node ChildPackage;
//        if (this.Content.GetRefCount() != 1) {// songlet exists in more than one place, use a pointer to library
//          ChildPackage = new JsonParse.Node();// multiple references, use a pointer to library instead
//          CollisionItem ci;// songlet is already in library, just create a child phrase and assign its textptr to that entry key
//          if ((ci = HitTable.GetItem(this.Content)) == null) {
//            ci = HitTable.InsertUniqueInstance(this.Content);// songlet is NOT in library, serialize it and add to library
//            ci.JsonPhrase = this.Content.Export(HitTable);
//          }
//          ChildPackage.Literal = ci.ItemTxtPtr;
//        } else {// songlet only exists in one place, make it inline.
//          ChildPackage = this.Content.Export(HitTable);
//        }
//        SelfPackage->AddSubPhrase(OffsetBoxBase::ContentName, ChildPackage);
      }
      return SelfPackage;
    }
    void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
      OffsetBoxBase::ShallowLoad(phrase);
      this->GroupScaleX = ITextable::GetNumberField(phrase, "GroupScaleX", 1.0);
    }
  };
  /* ********************************************************************************* */
  Group_OffsetBox* Spawn_OffsetBox() override {
    Group_OffsetBox *lbox = new Group_OffsetBox(this);// Spawn an OffsetBox specific to this type of songlet.
    // lbox->Attach_Songlet(this);
    this->Ref_Songlet();
    return lbox;
  }
  /* ********************************************************************************* */
  Group_Singer* Spawn_Singer() override {
    Group_Singer *GroupPlayer = new Group_Singer(this);// Spawn a singer specific to this type of songlet.
    if (this->MyProject!=nullptr){//to_do();// fix this bs.  we need to load sample rate at render time.
      GroupPlayer->Set_Project(this->MyProject);// inherit project
    }
    return GroupPlayer;
  }
};

#endif
