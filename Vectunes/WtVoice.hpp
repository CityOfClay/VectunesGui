#ifndef WtVoice_hpp
#define WtVoice_hpp

#include "PluckVoice.hpp"

// WORK IN PROGRESS - NOT FUNCTIONAL YET
/* ********************************************************************************* */
class WtVoicePoint: public VoicePoint {
public:
  TimeFloat Sweep;
};

/* ********************************************************************************* */
class WaveTable : public ArrayList<Wave*> {
public:
  void ResampleLooped() {// Just an abstract algorithm. Not used yet.
    int D0 = 13; //  72 //  6
    int D1 = 21; //  96 //  10
    int samplen = 5;
    int Sample[] = {0, 1, 2, 3, 4};
    int val;

    int big0 = Math::floor(D0 / samplen) * samplen; //  output chunks
    int big1 = Math::ceil(D1 / samplen) * samplen; //  inclusive

    int bigdex = D0;
    int lcnt = D0 % samplen;
    int topdex;
    for (int bigcnt=big0; bigcnt < big1; bigcnt+=samplen) { // Covers whole size, but too much.
      topdex = Math::min(D1 - bigcnt, samplen);
      while (lcnt < topdex) {
        val = Sample[lcnt];
        cout << " Loop0 " << val << ", bigdex " << bigdex;
        lcnt++;
        bigdex++;
      }
      lcnt = 0;
      //  cout << " topdex " << topdex;
    }
  }
};

// This will be a wave table pluck effect, precursor to fancier wave tables.
/* ********************************************************************************* */
class WtVoice: public PluckVoice {
  ArrayList<WtVoicePoint*> WtCPoints;
  WaveTable *MyWaveTable;// = new ArrayList<*Wave>();
  /* ********************************************************************************* */
  class WtPluckVoice_OffsetBox:public PluckVoice_OffsetBox {
  };
  /* ********************************************************************************* */
  class WtPluckVoice_Singer:public PluckVoice_Singer {
    public:
    WtVoice *MyWTVoice = nullptr;
    /* ********************************************************************************* */
//    double GetWaveForm(TimeFloat SubTimeAbsolute) override {
//      return this->MySample->GetResample(SubTimeAbsolute * BaseFreq);
//    }
    /* ********************************************************************************* */
    void BreakSweepFraction(int NumRasters, double SweepFraction, int& RasterIndex, double& MorphFraction) {
      double Temp = SweepFraction * NumRasters;
      //(*RasterIndex) = Math::floor(Temp); (*MorphFraction) = Temp - RasterIndex;
      RasterIndex = Math::floor(Temp); MorphFraction = Temp - RasterIndex;
    }
    void Render_Segment_Integral(VoicePoint& pnt0, VoicePoint& pnt1, Wave& wave) override {// stateless calculus integral approach
      WaveTable *WT = MyWTVoice->MyWaveTable;
      double SRate = this->SampleRate;
      double Time0 = this->InheritedMap.UnMapTime(pnt0.TimeX);
      double Time1 = this->InheritedMap.UnMapTime(pnt1.TimeX);
      double FrequencyFactorInherited = this->InheritedMap.GetFrequencyFactor();// inherit transposition
      int EndSample = (int) (Time1 * (double) SRate);// absolute
      double SampleRange = EndSample - this->Sample_Start;
      wave.Init_Sample(this->Sample_Start, EndSample, this->SampleRate, 0.9);

      double SubTime0 = pnt0.SubTime * this->InheritedMap.ScaleX;// tempo rescale
      double TimeRange = Time1 - Time0;
      double FrequencyFactorStart = pnt0.GetFrequencyFactor();
      double Octave0 = this->InheritedMap.OctaveY + pnt0.OctaveY, Octave1 = this->InheritedMap.OctaveY + pnt1.OctaveY;

      double OctaveRange = Octave1 - Octave0;
      double OctaveRate = OctaveRange / TimeRange;// octaves per second bend
      OctaveRate += this->Inherited_OctaveRate;// inherit note bend
      double LoudnessRange = pnt1.LoudnessFactor - pnt0.LoudnessFactor;
      double LoudnessRate = LoudnessRange / TimeRange;
      double SubTimeLocal, SubTimeAbsolute;

      double TimeAlong;
      double CurrentLoudness;
      double Amplitude, amp0, amp1;
      int PatternDex0 = 0, PatternDex1 = WT->size() - 1;// inclusive on both ends for now - both indexes will be hit.
      double PatternRange = PatternDex1 - PatternDex0;
      double FadeSampleRange = SampleRange / PatternRange;

      int PatDex = PatternDex0;
      double PatFractAlong = 0;
      double FadeFractAlong = 0, FadeFractAlongComp = 0;
      int PatSampleStart, PatEndSample;

      Wave *PrevPat, *NextPat;
      if (true) {
        int SampleDex = this->Sample_Start;
        int FadeCnt;
        PatSampleStart = this->Sample_Start;
        PrevPat = WT->get(PatDex++);
        while (PatDex <= PatternDex1) {// linear sweep across patterns of wave table
          PatFractAlong = (((double) PatDex) - (double) PatternDex0) / (double) PatternRange;
          PatEndSample = this->Sample_Start + (int) (PatFractAlong * (double) SampleRange);
          NextPat = WT->get(PatDex);
          FadeSampleRange = PatEndSample - SampleDex;
          while (SampleDex < PatEndSample) {// to do: clean up this mess. reduce to 1 counter.
            FadeCnt = SampleDex - PatSampleStart;
            FadeFractAlong = ((double) FadeCnt) / (double) FadeSampleRange;
            FadeFractAlongComp = 1.0 - FadeFractAlong;
            TimeAlong = (SampleDex / SRate) - Time0;
            CurrentLoudness = pnt0.LoudnessFactor + (TimeAlong * LoudnessRate);
            SubTimeLocal = Voice::Integral(OctaveRate, TimeAlong);
            SubTimeAbsolute = (SubTime0 + (FrequencyFactorStart * SubTimeLocal)) * FrequencyFactorInherited;
            amp0 = PrevPat->GetResampleLooped(SubTimeAbsolute);// to do: merge these, redundant calc
            amp1 = NextPat->GetResampleLooped(SubTimeAbsolute);
            Amplitude = (amp0 * FadeFractAlongComp) + (amp1 * FadeFractAlong);// crossfade
            wave.Set_Abs(SampleDex, Amplitude * CurrentLoudness);
            SampleDex++;
          }
          PatSampleStart = PatEndSample;
          PrevPat = NextPat;// drag
          PatDex++;
        }
      } else {// reduced to 1 counter
        int PatCnt = 0;
        int SampleCnt = 0;
        PatFractAlong = 0.0;
        PatSampleStart = this->Sample_Start;
        PrevPat = WT->get(PatternDex0 + PatCnt);
        PatCnt++;
        while (PatCnt <= PatternRange) {// linear sweep across patterns of wave table
          NextPat = WT->get(PatternDex0 + PatCnt);
          SampleCnt = 0;
          while (SampleCnt < FadeSampleRange) {
            FadeFractAlong = ((double) SampleCnt) / (double) FadeSampleRange;
            FadeFractAlongComp = 1.0 - FadeFractAlong;
            TimeAlong = (SampleCnt / SRate);
            CurrentLoudness = pnt0.LoudnessFactor + (TimeAlong * LoudnessRate);
            SubTimeLocal = Voice::Integral(OctaveRate, TimeAlong);
            SubTimeAbsolute = (SubTime0 + (FrequencyFactorStart * SubTimeLocal)) * FrequencyFactorInherited;
            amp0 = PrevPat->GetResampleLooped(SubTimeAbsolute);
            amp1 = NextPat->GetResampleLooped(SubTimeAbsolute);
            Amplitude = (amp0 * FadeFractAlongComp) + (amp1 * FadeFractAlong);// crossfade
            wave.Set_Abs(PatSampleStart + SampleCnt, Amplitude * CurrentLoudness);
            SampleCnt++;
          }
          PatFractAlong = ((double) PatCnt) / (double) PatternRange;
          PatSampleStart = this->Sample_Start + (int) (PatFractAlong * (double) SampleRange);
          PrevPat = NextPat;// drag
          PatCnt++;
        }
      }
      this->Sample_Start = EndSample;
    }
  };
};

/*
int NumRasters = (number of rows of whole table)
int MorphSamples = (number of samples between morph endpoints)
double SweepFraction0, SweepFraction1;
double RasterIndex, MorphFraction.
{
  int RasterIndex0;
  double MorphFraction0;
  BreakSweepFraction(NumRasters, SweepFraction0, RasterIndex0, MorphFraction0);
  BreakSweepFraction(NumRasters, SweepFraction1, RasterIndex1, MorphFraction1);
  Wave wav;
  GetMorph(RasterIndex0, MorphFraction0, 1.0, wav);
  // append each wav to larger wave.
}
void GetMorph(int RasterNum, double Morph0, double Morph1, Wave wav) {
  Form0 = RasterLine[RasterNum];
  Form1 = RasterLine[RasterNum+1];
  int SampleIndex0 = Morph0 * MorphSamples;
  int SampleIndex1 = Morph1 * MorphSamples;
  for (int scnt=SampleIndex0; scnt<SampleIndex1; scnt++) {
  }
}

void GetMorph(Line& Form0, Line& Form1, double Morph0, double Morph1, Wave wav) {
  int SampleIndex0 = Morph0 * MorphSamples;
  int SampleIndex1 = Morph1 * MorphSamples;
  for (int scnt=SampleIndex0; scnt<SampleIndex1; scnt++) {
  }
}

sampleA, sampleB
int start = 0, finish = sampA.length;
for (int scnt=start; scnt<finish; scnt++) {
  retval[cnt] = sampA[scnt];
}
cnt++;

*/

#endif
