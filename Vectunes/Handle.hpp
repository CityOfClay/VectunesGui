#ifndef Handle_hpp
#define Handle_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "IDrawable.hpp"
#include "CajaDelimitadora.hpp"
#include "IDeletable.hpp"
#include "ITextable.hpp"
#include "JsonParse.hpp"
#include "Transformer.hpp"
#include "IGrabber.hpp"

class Handle: public IMoveable, public IDeletable, public ITextable {
protected:
  // graphics support, may move to separate object
  SoundFloat OctavesPerRadius = 0.01;
public:
/*
Handle is any of the circles onscreen that can be drawn or clicked.
Handle keeps drawing size and click-detect size in sync.

By default, drawn as circle/ellipse.
Handle defines the size and shape of the circle.

Part of ITree. ITree is for update_guts, part of visual hierarchy.

Ancestor to all obox, voicepoint and loudnesshandle.
Unlike obox and voicepoint, does NOT have child songlets.

*/
  /* ********************************************************************************* */
  Handle() { }
  virtual ~Handle() { }
  /* ********************************************************************************* */
  // virtual Handle* GetParent() { } // maybe not useful here.
  /* ********************************************************************************* */
  virtual void AssignOctavesPerRadius(SoundFloat OPR) {
    this->OctavesPerRadius = OPR;
  }
  void MoveBy(TimeFloat XMove, SoundFloat YMove) override {
    TimeFloat XLoc = this->GetX() + XMove;
    SoundFloat YLoc = this->GetY() + YMove;
    this->MoveTo(XLoc, YLoc);
  }
  // IDrawable
//  void Draw_Me(DrawingContext& ParentDC) override {}
//  void UpdateBoundingBoxLocal() override {}
//  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {}// Look for mouse clicks on me or my children. to do: replace previous GoFishing

  // IMoveable is for things that can be selected, dragged, copied, pasted, deleted etc. through the UI.
//  void MoveTo(SoundFloat XLoc, SoundFloat YLoc) override {};
  /* ********************************************************************************* */
  bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override {// IDrawable.IMoveable
    printf("Handle HitsMe:");// click detection
    CajaDelimitadora SearchBoxLocal;// Do this all in local space, not pixel space.
    StackContext.MapTo(*Scoop.GetSearchBox(), SearchBoxLocal);// Map SearchBox from pixel space to obx local space.
    Point2D EllipseSize = GetEllipseSizeLocal(StackContext.RecurseDepth);// Get ellipse size in local space.
    return BoxHitsEllipse(SearchBoxLocal, this->GetX(), this->GetY(), EllipseSize.X, EllipseSize.Y);
  }
//  SoundFloat GetX() override { return Double::NEGATIVE_INFINITY; };
//  SoundFloat GetY() override { return Double::NEGATIVE_INFINITY; };
//  void SetSelected(bool Selected) override { this->IsSelected = Selected; }
//  IMoveable* Clone_Me() override { return nullptr; };
//  IMoveable* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; };

  /* ********************************************************************************* */
  void Copy_From(const Handle& donor) {
    IDrawable::Copy_From(donor);
    this->AssignOctavesPerRadius(donor.OctavesPerRadius);
  }
  /* ********************************************************************************* */
  double GetRecursedRadius(double RecurseDepth) {// Get outer bounds of handle at local scale. snox, Sloppy.
    double InvDepth = (1.0 / RecurseDepth);// Circles more distal on the tree are smaller.
    return (this->OctavesPerRadius + InvDepth * 0.02);// magic number 0.02!
  }
  /* ********************************************************************************* */
  virtual Point2D GetEllipseSizeLocal(double RecurseDepth) {
    double RadiusAbstract = this->GetRecursedRadius(RecurseDepth);
    Point2D EllipseSize(Math::abs(RadiusAbstract), Math::abs(RadiusAbstract));// Default ellipse size is not scaled, assumes my scale is same as for parent space.
    return EllipseSize;// Now EllipseSize represents the full distortions I make to my children.
  }
  /* ********************************************************************************* */
  Point2D GetEllipseSizePixels(Transformer& MapToPixels, double RecurseDepth) {// Get radius of handle ellipse, in pixels, at this zoom level.
    Point2D EllipsSizeLocal = GetEllipseSizeLocal(RecurseDepth);
    Point2D EllipseSize(Math::abs(MapToPixels.UnScaleTime(EllipsSizeLocal.X)), Math::abs(MapToPixels.UnScalePitch(EllipsSizeLocal.Y)));
    // Point2D EllipseSize(MapToPixels.UnScale(EllipsSizeLocal));
    return EllipseSize;
  }
  /* ********************************************************************************* */
  static bool BoxHitsEllipse(const CajaDelimitadora& SearchBoxLocal, double CtrX, double CtrY, double RadiusX, double RadiusY) {
    // Check if SearchBox overlaps my circle. https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
    Point2D BoxCtr = SearchBoxLocal.GetCenter();

    TimeFloat XDist = Math::abs(BoxCtr.X - CtrX);// dist between center of circle and center of box.
    SoundFloat YDist = Math::abs(BoxCtr.Y - CtrY);

    TimeFloat BoxRadX = SearchBoxLocal.GetWidth()/2.0;// box 'radius', ie center to edge.
    SoundFloat BoxRadY = SearchBoxLocal.GetHeight()/2.0;

    if (XDist > (BoxRadX + RadiusX)) { return false; }
    if (YDist > (BoxRadY + RadiusY)) { return false; }

    if (XDist <= (BoxRadX)) { return true; }
    if (YDist <= (BoxRadY)) { return true; }

    SoundFloat CornerXDist = XDist - BoxRadX;// StackContext.ScaleX
    SoundFloat CornerYDist = YDist - BoxRadY;

    double AspRatio = RadiusY/RadiusX;
    SoundFloat CircleCornerDist = Math::hypot(CornerXDist*AspRatio, CornerYDist);
    if (CircleCornerDist <= RadiusY) {
      printf("true");
      return true;
    }
    return false;
  }
  /* ********************************************************************************* */
  static bool BoxHitsCircle(const CajaDelimitadora& SearchBoxLocal, double CircleCtrX, double CircleCtrY, double CircleRadius) {
    // Check if SearchBox overlaps my circle. https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection
    Point2D BoxCtr = SearchBoxLocal.GetCenter();

    TimeFloat XDist = Math::abs(BoxCtr.X - CircleCtrX);// dist between center of circle and center of box.
    SoundFloat YDist = Math::abs(BoxCtr.Y - CircleCtrY);

    TimeFloat BoxRadX = SearchBoxLocal.GetWidth()/2.0;// box 'radius', ie center to edge.
    SoundFloat BoxRadY = SearchBoxLocal.GetHeight()/2.0;

    if (XDist > (BoxRadX + CircleRadius)) { return false; }
    if (YDist > (BoxRadY + CircleRadius)) { return false; }

    if (XDist <= (BoxRadX)) { return true; }
    if (YDist <= (BoxRadY)) { return true; }

    SoundFloat CornerDist = Math::hypot(XDist - BoxRadX, YDist - BoxRadY);
    if (CornerDist <= CircleRadius) {
      printf("true");
      return true;
    }
    return false;
  }
};


#endif // Handle_hpp
