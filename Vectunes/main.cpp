#include <iostream>
#include <time.h>
#include <chrono>
#include <ctime>
#include <fstream>

//#include <sys/types.h>
//#include <sys/stat.h>
//#include <unistd.h>
//stat buf; int what = stat("", buf);

//#include <experimental/filesystem> // C++14
//namespace fs = std::experimental::filesystem;
//#include <filesystem>

#include <sys/types.h>
#include <dirent.h>

#include "Vectunes.hpp"
//#include "DrawingContext2.hpp"
//#include "Transformer2.hpp"

using namespace std;

/*
quehacers:
get rid of Project/Config/SampleRate reference in all singers. pass SampleRate as a render-time parameter in wave or Start().
refine SampleVoice
import PluckVoice
import GraphicBox?
create LoopSong factory and exporter
finish text/untext tree/graphicbox of audproject

*/
/* ********************************************************************************* */
void Scan(const std::string &path) {// https://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
//  namespace fs = std::experimental::filesystem;
//  for (const auto &entry : fs::directory_iterator(path)){
//    std::cout << entry.path() << std::endl;
//  }
}
/* ********************************************************************************* */
void Convert(const String &FName) {
  AudProject aup;
  if (aup.ReadFile(FName)) {
    aup.Render_Test(FName + ".wav");
  }
}
/* ********************************************************************************* */
void Find_Jsongs(const std::string &DirPath, std::vector<String> &results) {
  String updir("../");// climb up directories until we find .jsong files, and render them to .wav
  std::vector<String> FoundList;
  String DirPathTemp = AudProject::Standardize_Path(DirPath);
  String FilePath;

  AudProject::Get_Directory("", FoundList);
  cout << "Empty:" << "\n";
  AudProject::Print_List(FoundList);
  AudProject::Get_Directory("./", FoundList);
  cout << "Ends with slash:" << "\n";
  AudProject::Print_List(FoundList);
  AudProject::Get_Directory("./", FoundList);
  cout << "Ends without slash:" << "\n";
  AudProject::Print_List(FoundList);

  int Limit = 4;// do not go up more than Limit directories
  int cnt = 0;
  for (cnt=0;cnt<Limit;cnt++){
    if (!AudProject::RealPath(DirPathTemp, DirPathTemp)){
      break;
    }
    AudProject::Get_Directory(DirPathTemp, FoundList);
    //Filter_List(FoundList, ".jsong");
    AudProject::Filter_List(FoundList, "/jsong");
    if (FoundList.size()>0){
      cout << "DirPathTemp: "<< DirPathTemp << "\n";
      cout << "size: "<< FoundList.size() << ", at: " << FoundList.at(0) << "\n";
      break;
    }
    //DirPathTemp += "/" + updir;
    DirPathTemp += updir;
    cout << "DirPathTemp: " << DirPathTemp << "\n";
  }

  results.clear();
  for (cnt=0;cnt<FoundList.size();cnt++){
    //FilePath = DirPathTemp + "/" + FoundList.at(cnt);
    FilePath = FoundList.at(cnt);
    cout << "FilePath: " << FilePath << "\n";
    Convert(FilePath);
    results.push_back(FilePath);
  }
}
/* ********************************************************************************* */
void Convert_Directory(const std::string &FPath) {
  std::vector<String> FileList;
  AudProject::Get_Directory(FPath, FileList);
  AudProject::Filter_List(FileList, ".jsong");
  for (int cnt=0;cnt<FileList.size();cnt++){
    Convert(FileList.at(cnt));
  }
}
/* ********************************************************************************* */
void Convert_Files(std::vector<String> &FileList) {
  for (int cnt=0;cnt<FileList.size();cnt++){
    if  (Ops::EndsWith(FileList.at(cnt), ".jsong")){
      Convert(FileList.at(cnt));
    }
  }
}
/* ********************************************************************************* */
void Render_To_End(OffsetBoxBase &RootHandle, Wave &wav) {// snox unfinished
  wav.Clear();
  wav.Assign_SampleRate(Globals::SampleRate);
  MetricsPacket metrics;
  RootHandle.Update_Guts(metrics);
  SingerBase *rsinger = RootHandle.Spawn_Singer();
  rsinger->SampleRate = Globals::SampleRate;
  rsinger->Render_To(RootHandle.Get_Duration(), wav);
  delete rsinger;
}
//#if true
  /* ********************************************************************************* */
  void Render_Test(AudProject &aup, const String &FileName) {// snox from AudProject.cpp
    ISonglet *songlet = aup.AudioRoot->GetContent();
    double Duration = songlet->Get_Duration();
    SingerBase *singer = aup.AudioRoot->Spawn_Singer();
    singer->SampleRate = Globals::SampleRate;
    Wave wav(Globals::SampleRate);
    singer->Render_To(Duration, wav);
    wav.SaveToWav(FileName);
  }
//#endif // false
/* ********************************************************************************* */
void Serial_Deserial_Test(AudProject &RootHandle0, String FileName) {// snox unfinished
  AudProject RootHandle1, RootHandle2;
  JsonParse::HashNode *RootJNode;
  String JsonFileName = FileName + ".test.json";
  String JsonTxt;
  Wave wav0, wav1;
  FileMapper fmap;
  {
    Render_To_End(*(RootHandle0.AudioRoot), wav0); // to do: save wave to file
    RootJNode = RootHandle0.Jsonify();

    JsonTxt = RootJNode->ToJson();
    cout << JsonTxt << "\n";
    // to do: save json string here
    Globals::SaveTextFile(JsonFileName, JsonTxt);

    RootHandle1.UnJsonify(RootJNode);
    //RootHandle1 = fmap.Create(RootJNode);
    delete RootJNode;
  }
  {
    Render_To_End(*(RootHandle1.AudioRoot), wav1); // to do: save wave to file
    RootJNode = RootHandle0.Jsonify();
    // to do: save json string here
    RootHandle2.UnJsonify(RootJNode);
    //RootHandle2 = fmap.Create(RootJNode);
    delete RootJNode;
  }

  wav0.SaveToWav(FileName + ".0.wav");
  wav1.SaveToWav(FileName + ".1.wav");

  // If everything works, the Diff waveform file should be a flat line - no difference between versions 0 and 1.
  wav0.Amplify(-1.0);
  wav1.Overdub(wav0);
  wav1.SaveToWav(FileName + ".Diff.wav");
}
/* ********************************************************************************* */
void Test_Files(){
  cout << "Test_Files()" << "\n";
  AudProject aup;
  std::vector<String> FileList;
  String FilePath;
  AudProject::Find_Jsong_Dir("./", FileList);
  OffsetBoxBase *RootHandle;
  for (int cnt=0;cnt<FileList.size();cnt++){
    FilePath = FileList.at(cnt);
    if (aup.ReadFile(FilePath)) {
      Serial_Deserial_Test(aup, FilePath + ".test");
    }
  }
}

class Tester{
public:
  int Luggage = 12345;
};

class Tester1{
public:
  Tester *Luggage = new Tester();
};

void GetTester(Tester &mytest){
   cout << mytest.Luggage << "\n";
}

void TestHash(){
  std::map<String, int> mymap0;
  mymap0["one"] = 1;
  mymap0["two"] = 2;
  mymap0["three"] = 3;
  mymap0["ten"] = 10;

  std::cout << "mymap0[\"three\"] is " << mymap0["three"] << '\n';
  mymap0["three"] = 4;
  std::cout << "mymap0[\"three\"] is " << mymap0["three"] << '\n';

  std::cout << "mymap0[\"two\"] is " << mymap0["two"] << '\n';
  mymap0.insert(std::make_pair("two", 6));
  std::cout << "mymap0[\"two\"] is " << mymap0["two"] << '\n';

  std::cout << "mymap0[\"two\"] is " << mymap0["two"] << '\n';
  (*((mymap0.insert(make_pair("two", 7))).first)).second = 7;
  std::cout << "mymap0[\"two\"] is " << mymap0["two"] << '\n';

  std::cout << "mymap0[\"ten\"] is " << mymap0["ten"] << '\n';
  typename std::map<String, int>::iterator it; // http://www.cplusplus.com/reference/map/map/find/
  it = mymap0.find("ten");
  if (it == mymap0.end()){
    mymap0.insert(std::make_pair("ten", 11));
  }else{
    it->second = 11;
  }
  std::cout << "mymap0[\"ten\"] is " << mymap0["ten"] << '\n';

  std::map<char,std::string> mymap;

  mymap['a']="an element";
  mymap['b']="another element";
  mymap['c']=mymap['b'];

  std::cout << "mymap['a'] is " << mymap['a'] << '\n';
  std::cout << "mymap['b'] is " << mymap['b'] << '\n';
  std::cout << "mymap['c'] is " << mymap['c'] << '\n';
  std::cout << "mymap['d'] is " << mymap['d'] << '\n';

  std::cout << "mymap now contains " << mymap.size() << " elements.\n";

//    typename std::map<KeyT, ValueT>::iterator it; // http://www.cplusplus.com/reference/map/map/find/
//    it = this->find(Key);
//    if (it == this->end()){
//      this->insert(std::make_pair(Key, Value));
//    }else{
//      it->second = Value;
//    }
}
/* ********************************************************************************* */
void Profile_Test(){
  cout << "Profile_Test()" << "\n";
  AudProject aup;
  String FilePath = "./jsong/horn_chords_long01.jsong";
  DrawingContext ParentDC;
  if (aup.ReadFile(FilePath)) {
    Init_Profilers();
    {
      ParentDC.RecurseDepth = 0;
      ParentDC.ClipBounds.Make_Unlimited();//.Assign(OglLeft, OglBottom, OglRight, OglTop);
      aup.GrOBox->MyBounds.Make_Unlimited();
      aup.GrOBox->Print_Me("Render GraphicRoot:");
      aup.GrOBox->Draw_Me(ParentDC);
    }
    Print_Profilers();
  }
}
/* ********************************************************************************* */
int main() {
  std::srand(std::time(nullptr)); // use current time as seed for random generator
  Voice_Iterative = false;// temporary for testing

  Config conf;
  MetricsPacket metrics;
  metrics.MaxDuration = 0.0;
  metrics.MyProject = &conf;
  metrics.FreshnessTimeStamp = 2;

  if (false) {
    AudProject aup;
    aup.ReadFile("RandomBloops.jsong");
    aup.Render_Test("RandomBloops.wav");
    return 0;
  }
  if (true) {
    LoopSong* tree;
    GroupSong* gsong;
    if (true) {
      //gsong = tree = PatternMaker::MakeRandom();
      gsong = tree = PatternMaker::Simple_Loop();
    } else if (true) {
      Voice* voz = PatternMaker::Create_Bent_Note(0.0, 0.1, 0.0, 1.0);
//      tree = PatternMaker::Create_Unbound_Triad_Rhythm(*voz);
      gsong = tree = PatternMaker::Create_LoopSong(voz, 2);
      tree->Set_Beats(2);
//    } else if (true) {
//      gsong = new GroupSong();
//      Voice* voz = PatternMaker::Create_Bent_Note(0.0, 0.1, 0.0, 1.0);
//      Voice::Voice_OffsetBox *vbox0 = voz->Spawn_OffsetBox(); gsong->Add_SubSong(vbox0);
//      Voice::Voice_OffsetBox *vbox1 = voz->Spawn_OffsetBox(); gsong->Add_SubSong(vbox1);
//      vbox1->TimeX = 0.1;
      GroupSong::Group_OffsetBox *root_gbox = gsong->Spawn_OffsetBox();
      root_gbox->OctaveY = 3.0;
      AudProject aup;
      aup.Wrap_For_Graphics(*root_gbox);
      aup.GrOBox->OctaveY = 1118.0;
      aup.GrOBox->TimeX = 392.0;
      aup.GrOBox->ScaleY = -211.0;
      aup.GrOBox->ScaleX = 211.0;
      aup.Update_Guts();
      aup.WriteFile("LoopRaw.jsong");

      AudProject aup2;
      aup2.ReadFile("LoopRaw.jsong");
      aup2.WriteFile("LoopRaw2.jsong");
      return 0;
    }
    GroupSong *grsong = new GroupSong();
    GroupSong::Group_OffsetBox *treebox = tree->Spawn_OffsetBox();
    treebox->OctaveY = 3.0;
    grsong->Add_SubSong(treebox);
    GroupSong::Group_OffsetBox *grobox = grsong->Spawn_OffsetBox();

    AudProject aup;
    aup.Wrap_For_Graphics(*grobox);
    aup.GrOBox->OctaveY = 1118.0;
    aup.GrOBox->TimeX = 392.0;
    aup.GrOBox->ScaleY = -211.0;
    aup.GrOBox->ScaleX = 211.0;
    aup.Update_Guts();

    GroupSong::Group_Singer *gsing = grobox->Spawn_Singer();
    gsing->SampleRate = Globals::SampleRate;

    Wave wave;
    wave.Assign_SampleRate(Globals::SampleRate);

    std::time_t time0 = std::time(nullptr);
    gsing->Start();
    printf("Render_To:\n");
    gsing->Render_To(tree->Duration, wave);
    std::time_t time1 = std::time(nullptr);
    printf("Render_To done.\n");
    delete gsing;
    printf("SaveToWav:\n");
    wave.SaveToWav("RandomTree.wav");
    printf("SaveToWav done.\n");

    if (true) {
      printf("Save Jsong:\n");
      String JsonTxt = aup.Textify();
      aup.WriteFile("RandomTreeRaw.jsong");
      printf("Save Jsong done.\n");
      AudProject aup1;
      aup1.UnTextify(JsonTxt);
      aup1.Render_Test("RandomTreeRedo.wav");
      printf("Second render done.\n");
    }
    /*
    next steps:
    make loop song draw_me work.
    add other effects to MakeRandom, horn, pluck, etc.
    slice chunk test for direct audio.
    rig up audproject so we can save jsong for every random song. - done
    load real songs from jsong dir and play live.
    add live play to gui, trigger it with space bar?
    gui still needs editing abilities.
    work on wave table png editor.
    clean up deserialization junk that is spread everywhere. (Consume)
    fix wave type to inherit directly from vector.
    find and fix all memory leaks with profiling! first find all classes, then all constructors and destructors.
    */

    //delete grobox;
    if (true) {
      wave.Normalize();
      DefaultAudioFeed audio; // AudioFeed
      audio.Start();
      audio.Feed(wave);
      audio.Stop();
    }
    std::stringstream Bruce;
    Bruce << "Length of audio:" << (aup.Get_Duration()) << " seconds.\n";
    Bruce << "Time to render:" << (time1-time0) << " seconds.\n";
    std::cout << Bruce.str();

    return 0;
  }

  if (true){
    Profile_Test();
    return 0;
  }

  if (false){
    TestHash();
    return 0;
  }

  if (false){
    Tester1 *test = new Tester1();
    GetTester(*(test->Luggage));
    return 0;
  }

  if (true){
    Test_Files();
    return 0;
  }
  if (false){
    std::vector<String> txtvec;
    AudProject::Find_Jsong_Dir("./", txtvec);
    return 0;
  }
  if (true) {
    String fname = "./jsong/";
    // Scan("./");
    Convert_Directory(fname);
    //return 0;
//    Convert("./AnyTest.jsong");
//    Convert("./simple3.jsong");
//    Convert("./slormbeat03.jsong");
//    Convert("./Depeche03.jsong");
//    Convert("./groans04.jsong");
//    Convert("./horn_chords_long01.jsong");
    return 0;
  }

  if (false) {
    cout << MonkeyBox::TimeXName << "\n";
    cout << MonkeyBox::OctaveYName << "\n";
    cout << MonkeyBox::ScaleXName << "\n";
    double test = 10;
    test = Double::ParseDouble("0.1", 11);
    printf("test:%f\n", test);
    test = Double::ParseDouble("abc", 12);
    printf("test:%f\n", test);
    // return 0;
  }

  if (false) {
    Tokenizer tkzer;
    String JsonTxt = "{ \"Hello\" : \"World\",  \"AlohaMars\" : { \"Bienvenidos\" : \"Phobos\" },  \"Howdy\" : \"Pluto\" }";
    ArrayList<Token*>* Tree = nullptr;
    printf("Tokenizer.Tokenize\n");
    Tree = tkzer.Tokenize(0, JsonTxt);
    printf("Tokenizer.Tokenize done\n");
    tkzer.Print_Tokens();

    JsonParse jp;
    jp.Parse(JsonTxt);
    //jp.Fold(*Tree);
    //return 0;
  }

  if (false) {
    Functor f;
    int i = f(3.14);
    return 0;
  }
  if (false) {
    ComparePowers();
    return 0;
  }

  if (false) {
    Wave wav;
    wav.Assign_SampleRate(Globals::SampleRate);
    SampleVoice *svoice = new SampleVoice();
    printf("SampleVoice.Preset_Horn();\n");
    svoice->Preset_Horn();
    FillVoice(svoice);
    svoice->Update_Guts(metrics);
    printf("SampleVoice.Spawn_OffsetBox();\n");
    SampleVoice::SampleVoice_OffsetBox *svobox = svoice->Spawn_OffsetBox();
    SampleVoice::SampleVoice_Singer *svsinger;
    printf("SampleVoice_Singer.Spawn_Singer();\n");
    svsinger = svobox->Spawn_Singer();
    svsinger->SampleRate = Globals::SampleRate;
    svsinger->Start();
    printf("SampleVoice_Singer.Render_To();\n");
    svsinger->Render_To(1.0, wav);
    delete svsinger;
    printf("wav.SaveToWav\n");
    wav.SaveToWav("Horn.wav");
    delete svobox;// also deletes svoice
    //return 0;
  }

  if (false) {
    LoopSong* tree = PatternMaker::MakeRandom();
    GroupSong *grsong = new GroupSong();
    GroupSong::Group_OffsetBox *treebox = tree->Spawn_OffsetBox();
    treebox->OctaveY = 3.0;
    grsong->Add_SubSong(treebox);

    grsong->Update_Guts(metrics);

    GroupSong::Group_OffsetBox *grobox = grsong->Spawn_OffsetBox();
    GroupSong::Group_Singer *gsing = grobox->Spawn_Singer();
    gsing->SampleRate = Globals::SampleRate;

    Wave wave;
    wave.Assign_SampleRate(Globals::SampleRate);
    gsing->Start();
    printf("Render_To:\n");
    gsing->Render_To(tree->Duration, wave);
    printf("Render_To done.\n");
    delete gsing;
    printf("SaveToWav:\n");
    wave.SaveToWav("RandomTree.wav");
    printf("SaveToWav done.\n");

    Voice_Iterative = true;// temporary for testing
    TestSpeed(*grsong, 3);
    Voice_Iterative = false;// temporary for testing
    TestSpeed(*grsong, 3);
    delete grobox;

    return 0;
  }

  if (true) {// create bent minor triad rhythm
    CreateBentTriad();
    //return 0;
  }

  if (true) {// Voice and  Group
    Voice *voz;
    voz = new Voice();
    voz->Update_Guts(metrics);

    //FillVoice(voz, 2000.0);// add voice bend points, 33 min 20 sec
    FillVoice(voz, 1.0);// add voice bend points, 1 min
    //FillVoice(voz, 0.2);// add voice bend points

    Voice::Voice_OffsetBox *vobox = voz->Spawn_OffsetBox();
    vobox->TimeX = 0.27;//0.11;// 0.0;
    //vobox->TimeX = 0.0;
    if (true) {
      Voice::Voice_Singer *vsing = vobox->Spawn_Singer();
      vsing->SampleRate = Globals::SampleRate;

      cout << "Current_Frequency:" << vsing->Current_Frequency << endl;
      vsing->Start();
      Chop_Test(vsing, "Voice");
      //Skip_Test(vsing, "Voice");
      delete vsing;
    }
    vobox->OctaveY = 2.0;
    GroupSong *gsong = new GroupSong();
    gsong->Add_SubSong(vobox);

    metrics.Reset();
    gsong->Update_Guts(metrics);

    GroupSong::Group_OffsetBox *grobox = gsong->Spawn_OffsetBox();
    //ISonglet::Unref(gb);
    //delete gb;// automatically deleted by grobox

    GroupSong::Group_Singer *gsing = grobox->Spawn_Singer();
    gsing->SampleRate = Globals::SampleRate;
    Chop_Test(gsing, "Group");
    //Skip_Test(gsing, "Group");
    delete gsing;
    delete grobox;
    //delete vobox;// the group already deleted everything.

    // delete voz;// voice is deleted automatically when we delete vobox
  }

  if (false) {// Span - simple group with one delayed voice
    Voice *voz0;
    voz0 = new Voice();
    FillVoice(voz0);

    Voice::Voice_OffsetBox *vobox0 = voz0->Spawn_OffsetBox();
    vobox0->TimeX = 0.27;//0.11;

    GroupSong *gsong = new GroupSong();
    gsong->Add_SubSong(vobox0);

    gsong->Update_Guts(metrics);

    GroupSong::Group_OffsetBox *gobox = gsong->Spawn_OffsetBox();

    SingerBase *singer = gobox->Spawn_Singer();
    singer->SampleRate = Globals::SampleRate;
    Chop_Test(singer, "Span");

    delete gsong;
  }

  if (false) {// Loop
    Voice *voz0;
    voz0 = new Voice();
    FillVoice(voz0);
    Voice::Voice_OffsetBox *vobox0 = voz0->Spawn_OffsetBox();

    GroupSong *chord = MakeChord(voz0);
    GroupSong::Group_OffsetBox *ChordHandle = chord->Spawn_OffsetBox();

    LoopSong *lsong = new LoopSong();

    //lsong->Add_SubSong(vobox0);
    lsong->Add_SubSong(ChordHandle);

    lsong->Set_Beats(100);
    //lsong->Set_Interval(0.10);// exposes loop/group chopping bug
    lsong->Set_Interval(0.25);
    GroupSong::Group_OffsetBox *lobox = lsong->Spawn_OffsetBox();

    lsong->Update_Guts(metrics);

    SingerBase *singer = lobox->Spawn_Singer();
    singer->SampleRate = Globals::SampleRate;

    Chop_Test(singer, "Loop");
    delete singer;

    TestSpeed(*lsong);
    //finished loop computation at Thu Nov 08 09:30:09 2018
    //elapsed time: 4.09459s

    delete lsong;
  }
  if (false) {
    Outer *ouch = new Outer();
    Outer::Inner *een = ouch->Spawn();
    cout << "een->NumberIn:" << een->NumberIn << endl;
    delete een;
    delete ouch;
  }

  if (false) {
    GroupSong *gb1 = new GroupSong();
    GroupSong::Group_OffsetBox *grobox1 = gb1->Spawn_OffsetBox();
    delete grobox1;
    //ISonglet::Unref(gb);
    //delete gb;// automatically deleted by grobox
  }

  OffsetBoxBase *obox = new OffsetBoxBase();

  OffsetBoxBase *oboxkid = obox->Clone_Me();

  if (false) {
    Wave *wav = new Wave();
    cout << "Hello world!" << endl;
    delete wav;
  }
  delete obox;
  delete oboxkid;

  return 0;
}

/*
Simplify API.  As it is:

create songlet
spawn offsetbox
attach project to tree
update tree guts with metric
spawn singer from offsetbox
singer.render to wav

possible:

offsetbox = songletclass.selfboxfactory()
update tree with metric but not with project (deprecate/delete updateproject)
spawn singer
singer.render to wav.  wav provides its own samplerate.

Somehow everyone should have a reference to the project top.
That could be through singer creation, but then spawn must be followed by singer.project = project.
Singers don't have a paramblob - yet.

*/

