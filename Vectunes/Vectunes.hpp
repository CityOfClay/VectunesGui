#ifndef Vectunes_hpp
#define Vectunes_hpp

// Single header file to include all Vectunes dependencies

#include "ICloneable.hpp"
#include "ISonglet.hpp"
#include "JsonParse.hpp"
#include "ITree.hpp"
#include "CajaDelimitadora.hpp"
#include "IDrawable.hpp"
#include "MonkeyBox.hpp"
#include "OffsetBoxBase.hpp"
#include "Voice.hpp"
#include "Line2D.hpp"
#include "Splines.hpp"
#include "Grabber.hpp"
#include "GroupSong.hpp"
#include "LoopSong.hpp"
#include "PatternMaker.hpp"
#include "SampleVoice.hpp"
#include "AudProject.hpp"
#include "Junkyard.hpp"
#include "GraphicBox.hpp"
#include "PluckVoice.hpp"
#include "Audio.hpp"
#include "GoLive.hpp"
#include "RythmicGroupSong.hpp"
#include "WtVoice.hpp"
#include "UndoStack.hpp"

#endif // Vectunes_hpp
