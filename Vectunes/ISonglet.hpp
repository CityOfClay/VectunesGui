#ifndef ISonglet_hpp
#define ISonglet_hpp

#include "IDeletable.hpp"
#include "ITextable.hpp"
#include "IDrawable.hpp"
#include "Config.hpp"
#include "ITree.hpp"

class OffsetBoxBase;// forward
class MonkeyBox;// forward
class SingerBase; // forward
class MetricsPacket;// forward

enum GlowType {Nothing, Blue, Red, Yellow };

class ISonglet: public IDrawable, public IDeletable, public ITextable {// Cancionita  , public ITree
public:
  Config *MyProject = nullptr;
  bool Selected = false;
  int FreshnessTimeStamp = -1;
  int RefCount = 0;
  virtual ~ISonglet() {};
  /* ********************************************************************************* */
  virtual OffsetBoxBase* Spawn_OffsetBox() = 0;// for compose time
  /* ********************************************************************************* */
  virtual SingerBase* Spawn_Singer() = 0;// for render time
  /* ********************************************************************************* */
  virtual SoundFloat Get_Duration() = 0;
  /* ********************************************************************************* */
  virtual SoundFloat Get_Max_Amplitude() = 0;
  /* ********************************************************************************* */
  virtual void Randomize_Color() = 0;
  /* ********************************************************************************* */
  // virtual void Update_Guts(MetricsPacket& metrics) = 0;
  /* ********************************************************************************* */
  virtual void Refresh_Me_From_Beneath(IMoveable& mbox) = 0;// should be just for IContainer types, but aren't all songs containers?
  /* ********************************************************************************* */
  //@Override ISonglet Deep_Clone_Me(ITextable.CollisionLibrary HitTable) = 0;
  /* ********************************************************************************* */
  virtual int Ref_Songlet() {// ISonglet Reference Counting: increment ref counter and return neuvo value just for kicks
    return ++this->RefCount;
  }
  virtual int UnRef_Songlet() {// ISonglet Reference Counting: decrement ref counter and return neuvo value just for kicks
    if (this->RefCount < 0) {
      printf("Voice: Negative RefCount:%d\n", this->RefCount);
      throw std::runtime_error("Voice: Negative RefCount:" + this->RefCount);
    }
    return --this->RefCount;
  }
  virtual int GetRefCount() {// ISonglet Reference Counting: get number of references for serialization
    return this->RefCount;
  }
  /* ********************************************************************************* */
  static int Unref(ISonglet** SongletPointerPointer){// Failed(?) experiment. Should probably delete this.
    ISonglet *songlet = *SongletPointerPointer;
    if (songlet==nullptr){ return -1; }
    int NumLeft = songlet->UnRef_Songlet();
    if (NumLeft<=0){
       delete songlet;
       *SongletPointerPointer = nullptr;
    }
    return NumLeft;
  }
  /* ********************************************************************************* */
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {
  }
  /* ********************************************************************************* */
  virtual bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) {// maybe move this to IDrawable?
    return false;// disabled by default, expand later.
  }
  /* ********************************************************************************* */
//  virtual void Remove_Child(MonkeyBox* child) { }
//  virtual void Add_Child(MonkeyBox* child) { }
};

class IContainer: public ISonglet {
public:
  /* ********************************************************************************* */
  //virtual void Refresh_Me_From_Beneath(IMoveable& mbox) = 0;// should be just for IContainer types, but aren't all songs containers?
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {};// should be just for IContainer types, but aren't all songs containers?
  virtual void Remove_Child(OffsetBoxBase* child) { }//  Remove_Child deletes child internally.
  virtual void Add_Child(OffsetBoxBase* child) { }
};
//
///* ********************************************************************************* */
//class MetricsPacket {// MetricsPacket is a ParamBlob
//public:
//  SoundFloat MaxDuration = 0.0;
//  Config* MyProject = nullptr;
//  int FreshnessTimeStamp = 1;
//  void Reset(){
//    MaxDuration = 0.0;
//    FreshnessTimeStamp = 1;
//  }
//};

#endif // ISonglet_hpp

