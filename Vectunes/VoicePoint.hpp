#ifndef VoicePoint_hpp
#define VoicePoint_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>

#include "Globals.hpp"
#include "IDeletable.hpp"
#include "MonkeyBox.hpp"
#include "DrawingContext.hpp"
#include "ITextable.hpp"
#include "IGrabber.hpp"
// #include "Voice.hpp"

/**
 *
 * @author MultiTool
*/

class LoudnessHandle;// forward
class Voice;// forward

/* ********************************************************************************* */
class VoicePoint: public MonkeyBox {
public:
  SoundFloat SubTime = 0.0;// SubTime is cumulative subjective time.

  // graphics support, will move to separate object
  SoundFloat OctavesPerLoudness = 0.125;// to do: loudness will have to be mapped to screen. not a pixel value right?
  StringConst UpHandleName = {"UpHandle"}, DownHandleName = {"DownHandle"};
  Voice* MyParentVoice = nullptr;
  /* ********************************************************************************* */
  VoicePoint() {
    mytype = 1;
    this->Create_Me();
  }
  /* ********************************************************************************* */
  virtual ~VoicePoint() { this->Delete_Me(); }
  /* ********************************************************************************* */
  // class LoudnessHandle: public IMoveable, public IDeletable, public ITextable, public ITree {// probably should not be ITextable
  class LoudnessHandle: public Handle {// probably should not be ITextable
  public:// This is the volume control node of a control point in a voice.
    VoicePoint* ParentPoint = nullptr;
    /* ********************************************************************************* */
    LoudnessHandle() {
      this->Create_Me();
      this->AssignOctavesPerRadius(0.007);
    }
    virtual ~LoudnessHandle(){ this->Delete_Me(); }
    /* ********************************************************************************* */
    Voice* GetParentVoice() {
      return this->ParentPoint->MyParentVoice;
    }
    /* ********************************************************************************* */
    void MoveTo(TimeFloat XLoc, SoundFloat YLoc) override {// IMoveable
      if (XLoc <= 0.0) { XLoc = 0.0; }// don't go backward in time
      this->ParentPoint->TimeX = XLoc;
      SoundFloat RelativeY = YLoc - this->ParentPoint->OctaveY;
      if (RelativeY >= 0) {// non negative loudness
        RelativeY /= this->ParentPoint->OctavesPerLoudness;
        if (RelativeY <= 1.0) {// shouldn't amplify loudness above 1.0, so that we can keep wave clipping under control
          this->ParentPoint->LoudnessFactor = RelativeY;
        }
      }
    }
    SoundFloat GetX() override {
      return this->ParentPoint->TimeX;
    }
    SoundFloat GetY() override {
      SoundFloat LoudnessHeight = this->ParentPoint->LoudnessFactor * this->ParentPoint->OctavesPerLoudness;// Map loudness to screen pixels.
      // to do: draw loudness scaled to inherited loudness. LoudnessHgt = pnt->LoudnessFactor * ParentDC.GlobalTransform.LoudnessFactor * pnt->OctavesPerLoudness;
      return this->ParentPoint->OctaveY + LoudnessHeight;
    }
    /* ********************************************************************************* */
    bool HitsMe(IGrabber& Scoop, FishingStackContext& StackContext) override {// IDrawable.IMoveable
      printf("LoudnessHandle HitsMe:");
      StackContext.RecurseDepth++;
      CajaDelimitadora SearchBoxLocal;// Do this all in local space, not pixel space.
      StackContext.MapTo(*Scoop.GetSearchBox(), SearchBoxLocal);// Map SearchBox from pixel space to obx local space.
      Point2D EllipseSize = GetEllipseSizeLocal(StackContext.RecurseDepth);// Get ellipse size in local space.
      // My handle rests *upon* the line I control, so I don't occlude my VoicePoint.
      return BoxHitsEllipse(SearchBoxLocal, this->GetX(), this->GetY() + EllipseSize.Y, EllipseSize.X, EllipseSize.Y);
    }
    /* ********************************************************************************* */
    void SetSelected(boolean Selected) {// IMoveable
      this->IsSelected = Selected;
    }
    /* ********************************************************************************* */
    void Draw_Me(DrawingContext& ParentDC) override {
      // Control points have the same space as their parent, so no need to create a local map.
      PROFILE_START;
      int RecurseDepth = ParentDC.RecurseDepth + 1;
//      if (false) {
//        SoundFloat BoundsWdtPixels = Math::abs(ParentDC.GlobalTransform.ScaleX) * MyBounds.GetWidth();
//        SoundFloat BoundsHgtPixels = Math::abs(ParentDC.GlobalTransform.ScaleY) * MyBounds.GetHeight();
//        if ((BoundsWdtPixels <= 10000.0) || (BoundsHgtPixels <= 10000.0)) {
//          std::cout << "************************* LOD CLIPPED *******" << "\n";
//          PROFILE_END(Globals::OffsetBoxBase_Hits);
//          return;
//        }
//      }

      Point2D pnt;
      Color yellow(1.0, 1.0, 0.0);

      if (false) {
        ParentDC.To_Screen(this->GetX(), this->GetY(), pnt);
        SoundFloat RadiusPixels = Math::abs(ParentDC.GlobalTransform.ScaleY) * OctavesPerRadius;
        SoundFloat YlocHigh = pnt.Y + RadiusPixels;// My handle rests *upon* the line I control, so I don't occlude my VoicePoint.
        MonkeyBox::Draw_Dot2(ParentDC, pnt.X, YlocHigh, OctavesPerRadius, this->IsSelected, yellow);
      } else {
        Point2D RadiusPixels2D = this->GetEllipseSizePixels(ParentDC.GlobalTransform, RecurseDepth);
        ParentDC.To_Screen(this->GetX(), this->GetY(), pnt);
        ParentDC.SetColor(0.7, 1.0, 0.7, 0.5);
        // My handle rests *upon* the line I control, so I don't occlude my VoicePoint.
        ParentDC.DrawEllipse(pnt.X, pnt.Y + RadiusPixels2D.Y, RadiusPixels2D.X, RadiusPixels2D.Y);
      }

      PROFILE_END(Globals::LoudnessHandle_Hits);
    }
    /* ********************************************************************************* */
    void UpdateBoundingBoxLocal() override {
      SoundFloat XLoc = this->GetX();
      SoundFloat YLoc = this->GetY() + this->OctavesPerRadius;// *upon* the line
      SoundFloat MinX = XLoc - this->OctavesPerRadius;
      SoundFloat MaxX = XLoc + this->OctavesPerRadius;
      SoundFloat MinY = YLoc - this->OctavesPerRadius;
      SoundFloat MaxY = YLoc + this->OctavesPerRadius;
      this->MyBounds.Assign(MinX, MinY, MaxX, MaxY);
    }
    /* ********************************************************************************* */
    void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
      StackContext.RecurseDepth++;// from offsetboxbase
      CajaDelimitadora PixelBounds;
      StackContext.UnMap(this->MyBounds, PixelBounds);// Map MyBounds from local handle space to root pixel space.
      if (Scoop.Intersects(PixelBounds)) {// PixelBounds and MyBounds are in pixel space.
        bool hit = this->HitsMe(Scoop, StackContext);// Results and obox(me) are in immediate parent space.
        if (hit) {// if Scoop hits either obox (me) or my songlet, add me to Scoop Fish stringer.
          printf("Hit Circle or Child!");
          Scoop.AddFish(this, StackContext);// Add me to scoop fish stringer here.
        }
      }
    }
    /* ********************************************************************************* */
    void Burn_Scale(int TimeStamp, TimeFloat ReScaleX, SoundFloat ReScaleY) override {
    }
    /* ********************************************************************************* */
    void Copy_From(const LoudnessHandle &donor) {
      Handle::Copy_From(donor); // this->MyBounds.Copy_From(donor.MyBounds);
      // this->AssignOctavesPerRadius(donor.OctavesPerRadius);// this->OctavesPerRadius = donor.OctavesPerRadius;
      this->ParentPoint = donor.ParentPoint;// snox is copying this pointer acceptable?
      //this->IsSelected = donor.IsSelected;
    }
    /* ********************************************************************************* */
    LoudnessHandle* Clone_Me() override {// ICloneable
      LoudnessHandle *child = new LoudnessHandle();// clone, probably never used
      return child;
    }
    /* ********************************************************************************* */
    LoudnessHandle* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
      LoudnessHandle *child = new LoudnessHandle();// clone, probably never used
      child->AssignOctavesPerRadius(this->OctavesPerRadius);// child->OctavesPerRadius = this->OctavesPerRadius;
      child->ParentPoint = this->ParentPoint;
      child->MyBounds.Copy_From(this->MyBounds);
      return child;
    }
    /* ********************************************************************************* */
    boolean Create_Me() override {// IDeletable
      //this->AssignOctavesPerRadius(this->OctavesPerRadius/3.0);
      return true;
    }
    void Delete_Me() override {// IDeletable
      this->MyBounds.Delete_Me();// wreck everything
      this->OctavesPerRadius = Double::NEGATIVE_INFINITY;
      this->ParentPoint = nullptr;
      this->IsSelected = false;
    }
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
      JsonParse::HashNode *phrase = new JsonParse::HashNode();
      phrase->AddSubPhrase("OctavesPerRadius", PackNumberField(this->OctavesPerRadius));
      if (false) {// can be calculated
        phrase->AddSubPhrase("IsSelected", PackBoolField(this->IsSelected));
        //phrase->AddSubPhrase("MyBounds", this->MyBounds.Export(HitTable));
      }
      return phrase;
    }
    void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
      this->AssignOctavesPerRadius(GetNumberField(phrase, "OctavesPerRadius", 0.007));// this->OctavesPerRadius = GetNumberField(phrase, "OctavesPerRadius", 0.007);
    }
    /* ********************************************************************************* */
    void Update_Guts(MetricsPacket &metrics) override {
      this->UpdateBoundingBoxLocal();
    }
  };
  /* ********************************************************************************* */
  LoudnessHandle UpHandle, DownHandle;
  /* ********************************************************************************* */
  void RefParent(Voice* Parent) {
    //this->MyParentSong = this->MyParentVoice = Parent;
    this->MyParentVoice = Parent;
  }
  /* ********************************************************************************* */
  Voice* GetParentVoice() {
    return this->MyParentVoice;
  }
  /* ********************************************************************************* */
  void CopyFrom(VoicePoint& source) {
    MonkeyBox::Copy_From(source);
    this->SubTime = source.SubTime;
  }
  /* ********************************************************************************* */
//  SoundFloat GetFrequencyFactor() {
//    return Math::pow(2.0, this->OctaveY);
//  }
  /* ********************************************************************************* */
  void Draw_Me(DrawingContext& ParentDC) override {// IDrawable
    // Control points have the same space as their parent, so no need to create a local map.
    PROFILE_START;

    Color yellow(1.0, 1.0, 0.0);
    Point2D pnt;
    ParentDC.To_Screen(this->TimeX, this->OctaveY, pnt);
    SoundFloat RadiusPixels = Math::abs(ParentDC.GlobalTransform.ScaleY) * OctavesPerRadius;
    RadiusPixels = Math::ceil(RadiusPixels);
    SoundFloat DiameterPixels = RadiusPixels * 2.0;
    this->UpHandle.Draw_Me(ParentDC);
//    this->DownHandle.Draw_Me(ParentDC);
    if (false) {
      MonkeyBox::Draw_Dot2(ParentDC, pnt.X, pnt.Y, OctavesPerRadius, this->IsSelected, yellow);
    } else {
      Point2D RadiusPixels2D = this->GetEllipseSizePixels(ParentDC.GlobalTransform, ParentDC.RecurseDepth);
      if (false) {
        SoundFloat LoudnessHeight = this->GetY() * ParentDC.GlobalTransform.LoudnessFactor;
        ParentDC.To_Screen(this->GetX(), LoudnessHeight, pnt);
      } else {
        ParentDC.To_Screen(this->GetX(), this->GetY(), pnt);
      }
      ParentDC.SetColor(0.7, 1.0, 0.7, 0.5);
      ParentDC.DrawEllipse(pnt.X, pnt.Y, RadiusPixels2D.X, RadiusPixels2D.Y);
    }
    PROFILE_END(Globals::VoicePoint_Hits);
  }
  /* ********************************************************************************* */
  void UpdateBoundingBoxLocal() override {// IDrawable
    SoundFloat LoudnessHeight = LoudnessFactor * OctavesPerLoudness;// Map loudness to screen pixels.
    SoundFloat MinX = TimeX - OctavesPerRadius;
    SoundFloat MaxX = TimeX + OctavesPerRadius;
    SoundFloat HeightRad = Math::max(OctavesPerRadius, LoudnessHeight);
    SoundFloat MinY = OctaveY - HeightRad;
    SoundFloat MaxY = OctaveY + HeightRad;
    this->MyBounds.Assign(MinX, MinY, MaxX, MaxY);
    this->MyBounds.Include(*(this->UpHandle.GetBoundingBox()));// Don't have to UnMap in this case because my points are already in my internal coordinates.
  }
  /* ********************************************************************************* */
  void Update_Guts(MetricsPacket& metrics) override {
    this->UpHandle.Update_Guts(metrics);
    this->UpdateBoundingBoxLocal();
  }
  /* ********************************************************************************* */
  void GoFishing(IGrabber& Scoop, FishingStackContext StackContext) override {// IDrawable
    printf("VoicePoint GoFishing\n");
    printf(" Point GoFishing: ");
    CajaDelimitadora PixelBounds;
    StackContext.UnMap(this->MyBounds, PixelBounds);// Map local bounds to pixel space.
    if (Scoop.Intersects(PixelBounds)) {
      // System.out.print(" InBounds, ");
      bool hit = this->HitsMe(Scoop, StackContext);// Results and obox(me) are in immediate parent space.
      if (hit) {// Hit the circle, add the handle.
        Scoop.AddFish(this, StackContext);
      }
      this->UpHandle.GoFishing(Scoop, StackContext);
    }
    printf("\n");
  }
  /* ********************************************************************************* */
  void Burn_Scale(int TimeStamp, TimeFloat ReScaleX, SoundFloat ReScaleY) override {
    this->TimeX *= ReScaleX;
    this->OctaveY *= ReScaleY;
  }
  /* ********************************************************************************* */
  VoicePoint* Clone_Me() override {// ICloneable
    VoicePoint *child = new VoicePoint();// clone
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  VoicePoint* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    VoicePoint *child = this->Clone_Me();// new VoicePoint();// clone
    //child->Copy_From(*this);
    child->UpHandle.Copy_From(this->UpHandle);
    child->UpHandle.ParentPoint = child;

    child->DownHandle.Copy_From(this->DownHandle);
    child->DownHandle.ParentPoint = child;

//    (child->UpHandle = this->UpHandle.Deep_Clone_Me(HitTable)).ParentPoint = child; //child.UpHandle.ParentPoint = child;
//    (child->DownHandle = this->DownHandle.Deep_Clone_Me(HitTable)).ParentPoint = child; //child.DownHandle.ParentPoint = child;
    return child;
  }
  /* ********************************************************************************* */
  void Copy_From(const VoicePoint& donor) {
    MonkeyBox::Copy_From(donor);
    Handle::Copy_From(donor);//this->AssignOctavesPerRadius(donor.OctavesPerRadius);//
    this->SubTime = donor.SubTime;
    this->OctavesPerLoudness = donor.OctavesPerLoudness;

    this->UpHandle.ParentPoint = this;// snox redundant, remove. this is already done in constructor.
    this->DownHandle.ParentPoint = this;
  }
  /* ********************************************************************************* */
  bool Create_Me() override {// IDeletable
    this->AssignOctavesPerRadius(0.007);
    this->UpHandle.ParentPoint = this;
    this->DownHandle.ParentPoint = this;
    return true;
  }
  void Delete_Me() override {// IDeletable
    this->SubTime = Double::NEGATIVE_INFINITY;// wreck everything
    this->OctavesPerLoudness = Double::NEGATIVE_INFINITY;
    this->MyBounds.Delete_Me();
    this->UpHandle.Delete_Me();
    this->DownHandle.Delete_Me();
  }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *phrase = MonkeyBox::Export(HitTable);
    phrase->AddSubPhrase("OctavesPerLoudness", PackNumberField(this->OctavesPerLoudness));
    // phrase->AddSubPhrase("SubTime", PackNumberField(this->SubTime));// can be calculated
    phrase->AddSubPhrase(VoicePoint::UpHandleName, this->UpHandle.Export(HitTable));
    phrase->AddSubPhrase(VoicePoint::DownHandleName, this->DownHandle.Export(HitTable));
    this->UpHandle.Export(HitTable);
    this->DownHandle.Export(HitTable);
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    MonkeyBox::ShallowLoad(phrase);
    this->OctavesPerLoudness = GetNumberField(phrase, "OctavesPerLoudness", 0.125);
    if (false) {
      this->SubTime = GetNumberField(phrase, "SubTime", 0.0);// can be calculated
    }
    this->UpHandle.ShallowLoad(*(JsonParse::HashNode*)phrase.Get(VoicePoint::UpHandleName));// another cast!
    this->DownHandle.ShallowLoad(*(JsonParse::HashNode*)phrase.Get(VoicePoint::DownHandleName));// another cast!
  }
};


#endif
