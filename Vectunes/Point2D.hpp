#ifndef Point2D_hpp
#define Point2D_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "IDeletable.hpp"

/**
 *
 * @author MultiTool
 */
/* ********************************************************************** */
class Point2D;// forward
typedef Point2D *Point2DPtr;
typedef std::vector<Point2DPtr> Point2DVec;
class Point2D : public IDeletable {
public:
  //SoundFloat x = 0.0, y = 0.0;
  SoundFloat X = 0.0, Y = 0.0;
  Point2D() {}
  Point2D(const Point2D& donor) : Point2D(donor.X, donor.Y) {
  }
  Point2D(SoundFloat XLoc, SoundFloat YLoc) {
    this->Assign(XLoc, YLoc);
  }
  virtual ~Point2D() {
    this->Delete_Me();
  }
  void SetLocation(SoundFloat XLoc, SoundFloat YLoc) {
    this->X = XLoc; this->Y = YLoc;
  }
  void SetLocation(const Point2D& pd) {
    this->X = pd.X; this->Y = pd.Y;
  }
  void Assign(SoundFloat XLoc, SoundFloat YLoc) {
    this->X = XLoc; this->Y = YLoc;
  }
  void Assign(const Point2D& other) {
    this->X = other.X; this->Y = other.Y;
  }
  void Copy_From(const Point2D& donor) {
    this->X = donor.X; this->Y = donor.Y;
  }
  void Add(const Point2D& other) {
    this->X += other.X; this->Y += other.Y;
  }
  void Subtract(const Point2D& other) {
    this->X -= other.X; this->Y -= other.Y;
  }
  void Multiply(SoundFloat factor) {
    this->X *= factor; this->Y *= factor;
  }
  void Normalize() {
    if (this->X != 0 || this->Y != 0) {
      SoundFloat magnitude = Math::sqrt((this->X * this->X) + (this->Y * this->Y));
      this->X /= magnitude; this->Y /= magnitude;
    }
  }
  SoundFloat GetMagnitude() {
    if (this->X == 0 && this->Y == 0) { return 0; }
    return Math::sqrt((this->X * this->X) + (this->Y * this->Y));
  }
  void MorphTo(const Point2D& Target, double Fraction) {
    double FractionComp = 1.0 -Fraction;
    this->X = (this->X * FractionComp) + (Target.X * Fraction);
    this->Y = (this->Y * FractionComp) + (Target.Y * Fraction);
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {
    return 0;
  }
  void Delete_Me() override {
    this->X = Double::NEGATIVE_INFINITY;
    this->Y = Double::NEGATIVE_INFINITY;
  }
  /* ********************************************************************************* */
};

#endif
