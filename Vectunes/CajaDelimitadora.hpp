#ifndef CajaDelimitadora_hpp
#define CajaDelimitadora_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "IDeletable.hpp"
#include "ITextable.hpp"
#include "Point2D.hpp"

/**
 *
 * @author MultiTool
 */

class CollisionLibrary;// forward
/* ********************************************************************************* */
class CajaDelimitadora: public IDeletable, ICloneable {//, ITextable {// DIY BoundingBox
public:
  Point2D Min, Max;
  Point2D *Limits[2] = {&Min, &Max};
  /* ********************************************************************************* */
  CajaDelimitadora() {
    this->Limits[0] = &Min;
    this->Limits[1] = &Max;
  }
  /* ********************************************************************************* */
  CajaDelimitadora(SoundFloat MinX, SoundFloat MinY, SoundFloat MaxX, SoundFloat MaxY) : CajaDelimitadora() {
    this->Assign(MinX, MinY, MaxX, MaxY);
  }
  /* ********************************************************************************* */
  virtual ~CajaDelimitadora() {
    this->Delete_Me();
  }
  /* ********************************************************************************* */
  void Assign(SoundFloat MinX, SoundFloat MinY, SoundFloat MaxX, SoundFloat MaxY) {
    this->Min.Assign(MinX, MinY);
    this->Max.Assign(MaxX, MaxY);
    this->Sort_Me();
  }
  /* ********************************************************************************* */
  void AssignCorner(int CornerNum, SoundFloat MinLimit, SoundFloat MaxLimit) {}
  /* ********************************************************************************* */
  bool Intersects(const CajaDelimitadora& other) {
    if (!this->LineFramed(this->Min.X, this->Max.X, other.Min.X, other.Max.X)) {
      return false;
    } else if (!this->LineFramed(this->Min.Y, this->Max.Y, other.Min.Y, other.Max.Y)) {
      return false;
    }
    return true;
  }
  /* ********************************************************************************* */
  bool Contains(const Point2D &Loc) const {
    TimeFloat XLoc = Loc.X;
    SoundFloat YLoc = Loc.Y;
    if (this->Min.X <= XLoc) {
      if (XLoc <= this->Max.X) {
        if (this->Min.Y <= YLoc) {
          if (YLoc <= this->Max.Y) {
            return true;
          }
        }
      }
    }
    return false;
  }
  /* ********************************************************************************* */
  bool Contains(SoundFloat XLoc, SoundFloat YLoc) const {
    if (this->Min.X <= XLoc) {
      if (XLoc <= this->Max.X) {
        if (this->Min.Y <= YLoc) {
          if (YLoc <= this->Max.Y) {
            return true;
          }
        }
      }
    }
    return false;
  }
  /* ********************************************************************************* */
  bool UnInit() {
    if (Math::abs(this->Min.X) == Double::POSITIVE_INFINITY) {
      return true;
    }
    if (Math::abs(this->Min.Y) == Double::POSITIVE_INFINITY) {
      return true;
    }
    if (Math::abs(this->Max.X) == Double::POSITIVE_INFINITY) {
      return true;
    }
    if (Math::abs(this->Max.Y) == Double::POSITIVE_INFINITY) {
      return true;
    }
    return false;
  }
  /* ********************************************************************************* */
  boolean LineFramed(SoundFloat MyMin, SoundFloat MyMax, SoundFloat YourMin, SoundFloat YourMax) {
    if (YourMax < MyMin) {// if my min is less than your max AND my max is greater than your min
      return false;
    } else if (MyMax < YourMin) {
      return false;
    }
    return true;
  }
  /* ********************************************************************************* */
  void Rebase_Time(TimeFloat TimeBase) {
    SoundFloat TimeRange = this->Max.X - this->Min.X;
    this->Min.X = TimeBase;
    this->Max.X = this->Min.X + TimeRange;
  }
  /* ********************************************************************************* */
  void Copy_From(const CajaDelimitadora& donor) {
    this->Min.Copy_From(donor.Min);
    this->Max.Copy_From(donor.Max);
  }
  /* ********************************************************************************* */
  CajaDelimitadora* Clone_Me() override {// ICloneable
    CajaDelimitadora* child = new CajaDelimitadora();// clone
    child->Copy_From(*this);
    return child;
  }
  CajaDelimitadora* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    return nullptr;// never used with CajaDelimitadora. reconsider inheriting from ICloneable.
  }
  /* ********************************************************************************* */
  bool ZeroCheck() const { // for debugging
    if (this->Min.X == 0.0) {
      if (this->Min.Y == 0.0) {
        if (this->Max.X == 0.0) {
          if (this->Max.Y == 0.0) {
            return true;
          }
        }
      }
    }
    return false;
  }
  /* ********************************************************************************* */
  void Sort_Me() {// CajaDelimitadora bounds are ALWAYS to be sorted min->max, even if we are in an inverted space such as screen graphics.
    SoundFloat temp;
    if (this->Max.X < this->Min.X) {
      temp = this->Max.X;// swap
      this->Max.X = this->Min.X;
      this->Min.X = temp;
    }
    if (this->Max.Y < this->Min.Y) {
      temp = this->Max.Y;// swap
      this->Max.Y = this->Min.Y;
      this->Min.Y = temp;
    }
    this->ZeroCheck();
  }
  /* ********************************************************************************* */
  SoundFloat GetWidth() const {
    //return Math::abs(this->Max.X - this->Min.X);
    return (this->Max.X - this->Min.X);
  }
  /* ********************************************************************************* */
  SoundFloat GetHeight() const {
    // return Math::abs(this->Max.Y - this->Min.Y);
    return (this->Max.Y - this->Min.Y);
  }
  /* ********************************************************************************* */
  Point2D GetCenter() const {
    Point2D ctr( (this->Min.X + this->Max.X)/2.0, (this->Min.Y + this->Max.Y)/2.0);
    return ctr;
  }
  /* ********************************************************************************* */
  void Swell(double Amount) {
    this->Min.X -= Amount; this->Min.Y -= Amount;
    this->Max.X += Amount; this->Max.Y += Amount;
  }
  /* ********************************************************************************* */
  void Multiply(double Factor) {
    double CtrX = (this->Min.X + this->Max.X) / 2.0;
    double CtrY = (this->Min.Y + this->Max.Y) / 2.0;
    double RadiusX = this->Max.X - CtrX;
    double RadiusY = this->Max.Y - CtrY;

    RadiusX *= Factor; RadiusY *= Factor;

    this->Min.X = CtrX - RadiusX; this->Min.Y = CtrY - RadiusY;
    this->Max.X = CtrX + RadiusX; this->Max.Y = CtrY + RadiusY;
  }
  /* ********************************************************************************* */
  void Make_Unitary() {// make this box 2x2
    this->Min.Assign(-1.0, -1.0);
    this->Max.Assign(1.0, 1.0);
    this->ZeroCheck();
  }
  /* ********************************************************************************* */
  void Make_Unlimited() {// make this box include the universe, effectively no clipping
//    this->Min.Assign(Double::NEGATIVE_INFINITY, Double::NEGATIVE_INFINITY);
//    this->Max.Assign(Double::POSITIVE_INFINITY, Double::POSITIVE_INFINITY);
    this->Min.Assign(Integer::MIN_VALUE, Integer::MIN_VALUE);
    this->Max.Assign(Integer::MAX_VALUE, Integer::MAX_VALUE);
    this->ZeroCheck();
  }
  /* ********************************************************************************* */
  void Reset() {// reset for min, max comparisons
    this->Min.Assign(Double::POSITIVE_INFINITY, Double::POSITIVE_INFINITY);
    this->Max.Assign(Double::NEGATIVE_INFINITY, Double::NEGATIVE_INFINITY);
    this->ZeroCheck();
  }
  /* ********************************************************************************* */
  void ClearZero() {// for empty boxes
    this->Min.Assign(0, 0);
    this->Max.Assign(0, 0);
  }
  /* ********************************************************************************* */
  void Include(const CajaDelimitadora& other) {// for aggregating with all of my child boxes
    this->Min.X = Math::min(this->Min.X, other.Min.X);
    this->Min.Y = Math::min(this->Min.Y, other.Min.Y);
    this->Max.X = Math::max(this->Max.X, other.Max.X);
    this->Max.Y = Math::max(this->Max.Y, other.Max.Y);
    this->ZeroCheck();
  }
  /* ********************************************************************************* */
  void IncludePoint(const Point2D& other) {// for aggregating with vertices
    IncludePoint(other.X, other.Y);
  }
  /* ********************************************************************************* */
  void IncludePoint(SoundFloat OtherX, SoundFloat OtherY) {// for aggregating with vertices
    this->Min.X = Math::min(this->Min.X, OtherX);
    this->Min.Y = Math::min(this->Min.Y, OtherY);
    this->Max.X = Math::max(this->Max.X, OtherX);
    this->Max.Y = Math::max(this->Max.Y, OtherY);
    this->ZeroCheck();
  }
  /* ********************************************************************************* */
  void MorphTo(const CajaDelimitadora& Target, double FractAlong) {
    double FractionComp = 1.0 - FractAlong;
    this->Min.X = (this->Min.X * FractionComp) + (Target.Min.X * FractAlong);
    this->Min.Y = (this->Min.Y * FractionComp) + (Target.Min.Y * FractAlong);
    this->Max.X = (this->Max.X * FractionComp) + (Target.Max.X * FractAlong);
    this->Max.Y = (this->Max.Y * FractionComp) + (Target.Max.Y * FractAlong);
  }
  /* ********************************************************************************* */
  boolean Create_Me() override {
    return true;
  }
  void Delete_Me() override {// if you can't delete it, at least mess it up so it can't be reused without exploding
    Min.Delete_Me(); Max.Delete_Me();// wreck everything
    this->Limits[0] = nullptr;
    this->Limits[1] = nullptr;
  }
  /* ********************************************************************************* */
//  JsonParse::HashNode* Export(CollisionLibrary& HitTable)=0;// to do: pass a collision table parameter
//  void ShallowLoad(JsonParse::HashNode& phrase)=0;// just fill in primitive fields that belong to this object, don't follow up pointers.
#if false
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse.Node phrase = neuvo JsonParse.Node();
    phrase.ChildrenHash = neuvo HashMap<String, JsonParse.Node>();
    phrase.AddSubPhrase("Min", IFactory.Utils.PackField(this->Min.toString()));
    phrase.AddSubPhrase("Max", IFactory.Utils.PackField(this->Max.toString()));
    return phrase;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {// ITextable
    HashMap<String, JsonParse.Node> Fields = phrase.ChildrenHash;
    throw std::runtime_error("Not supported yet.");
  }
#endif // false
};

#endif

