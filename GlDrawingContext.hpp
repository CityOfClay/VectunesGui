
#ifndef GlDrawingContext_hpp
#define GlDrawingContext_hpp

#include "./Vectunes/Vectunes.hpp"

// For now, giving up on Artist pattern and using straight Java-style graphics context pattern.
// Downside is all drawable types will be constrained to DrawLine, DrawCircle, etc.  Less freedom.
// Other downside is Artits are a better place to cache splines than in the songlets themselves.

// If DISABLE is defined, it turns off all graphics actions. It is mainly for performance testing.
//#define DISABLE return
#define DISABLE

/* ********************************************************************************* */
class CirclePoints : public ArrayList<Point2D> {
public:
  CirclePoints() {
    int NumVerts = 16;//32;
    this->clear();
    for(int VCnt=0; VCnt<=NumVerts; VCnt++){
      double angle =  Globals::TwoPi * (((double)VCnt) / (double)NumVerts);
      SoundFloat XLoc = (Math::cos(angle));
      SoundFloat YLoc = (Math::sin(angle));
      this->push_back(Point2D(XLoc, YLoc));// https://stackoverflow.com/questions/15802006/how-can-i-create-objects-while-adding-them-into-a-vector
    }
  }
};
/* ********************************************************************************* */
class GlDrawingContext : public DrawingContext {
public:
  static CirclePoints circlepoints;
  /* ********************************************************************************* */
  GlDrawingContext() {
    //this->gr = new GraphicsOgl();
  }
  /* ********************************************************************************* */
  virtual ~GlDrawingContext() {
    //delete this->gr;
  }
  /* ********************************************************************************* */
  GlDrawingContext(DrawingContext& Fresh_Parent, OffsetBoxBase& Fresh_Transform) {
    this->Create_Me();
    Fresh_Parent.Compound_To_Child(Fresh_Transform, *this);
  }
  /* ********************************************************************************* */
  GlDrawingContext* Clone_Me() override {
    GlDrawingContext *child = new GlDrawingContext();
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  GlDrawingContext* Compound_Clone(const Transformer& Fresh_Transform) const override {
    GlDrawingContext *child = new GlDrawingContext();
    Compound_To_Child(Fresh_Transform, *child);
    return child;
  }
  void SetLineWidth(double LineWidth) override {
    glLineWidth(LineWidth);
  }
  //class GraphicsOgl : Graphics2D
  void DrawLine(double X0, double Y0, double X1, double Y1) override {
    DISABLE;
//    GLenum mode; GLint first; GLsizei count;
//    glDrawArrays(mode, first, count);// http://antongerdelan.net/opengl/hellotriangle.html ?
//    GLuint vao;// https://www.khronos.org/opengl/wiki/Tutorial2:_VAOs,_VBOs,_Vertex_and_Fragment_Shaders_(C_/_SDL)
//    glBindVertexArray(vao);
    glBegin(GL_LINES);
    //glColor3f(1.0, 0.0, 1.0);
    glVertex2d(X0, Y0); glVertex2d(X1, Y1);
    glEnd();
  }
  void SetColor(const Color& color) override {
    DISABLE;
    glColor4f(color.RedChannel, color.GreenChannel, color.BlueChannel, color.AlphaChannel);
  }
  void SetColor(double Red, double Green, double Blue, double Alpha) override {
    DISABLE;
    glColor4f(Red, Green, Blue, Alpha);
  }
  void Render() override {// https://en.wikibooks.org/wiki/OpenGL_Programming/GLStart/Tut3
    DISABLE;
    //clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glLoadIdentity();//load identity matrix
    //glTranslatef(0.0f,0.0f,-4.0f);//move forward 4 units
    glColor3f(0.0f,0.0f,1.0f); //blue color
    glBegin(GL_POLYGON);//begin drawing of polygon
      glVertex3f(-0.5f,0.5f,0.0f);//first vertex
      glVertex3f(0.5f,0.5f,0.0f);//second vertex
      glVertex3f(1.0f,0.0f,0.0f);//third vertex
      glVertex3f(0.5f,-0.5f,0.0f);//fourth vertex
      glVertex3f(-0.5f,-0.5f,0.0f);//fifth vertex
      glVertex3f(-1.0f,0.0f,0.0f);//sixth vertex
    glEnd();//end drawing of polygon
  }
  ///////////////////////////////////////////////////////////////////////////////
  // draw a simple concave quad
  ///////////////////////////////////////////////////////////////////////////////
  void DrawConcave() override {
    DISABLE;
    // define concave quad data (vertices only)
    //  0 2
    //  \ \/ /
    //   \3 /
    //    \/
    //    1
    GLdouble factor = 0.1;
    GLdouble quad1[4][3] = { {-1*factor,3*factor,0}, {0*factor,0*factor,0}, {1*factor,3*factor,0}, {0*factor,2*factor,0} };

    // We are going to do 2-pass draw: draw to stencil buffer first,
    // then draw to color buffer.
    glEnable(GL_STENCIL_TEST);          // enable stencil test

    // PASS 1: draw to stencil buffer only
    // The reference value will be written to the stencil buffer plane if test passed
    // The stencil buffer is initially set to all 0s.
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // disable writing to color buffer
    glStencilFunc(GL_ALWAYS, 0x1, 0x1);
    glStencilOp(GL_KEEP, GL_INVERT, GL_INVERT);

    glBegin(GL_TRIANGLE_FAN);
        glVertex3dv(quad1[0]);
        glVertex3dv(quad1[1]);
        glVertex3dv(quad1[2]);
        glVertex3dv(quad1[3]);
    glEnd();

    // PASS 2: draw color buffer
    // Draw again the exact same polygon to color buffer where the stencil
    // value is only odd number(1). The even(0) area will be descarded.
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);    // enable writing to color buffer
    glStencilFunc(GL_EQUAL, 0x1, 0x1);                  // test if it is odd(1)
    glStencilOp(GL_KEEP, GL_KEEP , GL_KEEP);
    glColor3f(1,1,1);
    glBegin(GL_TRIANGLE_FAN);
        glVertex3dv(quad1[0]);
        glVertex3dv(quad1[1]);
        glVertex3dv(quad1[2]);
        glVertex3dv(quad1[3]);
    glEnd();

    glDisable(GL_STENCIL_TEST);
  }
  void FillPolygon(double Outline[][2],int NumDrawPoints) override {// only convex polygons for now
    DISABLE;
    //DrawConcave(); return;
    //Render(); return;
    double *pnt;
    glBegin(GL_POLYGON);
    //glBegin(GL_TRIANGLE_FAN);
    //glColor4f(0.5, 0.5, 0.5, 0.3);
    for (int cnt=0;cnt<NumDrawPoints;cnt++){
      pnt = Outline[cnt];
      glVertex2d(pnt[0], pnt[1]);
    }
    glEnd();
  }
  void DrawPolygon(double Outline[][2],int NumDrawPoints) override {
    DISABLE;
    double *pnt;
    glBegin(GL_LINE_LOOP);
    //glColor3f(1.0, 1.0, 0.0);
    for (int cnt=0;cnt<NumDrawPoints;cnt++){
      pnt = Outline[cnt];
      glVertex2d(pnt[0], pnt[1]);
    }
    glEnd();
  }
  void DrawPolygon(double *OutlineX,double *OutlineY,int NumDrawPoints) override { }
  void DrawPolyline(double PolyLine[][2],int NumDrawPoints) override {
    DISABLE;
    double *pnt;
    glBegin(GL_LINE_STRIP);
    //glColor3f(1.0, 0.0, 1.0);
    for (int cnt=0;cnt<NumDrawPoints;cnt++){
      pnt = PolyLine[cnt];
      //glVertex2f(pnt[0], pnt[1]);//
      glVertex2d(pnt[0], pnt[1]);
    }
    glEnd();
  }
  /* ********************************************************************************* */
  static void DrawCircle2(double XCtr, double YCtr, double Radius) {
    DISABLE;
    int NumVerts;
    double x, y;
    if (false){// supposed to be faster but actually slower than glBegin-glEnd
      // http://math.hws.edu/graphicsbook/c3/s4.html
      //float coords[6] = { -0.9,-0.9,  0.9,-0.9,  0,0.7 }; // two coords per vertex.
      float coords[6] = { -0.9,-0.9,  0.9,-0.9,  0,0.7 }; // two coords per vertex.
      float colors[9] = { 1,0,0,  0,1,0,  0,0,1 };  // three RGB values per vertex.

      for (int cnt=0;cnt<6;cnt+=2){
        coords[cnt] = XCtr + coords[cnt] * Radius;
        coords[cnt+1] = YCtr + coords[cnt+1] * Radius;
      }

      glVertexPointer(2, GL_FLOAT, 0, coords);  // Set data type and location.
      glColorPointer(3, GL_FLOAT, 0, colors);

      glEnableClientState(GL_VERTEX_ARRAY);  // Enable use of arrays.
      glEnableClientState(GL_COLOR_ARRAY);

      glDrawArrays(GL_TRIANGLES, 0, 3); // Use 3 vertices, starting with vertex 0.
      return;
    }
    if (true){
      NumVerts = circlepoints.size();
      Point2D *pnt;
      //glColor4f(0.7, 1.0, 0.7, 0.5);
      glBegin(GL_POLYGON);
      for(int VCnt=0; VCnt<NumVerts; VCnt++){
        pnt = &circlepoints.at(VCnt);
        x = XCtr + (pnt->X*Radius);
        y = YCtr + (pnt->Y*Radius);
        glVertex2d(x,y);
      }
      glEnd();
    }else{
      double angle;
      NumVerts = 32;
      //glColor4f(0.7, 1.0, 0.7, 0.5);
      glBegin(GL_POLYGON);
      for(int VCnt=0; VCnt<=NumVerts; VCnt++){
        angle =  Globals::TwoPi * (((double)VCnt) / (double)NumVerts);
        x = XCtr + (Math::cos(angle)*Radius);
        y = YCtr + (Math::sin(angle)*Radius);
        glVertex2d(x,y);
      }
      glEnd();
    }
  }
  /* ********************************************************************************* */
  static void DrawEllipse2(double XCtr, double YCtr, double RadiusX, double RadiusY) {
    DISABLE;
    int NumVerts;
    double x, y;
    NumVerts = circlepoints.size();
    Point2D *pnt;
    glBegin(GL_POLYGON);
    for(int VCnt=0; VCnt<NumVerts; VCnt++){
      pnt = &circlepoints.at(VCnt);
      x = XCtr + (pnt->X*RadiusX);
      y = YCtr + (pnt->Y*RadiusY);
      glVertex2d(x,y);
    }
    glEnd();
  }
  /* ********************************************************************************* */
  static void GenerateCircle(double XCtr, double YCtr, int NumVerts, double Radius, ArrayList<Point2D> &Points){
    // to do: create an array of vertices for reuse.
    // return as ArrayList<double>?, ArrayList<Point2D>?
    //int NumVerts = 32;
    Points.clear();
    for(int VCnt=0; VCnt<=NumVerts; VCnt++){
      double angle =  Globals::TwoPi * (((double)VCnt) / (double)NumVerts);
      SoundFloat XLoc = XCtr + (Math::cos(angle)*Radius);
      SoundFloat YLoc = YCtr + (Math::sin(angle)*Radius);
      Points.push_back(Point2D(XLoc, YLoc));// https://stackoverflow.com/questions/15802006/how-can-i-create-objects-while-adding-them-into-a-vector
    }
  }
  /* ********************************************************************************* */
  void DrawCircle(double XCtr, double YCtr, double Radius) override {
    DISABLE;
    DrawCircle2(XCtr, YCtr, Radius);
  }
  /* ********************************************************************************* */
  void DrawEllipse(double XCtr, double YCtr, double RadiusX, double RadiusY) override {
    DISABLE;
    DrawEllipse2(XCtr, YCtr, RadiusX, RadiusY);
  }
  /* ********************************************************************************* */
  void DrawCenteredSquare(double CtrX, double CtrY, double Sidelength) override {// https://stackoverflow.com/questions/38163188/openglc-draw-square
    double halfside = Sidelength / 2;// DrawSquare is for testing only and can be deleted eventually.

    //glColor3d(0,0,0);
    //glColor3f(0.4, 0.5, 0.4);// differently colored vertex
    glColor4f(1.0, 0.0, 0.0, 0.2);
    glBegin(GL_POLYGON);

    int invy = CtrY;// int invy = GetSize().Y - CtrY;
    glVertex2d(CtrX + halfside, invy + halfside);
    glVertex2d(CtrX + halfside, invy - halfside);
    glVertex2d(CtrX - halfside, invy - halfside);
    glVertex2d(CtrX - halfside, invy + halfside);

    glEnd();
  }
  /* ********************************************************************************* */
  void DrawRectangle(const CajaDelimitadora& box) override {
    //glColor3d(0,0,0);
    //glColor3f(0.4, 0.5, 0.4);// differently colored vertex

    Color FillColor;
    double RandFract = Math::frand();// Globals::Random::NextDouble();
    Color::ToColorWheel(RandFract, FillColor);
    glColor4f(FillColor.RedChannel, FillColor.GreenChannel, FillColor.BlueChannel, 0.2);

    //glColor4f(1.0, 0.0, 0.0, 0.2);
    glBegin(GL_POLYGON);

    glVertex2d(box.Max.X, box.Max.Y);
    glVertex2d(box.Max.X, box.Min.Y);
    glVertex2d(box.Min.X, box.Min.Y);
    glVertex2d(box.Min.X, box.Max.Y);
    glEnd();
  }
  /* ********************************************************************************* */
  void DrawRectangleHollow(const CajaDelimitadora& box) override {
    glBegin(GL_LINES);
    glVertex2d(box.Max.X, box.Max.Y); glVertex2d(box.Max.X, box.Min.Y);// right wall
    glVertex2d(box.Max.X, box.Min.Y); glVertex2d(box.Min.X, box.Min.Y);// floor
    glVertex2d(box.Min.X, box.Min.Y); glVertex2d(box.Min.X, box.Max.Y);// left wall
    glVertex2d(box.Min.X, box.Max.Y); glVertex2d(box.Max.X, box.Max.Y);// ceiling
    glEnd();
  }
};

CirclePoints GlDrawingContext::circlepoints;

#endif
