// wx on windows:
// https://wiki.wxwidgets.org/CodeBlocks_Setup_Guide
// https://forums.wxwidgets.org/viewtopic.php?t=42171
// https://forums.wxwidgets.org/viewtopic.php?t=38455
//deprecated stuff:
//https://wiki.wxwidgets.org/Updating_to_the_Latest_Version_of_wxWidgets

// wxWidgets "Hello world" Program
// For compilers that support precompilation, includes "wx/wx.h".
// Created 20190304

#include<iostream>

#ifdef _WIN32
#include <wx/msw/setup.h>
#endif
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wx/filedlg.h"
#include <wx/glcanvas.h>

#include "GlCanvas.hpp"

using namespace std;

enum { ID_Hello = 1, ID_New=2, ID_Open=3, ID_Save=4, ID_SaveAs=4 };

class MainFrame: public wxFrame {
  wxGLContext *m_context = nullptr;// needs wxGLCanvas
public:
  GlCanvas *ArtCanvas = nullptr;
  StringConst ProgName = "Vectunes";
  //wxString FileFilter = wxT("Jsong files (*.jsong)|*.jsong|Vt Json files (*.vt.json)|*.vt.json");
  wxString FileFilter = wxT("Jsong files (*.jsong)|*.jsong;*.vt.json");
  /* ********************************************************************************* */
  MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size) : wxFrame(NULL, wxID_ANY, title, pos, size) {
    wxMenu *MenuFile = new wxMenu;
    MenuFile->Append(ID_New, "&New Project\tCtrl-N", "New project");
    MenuFile->Append(ID_Open, "&Open File\tCtrl-O", "Open file");
//    MenuFile->Append(ID_Save, "&Save File\tCtrl-S", "Save file");
    //MenuFile->Append(ID_SaveAs, "&Save File As\tShift+Ctrl+S", "Save file as...");
    //MenuFile->Append(ID_SaveAs, "&Save File As\tCtrl-Alt-S", "Save file as...");
    MenuFile->Append(ID_Hello, "&Hello...\tCtrl-H", "Help string shown in status bar for this menu item");
    MenuFile->AppendSeparator();
    MenuFile->Append(wxID_EXIT);
    wxMenu *MenuHelp = new wxMenu;
    MenuHelp->Append(wxID_ABOUT);
    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append(MenuFile, "&File");
    menuBar->Append(MenuHelp, "&Help");
    SetMenuBar( menuBar );
    CreateStatusBar();
    SetStatusText(ProgName);

    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnHello, this, ID_Hello);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::StartNewProject, this, ID_New);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnOpen, this, ID_Open);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnSave, this, ID_Save);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnSaveAs, this, ID_SaveAs);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnAbout, this, wxID_ABOUT);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnExit, this, wxID_EXIT);
    //Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnPaint, this, wxEVT_PAINT);
    Bind(wxEVT_PAINT, &MainFrame::OnPaint, this, wxID_ANY);

    //Bind(wxFD_OPEN, &MainFrame::OnOpen, this, wxID_ANY);
    // Bind(wxOPEN, &MainFrame::OnOpen, this, wxID_ANY);
    // Bind(wxFD_SAVE, &MainFrame::OnSaveAs, this, wxID_ANY);

//  Bind(wxEVT_ERASE_BACKGROUND, &MainFrame::OnEraseBackground, this, -1);
//  Bind(wxEVT_ENTER_WINDOW, &MainFrame::OnMouse, this, -1);
//  Bind(wxEVT_LEAVE_WINDOW, &MainFrame::OnMouse, this, -1);

    //m_context = new wxGLContext(this);// https://wiki.wxwidgets.org/WxGLCanvas
    Bind(wxEVT_KEY_DOWN, &MainFrame::KeyPressed, this, wxID_ANY);
    // Bind(wxEVT_CHAR_HOOK, &MainFrame::KeyPressed, this, wxID_ANY); // This works but blocks everything
    CreateUI();
  }
  /* ********************************************************************************* */
  virtual ~MainFrame() {
    std::cout << "MainFrame destructor start\n";
    delete this->m_context;
    std::cout << "MainFrame destructor end\n";
  }
  /* ********************************************************************************* */
  void KeyPressed(wxKeyEvent& event) {
    event.DoAllowNextEvent();
    wxChar ch = event.GetUnicodeKey();
    wxChar keycode = event.GetKeyCode();
    wxObject *evtobj = event.GetEventObject();
    cout << "Main KeyPressed:" << ch << ": evtobj:" << evtobj << ":\n";
    event.Skip(true);
  }
  /* ********************************************************************************* */
  void StartNewProject(wxCommandEvent& event) {
    ArtCanvas->StartNewProject();
    this->SetTitle(ProgName);
  }
  /* ********************************************************************************* */
  void CreateUI() {
    // http://zetcode.com/gui/wxwidgets/layoutmanagement/

    int BorderWidth = 3;// or 10

    wxBoxSizer *VertSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(VertSizer);

    // wxPanel *TopPanel = new wxPanel(this, wxID_ANY);
    wxPanel *TopPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, 20));
    VertSizer->Add(TopPanel,
                   wxSTRETCH_NOT,// make vertically unstretchable
                   wxEXPAND |    // make horizontally stretchable
                   wxALL,        //   and make border all around
                   BorderWidth); // set border width
    TopPanel->SetBackgroundColour(wxColour(*wxCYAN));

    // Good diagrams: https://wiki.wxwidgets.org/WxSplitterWindow
    {// horizontal belt panel
      wxPanel *BeltPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1200, 500));//wxSize(250, 250));
      wxBoxSizer *HorizSizer = new wxBoxSizer(wxHORIZONTAL);
      BeltPanel->SetSizer(HorizSizer);
      VertSizer->Add(BeltPanel, wxEXPAND, wxEXPAND | wxALL, BorderWidth);
      BeltPanel->SetBackgroundColour(wxColour(*wxYELLOW));

      wxPanel *LeftPanel = new wxPanel(BeltPanel, wxID_ANY, wxDefaultPosition, wxSize(20, 250));
      HorizSizer->Add(LeftPanel, wxSTRETCH_NOT, wxEXPAND, BorderWidth);
      LeftPanel->SetBackgroundColour(wxColour(*wxRED));

      ArtCanvas = new GlCanvas(BeltPanel);
      HorizSizer->Add(ArtCanvas, wxEXPAND, wxEXPAND | wxALL, BorderWidth);

      wxPanel *RightPanel = new wxPanel(BeltPanel, wxID_ANY, wxDefaultPosition, wxSize(20, 250));
      HorizSizer->Add(RightPanel, wxSTRETCH_NOT, wxEXPAND, BorderWidth);
      RightPanel->SetBackgroundColour(wxColour(*wxGREEN));
    }

    wxPanel *BottomPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, 20));
    VertSizer->Add(BottomPanel,
                   wxSTRETCH_NOT,// make vertically unstretchable
                   wxEXPAND |    // make horizontally stretchable
                   wxALL,        //   and make border all around
                   BorderWidth); // set border width
    BottomPanel->SetBackgroundColour(wxColour(*wxBLUE));

    // Inside BottomPanel
    wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);
    BottomPanel->SetSizer(hbox);

    wxButton *CreateButton = new wxButton(BottomPanel, wxID_ANY, "Big Button!", wxDefaultPosition, wxSize(100, 20) );
    CreateButton->Bind(wxEVT_BUTTON, &MainFrame::CreateButtonClicked, this);
    hbox->Add(CreateButton);

    if (true) {
      wxButton *AddLoopButton = new wxButton(BottomPanel, wxID_ANY, "Add Loop", wxDefaultPosition, wxSize(100, 20) );
      AddLoopButton->Bind(wxEVT_BUTTON, &MainFrame::AddLoopButtonClicked, this);
      hbox->Add(AddLoopButton);
    }

    //SetSizerAndFit(VertSizer);
    VertSizer->SetSizeHints(this);
  }

private:
  /* ********************************************************************************* */
  void CreateButtonClicked(wxCommandEvent & event) {
    printf("Button pressed!\n");
    ArtCanvas->LaunchFloater();
  }
  /* ********************************************************************************* */
  void AddLoopButtonClicked(wxCommandEvent & event) {
    printf("Button pressed!\n");
    ArtCanvas->LaunchLoopFloater();
  }
  /* ********************************************************************************* */
  void OnExit(wxCommandEvent& event) {
    ArtCanvas->StopMusic();
    ArtCanvas->StopAnimation();
    Close(true);
  }
  void OnAbout(wxCommandEvent& event) {
    wxMessageBox( "This is Vectunes.\nhttps://gitlab.com/CityOfClay/VectunesGui", "About Vectunes", wxOK | wxICON_INFORMATION );
  }
  void OnHello(wxCommandEvent& event) {
    wxLogMessage("Hello world from wxWidgets!");
  }
  /* ********************************************************************************* */
  void OnPaint(wxPaintEvent& event) {
    return;

    wxPaintDC dc(this);

    // draw a circle
    dc.SetBrush(*wxGREEN_BRUSH); // green filling
    dc.SetPen( wxPen( wxColor(255,0,0), 5 ) ); // 5-pixels-thick red outline
    dc.DrawCircle( wxPoint(200,100), 25 /* radius */ );

    // draw some text
    dc.DrawText(wxT("Testing"), 40, 60);

    wxCoord x1 = 50, y1 = 60;
    wxCoord x2 = 190, y2 = 60;

    dc.DrawLine(x1, y1, x2, y2);

    if (false) {
      wxPoint star[6];
      //border_points[0] = wxPoint( 0, 0 );
      star[0] = wxPoint(100, 60);
      star[1] = wxPoint(60, 150);
      star[2] = wxPoint(160, 100);
      star[3] = wxPoint(40, 100);
      star[4] = wxPoint(140, 150);
      dc.SetPen( *wxBLACK );// draw tab outline
      //dc.SetBrush(*wxTRANSPARENT_BRUSH);
      dc.DrawPolygon(WXSIZEOF(star), star);
    } else {
      dc.SetPen( *wxBLACK );// draw tab outline
      wxPointList StarPoints;
      StarPoints.Append(new wxPoint(100, 60));
      StarPoints.Append(new wxPoint(60, 150));
      StarPoints.Append(new wxPoint(160, 100));
      StarPoints.Append(new wxPoint(40, 100));
      StarPoints.Append(new wxPoint(140, 150));

#if FALSE
      for (int cnt=0; cnt<StarPoints.size(); cnt++) {
        wxNodeBase *pnt = StarPoints.Item(cnt);
        cout << "x:" << pnt->X << ", y:" << pnt->Y << "\n";
      }
#endif

      dc.DrawPolygon(&StarPoints);
      dc.SetBrush(wxColor(255, 0, 0, 128));
      dc.DrawPolygon(&StarPoints, 20, 20);
//      gc->SetBrush(wxColor(255, 0, 0, 128));
//      gc->DrawPolygon(&StarPoints, 20, 20);
    }
    //delete gc;
  }
  /* ********************************************************************************* */
  void TestFd() {// https://flylib.com/books/en/3.138.1.75/1/
    wxString caption = wxT("Choose a file");// Not used.
    wxString wildcard = FileFilter;//wxT(FileFilter);//"Jsong files (*.jsong)|*.jsong|Vt Json files (*.vt.json)|*.vt.json");
    wxString DefaultDir = wxT("c:\\temp");
    wxString DefaultFilename = wxEmptyString;
    //wxFileDialog dialog(parent, caption, DefaultDir, DefaultFilename, wildcard, wxOPEN);
    wxFileDialog dialog(this, caption, DefaultDir, DefaultFilename, wildcard, wxFD_OPEN);
    if (dialog.ShowModal() == wxID_OK) {
      wxString path = dialog.GetPath();
      int filterIndex = dialog.GetFilterIndex();
    }
  }
  /* ********************************************************************************* */
  void OnOpen(wxCommandEvent& event) {//MyFrame:: // https://docs.wxwidgets.org/3.0/classwx_file_dialog.html
//    if (...current content has not been saved...){
//      if (wxMessageBox(_("Current content has not been saved! Proceed?"), _("Please confirm"), wxICON_QUESTION | wxYES_NO, this) == wxNO) {
//        return;
//      }
//      //else: proceed asking to the user the new file to open
//    }

    wxFileDialog OpenFileDialog(this, _("Open Jsong file"), "", "", FileFilter, wxFD_OPEN|wxFD_FILE_MUST_EXIST);

    if (OpenFileDialog.ShowModal() == wxID_CANCEL) {
      return; // The user changed their mind.
    }

    // Load the file chosen by the user.
    wxString FilePath = OpenFileDialog.GetPath();
    if (!this->ArtCanvas->Load_Composition(std::string(FilePath.mbc_str()))) {
      // Failed to load
      // wxLogError("Cannot open file '%s'.", OpenFileDialog.GetPath());
      return;
    }
    this->SetTitle(ProgName + " - " + OpenFileDialog.GetFilename());
  }
  /* ********************************************************************************* */
  void OnSave(wxCommandEvent& WXUNUSED(event)) {//  MyFrame::
  }
  /* ********************************************************************************* */
  void OnSaveAs(wxCommandEvent& WXUNUSED(event)) {//  MyFrame::
    //wxFileDialog saveFileDialog(this, _("Save XYZ file"), "", "", "XYZ files (*.xyz)|*.xyz", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    wxFileDialog saveFileDialog(this, _("Save Jsong file"), "", "", FileFilter, wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
      return; // the user changed their mind.
    }
//    // save the current contents in the file;
//    // this can be done with e.g. wxWidgets output streams:
//    wxFileOutputStream output_stream(saveFileDialog.GetPath());
//    if (!output_stream.IsOk()) {
//      wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
//      return;
//    }
  }
};

class MainApp: public wxApp {
public:
  virtual bool OnInit() {
    Math::InitRand();
    //Math::BetterRandom2();
    if (false) {
      wxFrame *frame = new wxFrame((wxFrame*) NULL, -1, _T("Hello wxWidgets World"));
      frame->CreateStatusBar();
      frame->SetStatusText(_T("Hello World"));
      frame->Show(true);
      SetTopWindow(frame);
      return true;
    } else {
      MainFrame *frame = new MainFrame(MainFrame::ProgName, wxPoint(50, 50), wxSize(450, 340));
      frame->Show(true);
      // delete MainFrame; // do this?
      return true;
    }
  }
};

wxIMPLEMENT_APP(MainApp);

